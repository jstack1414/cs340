package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.ChatMessenger;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientGameBank;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;

import java.util.List;
import java.util.Set;

/**
 * Created by Sean on 3/1/2017.
 *
 * interface for models methods in ClientGameModel
 */
public interface IClientGameModel
{
	ClientGameMap getGameMap();

	List<IGamePlayer> getPlayers();

	ClientGameBank getBank();

	IGamePlayer getClientPlayer();

	List<ClientTrainCard> getClientPlayerTrainCards();

	List<ClientDestinationCard> getClientPlayerDestinationCards();

	List<ClientTrainCard> getClientTrainCardsByID(List<String> trainCardIds);

	List<ClientDestinationCard> getComplementOfTempDestCardsByID(List<String> destinationCardIDs);

	List<ClientRoute> getRoutes();

	ClientRoute getRoute(String routeID);

	ClientTrainCard[] getRevealedCards();

	void setRevealedCards(ClientTrainCard[] cards);

	ClientDestinationCard[] getTemporaryDestinationCards();


	IGamePlayer getPlayer(String username);

	IGamePlayer getCurrentPlayer();

//	void setCurrentPlayer(String username);

	void addTrainCard(ClientTrainCard card, String username);

	void addTemporaryDestinationCards(List<ClientDestinationCard> cards, int minToKeep);

	void addDestinationCards(List<ClientDestinationCard> cards, String username);

	void addChatMessage(List<ChatMessage> message);

//	void setTrainCardDeckSize(int size);
//
//	void setDestinationCardDeckSize(int size);
//
//	void setTrainCardDiscardSize(int size);

	List<ChatMessage> pullChatMessages(int lastIndex);

	long getLastChatIndex();

	ChatMessenger getChatMessenger();

	String getRoomID();

	void claimRoute(String routeID, String playerName);

//	void setPlayerTrainsRemaining(String playerName, int trainsRemaining);

	void removeTrainCards(String username, List<ClientTrainCard> cards);

	void updatePlayerScore(String username, int score);

	void updateLocalPlayerPrivateScore(int privateScore);

	int getLastCommand();

	void addCommand(IBaseCommand cmd);

//	@Deprecated
//	void removeTrians(String playerName, String routeID);

	void updateRemainingTrains(int currentTrains, String playerName);

	void updateTrainDeckSize(int currentSize);

	void updateTrainDiscardPileSize(int discardSize);

	void updateDestCardDeckSize(int currentSize);

//	void updateDestCardDiscardPileSize(int discardSize);

	void updateCurrentPlayerTakingTurn(String playerName);

	void setIsLastRound(boolean isLastRound);

	boolean getIsLastRound();

	void updateGameOverData(IGameOverData data);

	void updateLongestRoute(Set<String> usersLongestRoute, int lengthLongestRoute);

	boolean hasDrawableDestCards();

	boolean hasDrawableTrainCards();
}
