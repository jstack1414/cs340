package edu.byu.cs340.models.gameModel;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.dataGeneration.ClientGameObjectFileParser;
import edu.byu.cs340.messages.newGameObsMsg.*;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.ChatMessenger;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.bank.*;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by joews on 2/1/2017.
 *
 * Client implementation of the game models.
 */
public class ClientGameModel implements IClientGameModel
{
	private ClientGameMap mGameMap;
	private List<IGamePlayer> mPlayers;
	private IGamePlayer mCurrentPlayer;
	private ClientGameBank mBank;
	private ChatMessenger mChatMessenger;
	private String mRoomID;
	private String mRoomName;
	private List<IBaseCommand> mCommands;
	private boolean mIsLastRound;

	public void setGameMap(ClientGameMap gameMap)
	{
		mGameMap = gameMap;
	}

	public void setPlayers(List<IGamePlayer> players)
	{
		mPlayers = players;
	}

	public void setBank(ClientGameBank bank)
	{
		mBank = bank;
	}

	public ClientGameModel(String clientUserName, int destSize, int trainSize, int numTrainsPerPlayer,
	                       List<ClientTrainCard> revealed, List<String> playerOrder, String gameID, String gameName)
	{
		mIsLastRound = false;
		mGameMap = new ClientGameMap();
		//set up the bank
		mBank = new ClientGameBank();
		updateDestCardDeckSize(destSize);
		updateTrainDeckSize(trainSize);
		updateTrainDiscardPileSize(0);
		ClientTrainCard[] revealedArray = new ClientTrainCard[revealed.size()];
		for (int i = 0; i < revealed.size(); i++)
		{
			revealedArray[i] = revealed.get(i);
		}
		setRevealedCards(revealedArray);

		//set up the players
		mPlayers = new ArrayList<>();
		for (String playerName : playerOrder)
		{
			if(playerName.equals(clientUserName))
			{
				LocalGamePlayer gamePlayer = new LocalGamePlayer(playerName);
				mPlayers.add(gamePlayer);
				ClientModelManager.getInstance().notifyObservers(
						new PlayerAddedToGameObsMsg(gamePlayer));
			}
			else
			{
				OpponentGamePlayer opponent = new OpponentGamePlayer(playerName);
				mPlayers.add(opponent);
				ClientModelManager.getInstance().notifyObservers(
					new PlayerAddedToGameObsMsg(opponent)
			);
			}
			//SEND Observevr message sayin the player has been added

			//use the game model funcitons that send observer messages to set trains and score
			updateRemainingTrains(numTrainsPerPlayer, playerName);
			updatePlayerScore(playerName, 0);
		}
		mCurrentPlayer = mPlayers.get(0);
		//now set the client player
		mBank.setClientPlayer(getPlayer(clientUserName));

		//set up commands and chat
		mCommands = new ArrayList<>();
		mChatMessenger = new ChatMessenger();

		mRoomID = gameID;
		mRoomName = gameName;
		//now set coordinates for all the cities
		populateCoordinates();
	}


	@Override
	public ClientGameMap getGameMap()
	{
		return mGameMap;
	}

	@Override
	public List<IGamePlayer> getPlayers()
	{
		return mPlayers;
	}

	@Override
	public ClientGameBank getBank()
	{
		return mBank;
	}

	@Override
	public IGamePlayer getClientPlayer()
	{
		return mBank.getClientPlayer();
	}

	public IGamePlayer getPlayer(String playerName)
	{
		IGamePlayer result = null;
		for (IGamePlayer player : mPlayers)
		{
			if (player.getUsername().equals(playerName))
			{
				result = player;
			}
		}
		return result;
	}

	@Override
	public List<ClientTrainCard> getClientPlayerTrainCards()
	{
		return ((LocalGamePlayer) mBank.getClientPlayer()).getTrainCards();
	}

	@Override
	public List<ClientDestinationCard> getClientPlayerDestinationCards()
	{
		return ((LocalGamePlayer) mBank.getClientPlayer()).getDestinationCards();
	}

	@Override
	public List<ClientTrainCard> getClientTrainCardsByID(List<String> trainCardIds)
	{
		List<ClientTrainCard> allClientTrainCards = ((LocalGamePlayer)mBank.getClientPlayer()).getTrainCards();
		List<ClientTrainCard> cardsToGet = new ArrayList<>();
		for (ClientTrainCard card : allClientTrainCards)
		{
			if (trainCardIds.contains(card.getId()))
			{
				cardsToGet.add(new ClientTrainCard(card));
			}
		}
		return cardsToGet;
	}

	@Override
	public List<ClientDestinationCard> getComplementOfTempDestCardsByID(List<String> destinationCardIDs)
	{
		ClientDestinationCard[] temporaryClientDestCards = mBank.getTemporaryCards();
		List<ClientDestinationCard> cardsToGet = new ArrayList<>();
		for (ClientDestinationCard card : temporaryClientDestCards)
		{
			if (!destinationCardIDs.contains(card.getId()))
			{
				cardsToGet.add(new ClientDestinationCard(card));
			}
		}
		return cardsToGet;
	}

	@Override
	public List<ClientRoute> getRoutes()
	{
		return mGameMap.getRoutes();
	}

	@Override
	public ClientRoute getRoute(String routeID)
	{
		return mGameMap.getRoute(routeID);
	}

	@Override
	public ClientTrainCard[] getRevealedCards()
	{
		return mBank.getRevealedCards();
	}

	public void setRevealedCards(ClientTrainCard[] cards)
	{
		mBank.setRevealedCards(cards);
		ArrayList<ClientTrainCard> cardArrayList = new ArrayList<>();
		for (ClientTrainCard card : mBank.getRevealedCards())
		{
			cardArrayList.add(card);
		}
		RevealedTrainCardsObsMsg revealedTrainCardsObsMsg = new RevealedTrainCardsObsMsg(
				cardArrayList);
		ClientModelManager.getInstance().notifyObservers(revealedTrainCardsObsMsg);
	}

	@Override
	public ClientDestinationCard[] getTemporaryDestinationCards()
	{
		return mBank.getTemporaryCards();
	}

	@Override
	public void addTrainCard(ClientTrainCard card, String username)
	{
		if (username.equals(getClientPlayer().getUsername()))
		{
			getClientPlayer().addTrainCard(card);
			//IF it is the client player then send off the cards in the hand
			ClientPlayerTrainCardsObsMsg trainCardsObsMsg = new
					ClientPlayerTrainCardsObsMsg(((LocalGamePlayer) getPlayer(username)).getTrainCards());
			ClientModelManager.getInstance().notifyObservers(trainCardsObsMsg);
		} else//this could be changed by adding an oponent class
		{
			getPlayer(username).addTrainCard(new DummyTrainCard());
		}
		//regardless send off the number of cards in hand
		ClientModelManager.getInstance().notifyObservers(
				new PlayerTrainCardrsObsMsg(getPlayer(username).getTrainCardsNum(), username));
	}

	@Override
	public void addTemporaryDestinationCards(List<ClientDestinationCard> cards, int minToKeep)
	{
		ClientDestinationCard[] tempCards = new ClientDestinationCard[cards.size()];
		for (int i = 0; i < cards.size(); i++)
		{
			ClientDestinationCard card = cards.get(i);
			tempCards[i] = new ClientDestinationCard(card);
		}
		mBank.setTemporaryCards(tempCards, minToKeep);
		//Send off the temporary cards that the bank has.
		ClientPlayerTempDestCardsObsMsg tempDestCardsObsMsg = new ClientPlayerTempDestCardsObsMsg(
				Arrays.asList(mBank.getTemporaryCards()), minToKeep);
		ClientModelManager.getInstance().notifyObservers(tempDestCardsObsMsg);
	}

	@Override
	public void addDestinationCards(List<ClientDestinationCard> cards, String username) {
		if (cards != null) {
			List<ClientDestinationCard> cardsFormatted = new ArrayList<>();
			if (getClientPlayer().getUsername().equals(username)) {
				getClientPlayer().addDestinationCards(cards);
				//IF it is the clients player we need to send the actual cards
				ClientModelManager.getInstance().notifyObservers(
						new ClientPlayerDestCardsObsMsg(((LocalGamePlayer) getClientPlayer()).getDestinationCards())
				);
			} else {
				for (int i = 0; i < cards.size(); i++) {
					cardsFormatted.add(new DummyDestinationCard());
				}
				getPlayer(username).addDestinationCards(cardsFormatted);
			}
			//regardless of who it is send off the number of cards they have

			ClientModelManager.getInstance().notifyObservers(
					new GamePlayerDestCardsObsMsg(getPlayer(username).getDestinationCardsNum(), username)
			);
		}


	}

	@Override
	public IGamePlayer getCurrentPlayer()
	{
		return mCurrentPlayer;
	}

//	@Override
//	@Deprecated
//	public void setCurrentPlayer(String username)
//	{
//		mCurrentPlayer = getPlayer(username);
//		ClientModelManager.getInstance().notifyObservers(
//				new NextPlayerTurnObsMsg(username)
//		);
//	}

	@Override
	public void addChatMessage(List<ChatMessage> messages)
	{
		for (ChatMessage message : messages)
		{
			mChatMessenger.addChatMessage(message);
			ClientModelManager.getInstance().notifyObservers(new GameChatObsMsg(message));
		}
	}

	@Override
	public List<ChatMessage> pullChatMessages(int lastIndex)
	{
		return mChatMessenger.getNewMessages(lastIndex);
	}

	@Override
	public long getLastChatIndex()
	{
		return mChatMessenger.getLastIndex();
	}

	@Override
	public ChatMessenger getChatMessenger()
	{
		return mChatMessenger;
	}

	@Override
	public String getRoomID()
	{
		return mRoomID;
	}

	public void populateCoordinates()
	{
		JsonParser parser = new JsonParser();
		List<ClientCity> cities = mGameMap.getCities();

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(ClassLoader.getSystemResourceAsStream(ClientGameObjectFileParser.FILE_PATH)));
		Object obj = parser.parse(reader);

		JsonObject json = (JsonObject) obj;
		JsonArray list = json.getAsJsonArray("cities");

		for (JsonElement city : list)
		{
			String cityName = city.getAsJsonObject().get("name").getAsString();
			int xCoord = city.getAsJsonObject().get("x").getAsInt();
			int yCoord = city.getAsJsonObject().get("y").getAsInt();

			for (ClientCity iterCity : cities)
			{
				if (iterCity.getName().equals(cityName))
				{
					iterCity.setXCoordinate(xCoord);
					iterCity.setYCoordinate(yCoord);
					break;
				}
			}
		}

	}

	@Override
	public void claimRoute(String routeID, String playerName)
	{
		//get result and send observer message todo: change to void
		 mGameMap.claimRoute(routeID, getPlayer(playerName));
		RouteClaimedObsMsg routeClaimedObsMsg = new RouteClaimedObsMsg(routeID, playerName);
		ClientModelManager.getInstance().notifyObservers(routeClaimedObsMsg);
	}


	@Override
	public void updatePlayerScore(String username, int score)
	{
		IGamePlayer player = getPlayer(username);
		player.setScore(score);
		ScoreChangedObsMsg scoreChangedObsMsg = new ScoreChangedObsMsg(score, username);
		ClientModelManager.getInstance().notifyObservers(scoreChangedObsMsg);
	}

	public void updateLocalPlayerPrivateScore(int privateScore)
	{
		((LocalGamePlayer) getClientPlayer()).setPrivateScore(privateScore);
		ClientModelManager.getInstance().notifyObservers(new PrivatePlayerScore(privateScore, getClientPlayer()
				.getUsername()));
	}

	private int LAST_COMMAND = -1;

	@Override
	public int getLastCommand()
	{
		return LAST_COMMAND;
	}

	@Override
	public void addCommand(IBaseCommand cmd)
	{
		mCommands.add(cmd);
		LAST_COMMAND = Math.max(LAST_COMMAND, cmd.getIndex());
		ClientModelManager.getInstance().notifyObservers(
				//TODO: Implement to string on commands
				new CmdAddedObsMsg(cmd.toString()));

	}

	@Override
	public void removeTrainCards(String username, List<ClientTrainCard> cards)
	{
		IGamePlayer player = getPlayer(username);
		for (ClientTrainCard card : cards)
		{
			player.removeTrainCard(card);
		}
		//always send a message saying the number of train cards remaining
		PlayerTrainCardrsObsMsg trainCardrsObsMsd =
				new PlayerTrainCardrsObsMsg(player.getTrainCardsNum(), player.getUsername());
		ClientModelManager.getInstance().notifyObservers(trainCardrsObsMsd);
		//if the player is the clients player then also send what those cards are
		if (username.equals(getClientPlayer().getUsername()))
		{
			ClientModelManager.getInstance().notifyObservers(
					new ClientPlayerTrainCardsObsMsg(
							((LocalGamePlayer)player).getTrainCards()
					)
			);
		}
	}


	@Override
	public void updateRemainingTrains(int currentTrains, String playerName)
	{
		getPlayer(playerName).setTrainsRemaining(currentTrains);
		PlayerNumTrainsObsMsg numTrainsObsMsg = new PlayerNumTrainsObsMsg(currentTrains, playerName);
		ClientModelManager.getInstance().notifyObservers(numTrainsObsMsg);
	}

	@Override
	public void updateTrainDeckSize(int currentSize)
	{
		mBank.setTrainCardDeckSize(currentSize);
		TrainDeckSizeObsMsg deckSizeObsMsg = new TrainDeckSizeObsMsg(mBank.getTrainCardDeckSize());
		ClientModelManager.getInstance().notifyObservers(deckSizeObsMsg);
	}

	@Override
	public void updateTrainDiscardPileSize(int discardSize)
	{
		mBank.setDiscardedPileSize(discardSize);
		ClientModelManager.getInstance().notifyObservers(
				new DiscardPileSizeObsMsg(mBank.getDiscardedPileSize())
		);
	}

	@Override
	public void updateDestCardDeckSize(int currentSize)
	{
		mBank.setDestinationCardDeckSize(currentSize);
		DestinationDeckSizeObsMsg deckSizeObsMsg = new DestinationDeckSizeObsMsg(mBank.getDestinationCardDeckSize());
		ClientModelManager.getInstance().notifyObservers(deckSizeObsMsg);
	}

	@Override
	public void updateCurrentPlayerTakingTurn(String playerName)
	{
		mCurrentPlayer = getPlayer(playerName);
		ClientModelManager.getInstance().notifyObservers(
				new NextPlayerTurnObsMsg(playerName)
		);
	}

	@Override
	public void setIsLastRound(boolean isLastRound) {
		mIsLastRound = isLastRound;
		GameIsLastRoundObsMsg lastRoundObsMsg = new GameIsLastRoundObsMsg(isLastRound);
		ClientModelManager.getInstance().notifyObservers(lastRoundObsMsg);
	}

	@Override
	public boolean getIsLastRound() {
		return mIsLastRound;
	}

	@Override
	public void updateGameOverData(IGameOverData data)
	{
		GameOverObsMsg overObsMsg = new GameOverObsMsg(data);
		ClientModelManager.getInstance().notifyObservers(overObsMsg);
	}

	@Override
	public void updateLongestRoute(Set<String> usersLongestRoute, int lengthLongestRoute)
	{
		Set<IGamePlayer> longestRoutePlayers = new HashSet<>();
		for (String name : usersLongestRoute)
		{
			longestRoutePlayers.add(getPlayer(name));
		}
		mGameMap.setLongestRoute(new ClientLongestRoute(longestRoutePlayers, lengthLongestRoute));
		ClientModelManager.getInstance().notifyObservers(new LongestRouteObstMsg(usersLongestRoute,
				lengthLongestRoute));
	}

	@Override
	public boolean hasDrawableDestCards()
	{
		return mBank.getDestinationCardDeckSize() > 0;
	}

	@Override
	public boolean hasDrawableTrainCards()
	{
		int revealedCardsDrawable = 0;
		if (mBank.getRevealedCards() != null)
		{
			for (ICard card : mBank.getRevealedCards())
			{
				if (card != null)
				{
					revealedCardsDrawable++;
				}
			}
		}
		return mBank.getDiscardedPileSize() + mBank.getTrainCardDeckSize() + revealedCardsDrawable > 0;
	}
}
