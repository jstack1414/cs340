package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.roomModel.Player;

import java.util.List;

/**
 * Created by Rebekah on 3/22/2017.
 */
public class OpponentGamePlayer extends Player implements IGamePlayer {

    protected int mDestinationCards = 0;
    protected int mTrainCards = 0;
    protected int mTrainsRemaining = 0;
    protected int mScore = 0;

    OpponentGamePlayer(String playerName)
    {
        super(playerName);
    }

    @Override
    public int getDestinationCardsNum() {
        return mDestinationCards;
    }

    @Override
    public void setDestinationCards(List<ClientDestinationCard> destinationCards) {
            mDestinationCards = destinationCards.size();
    }

    @Override
    public int getTrainCardsNum() {
        return mTrainCards;
    }

    @Override
    public void addTrainCard(ClientTrainCard trainCards) {
        mTrainCards++;
    }

    @Override
    public void removeTrainCard(ClientTrainCard card) {
        mTrainCards--;
    }

    @Override
    public void addDestinationCards(List<ClientDestinationCard> destinationCards) {
        mDestinationCards += destinationCards.size();
    }

    @Override
    public void setTrainCards(List<ClientTrainCard> trainCards) {
        mTrainCards = trainCards.size();
    }

    @Override
    public int getTrainsRemaining() {
        return mTrainsRemaining;
    }

    @Override
    public void setTrainsRemaining(int trainsRemaining) {
        mTrainsRemaining = trainsRemaining;
    }

    @Override
    public int getScore() {
        return mScore;
    }

    @Override
    public void setScore(int score) {
        mScore = score;
    }
}
