package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;

import java.util.List;

/**
 * Created by Rebekah on 3/22/2017.
 */
public interface IGamePlayer {

    int getDestinationCardsNum();

    void setDestinationCards(List<ClientDestinationCard> destinationCards);

    int getTrainCardsNum();

    void addTrainCard(ClientTrainCard trainCards);

    void removeTrainCard(ClientTrainCard card);

    void addDestinationCards(List<ClientDestinationCard> destinationCards);

    void setTrainCards(List<ClientTrainCard> trainCards);

    int getTrainsRemaining();

    void setTrainsRemaining(int trainsRemaining);

    int getScore();

    void setScore(int score);

    String getUsername();
}
