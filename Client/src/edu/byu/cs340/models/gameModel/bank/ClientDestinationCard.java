package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.ICity;

/**
 * Created by Sean on 3/1/2017.
 */
public class ClientDestinationCard extends DestinationCard implements ICard
{
	public ClientDestinationCard(String id, ICity[] cities, int points)
	{
		super(cities, points);
		setId(id);
	}

	public ClientDestinationCard(IDestinationCard that)
	{
		super((DestinationCard) that);
	}
}
