package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.TrainType;

/**
 * Created by Sean on 3/1/2017.
 */
public class DummyTrainCard extends ClientTrainCard
{
	public DummyTrainCard()
	{
		super(TrainType.None);
	}
}
