package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.TrainType;

/**
 * Created by Sean on 3/1/2017.
 */
public class ClientTrainCard extends TrainCard implements ICard
{
	public ClientTrainCard(TrainType trainType)
	{
		super(trainType);
	}

	public ClientTrainCard(ITrainCard that)
	{
		super(that);
	}

	/**
	 * Override equals to compare the cities by ID.
	 *
	 * @param obj Object to compare to.
	 * @return Returns true if the objects are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof ClientTrainCard)
		{
			return ((ClientTrainCard) obj).getId().equals(this.getId());
		} else
		{
			return false;
		}
	}

	/**
	 * Overrides hashcode based on id
	 *
	 * @return mId.hashCode()
	 */
	@Override
	public int hashCode()
	{
		return getId().hashCode();
	}
}
