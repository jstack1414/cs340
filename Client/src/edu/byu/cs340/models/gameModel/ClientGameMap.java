package edu.byu.cs340.models.gameModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 3/1/2017.
 *
 */
public class ClientGameMap {
	protected List<ClientCity> mCities;
	protected List<ClientRoute> mRoutes;
	protected ClientLongestRoute mLongestRoute;

	public ClientGameMap()
	{
		mCities = new ArrayList<>();
		mRoutes = new ArrayList<>();
		mLongestRoute = null;
	}

	public List<ClientCity> getCities()
	{
		return mCities;
	}
	
	public void setCities(List<ClientCity> cities)
	{
		mCities = cities;
	}
	
	public List<ClientRoute> getRoutes()
	{
		return mRoutes;
	}

	public ClientRoute getRoute(String routeID)
	{
		if (routeID != null)
		{
			for (ClientRoute route : mRoutes)
			{
				if (route.getId().equals(routeID))
				{
					return route;
				}
			}
		}
		return null;
	}
	
	public void setRoutes(List<ClientRoute> routes)
	{
		mRoutes = routes;
	}
	
	public ClientLongestRoute getLongestRoute()
	{
		return mLongestRoute;
	}
	
	public void setLongestRoute(ClientLongestRoute longestRoute)
	{
		mLongestRoute = longestRoute;
	}

	public boolean claimRoute(String routeID, IGamePlayer player)
	{
		if (player != null)
		{
			ClientRoute route = getRoute(routeID);
			if (route != null)
			{
				route.claimRoute(player.getUsername());
				return true;
			}
		}
		return false;
	}
}
