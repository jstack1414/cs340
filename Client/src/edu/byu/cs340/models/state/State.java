package edu.byu.cs340.models.state;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.messages.ErrorObsMsg;
import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.messages.StateObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.state.game.GameState;
import edu.byu.cs340.models.state.game.InitialTurnDrawDestinationCards;

/**
 * Created by joews on 3/15/2017.
 */
public abstract class State implements IState
{
	public State()
	{
	}


	@Override
	public void processEvent(Event event)
	{
		switch (event.getEventType())
		{
			case UISignOut:
			case CMDSignOut:
				onSignOut(event);
				break;
			case UILeaveGame:
				onUILeaveGame(event);
				break;
			case CMDLeaveGame:
				onReturnToJoinGame(event);
				break;
			case CMDErrorMessage:
				onErrorMessage(event);
				break;
			case CMDStartGame:
				onCMDStartGame(event);
				break;
		}
	}

	private void onCMDStartGame(Event event)
	{
		//force into the initial game state
		ClientModelManager.getInstance().setState(new InitialTurnDrawDestinationCards());
	}

	private void onErrorMessage(Event event)
	{
		int errorCode = (int) event.get(EventKeys.ERROR_CODE);
		String errorMessage = (String) event.get(EventKeys.ERROR_MESSAGE);
		boolean sendMsg = onError(errorMessage, errorCode);
		if (sendMsg)
		{
			ClientModelManager.getInstance().notifyObservers(new ErrorObsMsg(errorMessage));
		}
	}

	protected boolean onError(String errorMessage, int errorCode)
	{
		switch (errorCode)
		{
			case ErrorMessage.AUTHORIZATION_ERROR:
				ClientModelManager.getInstance().setState(new LoginState());
				break;
			case ErrorMessage.NULL_ROOM:
				return onNullRoom();
		}
		return true;
	}

	private boolean onNullRoom()
	{
		if (this instanceof GameState)
		{
			return true;
		} else if (this instanceof LobbyState)
		{
			return true;
		} else
		{
			return false;
			//if not in a game or lobby state
		}
	}


	protected void onUILeaveGame(Event event)
	{
		ClientModelManager.getInstance().setState(new JoinGameState());
		CommandManager.getInstance().leaveGame();
	}

	protected void onReturnToJoinGame(Event event)
	{
		//return to the join game state
		ClientModelManager.getInstance().setState(new JoinGameState());
	}

	protected void onSignOut(Event event)
	{
		//TODO: send a command off to sign the user out.

		//regardless force state to login
		ClientModelManager.getInstance().setState(new LoginState());
		if (event.getEventType() == EventType.UISignOut)
		{

			String username = ClientModelManager.getInstance().getUsername();
			ClientLobbyModel lobby = ClientModelManager.getInstance().getLobby();
			IClientGameModel gameModel = ClientModelManager.getInstance().getGame();
			String lobbyID = null;
			String gameID = null;
			if (lobby != null)
			{
				lobbyID = lobby.getRoomID();
			}
			if (gameModel != null)
			{
				gameID = gameModel.getRoomID();
			}
			//send off the command
			CommandManager.getInstance().signOut(username, lobbyID, gameID);
		}
	}

	/**
	 * Notifies observers of the model that the state has changed.
	 */
	public void onActivate()
	{
		//TODO: Uncomment when set up
		ClientModelManager.getInstance().notifyObservers(getObserverStateMessage());
		System.out.println("State onActivate: " + this.getClass().getSimpleName());
	}

	/**
	 * Gets the observer message specific to each state.
	 *
	 * @return The observer message specific to that state.
	 */
	protected IObsMsg getObserverStateMessage()
	{
		return new StateObsMsg(this);
	}
}
