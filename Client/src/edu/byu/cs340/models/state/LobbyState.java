package edu.byu.cs340.models.state;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class LobbyState extends State
{
	public LobbyState()
	{
		super();
	}

	@Override
	protected boolean onError(String errorMessage, int errorCode)
	{
		boolean sendMsg = super.onError(errorMessage, errorCode);
		switch (errorCode)
		{
			case ErrorMessage.PLAYER_NOT_IN_ROOM:
				onPlayerNotInRoom();
				break;
		}
		return sendMsg;
	}

	protected void onPlayerNotInRoom()
	{
		ClientModelManager.getInstance().setState(new JoinGameState());
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIReadyStatus:
				onUIReadyStatus(event);
				break;
			case CMDReadyStatus:
				onCMDReadyStatus(event);
				break;
			case UIStartGame:
				onUIStartGame(event);
				break;
			case UILeaveLobby:
				onUILeaveLobby(event);
				break;
			case UIRemoveLobbyPlayer:
				onUIRemovePlayer(event);
				break;
			case CMDRemoveLobbyPlayer:
				onCMDRemovePlayer(event);
				break;
			case CMDJoinGame:
				onPlayerJoining(event);
				break;
			case UIPollLobby:
				onUIPollLobby(event);
				break;
		}
	}

	private void onUIPollLobby(Event event)
	{
		CommandManager.getInstance().pollLobby();
	}

	private void onPlayerJoining(Event event)
	{
		//do nothing right now
		System.out.println(event.get(EventKeys.PLAYER_NAME));
	}

	private void onCMDRemovePlayer(Event event)
	{
		//get the players name
		String playerRemoved = (String) event.get(EventKeys.PLAYER_NAME);
		String username = ClientModelManager.getInstance().getUsername();
		//if local user removed force to join game state if they aren't already
		if (username.equals(playerRemoved) &&
				!(ClientModelManager.getInstance().getState() instanceof JoinGameState))
		{
			ClientModelManager.getInstance().setState(new JoinGameState());
		}
		//otherwise do nothing
	}

	private void onUIRemovePlayer(Event event)
	{
		String playerToRemove = (String) event.get(EventKeys.PLAYER_NAME);
		CommandManager.getInstance().removePlayerFromLobby(playerToRemove);
	}

	private void onUILeaveLobby(Event event)
	{
		//force the player into the join game state
		ClientModelManager.getInstance().setState(new JoinGameState());
		onUIRemovePlayer(event);
	}


	private void onUIStartGame(Event event)
	{
		CommandManager.getInstance().startGame();
	}

	private void onCMDReadyStatus(Event event)
	{
		//do nothing if a player readies or unreadies
	}

	private void onUIReadyStatus(Event event)
	{
		boolean isReady = (boolean) event.get(EventKeys.IS_READY);
		CommandManager.getInstance().playerReadyStatus(isReady);
	}
}
