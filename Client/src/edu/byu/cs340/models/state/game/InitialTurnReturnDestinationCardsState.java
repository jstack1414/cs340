package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;

import java.util.List;

/**
 * Created by joews on 3/15/2017.
 */
public class InitialTurnReturnDestinationCardsState extends GameState
{
	public InitialTurnReturnDestinationCardsState()
	{
		super();
	}

	int mMinDestinationCardsToKeep = 2;

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIReturnDestinationCards:
				onUIReturnDestinationCards(event);
				break;
			case CMDReturnDestinationCards:
				onCMDReturnDestinationCards(event);
				break;
			case CMDDrawTrainCard:
				//if train cards are drawn (should happen x4) do nothing
				break;
		}
	}

	private void onCMDReturnDestinationCards(Event event)
	{
		if (event.get(EventKeys.PLAYER_NAME).equals(ClientModelManager.getInstance().getUsername()))
		{
			ClientModelManager.getInstance().setState(new NotMyTurnState());
		}
	}

	private void onUIReturnDestinationCards(Event event)
	{
		List<String> cardsToKeepID = (List<String>) event.get(EventKeys
				.DESTINATION_CARD_ID_LIST);
		int minCardsToKeep = (int) event.get(EventKeys.MIN_CARD_TO_KEEP);
		IClientGameModel gameModel = ClientModelManager.getInstance().getGame();
		List<ClientDestinationCard> cardsToReturn = gameModel.getComplementOfTempDestCardsByID(cardsToKeepID);
		CommandManager.getInstance().returnDestinationCards(3, cardsToReturn);

	}

	/**
	 * Get value of MinDestinationCardsToKeep
	 *
	 * @return Returns the value of MinDestinationCardsToKeep
	 */
	public int getMinDestinationCardsToKeep()
	{
		return mMinDestinationCardsToKeep;
	}
}
