package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class InitialTurnDrawDestinationCards extends GameState
{
	public InitialTurnDrawDestinationCards()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case CMDDrawDestinationCards:
				//TODO: Somehow tell the UI to launch the destination card popup....
				ClientModelManager.getInstance().setState(new InitialTurnReturnDestinationCardsState());
				break;
			case CMDDrawTrainCard:
				//if train cards are drawn (should happen x4) do nothing
				break;
		}
	}

}
