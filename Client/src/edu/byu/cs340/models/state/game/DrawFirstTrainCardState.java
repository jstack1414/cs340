package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 3/15/2017.
 */
public class DrawFirstTrainCardState extends DrawTrainCardState
{
	public DrawFirstTrainCardState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UICancel:
				onUICancel(event);
				break;
		}
	}


	private void onUICancel(Event event)
	{
		ClientModelManager.getInstance().setState(new PickActionState());
	}


}
