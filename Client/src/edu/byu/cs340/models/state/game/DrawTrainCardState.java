package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;

/**
 * Created by joews on 3/15/2017.
 */
public abstract class DrawTrainCardState extends GameState
{
	public DrawTrainCardState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIDrawTrainCardFromDeck:
				onUIDrawTrainCardFromDeck(event);
				break;
			case UIDrawTrainCardFromZone:
				onUIDrawTrainCardFromZone(event);
				break;
			case CMDDrawTrainCard:
				onCMDDrawTrainCard(event);
				break;
		}
	}

	protected void onCMDDrawTrainCard(Event event)
	{
		DrawableTrainStateProcessor.onCMDDrawTrainCard(event);
	}


	protected void onUIDrawTrainCardFromDeck(Event event)
	{
		CommandManager.getInstance().drawTrainCard(0);
	}

	protected void onUIDrawTrainCardFromZone(Event event)
	{
		int zone = (int) event.get(EventKeys.TRAIN_CARD_ZONE);
		CommandManager.getInstance().drawTrainCard(zone);
	}
}
