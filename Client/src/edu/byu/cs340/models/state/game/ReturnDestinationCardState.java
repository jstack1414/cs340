package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;

import java.util.List;

/**
 * Created by joews on 3/15/2017.
 */
public class ReturnDestinationCardState extends GameState
{
	public ReturnDestinationCardState()
	{
		super();
	}

	int mMinDestinationCardsToKeep = 1;

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIReturnDestinationCards:
				onUIReturnDestinationCards(event);
				break;
			case CMDReturnDestinationCards:
				onCMDReturnDestinationCards(event);
				break;
		}
	}

	/**
	 * Get value of MinDestinationCardsToKeep
	 *
	 * @return Returns the value of MinDestinationCardsToKeep
	 */
	public int getMinDestinationCardsToKeep()
	{
		return mMinDestinationCardsToKeep;
	}

	private void onCMDReturnDestinationCards(Event event)
	{
		if (event.get(EventKeys.PLAYER_NAME).equals(ClientModelManager.getInstance().getUsername()))
		{
			ClientModelManager.getInstance().setState(new NotMyTurnState());
		}
	}

	private void onUIReturnDestinationCards(Event event)
	{
		int maxCardsToReturn = 2;
		List<String> cardIDsToKeep = (List<String>) event.get(EventKeys
				.DESTINATION_CARD_ID_LIST);
		IClientGameModel gameModel = ClientModelManager.getInstance().getGame();
		List<ClientDestinationCard> cardsToKeep = gameModel.getComplementOfTempDestCardsByID(cardIDsToKeep);
		CommandManager.getInstance().returnDestinationCards(maxCardsToReturn, cardsToKeep);
	}
}
