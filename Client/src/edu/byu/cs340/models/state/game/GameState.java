package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.command.CommandManager;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.state.JoinGameState;
import edu.byu.cs340.models.state.State;

/**
 * Created by joews on 3/15/2017.
 */
public abstract class GameState extends State
{
	public GameState()
	{
		super();
	}

	@Override
	public void processEvent(Event event)
	{
		super.processEvent(event);
		switch (event.getEventType())
		{
			case UIPollGame:
				onUIPollGame(event);
				break;
			case CMDGameOver:
				onCMDGameOver(event);
				break;
			case CMDNextPlayerTurn:
				onCMDStartTurn(event);
				break;
		}
	}

	/**
	 * If at ay point the server says its someone elses turn (or your turn) jump to the NotMyTurn or PickAction states
	 * respectivley.
	 *
	 * @param event
	 */
	protected void onCMDStartTurn(Event event)
	{
		String nextPlayer = (String) event.get(EventKeys.PLAYER_NAME);
		System.out.println("Players Turn: " + nextPlayer);
		if (nextPlayer.equals(ClientModelManager.getInstance().getUsername()))
		{
			ClientModelManager.getInstance().setState(new PickActionState());
		} else
		{
			ClientModelManager.getInstance().setState(new NotMyTurnState());
		}
	}

	protected void onCMDGameOver(Event event)
	{
		ClientModelManager.getInstance().setState(new GameOverState());
	}

	protected void onUIPollGame(Event event)
	{
		CommandManager.getInstance().pollGame();
	}


	@Override
	protected boolean onError(String errorMessage, int errorCode)
	{
		boolean sendMsg = super.onError(errorMessage, errorCode);
		switch (errorCode)
		{
			case ErrorMessage.PLAYER_NOT_IN_ROOM:
				onPlayerNotInRoom();
				break;
		}
		return sendMsg;
	}

	protected void onPlayerNotInRoom()
	{
		ClientModelManager.getInstance().setState(new JoinGameState());
	}
}
