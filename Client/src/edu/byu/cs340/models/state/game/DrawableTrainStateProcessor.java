package edu.byu.cs340.models.state.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 4/10/2017.
 */
public class DrawableTrainStateProcessor
{

	public static void onCMDDrawTrainCard(Event event)
	{
		String playerName = (String) event.get(EventKeys.PLAYER_NAME);
		if (playerName.equals(ClientModelManager.getInstance().getUsername()))
		{
			boolean canDrawAnother = (boolean) event.get(EventKeys.CAN_DRAW_ANOTHER);
			if (canDrawAnother)
			{
				ClientModelManager.getInstance().setState(new DrawSecondTrainCardState());
			} else
			{
				ClientModelManager.getInstance().setState(new NotMyTurnState());
			}
		}
	}
}
