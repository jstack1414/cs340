package edu.byu.cs340.communication;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.command.BaseCommand;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.login.BaseLoginCommand;
import edu.byu.cs340.message.ChatPostMessage;
import edu.byu.cs340.message.ChatPullMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;

/**
 * Created by joews on 2/1/2017.
 *
 * Implementation of IServer on the client side. The ClientFacade
 * communicates with this to send commands and receive responses from the server.
 */
public class ServerProxy implements IServer
{
	private static ServerProxy mSINGLETON;

	public static ServerProxy getInstance()
	{
		if (mSINGLETON == null)
		{
			mSINGLETON = new ServerProxy();
		}
		return mSINGLETON;
	}

	/**
	 * Executes the command on the server.
	 *
	 * @param baseCommand command to execute on server
	 * @return Returns a ErrorMessage if an error occurred, otherwise returns the result from the server (likely a
	 * CommandMessage or CommandListMessage)
	 *
	 * @pre baseCommand is an IBaseCommand
	 * @pre baseCommand != null
	 * @post executes the command on the server and returns the result in the form of an IMessage
	 */
	@Override
	public IMessage executeCommand(IBaseCommand baseCommand)
	{
		IMessage result;
		String handlerType = "";
		if (baseCommand instanceof BaseLoginCommand)
		{
			handlerType = "command/login";
		} else if (baseCommand instanceof BaseCommand)
		{
			handlerType = "command";
		}
		String authorizationToken = ClientModelManager.getInstance().getAuthenticationToken();
		String deserializedCommand = SerDes.serializeCommand(baseCommand, SerDes.Source.CLIENT);
		result = HTTPClientCommunicator.getInstance().post(authorizationToken, deserializedCommand, handlerType);

		//Test result. If its null create an error
		if (result == null)
		{
			result = new ErrorMessage("Error in ServerFacade", ErrorMessage.MILD_ERROR);
		}//if it is an instance of the error FIRST check to see if it has a command type associated, if not the n
		// assign it.

		return result;
	}

	public IMessage sendChatRequest(IMessage msg)
	{
		IMessage result;
		String handlerType = "chat";
		String authorizationToken = ClientModelManager.getInstance().getAuthenticationToken();
		String serialized;
		if (msg instanceof ChatPostMessage)
		{
			serialized = SerDes.serializeChat((ChatPostMessage) msg, SerDes.Source.CLIENT);
		} else
		{
			serialized = SerDes.serializeChat((ChatPullMessage) msg, SerDes.Source.CLIENT);
		}
		result = HTTPClientCommunicator.getInstance().post(authorizationToken, serialized, handlerType);

		//Test result. If its null create an error
		if (result == null && msg instanceof ChatPullMessage)
		{
			result = new ErrorMessage("Error in ServerProxy", ErrorMessage.MILD_ERROR);
		}//if it is an instance of the error FIRST check to see if it has a command type associated, if not the n
		// assign it.

		return result;
	}
}
