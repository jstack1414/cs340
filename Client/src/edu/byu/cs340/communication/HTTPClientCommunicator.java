package edu.byu.cs340.communication;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static edu.byu.cs340.communication.CommunicatorHelper.readString;

/**
 * Created by Rebekah on 1/30/2017.
 * this class allows the client to communicate with the server.
 *
 * @invariant mHost and mPort are never null when get() and post() are called
 * @invariant there is at most one instance of sInstance
 */
public class HTTPClientCommunicator implements IClientCommunicator
{

	private static HTTPClientCommunicator sInstance;//the instance of the class that can be referenced

	private String mHost = null;//the host the client is using
	private String mPort = null;//the port the client is using

	/**
	 * private constructor
	 */
	private HTTPClientCommunicator()
	{

	}

	/**
	 * Returns the HTTPClientCommunicator singleton
	 *
	 * @return returns an instance of the client communicator
	 *
	 * @pre none
	 * @post creates a new HTTPClientCommunicator if there is no instance of HTTPClientCommunicator yet.
	 */
	public static HTTPClientCommunicator getInstance()
	{
		if (sInstance == null)
		{
			sInstance = new HTTPClientCommunicator();
		}
		return sInstance;
	}

	/**
	 * this method does not send a request body to the server, only headers
	 *
	 * @param authorizationID the user's authorization code, most handlers on the server will return an error
	 *                           message if
	 *                        this is null.
	 * @param handlerType     this is the part of the url that tells the server which handler will be used
	 * @return a json string from the server.
	 *
	 * @pre the handler the get is going to is meant for a "get" (most are made for "post" only)
	 * @pre the handlerType must be a vaild handler from the server
	 * @pre the host and port are set to match the server and those you want to communicate with.
	 * @post this does not change the given params it only sends them to the server. the server will act on the handler
	 * and send back a copy of the requested information or an error message if the input was bad or an error occured
	 */
	@Override
	public IMessage get(String authorizationID, String handlerType)
	{
		IMessage severResponse = null;
		try
		{
			HttpURLConnection http = connect(handlerType, false, authorizationID);
			severResponse = processResponse(http);
		} catch (ClassNotFoundException e)
		{
			severResponse = new ErrorMessage("Unable to identify message class from server\n" + e.getMessage(),
					ErrorMessage.MILD_ERROR);
		} catch (IOException e)
		{
			severResponse = new ErrorMessage("IOException in HTTPClientCommunicator\n" + e.getMessage(), ErrorMessage
					.MILD_ERROR);
		}
		return severResponse;
	}

	private IMessage processResponse(HttpURLConnection http) throws IOException, ClassNotFoundException
	{
		//get the information from the server
		IMessage severResponse = null;
		if (http.getResponseCode() == HttpURLConnection.HTTP_OK)
		{
//                System.out.println("Command successfully sent via GET.");
			//get the message
			InputStream getResponse;
			getResponse = http.getInputStream();
			//the requestbody is edited by the server -> returned and updated here
			String response = readString(getResponse);
			getResponse.close();
			severResponse = SerDes.deserializeMessage(response, SerDes.Source.CLIENT);
		} else if (http.getResponseCode() == HttpURLConnection.HTTP_INTERNAL_ERROR)
		{
			System.out.println("Server ERROR: " + http.getResponseMessage());
			InputStream getResponse;
			getResponse = http.getErrorStream();
			//the requestbody is edited by the server -> returned and updated here
			String response = readString(getResponse);
			getResponse.close();
			severResponse = SerDes.deserializeMessage(response, SerDes.Source.CLIENT);
		} else
		{
			System.out.println("Unknown ERROR: " + http.getResponseMessage());
			severResponse = new ErrorMessage("HTTP Error\n" + http.getResponseMessage(), ErrorMessage.MILD_ERROR);
		}
		return severResponse;
	}

	private HttpURLConnection connect(String handlerType, boolean isPost, String authorizationID) throws
			IOException
	{
		URL url = new URL("http://" + getHost() + getPort() + handlerType);

		HttpURLConnection http = (HttpURLConnection) url.openConnection(); //opens connection
		if (isPost)
		{
			http.setRequestMethod("POST");
		} else
		{
			http.setRequestMethod("GET");
		}
		http.setDoOutput(isPost);    // There is no request body

		if (authorizationID != null)
		{//if there is an authorization it will go in the header
			http.addRequestProperty(CommunicatorHelper.AUTHORIZATION_HEADER_STRING, authorizationID);
		}//otherwise it will not send a Authorization header, thus you will only be sending a url

		http.connect();
		return http;
	}

	/**
	 * this function sends a "post" to the server. it takes in a post body and handler and sends the body to the
	 * handler
	 * the authorization is sent via header to the server if it is not null.
	 *
	 * @param authorizationID   (nullable) the users authorization code
	 * @param formattedPostBody the json string you want to send to the server
	 * @param handlerType       The type of handle (command/ or comand/login or etC)
	 * @return Returns an IMessage specifying what the client needs to do. This may be a ErrorMessage saying an error
	 * occured on the server, It may also be a CommandMessage or CommandListMessage which contain commands to be
	 * executed by the client.
	 *
	 * @pre the handlerType is a handler specified in the server (not all handlers are created equal, choose wisely)
	 * @pre the formattedPostBody is in the form of a json string that can be reconstructed in to an appropriate class
	 * @pre the authorizationID is meant to be null for logging in and registering, but not for anything else (if you
	 * want to have success on the server)
	 * @pre the host and port are set to match your server and those you want to communicate with
	 * @post none of the given parameters will be changed by the function. the formattedPostBody is sent in chunks to
	 * the server. the server then reconstructs the formattedPostBody into a class and acts on it.
	 */
	@Override
	public IMessage post(String authorizationID, String formattedPostBody, String handlerType)
	{
		IMessage severResponse = null;
		try
		{

			HttpURLConnection http = connect(handlerType, true, authorizationID);
			//send the postbody to the server
			OutputStream httpOutputStream = http.getOutputStream();
			CommunicatorHelper.writeString(formattedPostBody, httpOutputStream);
			httpOutputStream.close();

			severResponse = processResponse(http);
		} catch (ClassNotFoundException e)
		{
			severResponse = new ErrorMessage("Unable to identify message class from server\n" + e.getMessage(),
					ErrorMessage.MILD_ERROR);
		} catch (IOException e)
		{
			severResponse = new ErrorMessage("IOException in HTTPClientCommunicator\n" + e.getMessage(), ErrorMessage
					.MILD_ERROR);
		}
		return severResponse;
	}


	/**
	 * Set the value of host variable
	 *
	 * @param host The value to set host member variable to
	 * @pre the host is a string that is a valid host
	 */
	public void setHost(String host)
	{
		mHost = host;
	}

	/**
	 * Set the value of port variable
	 *
	 * @param port The value to set port member variable to
	 * @pre port is a string number that is a valid port number
	 */
	public void setPort(String port)
	{
		mPort = port;
	}

	/**
	 * this get's the host. if the host is not set it will set it for you and notify you
	 *
	 * @return a string or string number that will be the host and adds the : after it
	 *
	 * @pre this should match the host of the client hosting the server, if your host isn't the same as your friends'
	 * then you won't be playing together.
	 * @post host will be set to "localhost" if it has not been set, otherwise it will not be changed
	 */
	public String getHost()
	{
		if (mHost == null)
		{
			System.out.println("Client host is not set. setting to: localhost.");
			mHost = "localhost";
		}
		return mHost + ":";
	}

	/**
	 * this gets the port. if the port is not set it will be set for you and it will notify you
	 *
	 * @return a string number that is the port and add the / after it
	 *
	 * @pre this should match the port(s) that the server is listening on or there will be no communication
	 * @post the port will be set to "25563" if port it still null other wise it will not be changed
	 */
	public String getPort()
	{
		if (mPort == null)
		{
			System.out.println("Client port is not set. Setting to: 8080");
			mPort = "25563";
		}
		return mPort + "/";
	}
}
