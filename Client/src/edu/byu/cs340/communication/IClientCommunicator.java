package edu.byu.cs340.communication;

import edu.byu.cs340.message.IMessage;

/**
 * Created by Rebekah on 1/30/2017.
 */
public interface IClientCommunicator {


    /**
     * @pre setPortAndHost must have been used, otherwise a default will be set
     * @param authorizationID (nullable) the user's  authoriziton code
     * @param requestType this is the http handle you want it to have (ie. /command/trim)
     * @return
     * @post returns a string from the server
     */
    IMessage get(String authorizationID, String requestType);

    /**
     * @pre: setPortAndHost must have been used, otherwise a default will be set
     * @param authorizationID (nullable) the users authorization code
     * @param formattedPostBody the json string you want to send to the server
     * @param requestType the http handle (ie. /command/trim)
     * @return
     * @post returns a json formatted string given to it by the server based on the command
     */
    IMessage post(String authorizationID, String formattedPostBody, String requestType);


	/**
	 * Set the value of host variable
	 *
	 * @param host The value to set host member variable to
	 */
	void setHost(String host);

	/**
	 * Set the value of port variable
	 *
	 * @param port The value to set port member variable to
	 */
	void setPort(String port);

	/**
	 * this get's the host. if the host is not set it will set it for you and notify you
	 *
	 * @return a string or string number that will be the host and adds the : after it
	 */
	String getHost();

	/**
	 * @return a string number that is the port and add the / after it
	 * @pre this should match the port(s) that the server is listening on or there will be no communication this gets
	 * the port. if the port is not set it will be set for you and it will notify you
	 */
	String getPort();

}
