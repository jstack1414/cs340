package edu.byu.cs340;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IClientFacade;
import edu.byu.cs340.communication.HTTPClientCommunicator;
/**
 * Created by joews on 1/26/2017.
 *
 * Main that launches the client.
 */
public class ClientMain
{
	public static void main(String[] args)
	{
		String host = null;
		String port = null;
		if (args.length >= 2)
		{
			host = args[0];
			port = args[1];
		}
		//set communication port and host
		HTTPClientCommunicator.getInstance().setPort(port);
		HTTPClientCommunicator.getInstance().setHost(host);
		//create the poller
		ClientFacade.getInstance(IClientFacade.class).setUp();

	}
}
