package edu.byu.cs340.clientFacade;

/**
 * Created by joews on 2/4/2017.
 *
 * Interface used by the serverProxy to get the auhentication token
 */
public interface IAuthenticationModel extends IClientFacade
{
	/**
	 * Returns the authentication token of the ClientUser.
	 *
	 * @return The authentication token of the client's user
	 */
	String getAuthenticationToken();
}
