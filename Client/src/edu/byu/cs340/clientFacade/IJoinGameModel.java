package edu.byu.cs340.clientFacade;

import edu.byu.cs340.models.roomModel.IRoomsListInfo;

/**
 * Created by Sean on 2/1/2017.
 *
 * Interface for the functions related to Join Game
 */
public interface IJoinGameModel extends IClientFacade, IUIModel
{
	/**
	 * Request that the client join a game specified by gameID
	 *
	 * @param gameID the UID for the game to be joined
	 * @return nothing, will be observed
	 *
	 * @pre gameID must meet UID standards
	 * @pre client must not be in a game already
	 * @post game will be joined if game has room
	 */
	void joinLobby(String gameID);

	/**
	 * Request that a game be created with the given name
	 *
	 * @param title name of game to be created
	 * @return nothing, will be observed
	 *
	 * @pre title must be between 3 to 20 characters
	 * @pre client must not be in a game already
	 * @post game will be created if no game exists with duplicate name
	 * @post client will join game automatically
	 */
	void createLobby(String title);

	/**
	 * Requests the client to update the local models game list info
	 *
	 * @pre the user must be logged in
	 * @post the models game list info will be updated to match the server
	 * @post the presenter will be notified via the observer pattern
	 */
	void updateGamesListInfo();

	/**
	 * Requests the game list info from the client based on its locally stored information.
	 *
	 * @return returns the list of games
	 *
	 * @pre updateGamesListInfo() must be called first to ensure game list is up-todate.
	 * @pre this method should only be invoked when an observer is notified the games list is up to date or when
	 * initializing
	 * @post returns an accurate games list
	 */
	IRoomsListInfo getGameListInfo();
}
