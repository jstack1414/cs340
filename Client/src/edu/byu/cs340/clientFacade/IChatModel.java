package edu.byu.cs340.clientFacade;

import edu.byu.cs340.models.ChatMessage;

import java.util.List;

/**
 * Created by joews on 3/6/2017.
 */
public interface IChatModel extends IClientFacade
{
	/**
	 * Returns the chat messages from the lobby.
	 *
	 * @param lastIndex messages since index
	 * @return Chat messages formatter
	 */
	List<ChatMessage> getChatMessages(long lastIndex);

	/**
	 * Gets the index of hte last message
	 *
	 * @return Index of last message
	 */
	long getLastChatIndex();
}
