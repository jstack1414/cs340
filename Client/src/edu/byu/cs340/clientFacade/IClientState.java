package edu.byu.cs340.clientFacade;

import edu.byu.cs340.models.state.IState;

/**
 * Created by tjacobhi on 04-Feb-17.
 *
 * Is the interface to interact with clientState
 */
public interface IClientState extends IClientFacade
{
	/**
	 * Gets the current state of hte client.
	 *
	 * @return The state of the client
	 *
	 * @post result != null
	 */
	IState getState();
}
