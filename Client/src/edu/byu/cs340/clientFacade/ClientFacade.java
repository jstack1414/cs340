package edu.byu.cs340.clientFacade;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.communication.HTTPClientCommunicator;
import edu.byu.cs340.communication.Poller;
import edu.byu.cs340.communication.ServerProxy;
import edu.byu.cs340.message.ChatPostMessage;
import edu.byu.cs340.message.ChatPullMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.ChatUpdatedObsMsg;
import edu.byu.cs340.messages.ErrorObsMsg;
import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.ChatMessenger;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientRoute;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;
import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;
import edu.byu.cs340.models.roomModel.IPlayer;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;
import edu.byu.cs340.models.state.IState;
import edu.byu.cs340.models.state.LobbyState;
import edu.byu.cs340.models.state.LoginState;
import edu.byu.cs340.models.state.game.GameState;
import edu.byu.cs340.presenter.PresenterManager;
import edu.byu.cs340.presenter.game.GamePresenterProxy;
import edu.byu.cs340.view.WindowManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by joews on 2/1/2017.
 *
 * The ClientFacade is a facade between the presenter and models layers. It has functionality
 * called by the presenters and talks with the server models as well as the ClientModelManager
 * where the mData is actually controlled and stored.
 */
public class ClientFacade
		implements ILobbyModel,
		IJoinGameModel,
		ILoginModel,
		ICommandModel,
		IUIModel,
		IPollerModel,
		IGameModel
{
	//<editor-fold desc="Singleton">
	private static ClientFacade mSINGLETON;

	/**
	 * Gets the singleton of the ClientFacade and casts it as the type of interface specified by type.
	 *
	 * @param type Interface type to cast facade as
	 * @param <T>  Interface Type
	 * @return Returns the client facade as the specified type.
	 */
	public static <T extends IClientFacade> T getInstance(Class<T> type)
	{
		if (mSINGLETON == null)
		{
			mSINGLETON = new ClientFacade();
		}
		return type.cast(mSINGLETON);
	}

	/**
	 * Constructor for client facade.
	 */
	private ClientFacade()
	{

	}
	//</editor-fold>

	//<editor-fold desc="Methods for setting up the ClientFacade">
	public void setUp()
	{
		//force the window manager to load
		WindowManager.getInstance();
		//force presenters to load....
		PresenterManager.getInstance();
		//set the initial state
		ClientModelManager.getInstance().setState(new LoginState());
		Poller.getInstance().startPolling();
	}
	//</editor-fold>

	//<editor-fold desc="Methods implemented from IClientState">
	@Override
	public IState getState()
	{
		return ClientModelManager.getInstance().getState();
	}
	//</editor-fold>

	//<editor-fold desc="Methods implemented from IJoinGameModel">


	@Override
	synchronized public void joinLobby(String gameID)
	{
		Event event = new Event(EventType.UIJoinGame);
		event.put(EventKeys.LOBBY_ID, gameID);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	synchronized public void createLobby(String title)
	{
		Event event = new Event(EventType.UICreateLobby);
		event.put(EventKeys.LOBBY_NAME, title);
		ClientModelManager.getInstance().getState().processEvent(event);
	}


	@Override
	synchronized public void updateGamesListInfo()
	{
		Event event = new Event(EventType.UIPullGames);
		ClientModelManager.getInstance().processStateEvent(event);
	}


	synchronized public IRoomsListInfo getGameListInfo()
	{
		return ClientModelManager.getInstance().getGamesListInfo();
	}

	//</editor-fold>

	//<editor-fold desc="Methods implemented from ILobbyModel">

	@Override
	synchronized public void sendLobbyChatMessage(String message)
	{
		//TODO: Consider moving functionality
		String lobbyID = ClientModelManager.getInstance().getLobby().getRoomID();
		sendChatMessage(message, lobbyID);
	}

	private void sendChatMessage(String message, String roomID)
	{

		//TODO: Consider moving functionality
		//create the base command with mData
		String userName = ClientModelManager.getInstance().getUsername();

		IMessage msg = new ChatPostMessage(new ChatMessage(userName, message), roomID);
		//execute it on the server and get a message
		ServerProxy.getInstance().sendChatRequest(msg);
		//do nothin with the result...
	}

	@Override
	public List<ChatMessage> getChatMessages(long lastIndex)
	{
		IState state = ClientModelManager.getInstance().getState();
		if (state instanceof GameState)
		{
			return ClientModelManager.getInstance().getGame().getChatMessenger().getNewMessages(lastIndex);
		} else if (state instanceof LobbyState)
		{
			return ClientModelManager.getInstance().getLobby().getChatMessages(lastIndex);
		}
		return new ArrayList<>();
	}

	@Override
	public long getLastChatIndex()
	{
		return ClientModelManager.getInstance().getLobby().getLastIndex();
	}

	@Override
	synchronized public void sendReadySignal()
	{
		Event event = new Event(EventType.UIReadyStatus);
		event.put(EventKeys.IS_READY, true);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	synchronized public void sendUnreadySignal()
	{
		Event event = new Event(EventType.UIReadyStatus);
		event.put(EventKeys.IS_READY, false);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	synchronized public void sendStartSignal()
	{
		Event event = new Event(EventType.UIStartGame);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	synchronized public void sendLeaveSignal()
	{
		Event event = new Event(EventType.UILeaveLobby);
		event.put(EventKeys.PLAYER_NAME, ClientModelManager.getInstance().getUsername());
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	public void sendRemovePlayerSignal(String userToRemove)
	{
		Event event = new Event(EventType.UIRemoveLobbyPlayer);
		event.put(EventKeys.PLAYER_NAME, userToRemove);
		ClientModelManager.getInstance().processStateEvent(event);
	}


	@Override
	public List<ILobbyPlayer> getLobbyPlayers()
	{
		List<IPlayer> players = ClientModelManager.getInstance().getLobby().getPlayers();
		List<ILobbyPlayer> lobbyPlayers = new ArrayList<>();
		for (IPlayer player : players)
		{
			lobbyPlayers.add((ILobbyPlayer) player);
		}
		return lobbyPlayers;
	}

	@Override
	public boolean getAmIReady()
	{
		try
		{
			return ClientModelManager.getInstance().getLobby().getPlayer(getUserName()).isReady();
		} catch (Exception e)
		{
			return false;
		}
	}

	@Override
	public boolean getAmIHost()
	{
		try
		{
			return ClientModelManager.getInstance().getLobby().getPlayer(getUserName()).isHost();
		} catch (Exception e)
		{
			return false;
		}
	}

	@Override
	public String getLobbyName()
	{
		return ClientModelManager.getInstance().getLobby().getRoomName();
	}

	//</editor-fold>

	//<editor-fold desc="Methods implemented from ILoginModel">

	@Override
	public synchronized void login(String username, String password)
	{
		Event event = new Event(EventType.UILogin);
		event.put(EventKeys.USERNAME, username);
		event.put(EventKeys.PASSWORD, password);
		ClientModelManager.getInstance().getState().processEvent(event);
	}

	@Override
	synchronized public void register(String username, String password, String confirmPassword)
	{

		Event event = new Event(EventType.UIRegister);
		event.put(EventKeys.USERNAME, username);
		event.put(EventKeys.PASSWORD, password);
		event.put(EventKeys.CONFIRM_PASSWORD, confirmPassword);
		ClientModelManager.getInstance().getState().processEvent(event);
	}

	@Override
	public void setPort(String port)
	{
		HTTPClientCommunicator.getInstance().setPort(port);
	}

	@Override
	public void setHost(String host)
	{
		HTTPClientCommunicator.getInstance().setHost(host);
	}

	@Override
	public String getPort()
	{
		String result = HTTPClientCommunicator.getInstance().getPort();
		return result.substring(0, result.length() - 1);
	}

	@Override
	public String getHost()
	{
		String result = HTTPClientCommunicator.getInstance().getHost();
		return result.substring(0, result.length() - 1);
	}

	//</editor-fold>

	//<editor-fold desc="Methods implemented from IUIModel">
	@Override
	public void signOut()
	{
		Event event = new Event(EventType.UISignOut);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	public String getUserName()
	{
		return ClientModelManager.getInstance().getUsername();
	}

	//</editor-fold>

	//<editor-fold desc="Methods implemented from IPollerModel">

	@Override
	synchronized public void updateLobby()
	{
		Event event = new Event(EventType.UIPollLobby);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	synchronized public void updateGame()
	{
		Event event = new Event(EventType.UIPollGame);
		ClientModelManager.getInstance().processStateEvent(event);
	}


	//TODO: Consider moving this. Perhaps in the state pattern somewhere.
	@Override
	public void pullChat()
	{
		ChatMessenger messenger = null;
		String roomID = null;
		IClientGameModel game = null;
		ClientLobbyModel lobby = null;
		IState state = ClientModelManager.getInstance().getState();
		if (state instanceof GameState)
		{
			game = ClientModelManager.getInstance().getGame();
			messenger = game.getChatMessenger();
			roomID = game.getRoomID();
		} else if (state instanceof LobbyState)
		{
			lobby = ClientModelManager.getInstance().getLobby();
			messenger = lobby.getChatMessenger();
			roomID = lobby.getRoomID();
		}
		if (messenger != null && roomID != null)
		{
			IObsMsg observerMessage = null;
			long lastIndex = messenger.getLastIndex();
			IMessage msg = new ChatPullMessage(roomID, lastIndex, getUserName());
			IMessage serverResult = ServerProxy.getInstance().sendChatRequest(msg);
			if (serverResult instanceof ErrorMessage)
			{
				observerMessage = new ErrorObsMsg(((ErrorMessage) serverResult).getMessage
						());
			} else
			{
				ChatPullMessage response = (ChatPullMessage) serverResult;
				if (response.getChatMessages().size() > 0)
				{
					if (game != null)
					{
						game.addChatMessage(response.getChatMessages());
					} else if (lobby != null)
					{
						lobby.addChatMessage(response.getChatMessages());
					}
					observerMessage = new ChatUpdatedObsMsg(null);
				}
			}

			ClientModelManager.getInstance().notifyObservers(observerMessage);
		}
	}

	//</editor-fold>

	//<editor-fold desc="Methods implemented from IGameModel">
	@Override
	public void claimRoute(String routeID, List<String> cards)
	{
		Event event = new Event(EventType.UIClaimRoute).put(EventKeys.ROUTE_ID, routeID).put(EventKeys
				.TRAIN_CARD_ID_LIST, cards);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	public void drawTrainCardFromDeck()
	{
		Event event = new Event(EventType.UIDrawTrainCardFromDeck);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	public void drawTrainCardFromZone(int zone)
	{

		Event event = new Event(EventType.UIDrawTrainCardFromZone).put(EventKeys.TRAIN_CARD_ZONE, zone);
		ClientModelManager.getInstance().processStateEvent(event);
	}


	@Override
	public void chooseDestinationCards(List<String> cardIDs, int mMinToKeep)
	{
		Event event = new Event(EventType.UIReturnDestinationCards).put(EventKeys.DESTINATION_CARD_ID_LIST, cardIDs)
				.put
				(EventKeys.MIN_CARD_TO_KEEP, mMinToKeep);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	public void sendGameChatMessage(String message)
	{
		String gameID = ClientModelManager.getInstance().getGame().getRoomID();
		sendChatMessage(message, gameID);
	}


	@Override
	public List<ClientTrainCard> getTrainCards()
	{
		return ClientModelManager.getInstance().getGame().getClientPlayerTrainCards();
	}

	@Override
	public List<ClientDestinationCard> getDestinationCards()
	{
		return ClientModelManager.getInstance().getGame().getClientPlayerDestinationCards();
	}

	@Override
	public Map<String, ClientCity> getCities()
	{
		Map<String, ClientCity> map = new TreeMap<String, ClientCity>();
		for (ClientCity city : ClientModelManager.getInstance().getGame().getGameMap().getCities())
		{
			if (city != null)
			{
				map.put(city.getId(), city);
			}
		}
		return map;
	}

	@Override
	public Map<String, ClientRoute> getRoutes()
	{
		Map<String, ClientRoute> map = new TreeMap<String, ClientRoute>();
		for (ClientRoute route : ClientModelManager.getInstance().getGame().getRoutes())
		{
			map.put(route.getId(), route);
		}
		return map;
	}

	@Override
	public String getBackgroundImage()
	{
		//Temporarily retrn a hardcoded value
		String backgroundPath = "/game/usMap2.png";
		return backgroundPath; //todo implement this
	}

	@Override
	public List<IGamePlayer> getPlayers()
	{
		return ClientModelManager.getInstance().getGame().getPlayers();
	}

	@Override
	public ClientTrainCard[] getFaceUpCards()
	{
		return ClientModelManager.getInstance().getGame().getRevealedCards();
	}

	@Override
	public ClientDestinationCard[] getTemporaryDestinationCards()
	{
		return ClientModelManager.getInstance().getGame().getTemporaryDestinationCards();
	}

	@Override
	public void selectActionForTurn(int option)
	{
		switch (option)
		{
			case GamePresenterProxy.DRAW_DESTINATION_CARD_OPTION:
				ClientModelManager.getInstance().processStateEvent(new Event(EventType.UIDrawDestinationCardsPicked));
				break;
			case GamePresenterProxy.DRAW_TRAIN_CARD_OPTION:
				ClientModelManager.getInstance().processStateEvent(new Event(EventType.UIDrawTrainCardsPicked));
				break;
			case GamePresenterProxy.CLAIM_ROUTE_OPTION:
				ClientModelManager.getInstance().processStateEvent(new Event(EventType.UIClaimRoutePicked));
				break;
		}
	}

	@Override
	public void uiCancel()
	{
		Event event = new Event(EventType.UICancel);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	public void leaveGame()
	{
		Event event = new Event(EventType.UILeaveGame);
		ClientModelManager.getInstance().processStateEvent(event);
	}

	@Override
	public boolean isRouteClaimable(String routeID)
	{
		IClientGameModel game = ClientModelManager.getInstance().getGame();
		ClientRoute route = game.getRoute(routeID);
		if (route.isClaimed())
		{
			return false;
		} else if (route.getNumTrains() > game.getClientPlayer().getTrainsRemaining())
		{
			return false;
		} else if (route.getPairedRouteID() == null)
		{
			return true;
		} else if (game.getRoute(route.getPairedRouteID()) == null)
		{
			return true;
		} else if (!game.getRoute(route.getPairedRouteID()).isClaimed())
		{
			return true;
		} else if (game.getRoute(route.getPairedRouteID()).isClaimed() && game.getPlayers().size() < 4)
		{
			return false;
		} else if (game.getRoute(route.getPairedRouteID()).getClaimedUsername().equals(getUserName()))
		{
			return false;
		} else
		{
			return true;
		}
	}

	@Override
	public boolean canDrawDestCards()
	{
		if (ClientModelManager.getInstance().getGame() != null)
		{
			return ClientModelManager.getInstance().getGame().hasDrawableDestCards();
		} else
		{
			return false;
		}
	}

	@Override
	public boolean canDrawTrainCards()
	{
		if (ClientModelManager.getInstance().getGame() != null)
		{
			return ClientModelManager.getInstance().getGame().hasDrawableTrainCards();
		} else
		{
			return false;
		}
	}

	//</editor-fold>

//	//TODO: Refactor out of code
//	//<editor-fold desc="Demo functions">
//	@Override
//	synchronized public void drawFaceUpTrainCardDemo(String card)
//	{
//		//make sure face up cards are good
//		demoMagicCardMaker();
//		//make sure
//		ClientTrainCard[] faceUpCards = ClientModelManager.getInstance().getGame().getRevealedCards();
//		for (int i = 0; i < faceUpCards.length; i++)
//		{
//			if (faceUpCards[i].getId().equals(card))
//			{
//				ClientModelManager.getInstance().getGame().getClientPlayer().addTrainCard(faceUpCards[i]);
//				ClientModelManager.getInstance().getGame().getBank().removeRevealedCard(faceUpCards[i]);
//				break;
//			}
//		}
//		demoMagicCardMaker();
//		ClientModelManager.getInstance().notifyObservers(new TrainCardDrawnObsMsg(null, ClientModelManager
//				.getInstance().getGame()
//				.getClientPlayer()));
//
//	}
//
//	synchronized private void demoMagicCardMaker()
//	{
//		//test faceup cards..
//		IClientGameModel game = ClientModelManager.getInstance().getGame();
//		ClientTrainCard[] faceups = game.getRevealedCards();
//		ClientTrainCard[] newFaceUp = new ClientTrainCard[faceups.length];
//		for (int i = 0; i < faceups.length; i++)
//		{
//			ClientTrainCard card = faceups[i];
//			if (card == null)
//			{
//				newFaceUp[i] = ClientModelManager.getInstance().getDemoModel().popTrainCard();
//				ClientModelManager.getInstance().getGame().getBank().setTrainCardDeckSize(ClientModelManager
//						.getInstance().getDemoModel()
//						.getTrainCardList().size());
//			} else
//			{
//				newFaceUp[i] = card;
//			}
//		}
//		game.setRevealedCards(newFaceUp);
//	}
//
//	@Override
//	synchronized public void drawTrainCardFromDeckDemo()
//	{
//		ClientTrainCard card = ClientModelManager.getInstance().getDemoModel().popTrainCard();
//		ClientModelManager.getInstance().getGame().getClientPlayer().addTrainCard(card);
//		ClientModelManager.getInstance().getGame().getBank().setTrainCardDeckSize(ClientModelManager.getInstance()
//				.getGame().getBank()
//				.getTrainCardDeckSize() - 1);
//		ClientModelManager.getInstance().notifyObservers(new TrainCardDrawnObsMsg(null, ClientModelManager
//				.getInstance().getGame()
//				.getClientPlayer()));
//	}
//
//	@Override
//	synchronized public void drawDestinationCardsDemo(int minToKeep)
//	{
//		//check if it is the first turn, if not then send a message off to the server to draw destination cards.
//		//create fake destination cards and set them....
//		List<ClientDestinationCard> cards = new ArrayList<>();
//		for (int i = 0; i < 3; i++)
//		{
//			cards.add(ClientModelManager.getInstance().getDemoModel().popDestinationCard());
//		}
//		ClientModelManager.getInstance().getGame().addTemporaryDestinationCards(cards, minToKeep);
//
//		ClientModelManager.getInstance().getGame().getBank().setDestinationCardDeckSize(ClientModelManager.getInstance
//				().getGame().getBank()
//				.getDestinationCardDeckSize() - 3);
//		ClientModelManager.getInstance().notifyObservers(new DestinationCardDrawnObsMsg(null, minToKeep,
//				true));
//	}
//
//	@Override
//	synchronized public void returnDestinationCardsDemo(List<String> cardsToKeep)
//	{
//		//COMMAND WOULD DO THIS.....
//		List<ClientDestinationCard> currentHand = new ArrayList<>();
//		for (ClientDestinationCard card : ClientModelManager.getInstance().getGame()
//				.getTemporaryDestinationCards())
//		{
//			if (card != null)
//			{
//				currentHand.add(card);
//			}
//		}
//		List<ClientDestinationCard> newHand = new ArrayList<>();
//		for (ClientDestinationCard card : currentHand)
//		{
//			if (card != null)
//			{
//				if (cardsToKeep.contains(card.getId()))
//				{
//					newHand.add(card);
//				}
//			}
//		}
//		ClientModelManager.getInstance().getGame().getClientPlayer().addDestinationCards(newHand);
//		ClientModelManager.getInstance().getGame().getBank().setTemporaryCards(new ClientDestinationCard[0], 0);
//		ClientModelManager.getInstance().getGame().getBank().setDestinationCardDeckSize(ClientModelManager.getInstance
//				().getGame().getBank()
//				.getDestinationCardDeckSize() + 3 - cardsToKeep.size());
//		//END COMMAND STUFF
//
//		//TODO: Test if it returned the right number of cards
//		boolean success = true;
//		if (success)
//		{
//			ClientModelManager.getInstance().notifyObservers(new DestinationCardReturnedObsMsg(null,
//					ClientModelManager.getInstance()
//							.getGame()
//							.getClientPlayer()));
//		}
//	}
//
//	@Override
//	synchronized public void claimRouteDemo(String route, String username)
//	{
//		//COMAND DOES THIS
//		String routeId = route;
//		ClientModelManager.getInstance().getGame().claimRoute(routeId, username);
//		ClientRoute clientRoute = ClientModelManager.getInstance().getGame().getRoute(routeId);
//		ClientModelManager.getInstance().getGame().getPlayer(username).setTrainsRemaining(ClientModelManager
//				.getInstance().getGame().getPlayer
//						(username).getTrainsRemaining() - clientRoute.getNumTrains());
//		//END COMMAND STUFF
//
//		boolean success = true;
//		if (success)
//		{
//			ClientModelManager.getInstance().notifyObservers(new ClaimRouteObsMsg(null, ClientModelManager
//					.getInstance().getGame().getPlayer
//							(username),
//					routeId, clientRoute.getNumTrains()));
//		}
//	}
//
//	@Override
//	synchronized public void updateScoresDemo()
//	{
//		//go through each player and update their scores..
//		/*for (ClientGamePlayer player : ClientModelManager.getInstance().getGame().sendGetPlayers())
//		{
//			player.updateScore();
//		}*/
//
//		boolean success = true;
//		if (success)
//		{
//			ClientModelManager.getInstance().notifyObservers(new ScoreChangedObsMsg(null, ClientModelManager
//					.getInstance().getGame()
//					.getPlayers()));
//		}
//	}
//
//	@Override
//	public void launchRunDemoThread()
//	{
////		runDemo();
//		new Thread(() -> ClientFacade.getInstance(IGameDemo.class).runDemo()).start();
//	}
//
//	private void pauseDemo(int val)
//	{
//		try
//		{
//			Thread.sleep(val);
//		} catch (InterruptedException e)
//		{
//			e.printStackTrace();
//		}
//	}
//
//	private static final int LONG_PAUSE = 1000;
//	private static final int SHORT_PAUSE = 500;
//	private JFrame demoJframe;
//	private JTextArea demopopup;
//
//	synchronized public void showMessage(String message)
//	{
//		if (demoJframe == null || !demoJframe.isVisible())
//		{
//			demoJframe = new JFrame();
//			demoJframe.setMinimumSize(new Dimension(300, 100));
//			demopopup = new JTextArea();
//			demoJframe.setLocation(600, 600);
//			demopopup.setLineWrap(true);
//			demoJframe.add(demopopup);
//			demoJframe.pack();
//			demoJframe.setVisible(true);
//		}
//		demopopup.setText(DEMO + message);
//		demoJframe.toFront();
//		demoJframe.repaint();
//		pauseDemo(SHORT_PAUSE);
//	}
//
//	synchronized public void closeMessage()
//	{
//		demoJframe.dispose();
//	}
//
//	@Override
//	public void runDemo()
//	{
//
//		showMessage("Running Client Demo");
//		pauseDemo(LONG_PAUSE);
//
//		showMessage("Drawing a train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//		showMessage("Drawing another train card from the deck.");
//		drawTrainCardFromDeckDemo();
//		pauseDemo(LONG_PAUSE);
//
//
//		showMessage("Drawing a train card from the faceup zone 1.");
//		drawFaceUpTrainCardDemo(ClientModelManager.getInstance().getGame().getBank().getRevealedCards()[1].getId());
//		pauseDemo(LONG_PAUSE);
//
//
//		showMessage("Drawing a train card from the faceup zone 2.");
//		drawFaceUpTrainCardDemo(ClientModelManager.getInstance().getGame().getBank().getRevealedCards()[2].getId());
//		pauseDemo(LONG_PAUSE);
//
//
//		showMessage("Drawing destination cards from the deck. Selecting the first two and putting into hand.");
//		drawDestinationCardsDemo(1);
//		pauseDemo(LONG_PAUSE);
//		GamePresenterProxy gp = (GamePresenterProxy) PresenterManager.getInstance().demoGetPresenter(PresenterManager
//				.PresenterKey.GAME);
//		gp.forceCloseDestinationCardSelection();
//
//		showMessage("Claiming route from Vancouver to Seattle for the clients player.");
//		//claim the route associated b2fdca1e-b936-437e-8a97-c21afc7e9b0a vancoover to seattle
//		claimRouteDemo("b2fdca1e-b936-437e-8a97-c21afc7e9b0a", ClientModelManager.getInstance().getGame()
//				.getClientPlayer()
//				.getUsername());
//		pauseDemo(LONG_PAUSE);
//
//
//		showMessage("Claiming route from Helena to Omaha for an opponent.");
//		// claim this for opponet 391034a7-3f0c-4dd3-8db6-da328b532fcb helena to omaha
//		for (ClientGamePlayer player : ClientModelManager.getInstance().getGame().getPlayers())
//		{
//			if (!player.getUsername().equals(getUserName()))
//			{
//				claimRouteDemo("391034a7-3f0c-4dd3-8db6-da328b532fcb", player.getUsername());
//				break;
//			}
//		}
//		pauseDemo(LONG_PAUSE);
//
//		showMessage("Updating all players score to 5.");
//		for (ClientGamePlayer player : ClientModelManager.getInstance().getGame().getPlayers())
//		{
//
//			player.setScore(5);
//		}
//		updateScoresDemo();
//		pauseDemo(LONG_PAUSE);
//
//		showMessage("Sending off chat message \"here is the message\"");
//		sendGameChatMessage("here is the message");
//		javax.swing.SwingUtilities.invokeLater(() -> gp.openChat());
//		pauseDemo(LONG_PAUSE);
//		gp.closeChat();
//
//		//force the main window to display
//		WindowManager.getInstance().changeViewTo((MainView) gp.getView());
////		mGamePresenter.closeChat();
//
//		showMessage("Drawing 2 train cards  for an opponent");
//		for (ClientGamePlayer player : ClientModelManager.getInstance().getGame().getPlayers())
//		{
//			if (!player.getUsername().equals(getUserName()))
//			{
//				player.addTrainCard(new DummyTrainCard());
//				player.addTrainCard(new DummyTrainCard());
//
//				//send the observer message
//				//setChanged();
//				//notifyObservers(new TrainCardDrawnObsMsg(null, player));
//				break;
//			}
//		}
//		pauseDemo(LONG_PAUSE);
//
//
//		showMessage("Drawing 3 destination cards for an opponent");
//		for (ClientGamePlayer player : ClientModelManager.getInstance().getGame().getPlayers())
//		{
//			if (!player.getUsername().equals(getUserName()))
//			{
//				List<ClientDestinationCard> cards = new ArrayList<>();
//				for (int i = 0; i < 3; i++)
//				{
//					cards.add(new DummyDestinationCard());
//				}
//				player.addDestinationCards(cards);
//
//				//send the observer message
//				//setChanged();
//				//notifyObservers(new DestinationCardReturnedObsMsg(null, player));
//				break;
//			}
//		}
//		pauseDemo(LONG_PAUSE);
//
//		showMessage("The demo has completed");
//		pauseDemo(LONG_PAUSE);
//
//		closeMessage();
//
//
//	}
//
//	@Override
//	public int getNumTrainCardsInDeckDemo()
//	{
//		return ClientModelManager.getInstance().getGame().getBank().getTrainCardDeckSize();
//	}
//
//	@Override
//	public int getNumDestinationCardsInDeckDemo()
//	{
//		return ClientModelManager.getInstance().getGame().getBank().getDestinationCardDeckSize();
//	}
//
//	@Override
//	public int getNumDiscardCardsDemo()
//	{
//		return 0;
//	}
//
//	@Override
//	public List<ClientTrainCard> getReaveledTrainCardsDemo()
//	{
//		ClientTrainCard[] cards = ClientModelManager.getInstance().getGame().getBank().getRevealedCards();
//		List<ClientTrainCard> result = new ArrayList<>();
//		for (ClientTrainCard card : cards)
//		{
//			result.add(card);
//		}
//		return result;
//	}

	//</editor-fold>
}
