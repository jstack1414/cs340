package edu.byu.cs340.clientFacade;

/**
 * Created by Sean on 2/8/2017.
 *
 * Interface for the ClientFacade to be used by the poller
 */
public interface IPollerModel extends IClientState
{

	/**
	 * Polls the server for any changes to the lobby, called by the Poller about every second
	 */
	void updateLobby();

	/**
	 * Polls the server for any changes to the game, called by the Poller about every second
	 */
	void updateGame();

	/**
	 * Asks the client to pul lchat from the server
	 */
	void pullChat();

	void updateGamesListInfo();
}


