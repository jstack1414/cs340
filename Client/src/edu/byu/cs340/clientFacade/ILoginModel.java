package edu.byu.cs340.clientFacade;

/**
 * Created by joews on 2/1/2017.
 *
 * The ILoginModel is an interface the Login presenter uses to interface with the models. This is used to hide
 * functionality of the client facade.
 */
public interface ILoginModel extends IClientFacade, IUIModel
{
	/**
	 * The login command will authenticate user credentials. The message of which will
	 * be passed through the observer pattern update.
	 *
	 * @param userName The users name
	 * @param password The associated password
	 * @pre userName is alpha numeric
	 * @pre userName.length()&lte16
	 * @post if valid login then the authentication token will be stored in the models
	 */
	void login(String userName, String password);

	/**
	 * The register command will add the user if they do not already exist, the message of which will
	 * be passed through the observer pattern update.
	 *
	 * @param userName        The users name
	 * @param password        The associated password
	 * @param confirmPassword The users password confirmation
	 * @pre userName is alpha numeric
	 * @pre userName.length()&lte16
	 * @post if valid registration then the authentication token will be stored in the models
	 */
	void register(String userName, String password, String confirmPassword);

	/**
	 * Sets the port the communicator connects to.
	 *
	 * @param port The port to connect to.
	 */
	void setPort(String port);

	/**
	 * Sets the host the communicator connects to.
	 * @param host The host to connect to.
	 */
	void setHost(String host);

	/**
	 * Gets the port the communicator currently connects to.
	 * @return The port the communicator is connected to.
	 */
	String getPort();

	/**
	 * Gets the host the communicator currently connects to.
	 * @return The host the communicator is connected to.
	 */
	String getHost();
}
