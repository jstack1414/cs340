package edu.byu.cs340.clientFacade;

/**
 * Created by joews on 2/4/2017.
 *
 * This is the interface that commands see the client facade trhough
 */
public interface ICommandModel extends IClientFacade
{

}
