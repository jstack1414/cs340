package edu.byu.cs340.command.lobby;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.messages.StartGameObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientGameModel;
import edu.byu.cs340.models.gameModel.ClientRoute;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 2/1/2017.
 *
 * Command to start the game on the client side.
 */
public class ClientStartGameCommand extends StartGameCommand
{

	public ClientStartGameCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
	}

	/**
	 * Starts the game by setting the game models with the lobby models as an initializtion.
	 *
	 * @return Returns a StartGameObserver message.
	 */
	@Override
	public IMessage execute()
	{
		//get the local looby models
		IObsMsg message;
		//verify its the right models, if so launch the game and send a message telling presenters.

		createGameFromLobby();
		initializeMapObjects();
//		initializeDemoObjects();
		ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDStartGame));


		message = new StartGameObsMsg(null);
		return message;
	}

//	private void initializeDemoObjects()
//	{
//		DemoModel demoModel = new DemoModel();
//		List<ClientDestinationCard> demoDestinationCards = new ArrayList<>();
//		for (IDestinationCard card : mDemoDestinationCardDeck)
//		{
//			demoDestinationCards.add(new ClientDestinationCard(card));
//		}
//		demoModel.setDestinationCardList(demoDestinationCards);
//		List<ClientTrainCard> demoTrainCards = new ArrayList<>();
//		for (ITrainCard card : mDemoTrainCardDeck)
//		{
//			demoTrainCards.add(new ClientTrainCard(card));
//		}
//		demoModel.setTrainCardList(demoTrainCards);
//		ClientModelManager.getInstance().setDemoModel(demoModel);
//	}

	private void initializeMapObjects()
	{
		ClientGameModel game = (ClientGameModel) ClientModelManager.getInstance().getGame();
		Map<String, ClientCity> cityMap = new HashMap<>();

		for (ICity city : mCities)
		{
			cityMap.put(city.getId(), new ClientCity(city));
		}
		game.getGameMap().setCities(new ArrayList<>(cityMap.values()));
		List<ClientRoute> clientRoutes = new ArrayList<>();
		for (IRoute route : mRoutes)
		{
			ClientCity[] realCities = new ClientCity[2];
			for (int i = 0; i < 2; i++)
			{
				ICity city = route.getCities().get(i);
				realCities[i] = cityMap.get(city.getId());
			}
			clientRoutes.add(new ClientRoute(route, realCities));
		}
		game.getGameMap().setRoutes(clientRoutes);

		game.populateCoordinates();
	}

	private void createGameFromLobby()
	{
		ClientLobbyModel lobby = ClientModelManager.getInstance().getLobby();
		List<ClientTrainCard> clientCards = new ArrayList<>();
		for (ITrainCard card : mRevealedTrainCards)
		{
			clientCards.add(new ClientTrainCard(card));
		}
		ClientGameModel game = new ClientGameModel(ClientModelManager.getInstance().getUsername(),
				mNumDestinationCardsInDeck,
				mNumTrainCardsInDeck,
				mNumTrainsPerPlayer,
				clientCards,
				mPlayerOrder,
				mLobbyID,
				mLobbyName);
		ClientModelManager.getInstance().setGame(game);

		game.addCommand(this);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Game has started\n");
		if (mPlayerOrder != null && mPlayerOrder.size() > 0)
		{
			sb.append("Players in game are:\n");
			for (String playerName : mPlayerOrder)
			{
				sb.append(playerName + "\n");
			}
		}
		return sb.toString();
	}
}
