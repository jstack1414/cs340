package edu.byu.cs340.command.lobby;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.CreateLobbyObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;

/**
 * Created by joews on 2/5/2017.
 *
 * Command on the client side to create a noew lobby
 */
public class ClientCreateLobbyCommand extends CreateLobbyCommand
{

	public ClientCreateLobbyCommand(String username, String lobbyName)
	{
		super(username, null, lobbyName);
	}

	/**
	 * Creates the lobby models on the client side after successful operation on server.
	 *
	 * @return Retruns a CreateLobbyObsMsg if successfull, otherwise an errorObserverMessage.
	 */
	@Override
	public IMessage execute()
	{
		IMessage message = null;
		//check if the lobbyID or username are null
		if (mLobbyID != null && mUsername != null)
		{
			//create the lobby and set it as the client's lobby
			ClientLobbyModel lobby = new ClientLobbyModel(mLobbyName, mLobbyID, mUsername);
			ClientModelManager.getInstance().setLobby(lobby);
			lobby.addCommand(this);
			message = new CreateLobbyObsMsg(null);
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDCreateLobby));
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}


}
