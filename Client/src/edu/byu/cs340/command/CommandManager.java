package edu.byu.cs340.command;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.command.game.*;
import edu.byu.cs340.command.lobby.*;
import edu.byu.cs340.command.login.LoginCommand;
import edu.byu.cs340.command.login.RegisterCommand;
import edu.byu.cs340.communication.ServerProxy;
import edu.byu.cs340.message.CommandListMessage;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.messages.ErrorObsMsg;
import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.*;
import edu.byu.cs340.models.lobbyModel.ClientLobbyModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 3/16/2017.
 */
public class CommandManager
{
	//<editor-fold desc="Singleton">
	private static CommandManager mCommandManager;

	public static CommandManager getInstance()
	{
		if (mCommandManager == null)
		{
			mCommandManager = new CommandManager();
		}
		return mCommandManager;
	}
	//</editor-fold>

	public void login(String username, String password)
	{
		IBaseCommand cmd = new LoginCommand(username, password, null);
		sendCommandAndExecuteResponse(cmd);
	}

	public void register(String username, String password, String confirm_password)
	{
		IBaseCommand cmd = new RegisterCommand(username, password, null, confirm_password);
		sendCommandAndExecuteResponse(cmd);
	}

	public void signOut(String username, String lobbyID, String gameID)
	{
		IBaseCommand cmd = new SignOutCommand(username, lobbyID, gameID);
		sendCommandAndExecuteResponse(cmd);
	}

	public void pollGameList(String username)
	{
		IBaseCommand cmd = new GetGamesListCommand(username, null);
		sendCommandAndExecuteResponse(cmd);
	}

	public void joinLobby(String username, String lobbyID)
	{
		IBaseCommand cmd = new JoinLobbyCommand(username, lobbyID, null);
		sendCommandAndExecuteResponse(cmd);
	}

	public void createLobby(String username, String lobbyName)
	{
		IBaseCommand cmd = new CreateLobbyCommand(username, null, lobbyName);
		sendCommandAndExecuteResponse(cmd);
	}

	public void sendAmInGameRequest()
	{
		RoomCommandInfo info = new RoomCommandInfo(true);
		IBaseCommand cmd = new AmInGameRequest(info.mUsername);
		sendCommandAndExecuteResponse(cmd);
	}


	private class RoomCommandInfo
	{
		private String mUsername;
		private String mRoomID;
		private int mLastCommand;

		private RoomCommandInfo(boolean isLobby)
		{
			mUsername = ClientModelManager.getInstance().getUsername();
			if (isLobby)
			{
				ClientLobbyModel model = ClientModelManager.getInstance().getLobby();
				if (model != null)
				{
					mRoomID = model.getRoomID();
					mLastCommand = model.getLastCommand();
				}
			} else
			{
				IClientGameModel model = ClientModelManager.getInstance().getGame();
				if (model != null)
				{
					mRoomID = model.getRoomID();
					mLastCommand = model.getLastCommand();
				}
			}
		}
	}

	public void removePlayerFromLobby(String playerToRemove)
	{
		RoomCommandInfo info = new RoomCommandInfo(true);
		IBaseCommand cmd = new RemovePlayerFromLobbyCommand(info.mUsername, info.mLastCommand, info.mRoomID,
				playerToRemove);
		sendCommandAndExecuteResponse(cmd);
	}

	public void startGame()
	{
		RoomCommandInfo info = new RoomCommandInfo(true);
		IBaseCommand cmd = new StartGameCommand(info.mUsername, info.mLastCommand, info.mRoomID);
		sendCommandAndExecuteResponse(cmd);
	}

	public void playerReadyStatus(boolean isReady)
	{
		RoomCommandInfo info = new RoomCommandInfo(true);
		IBaseCommand cmd = new ReadyUpCommand(info.mUsername, info.mLastCommand, info.mRoomID, isReady);
		sendCommandAndExecuteResponse(cmd);
	}

	public void pollLobby()
	{
		RoomCommandInfo info = new RoomCommandInfo(true);
		if (info.mRoomID != null)
		{
			IBaseCommand cmd = new UpdateLobbyCommand(info.mUsername, info.mLastCommand, info.mRoomID);
			sendCommandAndExecuteResponse(cmd);
		}
	}

	public void pollGame()
	{
		RoomCommandInfo info = new RoomCommandInfo(false);
		if (info.mRoomID != null)
		{
			IBaseCommand cmd = new UpdateGameCommand(info.mUsername, info.mLastCommand, info.mRoomID);
			sendCommandAndExecuteResponse(cmd);
		}
	}

	public void claimRoute(String routeID, List<ClientTrainCard> cardsUsed)
	{
		List<ITrainCard> cardsToSend = new ArrayList<>();
		for (ClientTrainCard card : cardsUsed)
		{
			cardsToSend.add(new TrainCard(card));
		}
		RoomCommandInfo info = new RoomCommandInfo(false);
		IBaseCommand cmd = new ClaimRouteCommand(info.mUsername, info.mLastCommand, info.mRoomID, routeID,
				cardsToSend);
		sendCommandAndExecuteResponse(cmd);
	}

	public void drawDestinationCards()
	{
		RoomCommandInfo info = new RoomCommandInfo(false);
		IBaseCommand cmd = new DrawDestinationCardsCommand(info.mUsername, info.mLastCommand, info.mRoomID);
		sendCommandAndExecuteResponse(cmd);
	}

	public void returnDestinationCards(int minCardsToKeep, List<ClientDestinationCard> cardsToReturn)
	{
		List<IDestinationCard> cardsToReturnFormatted = new ArrayList<>();
		for (IDestinationCard card : cardsToReturn)
		{
			cardsToReturnFormatted.add(new DestinationCard(card));
		}
		RoomCommandInfo info = new RoomCommandInfo(false);
		IBaseCommand cmd = new ReturnDestinationCardsCommand(info.mUsername, info.mLastCommand, info.mRoomID,
				minCardsToKeep, cardsToReturnFormatted);
		sendCommandAndExecuteResponse(cmd);
	}

	public void drawTrainCard(int zone)
	{
		RoomCommandInfo info = new RoomCommandInfo(false);
		IBaseCommand cmd = new DrawTrainCardCommand(info.mUsername, info.mLastCommand, info.mRoomID, zone);
		sendCommandAndExecuteResponse(cmd);
	}

	public void leaveGame()
	{
		RoomCommandInfo info = new RoomCommandInfo(false);
		IBaseCommand cmd = new LeaveGameCommand(info.mUsername, info.mLastCommand, info.mRoomID);
		sendCommandAndExecuteResponse(cmd);

	}
	//<editor-fold desc="Command Execution from client facade">
	private void sendCommandAndExecuteResponse(IBaseCommand cmd)
	{
		IMessage serverResult = ServerProxy.getInstance().executeCommand(cmd);
		processMessage(serverResult);
	}

	/**
	 * Takes a message and processes it. If it is a command it will be executed, otherwise it will convert it to an
	 * ErrorObsMsg
	 *
	 * @param serverMessage Message from server to process. If it is a command it will be executed. The result of this
	 *                      will be returned.
	 * @return A list of messages for the observers
	 *
	 * @pre serverMessage is either an ErrorMessage, CommandMessage, CommandListMessage. If not a generic error message
	 * is returned.
	 * @pre class type is StringObsMsg or a subclass of it
	 * @post Returns a list of messages to send to the observers of the type classType. If the serverMessage was an
	 * Error, it sets success in the StringObsMsg result to false. If it was a command or command list it sets each
	 * observer message associated with that command(s) to true if they succeed. Fails if they have an error.
	 */
	private void processMessage(IMessage serverMessage)
	{
		List<IObsMsg> observerMessages = new ArrayList<>();
		//if the result is error add it to list, otherwise execute it
		if (serverMessage instanceof ErrorMessage)
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_CODE, ((ErrorMessage) serverMessage).getErrorCode())
					.put(EventKeys.ERROR_MESSAGE, ((ErrorMessage) serverMessage).getMessage()));
		} else if (serverMessage instanceof CommandMessage)
		{
			observerMessages.add(executeCommandMessage((CommandMessage) serverMessage));
		} else if (serverMessage instanceof CommandListMessage)
		{
			observerMessages.addAll(executeCommandListMessage((CommandListMessage) serverMessage));
		} else
		{
			observerMessages.add(new ErrorObsMsg("Result from server is of unknown type."));
		}
		ClientModelManager.getInstance().notifyObservers(observerMessages);
	}

	/**
	 * Executes each command in the commandListMessage. Adds the result of each message to a list and returns it.
	 *
	 * @param message Message containing list of commands to execute  on client
	 * @return A list of IMessage's containing SuccessMessage or ErrorMessage.
	 *
	 * @pre each command in message will only return SuccessMessage or ErrorMessage
	 * @post the result is a list of IMessage which are either error or success if executed successfully
	 */
	private List<IObsMsg> executeCommandListMessage(CommandListMessage message)
	{
		List<IObsMsg> result = new ArrayList<>();
		List<String> serializedCommands = (message).getSerializedCommands();
		for (String serializedCommand : serializedCommands)
		{
			try
			{
				IBaseCommand clientCommand = SerDes.deserializeCommand(serializedCommand, SerDes.Source.CLIENT);
				result.add((IObsMsg) executeCommand(clientCommand));
			} catch (ClassNotFoundException e)
			{
				ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
						.put(EventKeys.ERROR_CODE, (ErrorMessage.MEDIUM_ERROR))
						.put(EventKeys.ERROR_MESSAGE, ("Could not deserialize command from message.")));
			}
		}
		return result;
	}

	/**
	 * Executes the command contained in message.
	 *
	 * @param message CommandMessage containing command to execute
	 * @return Returns an IMessage either SuccessMessage or ErrorMessage depending on if it worked.
	 *
	 * @pre the command in message is not null
	 * @pre the command in message returns SuccessMessage or ErrorMessage
	 * @post the resultant IMessage is either SuccessMessage or ErrorMessage
	 */
	private IObsMsg executeCommandMessage(CommandMessage message)
	{
		IObsMsg result = null;
		String serializedCommand = message.getCmd();
		try
		{
			IBaseCommand clientCommand = SerDes.deserializeCommand(serializedCommand, SerDes.Source.CLIENT);
			result = (IObsMsg) executeCommand(clientCommand);
		} catch (ClassNotFoundException e)
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_CODE, (ErrorMessage.MEDIUM_ERROR))
					.put(EventKeys.ERROR_MESSAGE, ("Could not deserialize command from message.")));
		}
		return result;
	}

	/**
	 * Executes the command on the client side.
	 *
	 * @param baseCommand Command to be executed
	 * @return Returns the message of the command that is executed.
	 *
	 * @pre The baseCommand must be a client version of a command
	 * @pre the baseCommand only returns SuccessMessage or ErrorMessage
	 * @post The command is executed then some instantiation of an IObserverMessage will be passed back
	 * @post if an error occurred during execution a ErrorMessage will be sent back
	 */
	private IMessage executeCommand(IBaseCommand baseCommand)
	{
		try
		{
			return baseCommand.execute();
		} catch (IBaseCommand.CommandException e)
		{

			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_CODE, e.getErrorCode())
					.put(EventKeys.ERROR_MESSAGE, e.getMessage()));
		}
		return null;
	}


	//</editor-fold>

	//TODO make a GameCommandInfo class to store username, lastCommand, and gameID
}
