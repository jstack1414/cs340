package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public class ClientDrawDestinationCardsCommand extends DrawDestinationCardsCommand
{
	public ClientDrawDestinationCardsCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
	}

	public ClientDrawDestinationCardsCommand(DrawDestinationCardsCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage message = null;
		//check if the lobbyID or username are null
		if (mGameID != null && mUsername != null)
		{
			IClientGameModel game = ClientModelManager.getInstance().getGame();
			int minToKeep = mTemporaryCards.size() - mMaxCardsToReturn;
			List<ClientDestinationCard> cards = new ArrayList<>();
			for (IDestinationCard card : mTemporaryCards)
			{
				cards.add(new ClientDestinationCard(card));
			}
			game.addTemporaryDestinationCards(cards, minToKeep);
			int newDestDeckSize = Math.max(game.getBank().getDestinationCardDeckSize() - mTemporaryCards.size(), 0);
			game.updateDestCardDeckSize(newDestDeckSize);
//			message = new DestinationCardDrawnObsMsg("Draw Successful",
//					minToKeep, false);
			game.addCommand(this);

			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDDrawDestinationCards));
		} else
		{
			ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDErrorMessage)
					.put(EventKeys.ERROR_MESSAGE, "Result from server contained null data")
					.put(EventKeys.ERROR_CODE, ErrorMessage.MEDIUM_ERROR));
		}
		return message;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(mUsername).append(" drew 3 destination cards.\n");
		if (mTemporaryCards != null && mTemporaryCards.size() > 0)
		{
			sb.append("Choose atleast ").append(mTemporaryCards.size() - mMaxCardsToReturn).append(" cards to keep" +
					".\n");
			for (IDestinationCard card : mTemporaryCards)
			{
				sb.append("From ").append(card.getCities().get(0).getName()).append(" to ").append(card.getCities()
						.get(1).getName()).append("\n");
			}
		}
		return sb.toString();
	}
}
