package edu.byu.cs340.command.game;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.gameModel.IClientGameModel;

/**
 * Created by Jenessa Welker on 3/25/2017.
 * 
 * The command for telling the client the game is over and supplying data for the associated screen.
 * 
 */
public class ClientGameOverCommand extends GameOverCommand {
	/**
	 * Constructor uses super
	 * @param username the username of the current player
	 * @param lastCommand the command the client had received before this one
	 * @param gameID the id of the game this command belongs to
	 */
	public ClientGameOverCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	/**
	 * Copy constructor, calls super
	 * @param that the command to copy
	 */
	public ClientGameOverCommand(ClientGameOverCommand that)
	{
		super(that);
	}

	/**
	 * Tells the state the game is over so it can switch to that state and trigger the appropriate screen.
	 * @return null
	 * @throws CommandException if there is an error processing
	 */
	@Override
	public IMessage execute() throws CommandException {
		IClientGameModel game = ClientModelManager.getInstance().getGame();
		ClientModelManager.getInstance().processStateEvent(new Event(EventType.CMDGameOver));
		game.updateGameOverData(mData);
		game.addCommand(this);
		return null;
	}
}
