package edu.byu.cs340.dataGeneration;

import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.TrainCard;
import edu.byu.cs340.models.gameModel.map.City;
import edu.byu.cs340.models.gameModel.map.Route;
import edu.byu.cs340.models.gameModel.map.TrainType;

/**
 * Created by Sean on 3/4/2017.
 */
public class ClientDataObjectCreationMain {
	//This file contains the information to create te client text file for importing city cordinates.
	private static final String PITTSBURGH = "Pittsburgh";
	private static final String VANCOUVER = "Vancouver";
	private static final String SEATTLE = "Seattle";
	private static final String PORTLAND = "Portland";
	private static final String SAN_FRANCISCO = "San Francisco";
	private static final String LOS_ANGLES = "Los Angeles";
	private static final String CALGARY = "Calgary";
	private static final String HELENA = "Helena";
	private static final String SALT_LAKE_CITY = "Salt Lake City";
	private static final String LAS_VEGAS = "Las Vegas";
	private static final String PHOENIX = "Phoenix";
	private static final String DENVER = "Denver";
	private static final String SANTA_FE = "Santa Fe";
	private static final String EL_PASO = "El Paso";
	private static final String WINNIPEG = "Winnipeg";
	private static final String DULUTH = "Duluth";
	private static final String OMAHA = "Omaha";
	private static final String KANSAS_CITY = "Kansas City";
	private static final String OKLAHOMA_CITY = "Oklahoma City";
	private static final String DALLAS = "Dallas";
	private static final String HOUSTON = "Houston";
	private static final String LITTLE_ROCK = "Little Rock";
	private static final String SAINT_LOUIS = "Saint Louis";
	private static final String CHICAGO = "Chicago";
	private static final String SAULT_ST_MARIE = "Sault St. Marie";
	private static final String NEW_ORLEANS = "New Orleans";
	private static final String ATLANTA = "Atlanta";
	private static final String NASHVILLE = "Nashville";
	private static final String TORONTO = "Toronto";
	private static final String MONTREAL = "Montreal";
	private static final String BOSTON = "Boston";
	private static final String WASHINGTON = "Washington";
	private static final String NEW_YORK = "New York";
	private static final String RALEIGH = "Raleigh";
	private static final String CHARLESTON = "Charleston";
	private static final String MIAMI = "Miami";
	
	public static void main(String[] args)
	{
		ClientGameObjectFileParser useMe = new ClientGameObjectFileParser(GameObjectFileParser.FILE_PATH);
		/*Create Train Cards*/
		//add 12 of each train except locomotive
		for (int i = 0; i < 12; i++)
		{
			for (int j = 0; j < TrainType.values().length - 2; j++)
			{
				useMe.addTrainCards(new TrainCard(TrainType.values()[j]));
			}
		}
		//add 14 locomotives
		for (int i = 0; i < 14; i++)
		{
			useMe.addTrainCards(new TrainCard(TrainType.Locomotive));
		}


		/*Create Cities*/
		useMe.addCities(new City(VANCOUVER));
		useMe.addCities(new City(SEATTLE));
		useMe.addCities(new City(PORTLAND));
		useMe.addCities(new City(SAN_FRANCISCO));
		useMe.addCities(new City(LOS_ANGLES));
		useMe.addCities(new City(CALGARY));
		useMe.addCities(new City(HELENA));
		useMe.addCities(new City(SALT_LAKE_CITY));
		useMe.addCities(new City(LAS_VEGAS));
		useMe.addCities(new City(PHOENIX));
		useMe.addCities(new City(DENVER));
		useMe.addCities(new City(SANTA_FE));
		useMe.addCities(new City(EL_PASO));
		useMe.addCities(new City(WINNIPEG));
		useMe.addCities(new City(DULUTH));
		useMe.addCities(new City(OMAHA));
		useMe.addCities(new City(KANSAS_CITY));
		useMe.addCities(new City(OKLAHOMA_CITY));
		useMe.addCities(new City(DALLAS));
		useMe.addCities(new City(HOUSTON));
		useMe.addCities(new City(LITTLE_ROCK));
		useMe.addCities(new City(SAINT_LOUIS));
		useMe.addCities(new City(CHICAGO));
		useMe.addCities(new City(SAULT_ST_MARIE));
		useMe.addCities(new City(NEW_ORLEANS));
		useMe.addCities(new City(ATLANTA));
		useMe.addCities(new City(NASHVILLE));
		useMe.addCities(new City(TORONTO));
		useMe.addCities(new City(PITTSBURGH));
		useMe.addCities(new City(MONTREAL));
		useMe.addCities(new City(BOSTON));
		useMe.addCities(new City(NEW_YORK));
		useMe.addCities(new City(WASHINGTON));
		useMe.addCities(new City(RALEIGH));
		useMe.addCities(new City(CHARLESTON));
		useMe.addCities(new City(MIAMI));

		Route pairedRoute1 = new Route(useMe.getCityPair(VANCOUVER, SEATTLE), TrainType.None, 1, null);
		Route pairedRoute2 = new Route(useMe.getCityPair(VANCOUVER, SEATTLE), TrainType.None, 1, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(PORTLAND, SEATTLE), TrainType.None, 1, null);
		pairedRoute2 = new Route(useMe.getCityPair(PORTLAND, SEATTLE), TrainType.None, 1, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(PORTLAND, SAN_FRANCISCO), TrainType.Caboose, 5, null);
		pairedRoute2 = new Route(useMe.getCityPair(PORTLAND, SAN_FRANCISCO), TrainType.Box, 5, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(LOS_ANGLES, SAN_FRANCISCO), TrainType.Reefer, 3, null);
		pairedRoute2 = new Route(useMe.getCityPair(LOS_ANGLES, SAN_FRANCISCO), TrainType.Box, 3, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(LOS_ANGLES, LAS_VEGAS), TrainType.None, 2, null));

		pairedRoute1 = new Route(useMe.getCityPair(SAN_FRANCISCO, SALT_LAKE_CITY), TrainType.Freight, 5, null);
		pairedRoute2 = new Route(useMe.getCityPair(SAN_FRANCISCO, SALT_LAKE_CITY), TrainType.Passenger, 5, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(PORTLAND, SALT_LAKE_CITY), TrainType.Tanker, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(LAS_VEGAS, SALT_LAKE_CITY), TrainType.Freight, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SEATTLE, HELENA), TrainType.Reefer, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(VANCOUVER, CALGARY), TrainType.None, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SEATTLE, CALGARY), TrainType.None, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(WINNIPEG, CALGARY), TrainType.Passenger, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HELENA, CALGARY), TrainType.None, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HELENA, WINNIPEG), TrainType.Tanker, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HELENA, SALT_LAKE_CITY), TrainType.Box, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HELENA, DENVER), TrainType.Caboose, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HELENA, DULUTH), TrainType.Freight, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(WINNIPEG, DULUTH), TrainType.Hopper, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HELENA, OMAHA), TrainType.Coal, 5, null));

		pairedRoute1 = new Route(useMe.getCityPair(SALT_LAKE_CITY, DENVER), TrainType.Reefer, 3, null);
		pairedRoute2 = new Route(useMe.getCityPair(SALT_LAKE_CITY, DENVER), TrainType.Coal, 3, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(LOS_ANGLES, PHOENIX), TrainType.None, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(LOS_ANGLES, EL_PASO), TrainType.Hopper, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(PHOENIX, DENVER), TrainType.Passenger, 5, null));

		useMe.addRoutes(new Route(useMe.getCityPair(PHOENIX, SANTA_FE), TrainType.None, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(PHOENIX, EL_PASO), TrainType.None, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SANTA_FE, EL_PASO), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SANTA_FE, DENVER), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SANTA_FE, OKLAHOMA_CITY), TrainType.Tanker, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(OKLAHOMA_CITY, DENVER), TrainType.Coal, 4, null));

		pairedRoute1 = new Route(useMe.getCityPair(KANSAS_CITY, DENVER), TrainType.Hopper, 4, null);
		pairedRoute2 = new Route(useMe.getCityPair(KANSAS_CITY, DENVER), TrainType.Freight, 4, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(OMAHA, DENVER), TrainType.Box, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(EL_PASO, HOUSTON), TrainType.Caboose, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(EL_PASO, DALLAS), TrainType.Coal, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(EL_PASO, OKLAHOMA_CITY), TrainType.Reefer, 5, null));

		useMe.addRoutes(new Route(useMe.getCityPair(HOUSTON, NEW_ORLEANS), TrainType.None, 2, null));

		pairedRoute1 = new Route(useMe.getCityPair(DALLAS, HOUSTON), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(DALLAS, HOUSTON), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(DALLAS, OKLAHOMA_CITY), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(DALLAS, OKLAHOMA_CITY), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(DALLAS, LITTLE_ROCK), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(OKLAHOMA_CITY, LITTLE_ROCK), TrainType.None, 2, null));

		pairedRoute1 = new Route(useMe.getCityPair(OKLAHOMA_CITY, KANSAS_CITY), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(OKLAHOMA_CITY, KANSAS_CITY), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(KANSAS_CITY, SAINT_LOUIS), TrainType.Box, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(KANSAS_CITY, SAINT_LOUIS), TrainType.Tanker, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(LITTLE_ROCK, SAINT_LOUIS), TrainType.None, 2, null));

		pairedRoute1 = new Route(useMe.getCityPair(OKLAHOMA_CITY, OMAHA), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(OKLAHOMA_CITY, OMAHA), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(OMAHA, DULUTH), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(OMAHA, DULUTH), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(OMAHA, CHICAGO), TrainType.Tanker, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(DULUTH, CHICAGO), TrainType.Coal, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(WINNIPEG, SAULT_ST_MARIE), TrainType.None, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(DULUTH, SAULT_ST_MARIE), TrainType.None, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SAULT_ST_MARIE, TORONTO), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(DULUTH, TORONTO), TrainType.Box, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(TORONTO, CHICAGO), TrainType.Passenger, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(TORONTO, PITTSBURGH), TrainType.None, 2, null));

		pairedRoute1 = new Route(useMe.getCityPair(SAINT_LOUIS, CHICAGO), TrainType.Passenger, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(SAINT_LOUIS, CHICAGO), TrainType.Caboose, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(PITTSBURGH, CHICAGO), TrainType.Hopper, 3, null);
		pairedRoute2 = new Route(useMe.getCityPair(PITTSBURGH, CHICAGO), TrainType.Freight, 3, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(SAINT_LOUIS, PITTSBURGH), TrainType.Caboose, 5, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SAINT_LOUIS, NASHVILLE), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(ATLANTA, NASHVILLE), TrainType.Caboose, 1, null));

		useMe.addRoutes(new Route(useMe.getCityPair(SAULT_ST_MARIE, MONTREAL), TrainType.Hopper, 5, null));

		useMe.addRoutes(new Route(useMe.getCityPair(TORONTO, MONTREAL), TrainType.None, 3, null));

		pairedRoute1 = new Route(useMe.getCityPair(BOSTON, MONTREAL), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(BOSTON, MONTREAL), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(NEW_YORK, MONTREAL), TrainType.Tanker, 3, null));

		pairedRoute1 = new Route(useMe.getCityPair(BOSTON, NEW_YORK), TrainType.Coal, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(BOSTON, NEW_YORK), TrainType.Reefer, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(PITTSBURGH, NEW_YORK), TrainType.Caboose, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(PITTSBURGH, NEW_YORK), TrainType.Passenger, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(NEW_YORK, WASHINGTON), TrainType.Hopper, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(NEW_YORK, WASHINGTON), TrainType.Freight, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(PITTSBURGH, WASHINGTON), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(PITTSBURGH, RALEIGH), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(PITTSBURGH, NASHVILLE), TrainType.Reefer, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(NASHVILLE, RALEIGH), TrainType.Hopper, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(RALEIGH, CHARLESTON), TrainType.None, 2, null));

		useMe.addRoutes(new Route(useMe.getCityPair(LITTLE_ROCK, NEW_ORLEANS), TrainType.Caboose, 3, null));

		pairedRoute1 = new Route(useMe.getCityPair(ATLANTA, NEW_ORLEANS), TrainType.Reefer, 4, null);
		pairedRoute2 = new Route(useMe.getCityPair(ATLANTA, NEW_ORLEANS), TrainType.Freight, 4, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		useMe.addRoutes(new Route(useMe.getCityPair(LITTLE_ROCK, NASHVILLE), TrainType.Passenger, 3, null));

		useMe.addRoutes(new Route(useMe.getCityPair(MIAMI, NEW_ORLEANS), TrainType.Coal, 6, null));

		useMe.addRoutes(new Route(useMe.getCityPair(ATLANTA, MIAMI), TrainType.Tanker, 5, null));

		useMe.addRoutes(new Route(useMe.getCityPair(CHARLESTON, MIAMI), TrainType.Box, 4, null));

		useMe.addRoutes(new Route(useMe.getCityPair(CHARLESTON, ATLANTA), TrainType.None, 2, null));

		pairedRoute1 = new Route(useMe.getCityPair(WASHINGTON, RALEIGH), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(WASHINGTON, RALEIGH), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		pairedRoute1 = new Route(useMe.getCityPair(RALEIGH, ATLANTA), TrainType.None, 2, null);
		pairedRoute2 = new Route(useMe.getCityPair(RALEIGH, ATLANTA), TrainType.None, 2, pairedRoute1.getId());
		pairedRoute1.setPairedRouteID(pairedRoute2.getId());
		useMe.addRoutes(pairedRoute1);
		useMe.addRoutes(pairedRoute2);

		/*create destination cards*/
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(LOS_ANGLES, NEW_YORK), 21));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(DULUTH, HOUSTON), 8));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(SAULT_ST_MARIE, NASHVILLE), 8));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(NEW_YORK, ATLANTA), 6));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(PORTLAND, NASHVILLE), 17));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(VANCOUVER, MONTREAL), 10));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(DULUTH, EL_PASO), 10));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(TORONTO, MIAMI), 10));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(PORTLAND, PHOENIX), 11));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(DALLAS, NEW_YORK), 11));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(CALGARY, SALT_LAKE_CITY), 7));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(CALGARY, PHOENIX), 13));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(LOS_ANGLES, MIAMI), 20));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(WINNIPEG, LITTLE_ROCK), 11));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(SAN_FRANCISCO, ATLANTA), 17));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(KANSAS_CITY, HOUSTON), 5));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(LOS_ANGLES, CHICAGO), 16));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(DENVER, PITTSBURGH), 11));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(CHICAGO, SANTA_FE), 9));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(VANCOUVER, SANTA_FE), 13));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(BOSTON, MIAMI), 12));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(CHICAGO, NEW_ORLEANS), 7));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(MONTREAL, ATLANTA), 9));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(SEATTLE, NEW_YORK), 22));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(DENVER, EL_PASO), 4));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(HELENA, LOS_ANGLES), 8));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(WINNIPEG, HOUSTON), 12));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(MONTREAL, NEW_ORLEANS), 13));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(SAULT_ST_MARIE, OKLAHOMA_CITY), 9));
		useMe.addDestinationCards(new DestinationCard(useMe.getCityPair(SEATTLE, LOS_ANGLES), 9));


		useMe.writeToFile();
		
//		useMe = new ClientGameObjectFileParser(GameObjectFileParser.FILE_PATH);
//		useMe.readInFile();
	}
}
