package edu.byu.cs340.view;

import edu.byu.cs340.Utils;
import edu.byu.cs340.view.game.ChatCommandPopupView;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * TODO: Create functions to create popup JFrames
 *
 * This is used to handle the window.
 */
public class WindowManager implements IWindowManager
{
	// Window Manager fields
	private static WindowManager sInstance; // For singleton pattern
	private static final Dimension WINDOW_SIZE = new Dimension(900, 750); // Dimensions of the window
	private JFrame mMainWindowFrame; // The object representing the window
	private JPanel mContentPane; // The current view of the window
	private Map<String, popUpPair> mPopups;


// public static methods

	/**
	 * Singleton pattern: Required pattern
	 * If no instance of WindowManager has been created, it will call the private constructor and return the new
	 * instance. Otherwise it will return the existing instance.
	 *
	 * @return an instance of IWindowManager
	 */
	public static IWindowManager getInstance()
	{
		if (sInstance == null)
			sInstance = new WindowManager();

		return sInstance;
	}

// constructors

	/**
	 * Creates a Window Manager with the look and feel of the system. This will also create the window.
	 */
	private WindowManager()
	{
//
		try
		{
			// The following causes swing to adopt the look and feel of the operating system.
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
				UnsupportedLookAndFeelException e)
		{
			e.printStackTrace();
		}
		turnOffFocusBorders();
		mPopups = new HashMap<>();

		javax.swing.SwingUtilities.invokeLater(this::createGUI);
	}

	private void turnOffFocusBorders()
	{//PREVENT FOCUS BORDERS
		// Removes the dotted border around controls which is not consistent with Windows
		UIManager.put("Button.focus", new ColorUIResource(new Color(0, 0, 0, 0)));
		UIManager.put("ToggleButton.focus", new ColorUIResource(new Color(0, 0, 0, 0)));

		// ways to remove it from other controls...
		UIManager.put("CheckBox.focus", new ColorUIResource(new Color(0, 0, 0, 0)));
		UIManager.put("TabbedPane.focus", new ColorUIResource(new Color(0, 0, 0, 0)));
		UIManager.put("RadioButton.focus", new ColorUIResource(new Color(0, 0, 0, 0)));
		UIManager.put("Slider.focus", new ColorUIResource(new Color(0, 0, 0, 0)));
		UIManager.put("JTable.focus", new ColorUIResource(new Color(0, 0, 0, 0)));

		// figure out combobox
		UIManager.put("ComboBox.focus", new ColorUIResource(new Color(0, 0, 0, 0)));
	}

// public methods

	/**
	 * mainView is the mainView we are changing to. to get the actual JPanel we want to put in the window call mainView
	 * .getJPanel().
	 * This is important because MainView's make by the GUi builder do not have the root JPanel as the MainView
	 * itself. Instead they will return the appropriate JPanel.
	 *
	 * @param mainView The new mainView to display
	 */
	@Override
	public final void changeViewTo(MainView mainView)
	{
		javax.swing.SwingUtilities.invokeLater(() ->
				{
					if (mContentPane != null)
					{
						mainView.setSize(mContentPane.getSize());
						mContentPane.setPreferredSize(mContentPane.getSize());
					}
					mContentPane = mainView.getJPanel();
					mMainWindowFrame.setContentPane(mContentPane);
					mMainWindowFrame.revalidate();
					mMainWindowFrame.repaint();
				}
		);
	}

	@Override
	public final void changeMenuBar(JMenuBar menuBar)
	{
		javax.swing.SwingUtilities.invokeLater(() ->
				{
					mMainWindowFrame.setJMenuBar(menuBar);
					mMainWindowFrame.revalidate();
				}
		);
	}

	public Dimension getMainWindowSize()
	{
		return mMainWindowFrame.getSize();
	}

	public Point getMainWindowLocal()
	{
		return mMainWindowFrame.getLocationOnScreen();
	}

	public Dimension getPopupSize(View v)
	{
		return mPopups.get(v.getViewName()).mJframe.getSize();
	}

	public Point getPopupLocation(View v)
	{
		return mPopups.get(v.getViewName()).mJframe.getLocationOnScreen();
	}

// private methods

	/**
	 * Creates the window
	 */
	private void createGUI()
	{
		mMainWindowFrame = new JFrame(Utils.GAME_TITLE);
		mMainWindowFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		//mMainWindowFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		mMainWindowFrame.setMinimumSize(WINDOW_SIZE);
		mMainWindowFrame.pack();
		mMainWindowFrame.setVisible(true);
	}

	@Override
	public void launchNewPopup(PopupView view)
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			launchNewPopupHelper(view);
		} else
		{
			javax.swing.SwingUtilities.invokeLater(() ->
			{
				launchNewPopupHelper(view);
			});
		}
	}

	private void launchNewPopupHelper(PopupView view)
	{

		if (view != null)
		{
			String name = view.getViewName();
			if (name != null && !name.isEmpty())
			{
				//check if the mPopups has the mainView
				if (mPopups.containsKey(name))
				{
					mPopups.get(name).launch();
					mPopups.get(name).mJframe.setVisible(true);
				} else
				{
					popUpPair pp = new popUpPair(view);
					pp.launch();
					mPopups.put(name, pp);
					mPopups.get(name).mJframe.setVisible(true);
				}
			}
		}
	}

	@Override
	public void forceClosePopup(PopupView view)
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			closePopupHelper(view);
		} else
		{
			try
			{
				SwingUtilities.invokeAndWait(() ->
				{
					closePopupHelper(view);
				});
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			} catch (InvocationTargetException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void closePopup(PopupView view)
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			closePopupHelper(view);
		} else
		{
			javax.swing.SwingUtilities.invokeLater(() ->
			{
				closePopupHelper(view);
			});
		}
	}

	private void closePopupHelper(PopupView view)
	{
		if (view != null)
		{
			String name = view.getViewName();
			if (name != null)
			{
				popUpPair pp = mPopups.get(name);
				if (pp != null)
				{
					pp.close();
					mPopups.remove(name);
				}
			}
		}
	}


	/**
	 * Object for holding popup views and their jframe
	 */
	private class popUpPair
	{
		//private final Dimension WINDOW_SIZE = new Dimension(400, 300); // Dimensions of the window
		JFrame mJframe;
		PopupView mPopupView;
		String name;
		JPanel mPopupContentPane; // The current view of the window

		/**
		 * Constructor taking in the jpanel and the simple name the name of the view object
		 *
		 * @param view View to display in new
		 */
		public popUpPair(PopupView view)
		{
			mPopupView = view;
			this.name = view.getViewName();
		}

		/**
		 * Creates the popup window and sets name
		 */
		private void createGUI()
		{
			mJframe = new JFrame(this.name);
			if (mPopupView.getViewName().equals(ChatCommandPopupView.class.getSimpleName()))
			{
				mJframe.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
			} else
			{
				mJframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			}
			mJframe.setMinimumSize(mPopupView.getMinSize());
			mJframe.pack();
			mJframe.setVisible(true);
		}

		/**
		 * Closes the popup window if it isn't already closed
		 */
		public void close()
		{
			if (mJframe != null)
			{
				if (mJframe.isDisplayable())
				{
					mJframe.dispose();
				}
			}
			mPopupView = null;
			mJframe = null;
			mPopupContentPane = null;

		}

		/**
		 * Launches the popup window and changes the view to the popup jpanel
		 */
		public void launch()
		{
			if (mJframe != null)
			{
				if (mJframe.isDisplayable())
				{
					if (mPopupView.getJPanel().isDisplayable())
					{
						//already launched
						mJframe.toFront();
						mJframe.repaint();
					} else
					{
						changeViewTo();
					}
				} else
				{
					createGUI();
					changeViewTo();
				}
			} else
			{
				createGUI();
				changeViewTo();
			}
		}

		/**
		 * Changes the view in the jframe to the jpanel
		 */
		private void changeViewTo()
		{

			if (mPopupContentPane != null)
			{
				mPopupView.getJPanel().setSize(mPopupContentPane.getSize());
				mPopupContentPane.setPreferredSize(mPopupContentPane.getSize());
			}
			mPopupContentPane = mPopupView.getJPanel();
			mJframe.setContentPane(mPopupContentPane);
			if (mPopupView.hasSavedLocalSize())
			{
				mJframe.setSize(mPopupView.getSavedSize());
				mJframe.setLocation(mPopupView.getSavedLocal());
			} else
			{
				mJframe.setSize(mPopupContentPane.getSize());
			}
			mJframe.revalidate();
			mJframe.repaint();
		}

	}
}
