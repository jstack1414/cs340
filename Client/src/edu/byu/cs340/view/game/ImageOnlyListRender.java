package edu.byu.cs340.view.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joews on 3/7/2017.
 */
public class ImageOnlyListRender extends DefaultListCellRenderer
{
	private Map<String, ImageIcon> imageMap;
	int xScale;
	int yScale;
	private ActionListener mSelectedListner;

	public ImageOnlyListRender(int xScale, int yScale)
	{
		this.xScale = xScale;
		this.yScale = yScale;
	}

	@Override
	public Component getListCellRendererComponent(
			JList list, Object value, int index,
			boolean isSelected, boolean cellHasFocus)
	{

		JLabel label = (JLabel) super.getListCellRendererComponent(
				list, value, index, isSelected, cellHasFocus);
		if (value != null)
		{
			Image image = imageMap.get(value).getImage(); // transform it
			Image newimg = image.getScaledInstance(xScale, yScale, java.awt.Image.SCALE_SMOOTH); //todo: scale it to
			// better size
			Icon imageIcon = new ImageIcon(newimg);  // transform it back
			label.setIcon(imageIcon);
		}
		label.setText("");
//		label.setHorizontalAlignment(CENTER);
//		label.setVerticalAlignment(CENTER);
		return label;
	}
	public void createImageMap(String[] list)
	{
		imageMap = new HashMap<>();
		for (String s : list)
		{
			if (s != null)
			{
//				System.out.println(s);
				imageMap.put(s, new ImageIcon(
						getClass().getResource(s)));
			}
		}
	}
}
