package edu.byu.cs340.view.game;

import edu.byu.cs340.view.IView;

/**
 * Created by Rebekah on 2/24/2017.
 */
public interface IChatCommandPopupView extends IView {

	//NEVER USED
//    /**
//     * this takes in the data that will be displayed to the player
//     * @param messages the messages that will be displayed
//     */
//    public void displayTextHistory(List<ChatMessage> messages);

	//never used
//    void updateTextDisplay();

	/**
	 * Push a chat message to the view
	 *
	 * @param username The user ascoaited with hte mesage
	 * @param message  The message
	 */
	void pushChatMessage(String username, String message);

	/**
	 * add command to the command list
	 *
	 * @param cmdToString
	 */
	void pushCmd(String cmdToString);


	void clearView();
}
