package edu.byu.cs340.view.game.gameBoardView;

import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.map.TrainType;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TrainCardHandPanel extends JPanel
{
	private Image mCardImage;
	private TrainType mType;
	private List<ClientTrainCard> mCards;
	private int mSelected;
	private int mYPix = 90;
	private int mXPix = (int) (mYPix * 2.0 / 3);
	private int NUM_X = (int) (mXPix * .55);
	private int NUM_Y = mYPix - (mXPix - NUM_X);
	private int NUM_DIAMETER = (int) (mXPix * .35);
	private int NAM_DIAMETER_BUFFER = 4;
	//	private int SELECTED_DIAMETER = (int) (mXPix * .4);
	private int SELECTED_X = 0;//(int) (mXPix * .5 - SELECTED_DIAMETER / 2);
	private int SELECTED_Y = 0;//(int) (mYPix * .5 - SELECTED_DIAMETER / 2);
	private boolean mAllowSelection = false;
	private boolean mLowLight = false;
	private SelectionListener mSelectionListener;

	public interface SelectionListener
	{
		void onSeleciton(boolean selection);
	}

	TrainCardHandPanel(TrainType type, SelectionListener listener)
	{
		mType = type;
		mSelected = 0;
		mCards = new ArrayList<>();
		this.mSelectionListener = listener;
		this.setPreferredSize(new Dimension(mXPix, mYPix));
		this.setMaximumSize(new Dimension(mXPix, mYPix));
		this.setMinimumSize(new Dimension(mXPix, mYPix));
		this.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.setAlignmentY(Component.CENTER_ALIGNMENT);
		this.setOpaque(false);
		this.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				super.mouseClicked(e);
				onClick(e);
			}
		});
		try
		{
			mCardImage = ImageIO.read(getClass().getResourceAsStream(mType.getRotatedImage()));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void addCard(ClientTrainCard card)
	{
		mCards.add(card);
	}


	public void removeCard(ClientTrainCard card)
	{
		mCards.remove(card);
	}


	public int getCount()
	{
		return mCards.size();
	}

	public int getNumSelected()
	{
		return mSelected;
	}

	public void increaseSelected()
	{
		if (mSelected < mCards.size())
		{
			mSelected++;
			this.repaint();
		}
		this.repaint();
		if (mSelectionListener != null)
		{
			mSelectionListener.onSeleciton(true);
		}
	}

	public void decreaseSelected()
	{
		if (mSelected > 0)
		{
			mSelected--;
			this.repaint();
		}
		this.repaint();

		if (mSelectionListener != null)
		{
			mSelectionListener.onSeleciton(false);
		}
	}

	public void clearSelected()
	{
		mSelected = 0;
	}

	public void setAllowSelection(boolean allowSelection)
	{
		mAllowSelection = allowSelection;
		if (!mAllowSelection)
		{
			clearSelected();
		}
	}

	public void setLowLight(boolean lowLight)
	{
		mLowLight = lowLight;
		if (mLowLight)
		{
			mAllowSelection = false;
			clearSelected();
		}
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawImage(mCardImage, 0, 0, mXPix, mYPix, null);

		g2d.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		//draw faintly the number of cards selected
		if (mAllowSelection && mSelected > 0)
		{
			//draw inner circle
			g2d.setColor(new Color(240, 240, 240, 150));
//			g2d.fillOval(SELECTED_X, SELECTED_Y, SELECTED_DIAMETER, SELECTED_DIAMETER);
			g2d.fillRect(SELECTED_X, SELECTED_Y, mXPix, mYPix);

			//draw string in middle
			String num = String.valueOf(mSelected);
			g2d.setColor(new Color(0, 0, 0, 200));
			g2d.setFont(new Font(Font.DIALOG, Font.BOLD, 30));
			Font font = g2d.getFont();
			FontMetrics metrics = g.getFontMetrics(font);
			int x = SELECTED_X + (mXPix - metrics.stringWidth(num)) / 2;
			int y = (SELECTED_Y + (mYPix - metrics.getHeight()) / 2) + metrics.getAscent();
//			int x = SELECTED_X + (SELECTED_DIAMETER - metrics.stringWidth(num)) / 2 ;
//			int y = (SELECTED_Y + (SELECTED_DIAMETER- metrics.getHeight()) / 2)+ metrics.getAscent();
			g.drawString(num, x, y);
		} else if (mLowLight)
		{
			g2d.setColor(new Color(55, 55, 55, 150));
			g2d.fillRect(SELECTED_X, SELECTED_Y, mXPix, mYPix);
		}
		//draw circle in corner if more then one...
		if (getCount() > 1)
		{
			//draw outer circle
			g2d.setColor(Color.BLACK);
			g2d.fillOval(NUM_X - NAM_DIAMETER_BUFFER / 2, NUM_Y - NAM_DIAMETER_BUFFER / 2, NUM_DIAMETER +
					NAM_DIAMETER_BUFFER, NUM_DIAMETER + NAM_DIAMETER_BUFFER);

			//draw inner circle
			g2d.setColor(new Color(255, 255, 255, 240));
			g2d.fillOval(NUM_X, NUM_Y, NUM_DIAMETER, NUM_DIAMETER);

			//draw string in middle
			String num = String.valueOf(getCount());
			g2d.setColor(Color.BLACK);
			g2d.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
			Font font = g2d.getFont();
			FontMetrics metrics = g.getFontMetrics(font);
			int x = NUM_X + (NUM_DIAMETER - metrics.stringWidth(num)) / 2;
			int y = (NUM_Y + (NUM_DIAMETER - metrics.getHeight()) / 2) + metrics.getAscent();
			g.drawString(num, x, y);
		}


	}


	public List<String> getSelected()
	{
		List<String> result = new ArrayList<>();
		for (int i = 0; i < mSelected; i++)
		{
			result.add(mCards.get(i).getId());
		}
		return result;
	}


	public List<ClientTrainCard> getCards()
	{
		return Collections.unmodifiableList(mCards);
	}

	public boolean hasCard(ClientTrainCard card)
	{
		return mCards.contains(card);
	}


	private void onClick(MouseEvent e)
	{
		if (mAllowSelection)
		{
			if (SwingUtilities.isLeftMouseButton(e))
			{
				if (e.isShiftDown() || e.isControlDown())
				{
					decreaseSelected();
				} else
				{
					increaseSelected();
				}
			} else if (SwingUtilities.isRightMouseButton(e))
			{
				decreaseSelected();
			}
		}

	}


}