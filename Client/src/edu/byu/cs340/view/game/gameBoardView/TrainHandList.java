package edu.byu.cs340.view.game.gameBoardView;

import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.map.TrainType;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by joews on 3/10/2017.
 */
public class TrainHandList extends JPanel
{
	private JPanel mInnerPanel;
	private JButton acceptButton;
	private Map<TrainType, TrainCardHandPanel> mCardPanelMap;
	private List<ClientTrainCard> mClientTrainCards;
	private TrainType mTypeSelectionFilter = TrainType.None;
	private TrainType mRequiredTypeSelectionFilter = TrainType.None;
	private int mNumToSelect = 0;
	private TrainCardsSelectedListener mTrainCardsSelectedListener;

	public void clearView()
	{
		mCardPanelMap = new HashMap<>();
		mClientTrainCards = new ArrayList<>();
		mTypeSelectionFilter = TrainType.None;
		mRequiredTypeSelectionFilter = TrainType.None;
		mNumToSelect = 0;
		mInnerPanel.removeAll();
	}

	public interface TrainCardsSelectedListener
	{
		void onSelected(List<String> selected);
	}

	public TrainHandList(TrainCardsSelectedListener listener)
	{
		super();
		this.setLayout(new BorderLayout());
		mInnerPanel = new JPanel();
		mInnerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		mInnerPanel.setOpaque(false);
		mTrainCardsSelectedListener = listener;

		this.add(mInnerPanel, BorderLayout.CENTER);

		mCardPanelMap = new HashMap<>();
		mClientTrainCards = new ArrayList<>();
	}


	public void addTrainCard(ClientTrainCard card)
	{
		if (card == null)
		{
			return;
		}
		TrainType type = card.getTrainType();
		if (type == TrainType.None)
		{
			return;
		}
		TrainCardHandPanel panel = null;
		if (mCardPanelMap.containsKey(type))
		{
			panel = mCardPanelMap.get(type);
		} else
		{
			panel = new TrainCardHandPanel(type, this::processOnSelection);
			mCardPanelMap.put(type, panel);
			mClientTrainCards.add(card);
			mInnerPanel.add(panel);
		}
		if (!panel.hasCard(card))
		{
			panel.addCard(card);
		}
	}

	public synchronized void updateTrainCards(List<ClientTrainCard> cards)
	{
		if (cards == null)
		{
			return;
		}
		//get cards to remove
		List<ClientTrainCard> cardsToRemove = new ArrayList<>();
		for (ClientTrainCard card : mClientTrainCards)
		{
			if (!cards.contains(card))
			{
				cardsToRemove.add(card);
			}
		}
		//remove cards to remove
		for (ClientTrainCard card : cardsToRemove)
		{
			removeTrainCard(card);
		}
		//see which cards to add
		for (ClientTrainCard card : cards)
		{
			if (card.getTrainType() == TrainType.None)
			{
				continue;
			} else if (mClientTrainCards.contains(card))
			{
				//if card is already in list ignore
				continue;
			} else if (!mCardPanelMap.containsKey(card.getTrainType()))
			{
				TrainCardHandPanel panel = new TrainCardHandPanel(card.getTrainType(), this::processOnSelection);
				mCardPanelMap.put(card.getTrainType(), panel);
				mClientTrainCards.add(card);
				mInnerPanel.add(panel);
				panel.addCard(card);
			} else
			{
				TrainCardHandPanel panel = mCardPanelMap.get(card.getTrainType());
				if (!panel.hasCard(card))
				{
					panel.addCard(card);
					mClientTrainCards.add(card);
				}
			}
		}
	}


	private void processOnSelection(boolean selection)
	{
		int numSelected = getNumSelected();
		if (selection && mNumToSelect == numSelected && mNumToSelect > 0)
		{
			if (mTrainCardsSelectedListener != null)
			{
				List<String> selected = getSelectedIDs();
				mTrainCardsSelectedListener.onSelected(selected);
			}
		}
		//if the required type is none, update the selection filter
		else if (mRequiredTypeSelectionFilter == TrainType.None)
		{
			boolean selectionFound = false;
			for (TrainType type : mCardPanelMap.keySet())
			{
				if (type != TrainType.Locomotive && mCardPanelMap.get(type).getNumSelected() > 0)
				{
					mTypeSelectionFilter = type;
					selectionFound = true;
				}
			}
			if (!selectionFound)
			{
				mTypeSelectionFilter = TrainType.None;
			}
			updateSelectionFilers();
		}

	}

	public void removeTrainCard(ClientTrainCard card)
	{
		if (card == null)
		{
			return;
		}
		TrainType type = card.getTrainType();
		if (type == TrainType.None)
		{
			return;
		}
		if (!mClientTrainCards.contains(card))
		{
			return;
		} else
		{
			TrainCardHandPanel panel = mCardPanelMap.get(type);
			panel.removeCard(card);
			mClientTrainCards.remove(card);
			if (panel.getCount() < 1)
			{
				mInnerPanel.remove(panel);
				mCardPanelMap.remove(type);
			}
		}
	}

	public List<String> getSelectedIDs()
	{
		List<String> selected = new ArrayList<>();
		for (TrainCardHandPanel panel : mCardPanelMap.values())
		{
			if (panel.getNumSelected() > 0)
			{
				selected.addAll(panel.getSelected());
			}
		}
		return selected;
	}

	public int getNumSelected()
	{
		int sum = 0;
		for (TrainCardHandPanel panel : mCardPanelMap.values())
		{
			sum += panel.getNumSelected();
		}
		return sum;
	}

	public List<ClientTrainCard> getAllCards()
	{
		return Collections.unmodifiableList(mClientTrainCards);
	}

	public void clearSelected()
	{
		for (TrainCardHandPanel panel : mCardPanelMap.values())
		{
			panel.clearSelected();
			panel.setAllowSelection(false);
			panel.setLowLight(false);
		}
	}


	public void setParametersForSelection(int numTrains, TrainType selectionType)
	{
		mNumToSelect = numTrains;
		mRequiredTypeSelectionFilter = selectionType;
		mTypeSelectionFilter = selectionType;
		clearSelected();
		updateSelectionFilers();
	}

	private void updateSelectionFilers()
	{
		if (mTypeSelectionFilter == null)
		{
			clearSelected();
			return;
		}
		for (TrainType type : mCardPanelMap.keySet())
		{
			if (mTypeSelectionFilter == TrainType.None)
			{
				mCardPanelMap.get(type).setAllowSelection(true);
				mCardPanelMap.get(type).setLowLight(false);
			} else if (type == mTypeSelectionFilter || type == TrainType.Locomotive)
			{
				mCardPanelMap.get(type).setAllowSelection(true);
				mCardPanelMap.get(type).setLowLight(false);
			} else
			{
				mCardPanelMap.get(type).setLowLight(true);
				mCardPanelMap.get(type).setAllowSelection(false);
			}
		}
		repaint();
	}
}
