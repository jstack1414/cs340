package edu.byu.cs340.view.game.gameBoardView;

import edu.byu.cs340.view.ViewUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by joews on 3/10/2017.
 */
public class PlayerCardPanel extends Box
{
	private JPanel mRootPanel;
	private JLabel mPlayerScoreLabel;
	private JLabel mPlayerName;
	private JLabel mTrainImageLabel;
	private JLabel mNumDestinationCardsLabel;
	private JLabel mNumberTrainCardsLabel;
	private JLabel mNumberTrainsLabel;
	private JPanel mDataPanel;
	private JLabel mIsLongest;
	private JLabel mPrivateScore;

	private Color mDarkColor;
	private Color mLightColor;
	private Color mLeftGameDarkColor = new Color(50, 50, 50, 150);
	private Color mLeftGameLightColor = new Color(200, 200, 200, 150);
	private static final int BORDER_THICKNESS = 1;
	private static final int WIDTH = 120 + BORDER_THICKNESS * 2;
	private static final int HEIGHT = 75 + BORDER_THICKNESS * 2;
	private static final int ARC = 8;
	private boolean isFocused = false;
	private boolean mLeftGame = false;
	private boolean mIsLongestRoute = false;

	public boolean isLeftGame()
	{
		return mLeftGame;
	}

	public void setLeftGame(boolean leftGame)
	{
		mLeftGame = leftGame;
	}

	public PlayerCardPanel(String playerName, String trainImagePath, Color baseColor)
	{
		super(BoxLayout.Y_AXIS);
//		this.setLayout(new GridLayout(1,1));
		this.add(mRootPanel);
		this.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setMinimumSize(new Dimension(WIDTH, HEIGHT));

		mPlayerName.setText(playerName);

		mDarkColor = baseColor.darker().darker();
		int offset = 20;
		int r = baseColor.getRed();
		int g = baseColor.getGreen();
		int b = baseColor.getBlue();
		int increase = 255 - Math.min(r, Math.min(g, b)) + offset;
		int dim = 50;
		r = Math.min(255, r + increase - dim);
		g = Math.min(255, g + increase - dim);
		b = Math.min(255, b + increase - dim);
		mLightColor = new Color(r, g, b, baseColor.getAlpha());
		mTrainImageLabel.setIcon(new ImageIcon(getClass().getResource(trainImagePath)));

		this.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.setAlignmentY(Component.CENTER_ALIGNMENT);
		mPlayerName.setForeground(Color.WHITE);
		mPlayerScoreLabel.setForeground(Color.WHITE);
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		int width = this.getWidth();
		int height = this.getHeight();

		Color borderColor;
		int borderThick;

		Color dark = mDarkColor;
		;
		Color light;
		if (isFocused)
		{
			light = mLightColor;
			borderColor = mDarkColor;
			borderThick = (int) (BORDER_THICKNESS) * 2;
		} else
		{
			light = new Color(0, 0, 0, 0);
			borderColor = mDarkColor;
			borderThick = (int) (BORDER_THICKNESS);
		}
		if (mLeftGame)
		{
			dark = mLeftGameDarkColor;
			light = mLeftGameLightColor;
		}
		int titleHeight = mPlayerName.getHeight() + mPlayerName.getY() + 2;
		ViewUtils.drawRoundedTitleBox(g2d, 0, 0, width, height, titleHeight, borderThick, ARC, borderColor,
				dark, light);
	}

	public void setNumDestCards(int numCards)
	{
		mNumDestinationCardsLabel.setText(String.valueOf(numCards));
	}

	public void setNumTrainCards(int numCards)
	{
		mNumberTrainCardsLabel.setText(String.valueOf(numCards));
	}

	public void setNumTrains(int numTrains)
	{
		mNumberTrainsLabel.setText(String.valueOf(numTrains));
	}

	public void setScore(int score)
	{
		mPlayerScoreLabel.setText(String.valueOf(score));
	}

	public void setPrivateScore(int privateScore)
	{
		mPrivateScore.setVisible(true);
		if (privateScore < 0)
		{
			mPrivateScore.setText(String.valueOf(privateScore));
			mPrivateScore.setForeground(Color.RED);
		} else
		{
			mPrivateScore.setText("+" + String.valueOf(privateScore));
			mPrivateScore.setForeground(Color.GREEN);
		}
	}

	public void setIsLongest(boolean isLongest, int length)
	{
		mIsLongestRoute = isLongest;
		mIsLongest.setVisible(mIsLongestRoute);
		mIsLongest.setText(String.format("Longest Route: %s", String.valueOf(length)));
	}

	@Deprecated
	public void updatePlayer(int numDestinationCards, int numTrainCards, int numTrains, int score)
	{
		setNumDestCards(numDestinationCards);
		setNumTrainCards(numTrainCards);
		setNumTrains(numTrains);
		setScore(score);
	}

	public void setFocusedPlayer()
	{
		isFocused = true;
		repaint();
	}

	public void setUnFocusedPlayer()
	{
		isFocused = false;
		repaint();
	}
}
