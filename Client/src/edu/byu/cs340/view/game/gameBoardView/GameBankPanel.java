package edu.byu.cs340.view.game.gameBoardView;

import edu.byu.cs340.view.ViewUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by joews on 3/11/2017.
 */
public class GameBankPanel extends JPanel
{
	public static final int BORDER = 4;
	private JLabel mGameBankLabel;
	private JLabel mNumberDestCardsLabel;
	private JLabel mNumberTrainCardsLabel;
	private JLabel mNumDiscardLabel;
	private JPanel mRootPanel;
	private boolean mIsClickable = false;
	private boolean mIsSelected = false;
	private onGameBankSelected mSelectedListener;

	public GameBankPanel(int x, int y, onGameBankSelected listener)
	{
		super(new BorderLayout(0, 0));
		this.add(mRootPanel, BorderLayout.CENTER);
		addMouseAdapter();
		this.setOpaque(false);
		mRootPanel.setBorder(BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER));
		mRootPanel.setOpaque(false);
		this.setPreferredSize(new Dimension(x, y));
		this.setMaximumSize(new Dimension(x, y));
		mRootPanel.setMaximumSize(new Dimension(x - 2 * BORDER, y - 2 * BORDER));
		mRootPanel.setPreferredSize(new Dimension(x - 2 * BORDER, y - 2 * BORDER));
		mGameBankLabel.setForeground(Color.WHITE);
		mSelectedListener = listener;
	}

	public void clearView()
	{
		mIsClickable = false;
		updateDestCardsInDeck(0);
		updateTrainCardInDiscard(0);
		updateTrainCardsInDeck(0);
	}

	public interface onGameBankSelected
	{
		void onSelected();
	}

	private void addMouseAdapter()
	{
		this.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseReleased(MouseEvent e)
			{
				super.mouseReleased(e);
				mIsSelected = false;
				repaint();
			}

			@Override
			public void mousePressed(MouseEvent e)
			{
				super.mousePressed(e);
				if (mIsClickable)
				{
					mIsSelected = true;
					repaint();
					mIsSelected = false;
				}
			}

			@Override
			public void mouseClicked(MouseEvent e)
			{
				super.mouseClicked(e);
				if (mIsClickable)
				{
					mSelectedListener.onSelected();
				}
			}
		});
	}


	/**
	 * Set the value of clickable variable
	 *
	 * @param clickable The value to set clickable member variable to
	 */
	public void setClickable(boolean clickable)
	{
		mIsClickable = clickable;
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		int titleBoxHeight = mGameBankLabel.getHeight() + 3;
		ViewUtils.drawRoundedTitleBox(g2d, 0, 0, mRootPanel.getWidth(), mRootPanel.getHeight(),
				titleBoxHeight, 4, 20,
				new Color(100),
				new Color(100), Color.white);
	}

	public void updateTrainCardsInDeck(int numTrainCardsInDeck)
	{
		mNumberTrainCardsLabel.setText(String.valueOf(numTrainCardsInDeck));
	}

	public void updateTrainCardInDiscard(int numDiscardCards)
	{

		mNumDiscardLabel.setText(String.valueOf(numDiscardCards));
	}

	public void updateDestCardsInDeck(int numDestinationCardsInDeck)
	{
		mNumberDestCardsLabel.setText(String.valueOf(numDestinationCardsInDeck));
	}
}
