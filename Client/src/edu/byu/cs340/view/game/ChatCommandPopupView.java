package edu.byu.cs340.view.game;

import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.presenter.game.IChatCommandPopupPresenter;
import edu.byu.cs340.view.PopupView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by Rebekah on 3/1/2017.
 * this class cretes a window that can send messages to and get messages from the presentor and display them
 * pre- this class should be created by it's associated presentor of the same name.
 * pre - there must be a hub (server) of some sort were the logic is done and the messages are stored, that is not done here
 * post - you have a jpanel that will close when the main window does, but that can be closed on it's own without effecting
 * the main window.
 * post - you will be able to visually send and recive messages accross the "hub"
 */
public class ChatCommandPopupView extends PopupView implements IChatCommandPopupView
{
	private JPanel mRootPanel;
	private JPanel mMainPanel;
	private JPanel mLeftPanel;
	private JPanel mChatPanel;
	private JPanel mChatInputPanel;
	private JButton mSendButton;
	private JScrollPane mChatInputScroll;
	private JTextArea mChatInput;
	private JScrollPane mChatHistoryScroll;
	private JTextArea mChatHistoryArea;
	private JPanel mRightPanel;
	private JPanel mTablePanel;
	private JLabel mCommandsLabel;
	private JLabel mUsername;
	private JLabel mTitle;
	private JLabel mMessage;
	private JTextArea mCommandsTextArea;
	private JPanel mTopRootPanel;
	private JLabel mChatLabel;
	private JPanel mTopBar;
	private JScrollPane mCommandsScroll;


	private static final String TEXT_SUBMIT = "text-submit";
	private static final String INSERT_BREAK = "insert-break";

	/**
	 * pre- this is a pop up so it's requisite that there be a main window that has called it
	 * pre - the ChatCommandPopupPresenter is the creator of this object, so all of it's pre conditions apply.
	 * post - a chatCommandPopupView is created and set.
	 * post - a window appears
	 * constructor
	 *
	 */
	public ChatCommandPopupView()
	{
		mMessage.setText("");

		mSendButton.addActionListener((listener) -> onSendMessage());
//		mChatInput.addActionListener((listener)->onSendMessage());

		InputMap input = mChatInput.getInputMap();
		KeyStroke enter = KeyStroke.getKeyStroke("ENTER");
		KeyStroke shiftEnter = KeyStroke.getKeyStroke("shift ENTER");
		input.put(shiftEnter, INSERT_BREAK);  // input.get(enter)) = "insert-break"
		input.put(enter, TEXT_SUBMIT);

		ActionMap actions = mChatInput.getActionMap();
		actions.put(TEXT_SUBMIT, new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				onSendMessage();
			}
		});
	}


	/**
	 * this sends the chat message to the text area for diplay, can be seen by anyone wiht access to the same lobby or
	 * game.
	 * pre- you must have a username and message to send a message
	 * post - the message is added to the mchathistoryArea.
	 * @param username The user ascoaited with hte mesage
	 * @param message  The message
	 */
	@Override
	public void pushChatMessage(String username, String message)
	{
		mChatHistoryArea.append(username + ": " + message + "\n");
		JScrollBar sb = mChatHistoryScroll.getVerticalScrollBar();
		sb.setValue(sb.getMaximum());
	}

	@Override
	public void pushCmd(String cmdToString)
	{
		mCommandsTextArea.append(cmdToString + "\n");
		JScrollBar sb = mCommandsScroll.getVerticalScrollBar();
		sb.setValue(sb.getMaximum());
	}

	/**
	 * Sends the message in the mChatInput to the presenter.
	 * pre- the message must have somewhere to go
	 * post - if the message is not null or empty it will be sent to the presenter
	 */
	private void onSendMessage()
	{
		String message = mChatInput.getText();
		if (message != null && !message.isEmpty())
		{
			((IChatCommandPopupPresenter) getConnectedPresenter()).sendChatMessage(message);
		}
		mChatInput.setText("");
	}


	/**
	 * calls super
	 * @param presenter The associated presenter to the view
	 * @throws InvalidPresenterException
	 */
	@Override
	public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);
	}

	/**
	 * this sets the text areas
	 * pre- the constructor has been used
	 * post - the message area is blank and the username is set
	 */
	@Override
	public void onActivate()
	{
		super.onActivate();
		mMessage.setText("");
		mUsername.setText(getConnectedPresenter().getUsername());
	}

	/**
	 * this retives the base Jpanel in the window
	 * pre- the jpanel exists
	 * post the jpanel is sent by refrence.
	 * @return the base jpanel
	 */
	@Override
	public JPanel getJPanel()
	{
		return mTopRootPanel;
	}


	/**
	 * pre - none;
	 * this returns the dimensions for the minimun size
	 * post creates a dimension object and sets it
	 * @return returns the dimesion object
	 */
	@Override
	public Dimension getMinSize()
	{
		return new Dimension(600, 450);
	}


	@Override
	public void clearView()
	{
		mMessage.setText("");
		mChatInput.setText("");
		mChatHistoryArea.setText("");
		mCommandsTextArea.setText("");
	}
}
