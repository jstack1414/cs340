package edu.byu.cs340.view;

import javax.swing.*;
import java.awt.*;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * The interface which things can communicate to the WindowManager
 */
public interface IWindowManager
{
	/**
	 * This will be called to change the active mainView
	 *
	 * @param mainView the new mainView to be displayed
	 */
	void changeViewTo(MainView mainView);

	/**
	 * This will be called to change which menu bar to be displayed on the window
	 *
	 * @param menuBar the new menu bar to add to the window
	 */
	void changeMenuBar(JMenuBar menuBar);

	/**
	 * Launches a new popup unless a popup from the same view (based on the view's class name) already has been
	 * launched
	 * by the program.
	 *
	 * @param view The view to launch
	 * @pre view!=null
	 * @pre view.getJPanel().isVisible()==true
	 */
	void launchNewPopup(PopupView view);

	/**
	 * If the popup view is open, it disposes it.
	 *
	 * @param view The view associated with the popup to close.
	 * @pre view!=null
	 * @post view.getJPanel().isDisposed()==true
	 */
	void closePopup(PopupView view);

	void forceClosePopup(PopupView view);

	public Dimension getMainWindowSize();

	public Point getMainWindowLocal();

	public Dimension getPopupSize(View v);

	public Point getPopupLocation(View v);
}
