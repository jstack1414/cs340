package edu.byu.cs340.view.game_lobby;

import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tjacobhi on 07-Feb-17.
 */
public class PlayerListTableModel extends AbstractTableModel
{
	private static final String[] columnNames = {"", "Player Name", "Ready"};
	private List<Object[]> data = new ArrayList<>();
	private static String READY_ICON_PATH = "/lobby/ready.png";
	private static String UNREADY_ICON_PATH = "/lobby/unready.png";
	private static String PLAYER_ICON_PATH = "/lobby/player.png";
	private static String HOST_ICON_PATH = "/lobby/host.png";


	@Override
	public int getRowCount()
	{
		return data.size();
	}

	@Override
	public int getColumnCount()
	{
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		return data.get(rowIndex)[columnIndex];
	}

	/**
	 * Required because some items in table are ImageIcons and some are strings.
	 * This tells the table what the data type is.
	 *
	 * @param columnIndex column to get mData type from.
	 * @return Returns mData type of that column.
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex)
	{
		return getValueAt(0, columnIndex).getClass();
	}

	/**
	 * Returns the username of the player in the row.
	 * @param row row to get user name of
	 * @return Returns the user name.
	 */
	public String getUserName(int row)
	{
		return (String) data.get(row)[1];
	}

	/**
	 * Sets the mData of the table to players.
	 * @param players the mData to set in the table.
	 */
	public void setData(List<ILobbyPlayer> players)
	{
		data = new ArrayList<>(players.size());
		for (ILobbyPlayer player : players)
		{
			Object[] row = configureRow(player);
			data.add(row);
		}
		fireTableDataChanged();
	}

	/**
	 * Helper function to create a row from an ILobbyPlayer.
	 * @param player Player to create row from.
	 * @return Returns formatted row describing player.
	 */
	private Object[] configureRow(ILobbyPlayer player)
	{
		Object[] row = new Object[getColumnCount()];
		if (player.isHost())
		{
			//Creates an icon based on path.
			row[0] = new ImageIcon(getClass().getResource(HOST_ICON_PATH));
		} else
		{
			row[0] = new ImageIcon(getClass().getResource(PLAYER_ICON_PATH));
		}

		row[1] = player.getUsername();

		if (player.isReady())
		{
			row[2] = new ImageIcon(getClass().getResource(READY_ICON_PATH));
		} else
		{
			row[2] = new ImageIcon(getClass().getResource(UNREADY_ICON_PATH));
		}
		return row;
	}

	/**
	 * Updates or adds the player based on username to the data. If player is already in list it simply overrides
	 * the data previously assigned to that players name.
	 * Updates or adds the player based on username to the mData. If player is already in list it simply overrides
	 * the mData previosuly assigned to that players name.
	 * @param player
	 */
	public void updateAddPlayer(ILobbyPlayer player)
	{
		if (player != null)
		{
			boolean changed = false;
			for (int i = 0; i < getRowCount() && !changed; i++)
			{
				if (data.get(i)[1].equals(player.getUsername()))
				{
					data.set(i, configureRow(player));
					changed = true;
				}
			}
			if (!changed)
			{
				data.add(configureRow(player));
			}
		}
		fireTableDataChanged();
	}

	/**
	 * Removes player from table if they are in the mData.
	 * @param player player to remove.
	 */
	public void removePlayer(ILobbyPlayer player)
	{
		if (player != null)
		{
			boolean changed = false;
			for (int i = 0; i < getRowCount() && !changed; i++)
			{
				if (data.get(i)[1].equals(player.getUsername()))
				{
					data.remove(i);
					changed = true;
				}
			}
		}
		fireTableDataChanged();
	}

	public void setHost(String newHost)
	{
		if (newHost != null)
		{
			boolean changed = false;
			for (int i = 0; i < getRowCount() && !changed; i++)
			{
				if (data.get(i)[1].equals(newHost))
				{
					data.get(i)[0] = new ImageIcon(getClass().getResource(HOST_ICON_PATH));
					changed = true;
				}
			}
		}
		fireTableDataChanged();
	}

	@Override
	public String getColumnName(int column)
	{
		return columnNames[column];
	}
}
