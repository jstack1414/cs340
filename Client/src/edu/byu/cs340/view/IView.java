package edu.byu.cs340.view;

import edu.byu.cs340.presenter.IPresenter;

import javax.swing.*;
import java.awt.*;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * This defines the methods that all views must have in order for presenters to communicate with them.
 */
public interface IView
{
	/**
	 * This is called when the view is created. It passes the associated presenter to the view
	 * @param presenter The associated presenter to the view
	 */
	<T extends IPresenter> void initialize(T presenter) throws MainView.InvalidPresenterException;

	/**
	 * This will be called by the presenter when the view has been pushed to the screen
	 */
	void onActivate();

	/**
	 * Retruns the rootMost JPanel. For hand coded classes this will likely by the view itself (this). For GUI based
	 * ones this value has to be specified explicitly.
	 *
	 * @return The rootmost JPanel of the view.
	 */
	JPanel getJPanel();

	String getViewName();

	boolean hasSavedLocalSize();

	void storeSavedSizeLocal();

	Dimension getSavedSize();

	Point getSavedLocal();

}
