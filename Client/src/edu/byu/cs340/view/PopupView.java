package edu.byu.cs340.view;

import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.presenter.game.IPopupPresenter;

import java.awt.*;


/**
 * Created by joews on 3/1/2017.
 *
 * This is the view all popups extend for working with the WindowManager. This extends the swing UI component JPanel
 * in order to be drawn to the JFrame.
 */
public abstract class PopupView extends View implements IView
{

	private IPopupPresenter mConnectedPresenter;

	/**
	 * This should be called to associate the presenter with the view. It will only associate the presenter the first
	 * time this is called.
	 *
	 * @param presenter The associated presenter to the view
	 * @throws MainView.InvalidPresenterException when the presenter is not the presenter that matches the view
	 */
	@Override
	public void initialize(IPresenter presenter) throws edu.byu.cs340.view.View.InvalidPresenterException
	{
		if (presenter instanceof IPopupPresenter)
		{
			if (mConnectedPresenter == null)
				mConnectedPresenter = (IPopupPresenter) presenter;
		} else
		{
			throw new MainView.InvalidPresenterException();
		}
	}

	/**
	 * Sub-classes can call this to get the connected presenter
	 *
	 * @return the currently connected presenter.
	 */
	protected IPopupPresenter getConnectedPresenter()
	{
		return mConnectedPresenter;
	}

	@Override
	public void onActivate()
	{
		// Default implementation is to do nothing
	}

	public void storeSavedSizeLocal()
	{
		mSavedSize = WindowManager.getInstance().getPopupSize(this);
		mSavedLocal = WindowManager.getInstance().getPopupLocation(this);
		mHasSavedSize = true;
	}

	public Dimension getMinSize()
	{
		return new Dimension(200, 150);
	}


}
