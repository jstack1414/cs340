package edu.byu.cs340.view;

import java.awt.*;

/**
 * Created by joews on 3/10/2017.
 */
public class ViewUtils
{

	public static final Color SEMI_TRANSPARENT = new Color(230, 230, 230, 200);
	public static void drawRoundedTitleBox(Graphics2D g2d, int x, int y, int width, int height, int titleBoxHeight, int
			borderThickness, int arc, Color boundaryColor, Color titleFillColor, Color fillColor)
	{

		//fill baground
		g2d.setColor(fillColor);
		g2d.fillRoundRect(x, y, width, height, arc, arc);
		//fill title block
		g2d.setColor(titleFillColor);
		g2d.fillRoundRect(x, y, width, titleBoxHeight, arc, arc);
		//draw border
		g2d.setColor(boundaryColor);
		g2d.setStroke(new BasicStroke(borderThickness));
		g2d.setRenderingHint(
				RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.drawRoundRect(x + (borderThickness / 2), y + (borderThickness / 2), width - borderThickness, height -
				borderThickness, arc, arc);
	}

	public static void drawRoundedRectBehindTextComponent(Graphics2D g2d, Component component, int border, int rounds)
	{
		int titleX = component.getX() - border;
		int titleY = component.getY() - border;
		int titleWidth = component.getWidth() + 2 * border;
		int titleHeight = component.getHeight() + 2 * border;
		g2d.fillRoundRect(titleX, titleY, titleWidth, titleHeight, rounds, rounds);
	}
}
