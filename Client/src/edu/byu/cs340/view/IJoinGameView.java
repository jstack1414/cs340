package edu.byu.cs340.view;

import edu.byu.cs340.models.roomModel.IRoomInfo;

import java.util.List;

/**
 * Created by tjacobhi on 02-Feb-17.
 *
 * The interface for the GUI for joining a game.
 */
public interface IJoinGameView extends IView
{
	/**
	 * This is called when the game info on the UI needs to be updated.
	 * @param gameInfoList the info for current games available
	 */
	void pushCurrentGameList(List<IRoomInfo> gameInfoList);

	/**
	 * If the user performs an invalid action it displays by their name
	 *
	 * @param message the message for an invalid action to be displayed by the view
	 */
	void invalidAction(String message);
}
