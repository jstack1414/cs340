package edu.byu.cs340.view;

import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;

import java.util.List;

/**
 * Created by tjacobhi on 04-Feb-17.
 */
public interface ILobbyView extends IView
{
	/**
	 * This will either add the player if they aren't in the palyer list, or update them if they are.
	 *
	 * @param player the player who has joined
	 */
	void addUpdatePlayer(ILobbyPlayer player);

	/**
	 * This will be used to show the user that a player left
	 *
	 * @param player  the player who left
	 * @param newHost This is the name of hte new host
	 */
	void removePlayer(ILobbyPlayer player, ILobbyPlayer newHost);


	/**
	 * This will be used to display to the user a message from another user
	 *
	 * @param message the chat message to be displayed
	 */
	void pushChatMessage(String player, String message);

	/**
	 * If the user performs an invalid action it displays by their name
	 *
	 * @param message the message to display an invalid action
	 */
	void invalidAction(String message);

	/**
	 * Stores whether the client's player is ready or is the host. Updates GUI based on these values.
	 *
	 * @param isReady Whether the client's player is ready.
	 * @param isHost  Whether the client's player is the host.
	 */
	void setClientPlayerStatus(boolean isReady, boolean isHost);

	/**
	 * Sets the players in the lobby to the list players.
	 *
	 * @param players Players to set in table in lobby for view.
	 */
	void setLobbyPlayers(List<ILobbyPlayer> players);
}
