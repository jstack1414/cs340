package edu.byu.cs340.view;

import edu.byu.cs340.presenter.ILoginPresenter;
import edu.byu.cs340.presenter.IPresenter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * This is the screen the user will see before the log in.
 *
 * @invariant getConnectedPresenter() will always be the connected presenter or null if initialize() is not yet called.
 */
public class LoginView extends MainView implements ILoginView
{

	//Constants
	private static final String SIGN_IN_TEXT = "Sign In";
	private static final String REGISTER_TEXT = "Register";
	private static final String LOCKED_SIGN_IN_TEXT = "Signing In...";
	private static final String NEW_USER_TEXT = "First time? Create a new user!";
	private static final String USERNAME_LABEL_TEXT = "username:";
	private static final String PASSWORD_LABEL_TEXT = "password:";
	private static final String CONFIRM_PASSWORD_LABEL_TEXT = "confirm password:";
	private static final String HOST_BUTTON_TEXT = "Host";
	private static final String PORT_BUTTON_TEXT = "Port";
	private static final int EMPTY_SPACE = 0 * 160;
	private static final int GAP_BETWEEN_COMPONENTS = 10;
	private static final int TEXT_FIELD_WIDTH = 120;
	private static final int LABEL_GAP = 5;
	private static final int ARC_WIDTH = 10;
	private static final int ARC_HEIGHT = 10;

	// Layouts
	private LayoutManager mViewLayout;          // This is the layout for the entire view
	private LayoutManager mTopLayout;           // This is the layout for the top banner
	private LayoutManager mCenterLayout;        // This is the layout for the center view area

	// Internal Panels
	private JPanel mTopPanel;                   // This will be the panel for the top banner
	private JPanel mCenterPanel;                // This will be the panel for the center of the page
	private JPanel mRootPanel;

	// Text fields
	private JTextField mUsernameField;          // This is where the user types their username
	private JTextField mPasswordField;          // This is where the user types their password
	private JTextField mConfirmPasswordField;   // This is where the user confirms their password (if new user)

	// Text field labels
	private JLabel mUsernameLabel;              // This labels the username text field
	private JLabel mPasswordLabel;              // This labels the password text field
	private JLabel mConfirmPasswordLabel;       // This labels the confirm password text field
	private JLabel mMessage;                    // This is the place where messages are displayed

	// Buttons
	private JButton mSignInButton;              // This is the button to press to sign in
	private JButton mChangeHostButton;          // This is the button to change hosts
	private JButton mChangePortButton;          // This is the button to change ports

	// Checkboxes
	private JCheckBox mNewUserCheckBox;         // This is checked if the user is registering for the first time

	// Flags
	private boolean mIsNewUser;                 // If the user checks new user, this flag is raised
	private boolean mIsLocked;                  // If the presenter locks sign in, this flag is raised

	// MenuBar
	private JMenuBar mMenuBar;

	//Background image
	private BufferedImage mBackgroundImage = null;
	private String mBackgroundPath = "/ticketToRideBox.jpg";
	private static final int CENTER_WIDTH = 325;
	private static final int CENTER_HEIGHT = 200;

// Constructors

	/**
	 * Creates the LoginView and initializes all the UI components for the view
	 *
	 * @pre WindowManager must be initialized
	 * @post LoginView is created
	 * @invariant getConnectedPresenter() will still return null
	 */
	public LoginView()
	{
		// initialize layouts (must be done first)
		setupLayouts();

		// add panels to view
		addPanels();

		// set flags
		mIsNewUser = false;
		mIsLocked = false;

		// set up UI components
		setupTextFields();
		setupButtons();
		setupCheckBoxes();
		setupLabels();
		setupMenu();

		try
		{
			mBackgroundImage = ImageIO.read(getClass().getResourceAsStream(mBackgroundPath));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

// Public methods

	/**
	 * This is used to initialize the LoginView to allow it to be linked with the given presenter
	 * This must be called before any other method is called
	 *
	 * @param presenter The associated presenter to the view
	 * @throws InvalidPresenterException when passed in presenter isn't ILoginPresenter
	 * @pre presenter is an instance of ILoginPresenter. Failure to do so will result in an exception being thrown
	 * @post the presenter and view are officially linked up
	 * @invariant getConnectedPresenter() will now return presenter for the rest of the lifetime of LoginView
	 */
	@Override
	public void initialize(IPresenter presenter) throws InvalidPresenterException
	{
		super.initialize(presenter);    // This is called to initialize the presenter
		if (!(getConnectedPresenter() instanceof ILoginPresenter))   // This verifies that the presenter is valid
		{
			throw new MainView.InvalidPresenterException();
		}
	}

	/**
	 * This is called when the view becomes the active one being displayed.
	 *
	 * @pre initialize() must have been called
	 * @post adjusts the view to look how it should on being resumed.
	 * @invariant getConnectedPresenter is not changed
	 */
	@Override
	public void onActivate()
	{
		mMessage.setText("");
		mPasswordField.setText("");
		mConfirmPasswordField.setText("");
		mNewUserCheckBox.setSelected(false);
		onSwitchCheckBox();
		getConnectedPresenter().setMenuBar(mMenuBar);
	}

	/**
	 * This is used to prevent any sign in functionality to be used while waiting for a response from the presenter.
	 * This prevents multiple log ins.
	 *
	 * @pre none
	 * @post UI will not accept input
	 */
	@Override
	public void lockSignIn()
	{
		mSignInButton.setText(LOCKED_SIGN_IN_TEXT); // This allows the user to know why things are locked
		mIsLocked = true;                           // Set the lock flag
		changeLockStateOfUI();                      // Lock the UI to avoid changes to the UI
	}

	/**
	 * This is used to allow a user to provide input again after having locked the UI.
	 *
	 * @pre none
	 * @post UI will accept input again
	 */
	@Override
	public void unlockSignIn()
	{
		setSignInButtonText();                      // Set button text back
		mIsLocked = false;                          // Unset the lock flag
		changeLockStateOfUI();                      // Unlock the UI to allow user to change input parameters
	}

	@Override
	public String getViewName()
	{
		return "Login";
	}

	/**
	 * Is called when a message needs to be displayed to the screen due to invalid sign in credentials
	 *
	 * @param message Explains why the user couldn't log in.
	 * @pre none
	 * @post displays a message provided by the parameter.
	 */
	@Override
	public void invalidCredentials(String message)
	{
		mMessage.setText(message);
		mMessage.setForeground(Color.RED);
		System.out.println(message);
	}

// Private methods

	/**
	 * This will initialize layouts for the view and set the view layout
	 */
	private void setupLayouts()
	{
		mViewLayout = new BorderLayout(0, 0);
		mTopLayout = new FlowLayout(FlowLayout.CENTER);
		mCenterLayout = new SpringLayout();

		// set layout to view
		this.setLayout(mViewLayout);
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(mBackgroundImage, 0, 0, getWidth(), getHeight(), this);
		g.setColor(ViewUtils.SEMI_TRANSPARENT);
		g.fillRoundRect((getWidth() - CENTER_WIDTH) / 2, (getHeight() / 2 - CENTER_HEIGHT / 3), CENTER_WIDTH,
				CENTER_HEIGHT, ARC_WIDTH, ARC_HEIGHT);

	}

	/**
	 * This adds panels to the view
	 */
	private void addPanels()
	{
		mCenterPanel = new JPanel(mCenterLayout);
		//mCenterPanel.setBackground(Color.getHSBColor(0.6f, 0.5f, 0.8f));
		add(mCenterPanel, BorderLayout.CENTER); // adds center panel to the center
		mCenterPanel.setPreferredSize(new Dimension(-1, -1));
		mTopPanel = new JPanel(mTopLayout);
		add(mTopPanel, BorderLayout.NORTH);     // adds top panel to the top (North side)
		mCenterPanel.setOpaque(false);
		mTopPanel.setOpaque(false);


	}

	/**
	 * This will initialize and place text fields and their labels
	 */
	private void setupTextFields()
	{
		SpringLayout springLayout = (SpringLayout) mCenterLayout; // Save the type cast for easier readability later

		// Create the username field
		mUsernameField = new JTextField();
		springLayout.getConstraints(mUsernameField).setWidth(Spring.constant(TEXT_FIELD_WIDTH)); // Sets the width
		mCenterPanel.add(mUsernameField);   // adds the field to the center panel

		// Place the Field in the right place, this location is the root for everything else
		springLayout.putConstraint(SpringLayout.VERTICAL_CENTER,//SpringLayout.NORTH,
				mUsernameField,
				0,//EMPTY_SPACE,
				SpringLayout.VERTICAL_CENTER,//SpringLayout.NORTH,
				mCenterPanel);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER,
				mUsernameField,
				0,
				SpringLayout.HORIZONTAL_CENTER,
				mCenterPanel);

		mUsernameField.grabFocus();

		// Create the password field
		mPasswordField = new JPasswordField();
		springLayout.getConstraints(mPasswordField).setWidth(Spring.constant(TEXT_FIELD_WIDTH));
		mCenterPanel.add(mPasswordField);
		placeComponentUnder(mPasswordField, mUsernameField, GAP_BETWEEN_COMPONENTS);
		mPasswordField.addActionListener((listener) ->
		{
			if (!mIsNewUser)
				onButtonPress();
			else
				mConfirmPasswordField.grabFocus();
		});

		// Create the confirm password field
		mConfirmPasswordField = new JPasswordField();
		springLayout.getConstraints(mConfirmPasswordField).setWidth(Spring.constant(TEXT_FIELD_WIDTH));
		mCenterPanel.add(mConfirmPasswordField);
		placeComponentUnder(mConfirmPasswordField, mPasswordField, GAP_BETWEEN_COMPONENTS);
		mConfirmPasswordField.addActionListener((listener) ->
		{
			if (mIsNewUser)
				onButtonPress();
		});
	}

	/**
	 * This will initialize and place labels
	 */
	private void setupLabels()
	{
		// Username Label
		mUsernameLabel = new JLabel(USERNAME_LABEL_TEXT);
//		mUsernameLabel.setBackground(SEMI_TRANSPARENT_BACKGROUND);
//		mUsernameLabel.setOpaque(true);
		placeComponentToLeft(mUsernameLabel, mUsernameField, LABEL_GAP);
		mCenterPanel.add(mUsernameLabel);

		// Password Label
		mPasswordLabel = new JLabel(PASSWORD_LABEL_TEXT);
//		mPasswordLabel.setBackground(SEMI_TRANSPARENT_BACKGROUND);
//		mPasswordLabel.setOpaque(true);
		placeComponentToLeft(mPasswordLabel, mPasswordField, LABEL_GAP);
		mCenterPanel.add(mPasswordLabel);

		// Confirm Password Label
		mConfirmPasswordLabel = new JLabel(CONFIRM_PASSWORD_LABEL_TEXT);
//		mConfirmPasswordLabel.setBackground(SEMI_TRANSPARENT_BACKGROUND);
//		mConfirmPasswordLabel.setOpaque(true);
		placeComponentToLeft(mConfirmPasswordLabel, mConfirmPasswordField, LABEL_GAP);
		mCenterPanel.add(mConfirmPasswordLabel);
		if (mIsNewUser)
		{
			mConfirmPasswordLabel.setVisible(true);
		} else
		{
			mConfirmPasswordLabel.setVisible(false);
		}

		// Message Label
		mMessage = new JLabel();
		placeComponentAbove(mMessage, mNewUserCheckBox, GAP_BETWEEN_COMPONENTS);
		mCenterPanel.add(mMessage);
	}

	/**
	 * This will initialize and place buttons
	 */
	private void setupButtons()
	{
		mSignInButton = new JButton();
		setSignInButtonText();
		mCenterPanel.add(mSignInButton);
		placeComponentUnder(mSignInButton, mConfirmPasswordField, GAP_BETWEEN_COMPONENTS);
		mSignInButton.addActionListener((listener) -> onButtonPress());

		/*
		mChangeHostButton = new JButton(HOST_BUTTON_TEXT);
		mCenterPanel.add(mChangeHostButton);
		mChangeHostButton.addActionListener((listener) -> onChangeHost());

		mChangePortButton = new JButton(PORT_BUTTON_TEXT);
		mCenterPanel.add(mChangePortButton);
		mChangePortButton.addActionListener((listener) -> onChangePort());

		((SpringLayout)mCenterLayout).putConstraint(SpringLayout.SOUTH,
		                                            mChangePortButton,
		                                            -GAP_BETWEEN_COMPONENTS,
		                                            SpringLayout.SOUTH,
		                                            mCenterPanel);
		((SpringLayout)mCenterLayout).putConstraint(SpringLayout.EAST,
		                                            mChangePortButton,
		                                            -GAP_BETWEEN_COMPONENTS,
		                                            SpringLayout.EAST,
		                                            mCenterPanel);

		placeComponentToLeft(mChangeHostButton, mChangePortButton, GAP_BETWEEN_COMPONENTS);
		*/
	}

	/**
	 * This will initialize and place check boxes
	 */
	private void setupCheckBoxes()
	{
		mNewUserCheckBox = new JCheckBox(NEW_USER_TEXT);
		placeComponentAbove(mNewUserCheckBox, mUsernameField, GAP_BETWEEN_COMPONENTS);
		mCenterPanel.add(mNewUserCheckBox);
		mNewUserCheckBox.addActionListener((listener) -> onSwitchCheckBox());
		onSwitchCheckBox();
		mNewUserCheckBox.setOpaque(false);
	}

	/**
	 * This is used to setup the menu bar that will be displayed on the window.
	 * It includes:
	 * File:
	 * Host:
	 * Brings up a dialog to change the host
	 * Port:
	 * Brings up a dialog to change the port
	 * Exit:
	 * Closes the client application
	 */
	private void setupMenu()
	{
		mMenuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.getAccessibleContext().setAccessibleDescription("Testing");
		JMenuItem hostMenuItem = new JMenuItem(HOST_BUTTON_TEXT, KeyEvent.VK_H);
		JMenuItem portMenuItem = new JMenuItem(PORT_BUTTON_TEXT, KeyEvent.VK_P);
		JMenuItem exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_X);

		mMenuBar.add(fileMenu);
		fileMenu.add(hostMenuItem);
		fileMenu.add(portMenuItem);
		fileMenu.add(exitMenuItem);

		hostMenuItem.addActionListener((listener) -> onChangeHost());
		portMenuItem.addActionListener((listener) -> onChangePort());
		exitMenuItem.addActionListener((listener) -> System.exit(0));
	}

	/**
	 * Will place a UI element directly under another.
	 *
	 * @param component    Component to be placed
	 * @param under        Component that will be placed under
	 * @param spaceBetween Amount of space to place between components
	 */
	private void placeComponentUnder(JComponent component, JComponent under, int spaceBetween)
	{
		SpringLayout springLayout = (SpringLayout) mCenterLayout;
		springLayout.putConstraint(SpringLayout.NORTH,
				component,
				spaceBetween,
				SpringLayout.SOUTH,
				under);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER,
				component,
				0,
				SpringLayout.HORIZONTAL_CENTER,
				under);
	}

	/**
	 * Will place a UI component directly above another
	 *
	 * @param component    Component to be placed
	 * @param above        Component to be placed above of
	 * @param spaceBetween Amount of space to place between components
	 */
	private void placeComponentAbove(JComponent component, JComponent above, int spaceBetween)
	{
		SpringLayout springLayout = (SpringLayout) mCenterLayout;
		springLayout.putConstraint(SpringLayout.SOUTH,
				component,
				-spaceBetween,
				SpringLayout.NORTH,
				above);
		springLayout.putConstraint(SpringLayout.HORIZONTAL_CENTER,
				component,
				0,
				SpringLayout.HORIZONTAL_CENTER,
				above);
	}

	/**
	 * Will place a UI component directly to the left of another
	 *
	 * @param component    Component to be placed
	 * @param leftOf       Component to be placed to the left of
	 * @param spaceBetween Amount of space to place between components
	 */
	private void placeComponentToLeft(JComponent component, JComponent leftOf, int spaceBetween)
	{
		SpringLayout springLayout = (SpringLayout) mCenterLayout;
		springLayout.putConstraint(SpringLayout.EAST,
				component,
				-spaceBetween,
				SpringLayout.WEST,
				leftOf);
		springLayout.putConstraint(SpringLayout.VERTICAL_CENTER,
				component,
				0,
				SpringLayout.VERTICAL_CENTER,
				leftOf);
	}

	/**
	 * This will ensure the text of the button is correct depending on the state of the checkbox
	 */
	private void setSignInButtonText()
	{
		if (mIsNewUser)
		{
			mSignInButton.setText(REGISTER_TEXT);
		} else
		{
			mSignInButton.setText(SIGN_IN_TEXT);
		}
	}

	/**
	 * This is the function that is called when the checkbox changes
	 */
	private void onSwitchCheckBox()
	{
		if (mNewUserCheckBox.isSelected())
		{
			mIsNewUser = true;
			showConfirmPassword();
		} else
		{
			mIsNewUser = false;
			hideConfirmPassword();
		}
		setSignInButtonText();
	}

	/**
	 * This is the function that is called when the button is pressed
	 */
	private void onButtonPress()
	{
		if (!mIsLocked)
		{
			if (mIsNewUser)
			{
				((ILoginPresenter) getConnectedPresenter()).register(mUsernameField.getText(),
						mPasswordField.getText(),
						mConfirmPasswordField.getText());
			} else
			{
				((ILoginPresenter) getConnectedPresenter()).signIn(mUsernameField.getText(), mPasswordField.getText());
			}
		}
	}

	/**
	 * This is called when the confirm password field needs to be hidden.
	 */
	private void hideConfirmPassword()
	{
		try
		{
			mConfirmPasswordField.setVisible(false);
			mConfirmPasswordLabel.setVisible(false);
		} catch (NullPointerException e)
		{
			//ignore
		}
	}

	/**
	 * This is called when the confirm password field needs to be visible
	 */
	private void showConfirmPassword()
	{
		try
		{
			mConfirmPasswordField.setVisible(true);
			mConfirmPasswordLabel.setVisible(true);
		} catch (NullPointerException e)
		{
			//ignore
		}
	}

	/**
	 * This is called to lock or unlock the UI when the presenter calls lockSignIn() or unlockSignIn()
	 */
	private void changeLockStateOfUI()
	{
		mUsernameField.setEnabled(!mIsLocked);
		mPasswordField.setEnabled(!mIsLocked);
		mConfirmPasswordField.setEnabled(!mIsLocked);
		mNewUserCheckBox.setEnabled(!mIsLocked);
		mSignInButton.setEnabled(!mIsLocked);
	}

	/**
	 * Allows the user to change the host the client connects to
	 */
	private void onChangeHost()
	{
		String host = JOptionPane.showInputDialog(this,
				"Change Host from " + ((ILoginPresenter) getConnectedPresenter()).getHost() + " to:",
				"Change Host",
				JOptionPane.PLAIN_MESSAGE);
		if (host != null)
			((ILoginPresenter) getConnectedPresenter()).changeHost(host);
	}

	/**
	 * Allows the user to change the port the client connects with
	 */
	private void onChangePort()
	{
		int port = 0;
		String sPort = Integer.toString(((ILoginPresenter) getConnectedPresenter()).getPort());
		try
		{
			sPort = JOptionPane.showInputDialog(this,
					"Change Port from " + sPort + " to:",
					"Change Port",
					JOptionPane.PLAIN_MESSAGE);
			//TODO: Add exception handling here or just pass as string to change port and let it handle it
			port = Integer.parseInt(sPort);
		} catch (NumberFormatException | NullPointerException e)
		{
			//e.printStackTrace();
		}
		if (port != 0)
		{
			((ILoginPresenter) getConnectedPresenter()).changePort(port);
		}

	}
}