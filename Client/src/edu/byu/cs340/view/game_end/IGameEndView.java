package edu.byu.cs340.view.game_end;

import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.view.IView;

/**
 * Created by Sean on 3/24/2017.
 */
public interface IGameEndView extends IView
{
	void setGameOverData(IGameOverData data);

	void clearView();
}
