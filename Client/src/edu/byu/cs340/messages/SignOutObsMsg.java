package edu.byu.cs340.messages;

/**
 * Created by Rebekah on 2/11/2017.
 *
 * Message notifying observer that signout was successful.
 */
public class SignOutObsMsg extends StringObsMsg
{
	public SignOutObsMsg(String message)
	{
		super(message);
    }
}
