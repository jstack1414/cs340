package edu.byu.cs340.messages;

/**
 * Created by joews on 2/7/2017.
 *
 * Message notifying observers the game list has been updated
 */
public class GameListUpdateObsMsg extends StringObsMsg
{
	public GameListUpdateObsMsg(String message)
	{
		super(message);
	}
}
