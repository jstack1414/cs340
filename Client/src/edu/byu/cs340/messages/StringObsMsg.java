package edu.byu.cs340.messages;

/**
 * Created by joews on 2/5/2017.
 *
 * Message that has a string message as part of it for notifying observers.
 */
public abstract class StringObsMsg implements IObsMsg
{
	protected String message;

	public StringObsMsg(String message)
	{
		this.message = message;
	}


	/**
	 * Get value of Message
	 *
	 * @return Returns the value of Message
	 */
	public String getMessage()
	{
		return message;
	}


}
