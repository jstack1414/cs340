package edu.byu.cs340.messages;

import edu.byu.cs340.models.lobbyModel.LobbyPlayer;

/**
 * Created by joews on 2/2/2017.
 *
 * Message notifying observer of a lobby player that changed ready status.
 */
public class ReadyChangeObsMsg extends LobbyPlayerObsMsg
{
	public ReadyChangeObsMsg(String message, LobbyPlayer player)
	{
		super(message, player);
	}
}
