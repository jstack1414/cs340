package edu.byu.cs340.messages;

/**
 * Created by joews on 2/10/2017.
 *
 * Message notifying observers an error occurred in executing a command or action.
 */
public class ErrorObsMsg extends StringObsMsg
{
	public ErrorObsMsg(String message)
	{
		super(message);
	}
}
