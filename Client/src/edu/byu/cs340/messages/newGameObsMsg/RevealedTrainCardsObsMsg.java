package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.presenter.IGamePresenter;

import java.util.List;

/**
 * Created by joews on 3/20/2017.
 */
public class RevealedTrainCardsObsMsg implements GameObsMsg
{
	private List<ClientTrainCard> mRevealedTrainCards;

	public RevealedTrainCardsObsMsg(List<ClientTrainCard> revealedTrainCards)
	{
		mRevealedTrainCards = revealedTrainCards;
	}

	/**
	 * Get value of RevealedTrainCards
	 *
	 * @return Returns the value of RevealedTrainCards
	 */
	public List<ClientTrainCard> getRevealedTrainCards()
	{
		return mRevealedTrainCards;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateRevealedCards(mRevealedTrainCards);
	}
}
