package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class GamePlayerDestCardsObsMsg extends PlayerIntObsMsg implements GameObsMsg
{
	public GamePlayerDestCardsObsMsg(int anInt, String playerName)
	{
		super(anInt, playerName);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updatePlayerDestCardCount(getPlayerName(), getNumber());
	}
}
