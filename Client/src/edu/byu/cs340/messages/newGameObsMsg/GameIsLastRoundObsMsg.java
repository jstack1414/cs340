package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by Rebekah on 3/24/2017.
 */
public class GameIsLastRoundObsMsg implements GameObsMsg {

    private boolean mIsLastRound;

    public GameIsLastRoundObsMsg(boolean isLastRound)
    {
        mIsLastRound = isLastRound;
    }

    @Override
    public void updateUI(IGamePresenter presenter) {
	    presenter.updateIsLastRound();
    }
}
