package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.presenter.IGamePresenter;

import java.util.List;

/**
 * Created by joews on 3/20/2017.
 */
public class ClientPlayerDestCardsObsMsg implements IObsMsg, GameObsMsg
{
	private List<ClientDestinationCard> mCardsInHand;

	public ClientPlayerDestCardsObsMsg(List<ClientDestinationCard> cardsInHand)
	{
		mCardsInHand = cardsInHand;
	}

	/**
	 * Get value of CardsInHand
	 *
	 * @return Returns the value of CardsInHand
	 */
	public List<ClientDestinationCard> getCardsInHand()
	{
		return mCardsInHand;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateClientPlayerDestCards(mCardsInHand);
	}
}
