package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class CmdAddedObsMsg extends StringObsMsg implements GameObsMsg
{
	public CmdAddedObsMsg(String commandToString)
	{
		super(commandToString);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateCmdAdded(getString());
	}
}
