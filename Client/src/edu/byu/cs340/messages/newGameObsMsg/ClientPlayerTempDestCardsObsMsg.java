package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.presenter.IGamePresenter;

import java.util.List;

/**
 * Created by joews on 3/20/2017.
 */
public class ClientPlayerTempDestCardsObsMsg implements IObsMsg, GameObsMsg
{
	private List<ClientDestinationCard> mTempCards;
	private int mMinToKeep;

	public ClientPlayerTempDestCardsObsMsg(List<ClientDestinationCard> tempCards, int minToKeep)
	{
		mTempCards = tempCards;
		mMinToKeep = minToKeep;
	}

	/**
	 * Get value of TempCards
	 *
	 * @return Returns the value of TempCards
	 */
	public List<ClientDestinationCard> getTempCards()
	{
		return mTempCards;
	}

	/**
	 * Get value of MinToKeep
	 *
	 * @return Returns the value of MinToKeep
	 */
	public int getMinToKeep()
	{
		return mMinToKeep;
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateClientPlayerTempDestCards(mTempCards,mMinToKeep);
	}
}
