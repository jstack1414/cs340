package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.messages.IObsMsg;

/**
 * Created by joews on 3/20/2017.
 */
public abstract class StringObsMsg implements IObsMsg
{
	private String mString;

	public StringObsMsg(String string)
	{
		mString = string;
	}

	/**
	 * Get value of String
	 *
	 * @return Returns the value of String
	 */
	public String getString()
	{
		return mString;
	}
}
