package edu.byu.cs340.messages.newGameObsMsg;

/**
 * Created by joews on 3/20/2017.
 */
public abstract class PlayerIntObsMsg extends IntObsMsg
{

	private String mPlayerName;

	public PlayerIntObsMsg(int anInt, String playerName)
	{
		super(anInt);
		mPlayerName = playerName;
	}

	/**
	 * Get value of PlayerName
	 *
	 * @return Returns the value of PlayerName
	 */
	public String getPlayerName()
	{
		return mPlayerName;
	}
}
