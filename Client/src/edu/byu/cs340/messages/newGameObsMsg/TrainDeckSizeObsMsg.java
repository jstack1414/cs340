package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class TrainDeckSizeObsMsg extends IntObsMsg implements GameObsMsg
{
	public TrainDeckSizeObsMsg(int deckSize)
	{
		super(deckSize);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updateTrainDeckSize(this.getNumber());
	}
}
