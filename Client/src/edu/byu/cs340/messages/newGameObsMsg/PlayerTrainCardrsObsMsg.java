package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class PlayerTrainCardrsObsMsg extends PlayerIntObsMsg implements GameObsMsg
{
	// todo rename this class
	public PlayerTrainCardrsObsMsg(int anInt, String playerName)
	{
		super(anInt, playerName);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updatePlayerTrainCardCount(getPlayerName(), getNumber());
	}
}
