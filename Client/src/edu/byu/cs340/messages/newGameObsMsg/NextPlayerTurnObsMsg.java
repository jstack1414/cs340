package edu.byu.cs340.messages.newGameObsMsg;

import edu.byu.cs340.presenter.IGamePresenter;

/**
 * Created by joews on 3/20/2017.
 */
public class NextPlayerTurnObsMsg extends StringObsMsg implements GameObsMsg
{
	public NextPlayerTurnObsMsg(String name)
	{
		super(name);
	}

	@Override
	public void updateUI(IGamePresenter presenter)
	{
		presenter.updatePlayerTurn(getString());
	}
}
