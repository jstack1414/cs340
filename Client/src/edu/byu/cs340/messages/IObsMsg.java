package edu.byu.cs340.messages;

import edu.byu.cs340.message.IMessage;

/**
 * Created by joews on 2/1/2017.
 *
 * This interface will be inherited by multiple message types. It is the means by which the
 * ClientModelManager communicates with the presenter.
 */
public interface IObsMsg extends IMessage
{
}
