package edu.byu.cs340.messages;

/**
 * Created by joews on 2/2/2017.
 *
 * Message notifying observers of a game starting.
 */
public class StartGameObsMsg extends StringObsMsg
{
	public StartGameObsMsg(String message)
	{
		super(message);
	}
}
