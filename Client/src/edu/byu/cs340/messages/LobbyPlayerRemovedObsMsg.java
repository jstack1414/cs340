package edu.byu.cs340.messages;

import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;
import edu.byu.cs340.models.lobbyModel.LobbyPlayer;

/**
 * Created by joews on 2/2/2017.
 *
 * Message telling the presenter which player has been kicked
 */
public class LobbyPlayerRemovedObsMsg extends LobbyPlayerObsMsg
{
	/**
	 * Player that is the new host.
	 */
	protected ILobbyPlayer mNewHost;

	public LobbyPlayerRemovedObsMsg(String message, LobbyPlayer player, ILobbyPlayer newHost)
	{
		super(message, player);
		mNewHost = newHost;
	}

	/**
	 * Get value of NewHost
	 *
	 * @return Returns the value of NewHost
	 */
	public ILobbyPlayer getNewHost()
	{
		return mNewHost;
	}
}

