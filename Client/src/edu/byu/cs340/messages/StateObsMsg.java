package edu.byu.cs340.messages;

import edu.byu.cs340.models.state.IState;

/**
 * Created by joews on 3/15/2017.
 */
public class StateObsMsg implements IObsMsg
{
	IState state;

	public StateObsMsg(IState state)
	{
		this.state = state;
	}

	/**
	 * Get value of State
	 *
	 * @return Returns the value of State
	 */
	public IState getState()
	{
		return state;
	}
}
