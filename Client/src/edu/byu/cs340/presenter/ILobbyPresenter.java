package edu.byu.cs340.presenter;

/**
 * Created by tjacobhi on 04-Feb-17.
 *
 * This is the interface that connects the game lobby UI to the models
 */
public interface ILobbyPresenter extends IMainPresenter
{
	/**
	 * This will send a message to the server for chatting. (Idle chatting is always fun:)
	 *
	 * @pre must be connected to a lobby
	 * @pre message must not be null
	 * @post sends message to client
	 * @param message the string to be sent to other mPlayers
	 */
	void sendChatMessage(String message);

	/**
	 * Will send a signal to the models that they are ready to begin the game
	 * @pre must be connected to a lobby
	 * @pre must be unready
	 * @post sends ready message to client
	 */
	void sendReadySignal();

	/**
	 * Will undo the signal that they are ready
	 * @pre must be connected to a lobby
	 * @pre must be ready
	 * @post sends unready message to client
	 */
	void sendUnreadySignal();

	/**
	 * Will send a message to start the game
	 *
	 * @pre User must be host player
	 * @pre must be connected to lobby
	 * @pre all players must be ready
	 * @post sends start signal to client
	 */
	void sendStartSignal();

	/**
	 * Will send a signal to quit the game.
	 * @pre must be connected to a lobby
	 * @post sends message to client to remove user from lobby
	 */
	void sendLeaveSignal();

	/**
	 * sends a signal to remove the player with username from the lobby.
	 *
	 * @pre must be host
	 * @pre must be connected to the lobby
	 * @post sends a message
	 * @param username Player to remove
	 */
	void sendRemovePlayerSignal(String username);

	/**
	 * @pre must be connected to a lobby
	 * @post returns the title of the lobby
	 * @return the title of the lobby
	 */
	String getTitle();
}
