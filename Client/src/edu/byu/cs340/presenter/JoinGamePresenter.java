package edu.byu.cs340.presenter;

//import edu.byu.cs340.clientFacade.ClientFacade;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IJoinGameModel;
import edu.byu.cs340.messages.*;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;
import edu.byu.cs340.view.IJoinGameView;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.JoinGameView;
import edu.byu.cs340.view.MainView;

import java.util.Observable;

/**
 * Created by tjacobhi on 02-Feb-17.
 *
 * The Implementation for IJoinGamePresenter being used by the client
 */
public class JoinGamePresenter extends MainPresenter implements IJoinGamePresenter
{

	private IJoinGameView mJoinGameView;

	private String mUsername;

	/**
	 * Constructor for the presenter
	 */
	public JoinGamePresenter()
	{
		mJoinGameView = new JoinGameView();
		try
		{
			mJoinGameView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("MainView rejected presenter");
		}
	}

	@Override
	public void update(Observable o, Object arg)
	{
		super.update(o, arg);
		if (arg instanceof CreateLobbyObsMsg)
			{
				//TODO: Switch mState
//				ClientFacade.getInstance(IJoinGameModel.class).changeState(ClientState.gameLobby);
				System.out.println("Successful created lobby\n" + ((CreateLobbyObsMsg) arg).getMessage());
			} else if (arg instanceof GameListUpdateObsMsg)
			{
				IRoomsListInfo gameList = ClientFacade.getInstance(IJoinGameModel.class).getGameListInfo();
				mJoinGameView.pushCurrentGameList(gameList.getData());
			} else if (arg instanceof JoinLobbyObsMsg)
			{

			} else if (arg instanceof ErrorObsMsg && isActive())
			{
				mJoinGameView.invalidAction(((ErrorObsMsg) arg).getMessage());
			} else if (arg instanceof StartGameObsMsg && isActive())
			{
			}
	}

	@Override
	public IView getView()
	{
		return mJoinGameView;
	}

	@Override
	public void joinGame(String gameID)
	{
		ClientFacade.getInstance(IJoinGameModel.class).joinLobby(gameID);
	}

	@Override
	public void createGame(String title)
	{
		ClientFacade.getInstance(IJoinGameModel.class).createLobby(title);
	}

	@Override
	public void activate()
	{
		super.activate();
		mUsername = getUsername();
	}

	@Override
	public void refresh()
	{
		ClientFacade.getInstance(edu.byu.cs340.clientFacade.IJoinGameModel.class).updateGamesListInfo();
	}
}
