package edu.byu.cs340.presenter;

import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;

import java.util.List;
import java.util.Set;

/**
 * Created by tjacobhi on 11-Feb-17.
 *
 * This is the interface for the presenter when game play is created
 */
public interface IGamePresenter extends IMainPresenter
{
	void closeChat();

	void openChat();

	void updateClientPlayerDestCards(List<ClientDestinationCard> cards);

	void updateClientPlayerTrainCards(List<ClientTrainCard> cards);

	void updateClientPlayerTempDestCards(List<ClientDestinationCard> cards, int minToKeep);

	void updateCmdAdded(String cmdToString);

	void updateDestinationDeckSize(int size);

	void updateTrainDeckSize(int size);

	void updateTrainDiscardSize(int size);

	void updateGameOver(IGameOverData data);

	void updatePlayerTurn(String newPlayerTurn);

	void updatePlayerDestCardCount(String playerName, int count);

	void updatePlayerTrainCardCount(String playerName, int count);

	void updatePlayerTrainCount(String playerName, int count);

	void updateRevealedCards(List<ClientTrainCard> revealedCards);

	void updateRouteClaimed(String playerName, String routeID);

	void updateScore(String playerName, int score);

	void updatePrivateScore(String playerName, int privateScore);

	void updateAddChatMessage(ChatMessage message);

	void updateAddPlayer(IGamePlayer player);

	void updateIsLastRound();

	void updatePlayerLeft(String username);

	void updateLongestRoute(Set<String> username, int length);

}
