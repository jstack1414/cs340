package edu.byu.cs340.presenter;

import edu.byu.cs340.messages.StateObsMsg;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.ModelObservable;
import edu.byu.cs340.models.state.IState;
import edu.byu.cs340.models.state.JoinGameState;
import edu.byu.cs340.models.state.LobbyState;
import edu.byu.cs340.models.state.LoginState;
import edu.byu.cs340.models.state.game.GameState;
import edu.byu.cs340.presenter.game.GamePresenterProxy;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by joews on 3/16/2017.
 *
 * Manager for presenters. This holds the presenters and manages activating them based on the game state.
 *
 * @invariant getInstance()!=null
 */
public class PresenterManager implements Observer
{
	private static PresenterManager mSingleton;
	private Map<PresenterKey, IPresenter> mPresenterMap;

	/**
	 * Enumeration representing the four presenters which are keys for the presenter map in the manager.
	 */
	public enum PresenterKey
	{
		LOGIN,
		JOIN_GAME,
		LOBBY,
		GAME
	}

	/**
	 * Gets the singleton instance of the presenter manager
	 *
	 * @return The Presenter manager singleton
	 *
	 * @pre none
	 * @post result !=null
	 */
	public static PresenterManager getInstance()
	{
		if (mSingleton == null)
		{
			mSingleton = new PresenterManager();
		}
		return mSingleton;
	}

	/**
	 * Constructor which creates the presenter manager and adds the login, joingame, lobby and game
	 * presenters to the map.
	 *
	 * @pre none
	 * @post mPresenterMap.size()==4
	 */
	private PresenterManager()
	{
		mPresenterMap = new HashMap<>();
		mPresenterMap.put(PresenterKey.LOGIN, new LoginPresenter());
		mPresenterMap.put(PresenterKey.JOIN_GAME, new JoinGamePresenter());
		mPresenterMap.put(PresenterKey.LOBBY, new LobbyPresenter());
		mPresenterMap.put(PresenterKey.GAME, new GamePresenterProxy());
		for (IPresenter presenter : mPresenterMap.values())
		{
			ClientModelManager.getInstance().addObserver(presenter);
		}
		ClientModelManager.getInstance().addObserver(this);
	}

	/**
	 * Activates the presenter associated with the key. This calls the activate method on the
	 * associated presenter if it is not already active
	 *
	 * @param presenterKey The key associated with the presenter to activate.
	 * @pre presenterKey!=null
	 * @post the presenter associated with the key is presenter.isActive()==true
	 * @post the view with that presenter will be displayed.
	 */
	public void activatePresenter(PresenterKey presenterKey)
	{
		IPresenter presenter = mPresenterMap.get(presenterKey);
		if (!presenter.isActive())
		{
			presenter.activate();
		}
	}

	/**
	 * Deactivates the presenter associated with the key.
	 *
	 * @param presenterKey The key associated with the key to deactivate
	 * @pre presenterKey!=null
	 * @post the presenter assoicated iwth key is presenter.isActive()==false
	 * @post the view associated with that presenter will not be displayed and will be closed if it was displayed
	 */
	public void deactivatePresenter(PresenterKey presenterKey)
	{
		if (mPresenterMap.get(presenterKey).isActive())
		{
			mPresenterMap.get(presenterKey).deactivate();
		}
	}

	/**
	 * Deactivates all presenters based on key values in the PresenterKey enumeration.
	 *
	 * @post all views associated with the presenters will not be displayed
	 * @post all presenters defined by PresenterKey's will be presenter.isActive()==false
	 */
	public void deactivateAll()
	{
		for (PresenterKey key : PresenterKey.values())
		{
			deactivatePresenter(key);
		}
	}

	/**
	 * Update method as part of the observer message. On StateObsMsg this method will activate the
	 * appropirate presenter that corresponds with the state.
	 *
	 * @param o   Observable object.
	 * @param arg Argument (IObserverMessage) passed by observable.
	 * @pre None
	 * @post if o instanceof ModelObservable && arg instanceof StateObsMsg then the appropriate presenter will be
	 * activated.
	 */
	@Override
	public void update(Observable o, Object arg)
	{
		if (o instanceof ModelObservable)
		{
			if (arg instanceof StateObsMsg)
			{
				//TODO: Consider changing to a method in the state.
				IState state = ((StateObsMsg) arg).getState();
				if (state instanceof LoginState)
				{
					activatePresenter(PresenterKey.LOGIN);
				} else if (state instanceof JoinGameState)
				{
					activatePresenter(PresenterKey.JOIN_GAME);
				} else if (state instanceof LobbyState)
				{
					activatePresenter(PresenterKey.LOBBY);
				} else if (state instanceof GameState)
				{
					activatePresenter(PresenterKey.GAME);
				}
			}
		}
	}
}
