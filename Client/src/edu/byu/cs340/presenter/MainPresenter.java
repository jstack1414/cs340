package edu.byu.cs340.presenter;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IUIModel;
import edu.byu.cs340.messages.ErrorObsMsg;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.WindowManager;

import javax.swing.*;
import java.util.Observable;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * This is the base class for Presenters that are directly associated with a main view (i.e. lobby, game, login etc)
 */
public abstract class MainPresenter implements IMainPresenter
{
	private boolean mIsActive = false;

	@Override
	public void signOut()
	{
		ClientFacade.getInstance(IUIModel.class).signOut();
//		ClientFacade.getInstance(IUIModel.class).changeState(ClientState.login);
	}

	/**
	 * Sets the view associated with the presenter to the main view of the GUI.
	 */
	@Override
	public void activate()
	{
		PresenterManager.getInstance().deactivateAll();
		mIsActive = true;
		WindowManager.getInstance().changeViewTo((MainView) getView());
		getView().onActivate();
	}


	public boolean isActive()
	{
		return mIsActive;
	}

	/**
	 * Deactivates the view as the main view on the GUI and removes the menu bar.
	 */
	@Override
	public void deactivate()
	{
		if (mIsActive && getView() != null)
		{
			getView().storeSavedSizeLocal();
		}
		mIsActive = false;

		WindowManager.getInstance().changeMenuBar(null);
	}

	@Override
	public void update(Observable o, Object arg)
	{
		if (arg instanceof ErrorObsMsg)
		{
			System.out.println(((ErrorObsMsg) arg).getMessage());
		}

		getView().getJPanel().revalidate();
		getView().getJPanel().repaint();
	}

	@Override
	public String getUsername()
	{
		return ClientFacade.getInstance(IUIModel.class).getUserName();
	}


	@Override
	public void setMenuBar(JMenuBar menuBar)
	{
		WindowManager.getInstance().changeMenuBar(menuBar);
	}

}
