package edu.byu.cs340.presenter.game;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;

import java.util.List;

/**
 * Created by Rebekah on 2/22/2017.
 */
public interface IDestinationCardPopupPresenter extends IPopupPresenter
{
	/**
	 * set the cards in the presenter to display for the view to select from. Tells the view of hte cards.
	 *
	 * @param cards
	 */
	void setTemporaryCards(List<ClientDestinationCard> cards);

	/**
	 *Specify the minimum cards to keep
	 *
	 * @param maxToReturn Maximum number of cards to return.
	 */
	void setMinToKeep(int maxToReturn);


	/**
	 * REturns the temporary cards to the view.
	 *
	 * @return
	 */
	List<ClientDestinationCard> getTemporaryCards();

    /**
     * this gets the card choice from the UI and sends it to the models for comfirmation of a valid draw
     * @param selectedCards the cards the player chose
     * @return true if the card is a vaild draw and is successfully added to the player's hand in the models
     *          if it returns false then the draw was not valid and the cards were not added to the player's hand
     *
     */
    String validateAndDrawCards(List<String> selectedCards);


	public int getMinCardsToKeep();


	void clearView();
}
