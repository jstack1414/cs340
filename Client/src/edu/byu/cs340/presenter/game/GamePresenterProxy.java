package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IGameModel;
import edu.byu.cs340.messages.StateObsMsg;
import edu.byu.cs340.messages.newGameObsMsg.GameObsMsg;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.models.state.IState;
import edu.byu.cs340.models.state.game.*;
import edu.byu.cs340.presenter.IGamePresenter;
import edu.byu.cs340.presenter.MainPresenter;
import edu.byu.cs340.view.IView;

import java.util.List;
import java.util.Observable;
import java.util.Set;

/**
 * Created by crown on 2/22/2017.
 * This is the singleton proxy that implements the IGamePresenter interface that the ClientFacade knows about
 */
public class GamePresenterProxy extends MainPresenter implements IGamePresenter
{
	public static final int DRAW_DESTINATION_CARD_OPTION = 1;
	public static final int DRAW_TRAIN_CARD_OPTION = 2;
	public static final int CLAIM_ROUTE_OPTION = 3;
	// the int 10 is being used to invoke the default by other popups

	private IGameBoardPresenter mGameBoardPresenter;
	private ITurnStartPopupPresenter mTurnStartPopupPresenter;
	private IDestinationCardPopupPresenter mDestinationCardPopupPresenter;
	private IChatCommandPopupPresenter mChatCommandPopupPresenter;
	private IGameOverPresenter mGameOverPresenter;


	public GamePresenterProxy()
	{
		super();
		mGameBoardPresenter = new GameBoardPresenter(this);
		mTurnStartPopupPresenter = new TurnStartPopupPresenter(this);
		mDestinationCardPopupPresenter = new DestinationCardPopupPresenter(this);
		mChatCommandPopupPresenter = new ChatCommandPopupPresenter(this);
		mGameOverPresenter = new GameOverPresentesr(this);
	}

	/**
	 * Select what the user chose to do for their turn
	 *
	 * @param option the index of the option to choose.
	 */
	void startTurnOptionSelected(int option)
	{
		mTurnStartPopupPresenter.deactivate();
		ClientFacade.getInstance(IGameModel.class).selectActionForTurn(option);
	}

	/**
	 * Called when the user wants to open the chat window
	 */
	public void openChat()
	{
		mChatCommandPopupPresenter.activate();
	}

	public void closeChat()
	{
		mChatCommandPopupPresenter.deactivate();
	}


	@Override
	public void update(Observable o, Object arg)
	{
		//todo refactor this bad boy outta here
		super.update(o, arg);
		if (arg instanceof StateObsMsg)
		{
			mGameBoardPresenter.update(o, arg);
			onStateChange((StateObsMsg) arg);
		} else if (arg instanceof GameObsMsg)
		{
			((GameObsMsg) arg).updateUI(this);
		}
	}

	private void onStateChange(StateObsMsg arg)
	{
		//TODO: Implement. Also remove some of the old observer messages
		IState state = arg.getState();
		if (state instanceof GameState)
		{
			//we are in the game...
			if (state instanceof PickActionState)
			{
				//popup the turn selected
				if (!mTurnStartPopupPresenter.isActive())
				{
					mTurnStartPopupPresenter.activate();
				}
				if (mDestinationCardPopupPresenter.isActive())
				{
					mDestinationCardPopupPresenter.deactivate();
				}
			} else if (state instanceof ReturnDestinationCardState
					|| state instanceof InitialTurnReturnDestinationCardsState)
			{
				if (!mDestinationCardPopupPresenter.isActive())
				{
					mDestinationCardPopupPresenter.activate();
				}
				if (mTurnStartPopupPresenter.isActive())
				{
					mTurnStartPopupPresenter.deactivate();
				}
			} else
			{
				if (mTurnStartPopupPresenter.isActive())
				{
					mTurnStartPopupPresenter.deactivate();
				}
				if (mDestinationCardPopupPresenter.isActive())
				{
					mDestinationCardPopupPresenter.deactivate();
				}
			}
		} else if (state instanceof GameOverState)
		{
			mDestinationCardPopupPresenter.deactivate();
			mTurnStartPopupPresenter.deactivate();
			mGameOverPresenter.activate();
		}
	}


	@Override
	public IView getView()
	{
		return mGameBoardPresenter.getView();
	}

	@Override
	public void activate()
	{
		super.activate();
	}


	@Override
	public void deactivate()
	{
		super.deactivate();
		if (mGameBoardPresenter.isActive()) mGameBoardPresenter.deactivate();
		if (mTurnStartPopupPresenter.isActive()) mTurnStartPopupPresenter.deactivate();
		if (mDestinationCardPopupPresenter.isActive()) mDestinationCardPopupPresenter.deactivate();
		if (mChatCommandPopupPresenter.isActive()) mChatCommandPopupPresenter.deactivate();
		if (mGameOverPresenter.isActive()) mGameOverPresenter.deactivate();
		//now clear all of the views
		mGameOverPresenter.clearView();
		mGameBoardPresenter.clearView();
		mTurnStartPopupPresenter.clearView();
		mDestinationCardPopupPresenter.clearView();
		mChatCommandPopupPresenter.clearView();
	}

	@Deprecated
	private void openDestinationCardPopup()
	{
		if (!mDestinationCardPopupPresenter.isActive()) mDestinationCardPopupPresenter.activate();
	}


	//<editor-fold desc="Updates called by observer messages">
	@Override
	public void updateClientPlayerDestCards(List<ClientDestinationCard> cards)
	{
		mGameBoardPresenter.updateClientPlayerDestCards(cards);
	}

	@Override
	public void updateClientPlayerTrainCards(List<ClientTrainCard> cards)
	{
		mGameBoardPresenter.updateClientPlayerTrainCards(cards);
	}

	@Override
	public void updateClientPlayerTempDestCards(List<ClientDestinationCard> cards, int minToKeep)
	{

		mDestinationCardPopupPresenter.setMinToKeep(minToKeep);
		mDestinationCardPopupPresenter.setTemporaryCards(cards);
	}

	@Override
	public void updateCmdAdded(String cmdToString)
	{
		mChatCommandPopupPresenter.updateAddCmd(cmdToString);
	}

	@Override
	public void updateDestinationDeckSize(int size)
	{
		mGameBoardPresenter.updateDestinationDeckSize(size);
	}

	@Override
	public void updateTrainDiscardSize(int size)
	{
		mGameBoardPresenter.updateTrainDiscardSize(size);
	}

	@Override
	public void updateTrainDeckSize(int size)
	{
		mGameBoardPresenter.updateTrainDeckSize(size);
	}

	@Override
	public void updateGameOver(IGameOverData data)
	{
		mGameOverPresenter.setGameOverData(data);
	}

	@Override
	public void updatePlayerTurn(String newPlayerTurn)
	{
		mGameBoardPresenter.updatePlayerTurn(newPlayerTurn);
	}

	@Override
	public void updatePlayerDestCardCount(String playerName, int count)
	{
		mGameBoardPresenter.updatePlayerDestCardCount(playerName, count);
	}

	@Override
	public void updatePlayerTrainCardCount(String playerName, int count)
	{
		mGameBoardPresenter.updatePlayerTrainCardCount(playerName, count);
	}

	@Override
	public void updatePlayerTrainCount(String playerName, int count)
	{
		mGameBoardPresenter.updatePlayerTrainCount(playerName, count);
	}

	@Override
	public void updateRevealedCards(List<ClientTrainCard> revealedCards)
	{
		mGameBoardPresenter.updateRevealedCards(revealedCards);
	}

	@Override
	public void updateRouteClaimed(String playerName, String routeID)
	{
		mGameBoardPresenter.updateRouteClaimed(playerName, routeID);
	}

	@Override
	public void updateScore(String playerName, int score)
	{
		mGameBoardPresenter.updateScore(playerName, score);
	}

	@Override
	public void updatePrivateScore(String playerName, int privateScore)
	{
		mGameBoardPresenter.updatePrivateScore(playerName, privateScore);
	}

	@Override
	public void updateAddChatMessage(ChatMessage message)
	{
		mChatCommandPopupPresenter.updateAddChatMessage(message);
	}

	@Override
	public void updateAddPlayer(IGamePlayer player)
	{
		mGameBoardPresenter.updateAddPlayer(player);
	}

	@Override
	public void updateIsLastRound()
	{
		mGameBoardPresenter.updateIsLastRound();
		mTurnStartPopupPresenter.updateIsLastRound();
	}

	@Override
	public void updatePlayerLeft(String username)
	{
		mGameBoardPresenter.updatePlayerLeft(username);
	}


	@Override
	public void updateLongestRoute(Set<String> username, int length)
	{
		mGameBoardPresenter.updateLongestRoute(username, length);
	}

	//</editor-fold>

}
