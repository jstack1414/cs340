package edu.byu.cs340.presenter.game;

import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientRoute;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.presenter.IMainPresenter;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by crown on 2/24/2017.
 * This is the interface for a class that implements the game board, including side frames
 */
public interface IGameBoardPresenter extends IMainPresenter
{

	List<IGamePlayer> sendGetPlayers();

	Map<String, ClientCity> sendGetCities();

	Map<String, ClientRoute> sendGetRoutes();

	String sendGetBackgroundImage();

	void sendDrawTrainCardFromDeck();


	void sendDrawTrainCardFromZone(int zone);


	void sendCancelOperation();


	void sendClaimRoute(String selectedRouteID, List<String> trainCardIDs);

	void openChat();

	/**
	 * @param RevealedZone
	 * @return
	 *
	 * @pre ReaveledZone in [1,5]
	 */
	boolean isRevealedTrainCardLocomotive(int RevealedZone);

	void updateClientPlayerDestCards(List<ClientDestinationCard> cards);

	void updateClientPlayerTrainCards(List<ClientTrainCard> cards);

	void updateDestinationDeckSize(int size);

	void updateTrainDeckSize(int size);

	void updateTrainDiscardSize(int size);


	void updatePlayerTurn(String newPlayerTurn);

	void updatePlayerDestCardCount(String playerName, int count);

	void updatePlayerTrainCardCount(String playerName, int count);

	void updatePlayerTrainCount(String playerName, int count);

	void updateRevealedCards(List<ClientTrainCard> revealedCards);

	void updateRouteClaimed(String playerName, String routeID);

	void updateScore(String playerName, int score);

	void updatePrivateScore(String playerName, int privateScore);

	void updateAddPlayer(IGamePlayer player);

	void updateIsLastRound();

	void leaveGame();

	boolean isRouteClaimable(String routeID);

	void updatePlayerLeft(String username);

	void updateLongestRoute(Set<String> username, int length);

	void clearView();
}
