package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IUIModel;
import edu.byu.cs340.messages.ErrorObsMsg;
import edu.byu.cs340.presenter.IPresenter;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.PopupView;
import edu.byu.cs340.view.WindowManager;

import java.util.Observable;

/**
 * Created by joews on 3/1/2017.
 */
public abstract class PopupPresenter implements IPresenter
{

	private boolean mIsActive = false;

	protected abstract IView getView();

	/**
	 * Launches a popup for the view associated with the presenter.
	 */
	public void activate()
	{
		mIsActive = true;
		WindowManager.getInstance().launchNewPopup((PopupView) getView());
		getView().onActivate();
		getView().getJPanel().revalidate();
		getView().getJPanel().repaint();
	}

	public boolean isActive()
	{
		return mIsActive;
	}

	/**
	 * Closes the popup (if any) of the view associated with the presenter.
	 */
	public void deactivate()
	{
		if (mIsActive && getView() != null)
		{
			getView().storeSavedSizeLocal();
		}
		mIsActive = false;
		WindowManager.getInstance().closePopup((PopupView) getView());
	}

	public void update(Observable o, Object arg)
	{
		if (arg instanceof ErrorObsMsg)
		{
			System.out.println(((ErrorObsMsg) arg).getMessage());
		}

		getView().getJPanel().repaint();
		getView().getJPanel().revalidate();
	}

	@Override
	public String getUsername()
	{
		return ClientFacade.getInstance(IUIModel.class).getUserName();
	}
}
