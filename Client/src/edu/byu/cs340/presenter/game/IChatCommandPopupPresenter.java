package edu.byu.cs340.presenter.game;

import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.IChatMessage;

import java.util.ArrayList;

/**
 * Created by Rebekah on 2/22/2017.
 * this class handles the view for the chat/command popup
 */
public interface IChatCommandPopupPresenter extends IPopupPresenter
{

    /**
     * sends the players chat message to...somewhere, wherever it needs to go
     * @param message the message being sent
     * @return true if the message was sent without error
     */
    void sendChatMessage(String message);

    /**
     * gets the current list of chat messages
     * @return the current list of chat messages
     */
    ArrayList<IChatMessage> getCurrentChatHistory();

    /**
     * gets the current list of commands from the history
     * @return the command list
     */
    ArrayList<String> getCurrentCommandHistory();


	void updateAddChatMessage(ChatMessage message);

	void updateAddCmd(String cmdToString);

	@Deprecated
	void updateChatMessages();

	void clearView();
}
