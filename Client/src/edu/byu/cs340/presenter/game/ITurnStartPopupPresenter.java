package edu.byu.cs340.presenter.game;

/**
 * Created by crown on 2/24/2017.
 * This is the interface for a class that handles presenting options to the user when their turn starts
 */
public interface ITurnStartPopupPresenter extends IPopupPresenter
{

	/**
	 * The view calls this when a user chooses what they will do for their turn
	 * @param option the index of the option to choose
	 *               0: Draw Destination Cards
	 *               1: Draw Train Cards
	 *               2: Claim Route
	 */
	void optionSelected(int option);

	void updateIsLastRound();

	void clearView();
}
