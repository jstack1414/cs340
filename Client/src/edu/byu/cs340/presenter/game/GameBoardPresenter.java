package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IGameModel;
import edu.byu.cs340.messages.StateObsMsg;
import edu.byu.cs340.models.gameModel.ClientCity;
import edu.byu.cs340.models.gameModel.ClientRoute;
import edu.byu.cs340.models.gameModel.IGamePlayer;
import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.map.TrainType;
import edu.byu.cs340.models.state.IState;
import edu.byu.cs340.models.state.game.*;
import edu.byu.cs340.presenter.MainPresenter;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.game.IGameBoardView;
import edu.byu.cs340.view.game.gameBoardView.GameBoardView;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

/**
 * Created by crown on 3/2/2017.
 * The presenter for the main window of the game.
 * Includes drawing train cards and claiming routes.
 */
public class GameBoardPresenter extends MainPresenter implements IGameBoardPresenter
{

	private IGameBoardView mView;
	private GamePresenterProxy mGamePresenterProxy;

	public GameBoardPresenter(GamePresenterProxy mainGamePresenter)
	{
		mGamePresenterProxy = mainGamePresenter;
		mView = new GameBoardView();
		try
		{
			mView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("Presenter rejected view");
		}
		//TEMPORARY: Below temporarily hard codes a state just for testing the view
//		mView.setGameBoardAction(IGameBoardView.GAME_BOARD_ACTION.CLAIM_ROUTE);
	}

	//TODO: Add methods that the game presenter proxy will call to do things like starting the claim route sequence
	//or drawing train cards (note this is different when the observer messages, because those say what changed)
	//these say what we want to change. That will then get sent to the model/server and if the change happens
	//the observer will come back and update the model.

	//<editor-fold desc="Callbacks from View">
	private void setUpCallbacks()
	{
		//set callbacks in the view

	}

	@Override
	public void sendClaimRoute(String selectedRouteID, List<String> trainCardIDs)
	{
		ClientFacade.getInstance(IGameModel.class).claimRoute(selectedRouteID, trainCardIDs);
	}

	@Override
	public List<IGamePlayer> sendGetPlayers()
	{
		return ClientFacade.getInstance(IGameModel.class).getPlayers();
	}

	@Override
	public Map<String, ClientCity> sendGetCities()
	{
		return ClientFacade.getInstance(IGameModel.class).getCities();
	}

	@Override
	public Map<String, ClientRoute> sendGetRoutes()
	{
		return ClientFacade.getInstance(IGameModel.class).getRoutes();
	}

	@Override
	public String sendGetBackgroundImage()
	{
		return ClientFacade.getInstance(IGameModel.class).getBackgroundImage();
	}

	@Override
	public void sendDrawTrainCardFromDeck()
	{
		ClientFacade.getInstance(IGameModel.class).drawTrainCardFromDeck();
	}

	@Override
	public void sendDrawTrainCardFromZone(int zone)
	{
		ClientFacade.getInstance(IGameModel.class).drawTrainCardFromZone(zone);
	}

	@Override
	public void sendCancelOperation()
	{
		ClientFacade.getInstance(IGameModel.class).uiCancel();
	}

	@Override
	public boolean isRevealedTrainCardLocomotive(int RevealedZone)
	{
		ClientTrainCard card = ClientFacade.getInstance(IGameModel.class).getFaceUpCards()[RevealedZone - 1];
		return card != null && card.getTrainType() == TrainType.Locomotive;
	}
	//</editor-fold>


	//<editor-fold desc="Implemented Methods From IPresenter">
	@Override
	public IView getView()
	{
		return mView;
	}

	@Override
	public void activate()
	{
		super.activate();
	}
	//</editor-fold>


	//<editor-fold desc="Methods triggered by observer pattern">
	@Override
	public void update(Observable o, Object arg)
	{
		super.update(o, arg);
		if (arg instanceof StateObsMsg)
		{
			onStateMessage((StateObsMsg) arg);
		}
	}


	private void onStateMessage(StateObsMsg arg)
	{
		IState state = arg.getState();
		if (state instanceof GameState)
		{
			if (state instanceof ClaimRouteState)
			{
				mView.setGameBoardAction(IGameBoardView.GAME_BOARD_ACTION.CLAIM_ROUTE);
			} else if (state instanceof DrawFirstTrainCardState)
			{
				mView.setGameBoardAction(IGameBoardView.GAME_BOARD_ACTION.DRAW_FIRST_TRAIN_CARD);
			} else if (state instanceof DrawSecondTrainCardState)
			{
				mView.setGameBoardAction(IGameBoardView.GAME_BOARD_ACTION.DRAW_SECOND_TRAIN_CARD);
			} else if (state instanceof PickActionState)
			{
				mView.setGameBoardAction(IGameBoardView.GAME_BOARD_ACTION.SELECT_ACTION);
			} else
			{
				mView.setGameBoardAction(IGameBoardView.GAME_BOARD_ACTION.NOT_MY_TURN);
			}
		}
	}


	@Override
	public void openChat()
	{
		mGamePresenterProxy.openChat();
	}

	//</editor-fold>

	//<editor-fold desc="Update the UI Methods">
	@Override
	public void updateClientPlayerDestCards(List<ClientDestinationCard> cards)
	{
		mView.updateClientPlayerDestinationCards(cards);
	}

	@Override
	public void updateClientPlayerTrainCards(List<ClientTrainCard> cards)
	{

		mView.updateClientPlayerTrainCards(cards);
	}


	@Override
	public void updateDestinationDeckSize(int size)
	{
		mView.updateDestCardsInDeck(size);
	}

	@Override
	public void updateTrainDiscardSize(int size)
	{
		mView.updateTrainCardsInDiscard(size);
	}

	@Override
	public void updateTrainDeckSize(int size)
	{
		mView.updateTrainCardsInDeck(size);
	}


	@Override
	public void updatePlayerTurn(String newPlayerTurn)
	{
		mView.setPlayerTurn(newPlayerTurn);
	}

	@Override
	public void updatePlayerDestCardCount(String playerName, int count)
	{
		mView.updatePlayerDestCardCount(playerName, count);
	}

	@Override
	public void updatePlayerTrainCardCount(String playerName, int count)
	{
		mView.updatePlayerTrainCardCount(playerName, count);
	}

	@Override
	public void updatePlayerTrainCount(String playerName, int count)
	{
		mView.updatePlayerTrainCount(playerName, count);
	}

	@Override
	public void updateRevealedCards(List<ClientTrainCard> revealedCards)
	{
		mView.updateRevealedTrainCards(revealedCards);
	}

	@Override
	public void updateRouteClaimed(String playerName, String routeID)
	{
		mView.setRouteAsClaimed(routeID, playerName);
	}

	@Override
	public void updateScore(String playerName, int score)
	{
		mView.updatePlayerScore(playerName, score);
	}

	public void updatePrivateScore(String playerName, int privateScore)
	{
		mView.updatePlayerPrivateScore(playerName, privateScore);
	}

	@Override
	public void updateAddPlayer(IGamePlayer player)
	{
		mView.addPlayer(player);
	}

	@Override
	public void updateIsLastRound()
	{
		mView.updateIsLastRound();
	}

	@Override
	public void leaveGame()
	{
		ClientFacade.getInstance(IGameModel.class).leaveGame();
	}

	@Override
	public boolean isRouteClaimable(String routeID)
	{
		return ClientFacade.getInstance(IGameModel.class).isRouteClaimable(routeID);
	}

	public void updatePlayerLeft(String username)
	{
		mView.playerLeft(username);
	}

	@Override
	public void updateLongestRoute(Set<String> username, int length)
	{
		mView.updateLongestRoute(username, length);
	}

	@Override
	public void deactivate()
	{
		super.deactivate();
	}

	public void clearView()
	{
		mView.clearView();
	}
	//</editor-fold>


}
