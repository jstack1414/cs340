package edu.byu.cs340.presenter.game;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.IGameModel;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.MainView;
import edu.byu.cs340.view.game_end.GameEndView;
import edu.byu.cs340.view.game_end.IGameEndView;

/**
 * Created by joews on 3/24/2017.
 */
public class GameOverPresentesr extends PopupPresenter implements IGameOverPresenter
{
	IGameEndView mView;
	GamePresenterProxy mGamePresenterProxy;

	public GameOverPresentesr(GamePresenterProxy gamePresenterProxy)
	{
		mGamePresenterProxy = gamePresenterProxy;
		mView = new GameEndView();
		try
		{
			mView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("Popup rejected view");
		}
	}

	@Override
	public void setGameOverData(IGameOverData data)
	{
		mView.setGameOverData(data);
	}

	@Override
	public void sendLeaveGameSignal()
	{
		ClientFacade.getInstance(IGameModel.class).leaveGame();
	}

	@Override
	protected IView getView()
	{
		return mView;
	}

	@Override
	public void deactivate()
	{
		super.deactivate();
	}

	public void clearView()
	{
		mView.clearView();
	}
}
