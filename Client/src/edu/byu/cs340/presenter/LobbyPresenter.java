package edu.byu.cs340.presenter;

import edu.byu.cs340.clientFacade.ClientFacade;
import edu.byu.cs340.clientFacade.ILobbyModel;
import edu.byu.cs340.messages.*;
import edu.byu.cs340.models.ChatMessage;
import edu.byu.cs340.models.ModelObservable;
import edu.byu.cs340.models.lobbyModel.ILobbyPlayer;
import edu.byu.cs340.view.ILobbyView;
import edu.byu.cs340.view.IView;
import edu.byu.cs340.view.LobbyView;
import edu.byu.cs340.view.MainView;

import java.util.List;
import java.util.Observable;

/**
 * Created by joews on 2/9/2017.
 * Presenter for the lobby screen (joined a game but not started)
 */
public class LobbyPresenter extends MainPresenter implements ILobbyPresenter
{

	private ILobbyView mLobbyView;
	private long lastChatIndex = -1;

	public LobbyPresenter()
	{
		mLobbyView = new LobbyView();
		try
		{
			mLobbyView.initialize(this);

		} catch (MainView.InvalidPresenterException e)
		{
			System.err.println("MainView rejected presenter");
		}
	}

	//<editor-fold desc="Required by ILobbyPresenter">

	/**
	 * Sends chat message to models.
	 *
	 * @param message the string to be sent to other mPlayers
	 */
	@Override
	public void sendChatMessage(String message)
	{
		ClientFacade.getInstance(ILobbyModel.class).sendLobbyChatMessage(message);
	}

	/**
	 * Sends ready message to models.
	 */
	@Override
	public void sendReadySignal()
	{
		ClientFacade.getInstance(ILobbyModel.class).sendReadySignal();
	}

	/**
	 * sends unready message to models.
	 */
	@Override
	public void sendUnreadySignal()
	{
		ClientFacade.getInstance(ILobbyModel.class).sendUnreadySignal();
	}

	/**
	 * sends start message to models.
	 */
	@Override
	public void sendStartSignal()
	{
		ClientFacade.getInstance(ILobbyModel.class).sendStartSignal();
	}

	/**
	 * Sends message to models that the player is leaving the game.
	 */
	@Override
	public void sendLeaveSignal()
	{
		ClientFacade.getInstance(ILobbyModel.class).sendLeaveSignal();
	}

	/**
	 * Sends message to models to remove player.
	 *
	 * @param username Player to remove
	 */
	@Override
	public void sendRemovePlayerSignal(String username)
	{
		ClientFacade.getInstance(ILobbyModel.class).sendRemovePlayerSignal(username);
	}

	/**
	 * Gets the title of the lobby from the models.
	 *
	 * @return Returns the lobby's title.
	 */
	@Override
	public String getTitle()
	{
		return ClientFacade.getInstance(ILobbyModel.class).getLobbyName();
	}

	//</editor-fold>
	//<editor-fold desc="Required by observer">

	/**
	 * Callback from the observer pattern. Updates the lobby presenter. If the presenter cars about the Object arg
	 * (which is an IObserverUpdateMessage) it will update.
	 *
	 * @param o   Source of he message (Always ClientFacade)
	 * @param arg a Message of type IObsMsg saying what has changed in the models.
	 */
	@Override
	public void update(Observable o, Object arg)
	{
		super.update(o, arg);
		if (o instanceof ModelObservable)
		{
			//message saying a player has readied up.
			if (arg instanceof ReadyChangeObsMsg)
			{
				processReadyChangeMessage((ReadyChangeObsMsg) arg);
			}
			//message saying player has been removed from game
			else if (arg instanceof LobbyPlayerRemovedObsMsg)
			{
				processPlayerRemovedMessage((LobbyPlayerRemovedObsMsg) arg);
			} else if (arg instanceof ErrorObsMsg && isActive())
			{
				mLobbyView.invalidAction(((ErrorObsMsg) arg).getMessage());
			} else if (arg instanceof StartGameObsMsg && isActive())
			{
				System.out.println("Starting game");
			} else if (arg instanceof ChatUpdatedObsMsg)
			{
				updateChatMessages();
			} else if (arg instanceof JoinLobbyObsMsg)
			{
				processJoinLobbyMessage((JoinLobbyObsMsg) arg);
			}
		}
	}

	private void updateChatMessages()
	{
		List<ChatMessage> chatMessages = ClientFacade.getInstance(ILobbyModel.class).getChatMessages(lastChatIndex);
		lastChatIndex = ClientFacade.getInstance(ILobbyModel.class).getLastChatIndex();
		for (ChatMessage message : chatMessages)
		{
			mLobbyView.pushChatMessage(message.getUserName(), message.getMessage().trim());
		}
	}

	/**
	 * Handles when a player has changed ready status.
	 *
	 * @param message message containing the player to update.
	 * @pre message!=null
	 * @post if message.getPlayerWhoChanged().equals(getUsername()) then the local client player is readying up and the
	 * ready button will change as needed.
	 * @post if !message.getPlayerWhoChanged().equals(getUsername()) then the player changing is someone else and that
	 * player will be updated
	 */
	private void processReadyChangeMessage(ReadyChangeObsMsg message)
	{
		ILobbyPlayer player = message.getPlayer();
		if (player != null)
		{
			if (player.getUsername().equals(this.getUsername()))
			{
				mLobbyView.setClientPlayerStatus(player.isReady(), player.isHost());
			}
			mLobbyView.addUpdatePlayer(player);
		}
	}

	private void processJoinLobbyMessage(JoinLobbyObsMsg message)
	{
		ILobbyPlayer player = message.getPlayer();
		if (player != null)
		{
			if (player.getUsername().equals(this.getUsername()))
			{
				mLobbyView.setClientPlayerStatus(player.isReady(), player.isHost());
			}
			mLobbyView.addUpdatePlayer(player);
		}
	}

	/**
	 * Handles when a player is to be removed
	 *
	 * @param arg the message with the remove info
	 * @pre message!=null
	 * @post if message.getRemovedPlayer().equals(getUsername()) then the local client player is being removed. in this
	 * case change the mState to login.
	 * @post if !message.getRemovedPlayer().equals(getUsername()) then the player changing is someone else and they
	 * have
	 * been removed. Remove them.
	 */
	private void processPlayerRemovedMessage(LobbyPlayerRemovedObsMsg arg)
	{
		ILobbyPlayer playerToRemove = arg.getPlayer();
		ILobbyPlayer newHost = arg.getNewHost();
		if (playerToRemove != null && playerToRemove.getUsername().equals(getUsername()))
		{
//			ClientFacade.getInstance(ILobbyModel.class).changeState(ClientState.joinGame);
//			System.out.println("You have been removed from the lobby");
		} else
		{
			mLobbyView.removePlayer(playerToRemove, newHost);
			if (newHost != null && newHost.getUsername().equals(getUsername()))
			{
				mLobbyView.setClientPlayerStatus(newHost.isReady(), newHost.isHost());
			}
		}
	}

	//</editor-fold>


	//<editor-fold desc="IMainPresenter methods">

	/**
	 * Returns the view of this presenter
	 *
	 * @return returns the LobbyView
	 */
	@Override
	public IView getView()
	{
		return mLobbyView;
	}

	/**
	 * sets the players in the lobby to the local lobby players.
	 */
	@Override
	public void activate()
	{
		super.activate();
		//On activate et the player list
		List<ILobbyPlayer> players = ClientFacade.getInstance(ILobbyModel.class).getLobbyPlayers();
		mLobbyView.setLobbyPlayers(players);
		lastChatIndex = -1;
	}
	//</editor-fold>

}
