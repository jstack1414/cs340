package edu.byu.cs340.presenter;

/**
 * Created by tjacobhi on 29-Jan-17.
 *
 * The interface which the loginView can access the associated presenter.
 */
public interface ILoginPresenter extends IMainPresenter
{
	/**
	 * Used to sign in to server
	 * @pre username must be valid
	 * @pre password must be valid
	 * @post sends a request through the client to login
	 * @param username this is the username the user must pass
	 * @param password this is the password the user must pass
	 */
	void signIn(String username, String password);

	/**
	 * This is to create a new user on the server
	 * @pre username must be valid format
	 * @pre password and confirm password must match
	 * @pre password must fit proper format
	 * @post sends a request through the cleint to create a new user and then signs in the new user
	 * @param username this is the username to be created
	 * @param password this is the password to be created
	 * @param confirmPassword this is to verify the password to be created
	 */
	void register(String username, String password, String confirmPassword);

	/**
	 * Changes host to specified host
	 * @pre host must be proper syntax for a hostname
	 * @pre host must not be null
	 * @post changes the port the client will use to connect to the server
	 * @param host hostname to connect to server
	 */
	void changeHost(String host);

	/**
	 * Changes port to the specified host
	 * @pre port must be greater than zero.
	 * @post changes the port the client will use to connect the server
	 * @param port port to change to
	 */
	void changePort(int port);

	/**
	 * Retrieves the host the client will connect to
	 * @return the host
	 */
	String getHost();

	/**
	 * Retrieves the port the client will connect with
	 * @return the port
	 */
	int getPort();
}
