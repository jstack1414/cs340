
I created three packages: model, mainView, presenter.

In the UML diagram, the colored rectangles representing these three categories within the project should be placed
within the packages to keep everything separated correctly.