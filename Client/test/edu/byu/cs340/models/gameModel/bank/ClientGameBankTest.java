package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.LocalGamePlayer;
import edu.byu.cs340.models.gameModel.map.TrainType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Created by Sean on 3/8/2017.
 *
 * Testing suite for the ClientGameBank
 */
class ClientGameBankTest {
	ClientGameBank mBank;
	@BeforeEach
	void setUp()
	{
		ClientTrainCard[] revCards = new ClientTrainCard[5];
		revCards[0] = new ClientTrainCard(TrainType.Box);
		revCards[1] = new ClientTrainCard(TrainType.Caboose);
		revCards[2] = new ClientTrainCard(TrainType.Coal);
		revCards[3] = new ClientTrainCard(TrainType.Freight);
		revCards[4] = new ClientTrainCard(TrainType.Hopper);


		mBank = new ClientGameBank();
		mBank.setRevealedCards(revCards);
		mBank.setTrainCardDeckSize(3);
		mBank.setDestinationCardDeckSize(3);
	}
	
	@Test
	void getClientPlayer()
	{
		assertNull(mBank.getClientPlayer());
		// Batman likes Ticket to Ride
		mBank.setClientPlayer(new LocalGamePlayer("Batman"));
		assertEquals(mBank.getClientPlayer().getUsername(), "Batman");
	}
	
	@Test
	void getDestinationCardDeckSize()
	{
		assertEquals(mBank.getDestinationCardDeckSize(), 3);
		mBank.decrementDestinationCardDeck(1);
		assertEquals(mBank.getDestinationCardDeckSize(), 2);
		mBank.incrementDestinationCardDeck(1);
		assertEquals(mBank.getDestinationCardDeckSize(), 3);
		mBank.setDestinationCardDeckSize(42);
		assertEquals(mBank.getDestinationCardDeckSize(), 42);
	}
	
	@Test
	void getTrainCardDeckSize()
	{
		assertEquals(mBank.getTrainCardDeckSize(), 3);
		mBank.decrementTrainCardDeck(1);
		assertEquals(mBank.getTrainCardDeckSize(), 2);
		mBank.incrementTrainCardDeck(1);
		assertEquals(mBank.getTrainCardDeckSize(), 3);
		mBank.setTrainCardDeckSize(42);
		assertEquals(mBank.getTrainCardDeckSize(), 42);
	}
	
	@Test
	void getRevealedCards()
	{
		assertEquals(5, mBank.getRevealedCards().length);
		mBank.removeRevealedCard(mBank.getRevealedCards()[0]);
		assertNull(mBank.getRevealedCards()[0]);
	}
	
}