package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.TrainType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by crown on 3/8/2017.
 */
class LocalGamePlayerTest {
	private LocalGamePlayer testPlayer;
	
	@BeforeEach
	void setUp() {
		testPlayer = new LocalGamePlayer("meepit");
	}

	@AfterEach
	void tearDown() {
		testPlayer = null;
	}

	@Test
	void getDestinationCards() {
		ICity[] cities1 = {new ClientCity("Paris"), new ClientCity("Lyon")};
		ICity[] cities2 = {new ClientCity("London"), new ClientCity("Edinborough")};
		testPlayer.mDestinationCards.add(new ClientDestinationCard("testDestCard1", cities1, 50));
		testPlayer.mDestinationCards.add(new ClientDestinationCard("testDestCard2", cities2, 30));
		
		List<ClientDestinationCard> destCards = testPlayer.getDestinationCards();
		ClientDestinationCard destCard1 = destCards.get(0);
		assertEquals("testDestCard1", destCard1.getId());
		assertEquals("Paris", destCard1.getCities().get(0).getName());
		assertEquals("Lyon", destCard1.getCities().get(1).getName());
		assertEquals(50, destCard1.getPoints());

		ClientDestinationCard destCard2 = destCards.get(1);
		assertEquals("testDestCard2", destCard2.getId());
		assertEquals("London", destCard2.getCities().get(0).getName());
		assertEquals("Edinborough", destCard2.getCities().get(1).getName());
		assertEquals(30, destCard2.getPoints());
	}

	@Test
	void setDestinationCards() {
		ICity[] cities1 = {new ClientCity("Paris"), new ClientCity("Lyon")};
		ICity[] cities2 = {new ClientCity("London"), new ClientCity("Edinborough")};
		List<ClientDestinationCard> destCards = new ArrayList<>();
		destCards.add(new ClientDestinationCard("testDestCard1", cities1, 50));
		destCards.add(new ClientDestinationCard("testDestCard2", cities2, 30));
		testPlayer.setDestinationCards(destCards);
		assertEquals(2, testPlayer.mDestinationCards.size());
	}

	@Test
	void addTrainCard() {
		ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
		testPlayer.addTrainCard(testTrainCard1);
		assertEquals((testPlayer.mTrainCards.get(0)).getTrainType(), TrainType.Locomotive);
		
		ClientTrainCard testTrainCard2 = new ClientTrainCard(TrainType.Reefer);
		testPlayer.addTrainCard(testTrainCard2);
		assertEquals((testPlayer.mTrainCards.get(1)).getTrainType(), TrainType.Reefer);
	}

	@Test
	void addDestinationCards() {
		ICity[] cities1 = {new ClientCity("Paris"), new ClientCity("Lyon")};
		ICity[] cities2 = {new ClientCity("London"), new ClientCity("Edinborough")};
		ClientDestinationCard testDestCard1 = new ClientDestinationCard("testDestCard1", cities1, 50);
		ClientDestinationCard testDestCard2 = new ClientDestinationCard("testDestCard2", cities2, 30);
		List<ClientDestinationCard> destCards = new ArrayList<>();
		destCards.add(testDestCard1);
		destCards.add(testDestCard2);
		
		testPlayer.addDestinationCards(destCards);
		ClientDestinationCard destCard1 = testPlayer.mDestinationCards.get(0);
		assertEquals("testDestCard1", destCard1.getId());
		assertEquals("Paris", destCard1.getCities().get(0).getName());
		assertEquals("Lyon", destCard1.getCities().get(1).getName());
		assertEquals(50, destCard1.getPoints());

		ClientDestinationCard destCard2 = testPlayer.mDestinationCards.get(1);
		assertEquals("testDestCard2", destCard2.getId());
		assertEquals("London", destCard2.getCities().get(0).getName());
		assertEquals("Edinborough", destCard2.getCities().get(1).getName());
		assertEquals(30, destCard2.getPoints());
	}

	@Test
	void getTrainCards() {
		ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
		ClientTrainCard testTrainCard2 = new ClientTrainCard(TrainType.Reefer);
		testPlayer.mTrainCards.add(testTrainCard1);
		testPlayer.mTrainCards.add(testTrainCard2);
		
		List<ClientTrainCard> trainCards = testPlayer.getTrainCards();
		assertEquals((trainCards.get(0)).getTrainType(), TrainType.Locomotive);
		assertEquals((trainCards.get(1)).getTrainType(), TrainType.Reefer);
	}

	@Test
	void setTrainCards() {
		ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
		ClientTrainCard testTrainCard2 = new ClientTrainCard(TrainType.Reefer);
		List<ClientTrainCard> testTrainCards = new ArrayList<>();
		testTrainCards.add(testTrainCard1);
		testTrainCards.add(testTrainCard2);
		
		testPlayer.setTrainCards(testTrainCards);
		assertEquals(2, testPlayer.mTrainCards.size());
	}

	@Test
	void getTrainsRemaining() {
		testPlayer.mTrainsRemaining = 42;
		assertEquals(42, testPlayer.getTrainsRemaining());
		
		testPlayer.mTrainsRemaining = 5;
		assertEquals(5, testPlayer.getTrainsRemaining());
	}

	@Test
	void setTrainsRemaining() {
		testPlayer.setTrainsRemaining(42);
		assertEquals(42, testPlayer.mTrainsRemaining);
		
		testPlayer.setTrainsRemaining(5);
		assertEquals(5, testPlayer.getTrainsRemaining());
	}

	@Test
	void getScore() {
		testPlayer.mScore = 64;
		assertEquals(64, testPlayer.getScore());
		
		testPlayer.mScore = 40;
		assertEquals(40, testPlayer.getScore());
	}

	@Test
	void setScore() {
		testPlayer.setScore(64);
		assertEquals(64, testPlayer.mScore);
		
		testPlayer.setScore(40);
		assertEquals(40, testPlayer.mScore);
	}
}