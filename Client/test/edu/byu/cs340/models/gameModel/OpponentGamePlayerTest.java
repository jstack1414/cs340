package edu.byu.cs340.models.gameModel;

import edu.byu.cs340.models.gameModel.bank.ClientDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ClientTrainCard;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.TrainType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Rebekah on 3/25/2017.
 */
class OpponentGamePlayerTest {
    OpponentGamePlayer testPlayer;
    String mPlayerName = "Cassandra";
    @BeforeEach
    void setUp() {
        testPlayer = new OpponentGamePlayer(mPlayerName);
    }

    @AfterEach
    void tearDown() {
        testPlayer = null;
    }

    @Test
    void getDestinationCardsNum() {
        ICity[] cities1 = {new ClientCity("Paris"), new ClientCity("Lyon")};
        ICity[] cities2 = {new ClientCity("London"), new ClientCity("Edinborough")};
        List<ClientDestinationCard> destCards = new ArrayList<>();
        destCards.add(new ClientDestinationCard("testDestCard1", cities1, 50));
        destCards.add(new ClientDestinationCard("testDestCard2", cities2, 30));
        testPlayer.setDestinationCards(destCards);
        assertEquals(2, testPlayer.mDestinationCards);
        assertEquals(2, testPlayer.getDestinationCardsNum());
    }

    @Test
    void setDestinationCards() {
        ICity[] cities1 = {new ClientCity("Paris"), new ClientCity("Lyon")};
        ICity[] cities2 = {new ClientCity("London"), new ClientCity("Edinborough")};
        List<ClientDestinationCard> destCards = new ArrayList<>();
        destCards.add(new ClientDestinationCard("testDestCard1", cities1, 50));
        destCards.add(new ClientDestinationCard("testDestCard2", cities2, 30));

        testPlayer.setDestinationCards(destCards);
        assertEquals(2, testPlayer.mDestinationCards);
        assertEquals(2, testPlayer.getDestinationCardsNum());

        testPlayer.addDestinationCards(destCards);
        assertEquals(4, testPlayer.mDestinationCards);
        assertEquals(4, testPlayer.getDestinationCardsNum());
    }

    @Test
    void getTrainCardsNum() {
        ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
        ClientTrainCard testTrainCard2 = new ClientTrainCard(TrainType.Reefer);
        List<ClientTrainCard> testTrainCards = new ArrayList<>();
        testTrainCards.add(testTrainCard1);
        testTrainCards.add(testTrainCard2);

        testPlayer.setTrainCards(testTrainCards);
        assertEquals(2, testPlayer.mTrainCards);
        assertEquals(2, testPlayer.getTrainCardsNum());
    }

    @Test
    void addTrainCard() {
        ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
        testPlayer.addTrainCard(testTrainCard1);
        assertEquals(testPlayer.getTrainCardsNum(), 1);

        ClientTrainCard testTrainCard2 = new ClientTrainCard(TrainType.Reefer);
        testPlayer.addTrainCard(testTrainCard2);
        assertEquals(testPlayer.getTrainCardsNum(), 2);
    }

    @Test
    void removeTrainCard() {
        addTrainCard();

        testPlayer.removeTrainCard(null);
        assertEquals(testPlayer.getTrainCardsNum(), 1);

        ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
        testPlayer.removeTrainCard( testTrainCard1);
        assertEquals(testPlayer.getTrainCardsNum(), 0);

        //this could be a problem, but it shouldn't happen because on a player's turn they are not an opponent player
        //and local players can not have negative cards, because they use a list.
        testPlayer.removeTrainCard(null);
        assertEquals((-1), testPlayer.getTrainCardsNum());
    }

    @Test
    void addDestinationCards() {
        ICity[] cities1 = {new ClientCity("Paris"), new ClientCity("Lyon")};
        ICity[] cities2 = {new ClientCity("London"), new ClientCity("Edinborough")};
        ClientDestinationCard testDestCard1 = new ClientDestinationCard("testDestCard1", cities1, 50);
        ClientDestinationCard testDestCard2 = new ClientDestinationCard("testDestCard2", cities2, 30);
        List<ClientDestinationCard> destCards = new ArrayList<>();
        destCards.add(testDestCard1);
        destCards.add(testDestCard2);

        testPlayer.addDestinationCards(destCards);
        assertEquals(2, testPlayer.getDestinationCardsNum());
    }

    @Test
    void setTrainCards() {
        ClientTrainCard testTrainCard1 = new ClientTrainCard(TrainType.Locomotive);
        ClientTrainCard testTrainCard2 = new ClientTrainCard(TrainType.Reefer);
        List<ClientTrainCard> testTrainCards = new ArrayList<>();
        testTrainCards.add(testTrainCard1);
        testTrainCards.add(testTrainCard2);

        testPlayer.setTrainCards(testTrainCards);
        assertEquals(2, testPlayer.mTrainCards);
        assertEquals(2, testPlayer.getTrainCardsNum());
    }

    void getTrainsRemaining() {
        testPlayer.mTrainsRemaining = 42;
        assertEquals(42, testPlayer.getTrainsRemaining());

        testPlayer.mTrainsRemaining = 5;
        assertEquals(5, testPlayer.getTrainsRemaining());
    }

    @Test
    void setTrainsRemaining() {
        testPlayer.setTrainsRemaining(42);
        assertEquals(42, testPlayer.mTrainsRemaining);

        testPlayer.setTrainsRemaining(5);
        assertEquals(5, testPlayer.getTrainsRemaining());
    }

    @Test
    void getScore() {
        testPlayer.mScore = 64;
        assertEquals(64, testPlayer.getScore());

        testPlayer.mScore = 40;
        assertEquals(40, testPlayer.getScore());
    }

    @Test
    void setScore() {
        testPlayer.setScore(64);
        assertEquals(64, testPlayer.mScore);

        testPlayer.setScore(40);
        assertEquals(40, testPlayer.mScore);
    }

    @Test
    void getUsername() {
        assertEquals(mPlayerName, testPlayer.getUsername());
    }

    @Test
    void isHost() {
        assertEquals(false,testPlayer.isHost());
        testPlayer.setIsHost(true);
        assertEquals(true,testPlayer.isHost());
    }

    @Test
    void setIsHost() {
        assertEquals(false,testPlayer.isHost());
        testPlayer.setIsHost(true);
        assertEquals(true,testPlayer.isHost());
        testPlayer.setIsHost(false);
        assertEquals(false, testPlayer.isHost());
    }

}