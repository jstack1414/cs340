package edu.byu.cs340.models.state;

import edu.byu.cs340.clientFacade.Event.Event;
import edu.byu.cs340.clientFacade.Event.EventKeys;
import edu.byu.cs340.clientFacade.Event.EventType;
import edu.byu.cs340.models.ClientModelManager;
import edu.byu.cs340.models.ClientUser;
import edu.byu.cs340.models.state.game.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by joews on 3/23/2017.
 * Test for the states
 */
class StateTest
{

	private static String username = "testName";
	private static String otherUsername = "otherName";

	@BeforeAll
	public static void initialSetup()
	{
		ClientModelManager.getInstance().setUser(new ClientUser(username, null));
	}

	@BeforeEach
	public void setup()
	{
		//not needed currently
	}

	@Test
	public void testLoginState()
	{
		IState state = new LoginState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test login working
		Event event = new Event(EventType.CMDLogin);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof JoinGameState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testJoinGameState()
	{
		IState state = new JoinGameState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test joining game
		Event event = new Event(EventType.CMDJoinGame);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof LobbyState);

		ClientModelManager.getInstance().setState(state);
		//test creating a game
		event = new Event(EventType.CMDCreateLobby);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof LobbyState);

		ClientModelManager.getInstance().setState(state);
		//test what happens when the game list updates (should state on join game state)
		event = new Event(EventType.CMDGameListUpdated);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof JoinGameState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testClaimRouteState()
	{
		IState state = new ClaimRouteState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test claiming the route
		Event event = new Event(EventType.CMDClaimRoute);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testDrawDestinationCardsState()
	{
		IState state = new DrawDestinationCardState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test acctually drawing
		Event event = new Event(EventType.CMDDrawDestinationCards);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof ReturnDestinationCardState);


		//TODO: Test invalid inputs
	}

	@Test
	public void testDrawFirstTrainCardState()
	{
		IState state = new DrawFirstTrainCardState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test drawing the card for an opponent
		Event event = new Event(EventType.CMDDrawTrainCard).put(EventKeys.PLAYER_NAME, otherUsername);
		ClientModelManager.getInstance().processStateEvent(event);
		assertFalse(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);
		assertFalse(ClientModelManager.getInstance().getState() instanceof DrawSecondTrainCardState);
		assertTrue(ClientModelManager.getInstance().getState() instanceof DrawFirstTrainCardState);

		ClientModelManager.getInstance().setState(state);
		//test drawing card for me that isn't a locomotive
		event = new Event(EventType.CMDDrawTrainCard)
				.put(EventKeys.PLAYER_NAME, username)
				.put(EventKeys.CAN_DRAW_ANOTHER, true);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof DrawSecondTrainCardState);

		ClientModelManager.getInstance().setState(state);
		//test drawing card for me that is a locomotive
		event = new Event(EventType.CMDDrawTrainCard)
				.put(EventKeys.PLAYER_NAME, username)
				.put(EventKeys.CAN_DRAW_ANOTHER, false);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testDrawSecondTrainCardState()
	{
		IState state = new DrawSecondTrainCardState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test drawing the card for an opponent
		Event event = new Event(EventType.CMDDrawTrainCard).put(EventKeys.PLAYER_NAME, otherUsername);
		ClientModelManager.getInstance().processStateEvent(event);
		assertFalse(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);
		assertTrue(ClientModelManager.getInstance().getState() instanceof DrawSecondTrainCardState);

		ClientModelManager.getInstance().setState(state);
		//test drawing card for me
		event = new Event(EventType.CMDDrawTrainCard).put(EventKeys.PLAYER_NAME, username);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testGameOverState()
	{
		IState state = new GameOverState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test if we can return the Join Game state
		Event event = new Event(EventType.CMDLeaveGame);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof JoinGameState);

		ClientModelManager.getInstance().setState(state);
		//test if we can return the Join Game state
		event = new Event(EventType.UILeaveGame);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof JoinGameState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testInitialTurnDrawDestinationCardsState()
	{
		IState state = new InitialTurnDrawDestinationCards();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test if the cards are drawn
		Event event = new Event(EventType.CMDDrawDestinationCards);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof InitialTurnReturnDestinationCardsState);

		//TODO: Test invalid inputs
	}

	@Test
	public void testInitialTurnReturnDestinationCardsState()
	{
		IState state = new InitialTurnReturnDestinationCardsState();

		//TEST VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test if the scmd is mine to return the cards
		Event event = new Event(EventType.CMDReturnDestinationCards).put(EventKeys.PLAYER_NAME, username);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);


		//TODO: Test invalid inputs
		ClientModelManager.getInstance().setState(state);
		//test if the scmd is mine to return the cards
		event = new Event(EventType.CMDReturnDestinationCards).put(EventKeys.PLAYER_NAME, otherUsername);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof InitialTurnReturnDestinationCardsState);
		assertFalse(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);
	}

	@Test
	public void testNotMyTurnState()
	{
		IState state = new NotMyTurnState();

		//TEST VALID INPUTS:
		ClientModelManager.getInstance().setState(state);
		//test to see if it changes to the players turn when there name is passed
		Event event = new Event(EventType.CMDNextPlayerTurn).put(EventKeys.PLAYER_NAME, username);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof PickActionState);

		ClientModelManager.getInstance().setState(state);
		//test to see if it does not change when its another player
		event = new Event(EventType.CMDNextPlayerTurn).put(EventKeys.PLAYER_NAME, otherUsername);
		ClientModelManager.getInstance().processStateEvent(event);
		assertFalse(ClientModelManager.getInstance().getState() instanceof PickActionState);
		assertTrue(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);

		//TODO: Test invalid inputs
		//test drawing cards, claimng route, etc
	}

	@Test
	public void testPickActionState()
	{
		IState state = new PickActionState();

		//TEST VALID INPUTS:
		ClientModelManager.getInstance().setState(state);
		//test to see if the state changes to draw train card state
		Event event = new Event(EventType.UIDrawTrainCardsPicked);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof DrawTrainCardState);

		ClientModelManager.getInstance().setState(state);
		//test to see if the state changes to draw destination card state
		event = new Event(EventType.UIDrawDestinationCardsPicked);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof DrawDestinationCardState);

		ClientModelManager.getInstance().setState(state);
		//test to see if the state changes to claim route card state
		event = new Event(EventType.UIClaimRoutePicked);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof ClaimRouteState);

		//TODO: TEST INVALID INPUTS:
		//test drawing second train card

	}

	@Test
	public void testReturnDestinationCardState()
	{
		IState state = new ReturnDestinationCardState();

		//TEXT VALID INPUTS
		ClientModelManager.getInstance().setState(state);
		//test to see what happens on return card
		Event event = new Event(EventType.CMDReturnDestinationCards).put(EventKeys.PLAYER_NAME, username);
		ClientModelManager.getInstance().processStateEvent(event);
		assertTrue(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);


		ClientModelManager.getInstance().setState(state);
		//test to see what happens on return card if it is an opponent
		event = new Event(EventType.CMDReturnDestinationCards).put(EventKeys.PLAYER_NAME, otherUsername);
		ClientModelManager.getInstance().processStateEvent(event);
		assertFalse(ClientModelManager.getInstance().getState() instanceof NotMyTurnState);
		assertTrue(ClientModelManager.getInstance().getState() instanceof ReturnDestinationCardState);

		//Todo: TEST INVALID IMNPUTS

	}

}