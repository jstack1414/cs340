package edu.byu.cs340.communication;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.message.IMessage;

/**
 * Created by joews on 2/1/2017.
 *
 * Interface for server
 */
public interface IServer {
	/**
	 * Method that executes command on server.
	 *
	 * @param baseCommand Command to execute
	 * @return IMessage specifying result
	 *
	 * @throws IBaseCommand.CommandException thrown with error and holds error message and error code.
	 */
	IMessage executeCommand(IBaseCommand baseCommand) throws IBaseCommand.CommandException;
}
