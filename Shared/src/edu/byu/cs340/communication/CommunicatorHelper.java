package edu.byu.cs340.communication;

import java.io.*;

/**
 * Created by Rebekah on 1/30/2017.
 */
public class CommunicatorHelper {

    public static final String AUTHORIZATION_HEADER_STRING = "Authorization";

    public static String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    public static void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
        sw.close();
        os.close();
    }
}
