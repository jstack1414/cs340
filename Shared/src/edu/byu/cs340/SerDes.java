package edu.byu.cs340;

import com.google.gson.*;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.message.ChatPostMessage;
import edu.byu.cs340.message.ChatPullMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.bank.TrainCard;
import edu.byu.cs340.models.gameModel.gameOver.GameOverData;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.models.gameModel.map.City;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.gameModel.map.Route;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.roomModel.*;

import java.lang.reflect.Type;

/**
 * Created by joews on 1/19/2017.
 * Functionality to serialize and deserialize to and from BaseCommand to a json string.
 */
public class SerDes
{
	public static String serializeObject(Object obj, Source source)
	{
		Gson gson = getGSON(source);
		return gson.toJson(obj);
	}

	public static Object deserializeObject(String jsonString, Source source) throws
			ClassNotFoundException
	{
		if (jsonString == null || jsonString.isEmpty() || jsonString.equalsIgnoreCase("null"))
		{
			return null;
		}
		JsonObject jsonObject = getGSON(source).fromJson(jsonString, JsonObject.class);
		//Get the commands inherited type for rebuilding
		String classRoot = jsonObject.get("mClassName").getAsString();
		String pkg = jsonObject.get("mClassPackage").getAsString();
		Class c = Class.forName(pkg + "." + source.getName() + classRoot);//reflection
		//create the command and execute it
		return getGSON(source).fromJson(jsonString, c);
	}

	public enum Source
	{
		SERVER("Server"), CLIENT("Client");

		private String name;

		Source(String name)
		{
			this.name = name;
		}

		/**
		 * Get value of Name
		 *
		 * @return Returns the value of Name
		 */
		public String getName()
		{
			return name;
		}
	}

	/**
	 * Converts a BaseCommand Object into a jsonString representing the instance
	 *
	 * @param command Command to serialize
	 * @return Returns a string of the deserialize command
	 */
	public synchronized static String serializeCommand(IBaseCommand command, Source source)
	{//serialize the gson object and add name property
		Gson gson = getGSON(source);
		return gson.toJson(command);
	}

	/**
	 * serializes a chat message
	 *
	 * @param chat
	 * @param source
	 * @return
	 */
	public synchronized static String serializeChat(ChatPostMessage chat, Source source)
	{//serialize the gson object and add name property
		Gson gson = getGSON(source);
		return gson.toJson(chat);
	}

	/**
	 * serializes a chat message
	 *
	 * @param chat
	 * @param source
	 * @return
	 */
	public synchronized static String serializeChat(ChatPullMessage chat, Source source)
	{//serialize the gson object and add name property
		Gson gson = getGSON(source);
		return gson.toJson(chat);
	}

	/**
	 * Converts a jsonString into a Command.
	 *
	 * @param jsonString The string to deserialize
	 * @param source     A string consisting of the "pkg.source", where pkg is the package name (typically command) and
	 *                   where source is either "Client" or "Source" depending which object type the jsonString is
	 *                   to be
	 *                   serialized into.
	 * @return Returns the BaseCommand
	 *
	 * @throws ClassNotFoundException Throw exception if it can not deserialize into class of type mClassName
	 * @pre jsonString is of type IBaseCommand and has mClassName properly associated with object
	 * @pre source == "Client" or "Server"
	 * @pre jstonString has mClassName that coresponds to a command object implemented on server/client as specified by
	 * source
	 */
	public synchronized static IBaseCommand deserializeCommand(String jsonString, Source source) throws
			ClassNotFoundException
	{
		if (jsonString == null || jsonString.isEmpty() || jsonString.equalsIgnoreCase("null"))
		{
			return null;
		}
		JsonObject jsonObject = getGSON(source).fromJson(jsonString, JsonObject.class);
		//Get the commands inherited type for rebuilding
		String classRoot = jsonObject.get("mClassName").getAsString();
		String pkg = jsonObject.get("mClassPackage").getAsString();
		Class c = Class.forName(pkg + "." + source.getName() + classRoot);//reflection
		//create the command and execute it
		return (IBaseCommand) getGSON(source).fromJson(jsonString, c);
	}


	public synchronized static String serializeMessage(IMessage result, Source source)
	{
		Gson gson = getGSON(source);
		return gson.toJson(result);
	}

	/**
	 * Deserializes a message from the json string
	 *
	 * @param jsonString Json string to deserialize
	 * @return returns an IMessage of apprpriate type based on variable "mClassName"
	 *
	 * @throws ClassNotFoundException If mClassName specifies the wrong message type, throws exception
	 * @pre jsonString is the string for an IMessage object that has mClassName as a final static String.
	 */
	public synchronized static IMessage deserializeMessage(String jsonString, Source source) throws
			ClassNotFoundException
	{
		if (jsonString == null || jsonString.isEmpty() || jsonString.equalsIgnoreCase("null"))
		{
			return null;
		}
		JsonObject jsonObject = getGSON(source).fromJson(jsonString, JsonObject.class);
		//Get the commands inherited type for rebuilding
		String classRoot = jsonObject.get("mClassName").getAsString();
		String pkg = jsonObject.get("mClassPackage").getAsString();
		Class c = Class.forName(pkg + "." + classRoot);//reflection
		//create the command and execute it
		return (IMessage) new Gson().fromJson(jsonString, c);
	}

	/**
	 * deserialize chat
	 *
	 * @param jsonString
	 * @param source
	 * @return
	 *
	 * @throws ClassNotFoundException
	 */
	public synchronized static Object deserializeChat(String jsonString, Source source) throws ClassNotFoundException
	{
		if (jsonString == null || jsonString.isEmpty() || jsonString.equalsIgnoreCase("null"))
		{
			return null;
		}
		JsonObject jsonObject = getGSON(source).fromJson(jsonString, JsonObject.class);
		String classRoot = jsonObject.get("mClassName").getAsString();
		String pkg = jsonObject.get("mClassPackage").getAsString();
		Class c = Class.forName(pkg + "." + classRoot);//reflection

		return new Gson().fromJson(jsonString, c);
	}

	/**
	 * Creates a GSON object that can recognize and deserialize objects based on interface-abstract class pairs.
	 * This funciton contains all of the InterfaceAdapters that GSON needs to deserialize for the applicaiton.
	 * When new funcitnality is neded add another .registerTypeAdapter line.
	 *
	 * @return
	 *
	 * @pre this will be called and used only on objects that contain default java objects (Strings, primitives,
	 * etc) or
	 * actual objects. IF it contains interfaces then the appropriate interface abstract class is added as an adapter.
	 * @pre if this is used for deserializeation the message sent must also use this
	 * @post returns a Gson object able to serialize/deserialize an object that meets the pre conditions.
	 */
	public synchronized static Gson getGSON(Source src)
	{
		GsonBuilder gsonBuilder = new GsonBuilder();
		if (src == Source.CLIENT)
		{
			gsonBuilder.registerTypeAdapter(IRoomsListInfo.class, new ClientInterfaceAdapter<RoomsListInfo>())
					.registerTypeAdapter(IRoomInfo.class, new ClientInterfaceAdapter<RoomInfo>())
					.registerTypeAdapter(IPlayer.class, new ClientInterfaceAdapter<Player>())
					.registerTypeAdapter(ICity.class, new ClientInterfaceAdapter<City>())
					.registerTypeAdapter(IRoute.class, new ClientInterfaceAdapter<Route>())
					.registerTypeAdapter(LobbyModel.class, new ClientInterfaceAdapter<LobbyModel>())
					.registerTypeAdapter(IDestinationCard.class, new ClientInterfaceAdapter<DestinationCard>())
					.registerTypeAdapter(IGameOverData.class, new ClientInterfaceAdapter<GameOverData>())
					.registerTypeAdapter(ITrainCard.class, new ClientInterfaceAdapter<TrainCard>());
		} else
		{
			gsonBuilder.registerTypeAdapter(IRoomsListInfo.class, new ServerInterfaceAdapter<RoomsListInfo>())
					.registerTypeAdapter(IRoomInfo.class, new ServerInterfaceAdapter<RoomInfo>())
					.registerTypeAdapter(IPlayer.class, new ServerInterfaceAdapter<Player>())
					.registerTypeAdapter(ICity.class, new ServerInterfaceAdapter<City>())
					.registerTypeAdapter(IRoute.class, new ServerInterfaceAdapter<Route>())
					.registerTypeAdapter(LobbyModel.class, new ServerInterfaceAdapter<LobbyModel>())
					.registerTypeAdapter(IDestinationCard.class, new ServerInterfaceAdapter<DestinationCard>())
					.registerTypeAdapter(IGameOverData.class, new ServerInterfaceAdapter<GameOverData>())
					.registerTypeAdapter(ITrainCard.class, new ServerInterfaceAdapter<TrainCard>());
		}

		return gsonBuilder.create();
	}


	/**
	 * Generic Adapter for serializing and deserializing interfaces into an appropriate subclass.
	 *
	 * This Adapter will create an instance of object of type T when deserializing a variable of a specified interface
	 * in GSON. It also has functinality to create the object as a subclass of T based on the name of the class. For
	 * example If an object J holds a list of objects that inherit an interface IL and extend a calss L, then pretend
	 * this list has two objects one which is type M which extends L and another type N which extends L. If when the
	 * Gson object is created, the builder is given .registerTypeAdapter(IL.class, new InterfaceAdapter\<\L\>()), the
	 * serializtion will store the class type (M or N) with the key "type". When it is then deserialized, it will see
	 * that the object is of the interface IL, and attempt to convert it into an Object L. It will see the "type"
	 * tag of M or N and and get the type of M or N. Then it will deserialize the mData into an object of type M or N
	 * and
	 * return it cast as type L. This ofcourse will go into the new object of list\<\IL\> and all functionality should
	 * remain across serializtion.
	 *
	 * @param <T>
	 */
	public abstract static class InterfaceAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T>
	{


		private final static String CLASSNAME = "THECLASSNAME";
		private final static String DATA = "THEDATA";
		private final static String PREFIX_HOLDER = "PREFIXHOLDER";

		protected abstract String getPrefix();

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Serializes the object and stores its class type.
		 */
		public JsonElement serialize(T object, Type interfaceType, JsonSerializationContext context)
		{
			final JsonObject wrapper = new JsonObject();
			wrapper.addProperty(CLASSNAME, getObjectClassName(object));
			wrapper.add(DATA, context.serialize(object));
			return wrapper;
		}

		public String getObjectClassName(T object)
		{
			String className = object.getClass().getName();
			if (getPrefix() != null && !getPrefix().isEmpty())
			{
				return className.replaceAll(getPrefix(), PREFIX_HOLDER);
			} else
			{
				return className;
			}
		}

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Deserializes the object based on the type stored in the Json.
		 */
		public T deserialize(JsonElement elem, Type interfaceType, JsonDeserializationContext context) throws
				JsonParseException
		{
			JsonObject wrapper = null;
			try
			{
				wrapper = (JsonObject) elem;
			} catch (Exception e)
			{
				return (T) new City("blah");
//				e.printStackTrace();
			}
			final JsonElement typeName = get(wrapper, CLASSNAME);
			final JsonElement data = get(wrapper, DATA);
			final Type actualType = typeForName(typeName);
			return context.deserialize(data, actualType);

		}

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Attempts to get the class type from the Json string.
		 */
		private Type typeForName(final JsonElement typeElem)
		{
			try
			{
				String className = typeElem.getAsString();
				className = className.replaceAll(PREFIX_HOLDER, getPrefix());
				return Class.forName(className);
			} catch (ClassNotFoundException e)
			{
				throw new JsonParseException(e);
			}
		}

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Gets the json element associated with the wrapper. This will either be the class type or the mData.
		 */
		private JsonElement get(final JsonObject wrapper, String memberName)
		{
			final JsonElement elem = wrapper.get(memberName);
			if (elem == null)
				throw new JsonParseException("no '" + memberName + "' member found in what was expected to be an " +
						"interface wrapper");
			return elem;
		}
	}

	public static class ClientInterfaceAdapter<T> extends InterfaceAdapter<T>
	{

		private final static String Prefix = Source.CLIENT.getName();

		@Override
		protected String getPrefix()
		{
			return Prefix;
		}
	}

	public static class ServerInterfaceAdapter<T> extends InterfaceAdapter<T>
	{

		private final static String Prefix = Source.SERVER.getName();

		@Override
		protected String getPrefix()
		{
			return Prefix;
		}
	}

}
