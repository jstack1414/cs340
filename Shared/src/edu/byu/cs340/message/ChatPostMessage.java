package edu.byu.cs340.message;

import edu.byu.cs340.models.ChatMessage;

/**
 * Created by Rebekah on 2/18/2017.
 */
public class ChatPostMessage extends BaseMessage
{
	protected ChatMessage mChatMessage;
	protected String mRoomId;

	public ChatPostMessage(ChatMessage chatMessage, String roomId)
	{
		mChatMessage = chatMessage;
		mRoomId = roomId;
	}

	/**
	 * Get value of ChatMessage
	 *
	 * @return Returns the value of ChatMessage
	 */
	public ChatMessage getChatMessage()
	{
		return mChatMessage;
	}

	/**
	 * Set the value of chatMessage variable
	 *
	 * @param chatMessage The value to set chatMessage member variable to
	 */
	public void setChatMessage(ChatMessage chatMessage)
	{
		mChatMessage = chatMessage;
	}

	/**
	 * Get value of RoomId
	 *
	 * @return Returns the value of RoomId
	 */
	public String getRoomId()
	{
		return mRoomId;
	}

	/**
	 * Set the value of roomId variable
	 *
	 * @param roomId The value to set roomId member variable to
	 */
	public void setRoomId(String roomId)
	{
		mRoomId = roomId;
	}
}
