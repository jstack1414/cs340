package edu.byu.cs340.message;

/**
 * Created by joews on 2/1/2017.
 * A message containing a single command.
 */

public class CommandMessage extends BaseMessage
{
	public String cmd;

	public CommandMessage(String cmd)
	{
		this.cmd = cmd;
	}

	/**
	 * Get value of Cmd
	 *
	 * @return Returns the value of Cmd
	 */
	public String getCmd()
	{
		return cmd;
	}
}
