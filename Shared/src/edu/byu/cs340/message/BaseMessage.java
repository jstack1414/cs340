package edu.byu.cs340.message;

import edu.byu.cs340.command.IBaseCommand;

/**
 * Created by joews on 2/5/2017.
 *
 * Base message type.
 */
public abstract class BaseMessage implements IMessage
{
	/**
	 * Gets the class name for serializetion.
	 */
	protected final String mClassName = getClass().getSimpleName();
	/**
	 * gets the package name for serialization
	 */
	protected final String mClassPackage = getClass().getPackage().getName();

	public <T extends IBaseCommand> BaseMessage()
	{
	}


}
