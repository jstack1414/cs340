package edu.byu.cs340.message;

/**
 * Created by joews on 2/1/2017.
 *
 * Message specifying an error
 */
public class ErrorMessage extends BaseMessage
{
	/**
	 * The message of the error.
	 */
	protected String mMessage;
	/**
	 * The error code associated with the message.
	 */
	protected int mErrorCode;

	public ErrorMessage(String message, int errorCode)
	{
		this.mMessage = message;
		this.mErrorCode = errorCode;
	}

	/**
	 * Get value of Message
	 *
	 * @return Returns the value of Message
	 */
	public String getMessage()
	{
		return this.mMessage;
	}

	/**
	 * Set the value of message variable
	 *
	 * @param message The value to set message member variable to
	 */
	public void setMessage(String message)
	{
		this.mMessage = message;
	}

	/**
	 * Get value of ErrorCode
	 *
	 * @return Returns the value of ErrorCode
	 */
	public int getErrorCode()
	{
		return mErrorCode;
	}

	//<editor-fold desc="Error Codes">
	/**
	 * LIST ALL ERROR CODES HERE
	 */
	public final static int MILD_ERROR = 100;
	public final static int MEDIUM_ERROR = 200;
	public final static int FATAL_ERROR = 300;
	public final static int LOBBY_NOT_FOUND = 260;
	public final static int PLAYER_NOT_IN_ROOM = 261;
	public static final int NO_CARDS_IN_DEST_DECK = 131;
	public static final int AUTHORIZATION_ERROR = 321;
	public static final int NULL_ROOM = 151;
	//</editor-fold>


}
