package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.BaseCommand;

/**
 * Created by joews on 4/4/2017.
 */
public class AmInGameRequest extends BaseCommand
{
	public AmInGameRequest(String username)
	{
		super(username);
	}
}
