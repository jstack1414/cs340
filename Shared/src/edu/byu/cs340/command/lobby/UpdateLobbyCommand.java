package edu.byu.cs340.command.lobby;

/**
 * Created by Sean on 2/8/2017.
 * Base command mData for getting an update on the lobby
 */
public class UpdateLobbyCommand extends BaseLobbyCommand
{
	public UpdateLobbyCommand(String username, int lastCommand, String lobbyID)
	{
		super(username, lastCommand, lobbyID);
	}
}
