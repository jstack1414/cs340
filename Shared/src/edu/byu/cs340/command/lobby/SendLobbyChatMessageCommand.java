package edu.byu.cs340.command.lobby;


/**
 * Created by Sean on 2/10/2017.
 *
 * Base command mData for sending a lobby chat message
 */
public class SendLobbyChatMessageCommand extends BaseLobbyCommand
{
	String mMessage;

	public SendLobbyChatMessageCommand(String username, int lastCommand, String lobbyID, String message)
	{
		super(username, lastCommand, lobbyID);
		mMessage = message;
	}
}
