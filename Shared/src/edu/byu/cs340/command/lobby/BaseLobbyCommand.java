package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.BaseRoomCommand;

/**
 * Created by joews on 2/1/2017.
 *
 * base command functionality for lobbiese
 */
public abstract class BaseLobbyCommand extends BaseRoomCommand
{
	protected String mLobbyID;

	public BaseLobbyCommand(String username, int lastCommand, String lobbyID)
	{
		super(username, lastCommand);
		mLobbyID = lobbyID;
	}

	public BaseLobbyCommand(BaseLobbyCommand that)
	{
		super(that);
		mLobbyID = that.mLobbyID;
	}

	/**
	 * Get value of LobbyID
	 *
	 * @return Returns the value of LobbyID
	 */
	public String getLobbyID()
	{
		return mLobbyID;
	}

	/**
	 * Set the value of lobbyID variable
	 *
	 * @param lobbyID The value to set lobbyID member variable to
	 */
	public void setLobbyID(String lobbyID)
	{
		mLobbyID = lobbyID;
	}

}
