package edu.byu.cs340.command.lobby;

import edu.byu.cs340.models.roomModel.IPlayer;

/**
 * Created by Sean on 2/4/2017.
 * Base command mData for joining a lobby
 *
 */
public class JoinLobbyCommand extends BaseLobbyCommand
{
	protected IPlayer mJoiningPlayer;
	protected String mLobbyName;

	public JoinLobbyCommand(String username, String lobbyID, IPlayer joiningPlayer)
	{
		super(username, -1, lobbyID);
		mJoiningPlayer = joiningPlayer;
	}
}
