package edu.byu.cs340.command.lobby;

/**
 * Created by joews on 2/5/2017.
 *
 * Base command mData for creating a lobby.
 */
public class CreateLobbyCommand extends BaseLobbyCommand
{
	protected String mLobbyName;

	public CreateLobbyCommand(String username, String lobbyID, String lobbyName)
	{
		super(username, -1, lobbyID);
		mLobbyName = lobbyName;
	}
}
