package edu.byu.cs340.command;

import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;

/**
 * Created by joews on 2/6/2017.
 *
 * Base interface all commands inherit.
 */
public interface IBaseCommand extends Comparable<IBaseCommand>
{
	/**
	 * Allows execution of the command.
	 *
	 * @return IMessage with result.
	 *
	 * @throws IBaseCommand.CommandException exception with error message and error code.
	 */
	IMessage execute() throws IBaseCommand.CommandException;

	/**
	 * //Not implemented
	 * Undo commands
	 */
	void undo();

	/**
	 * //TODO: Not implemented yet
	 * Strip data from each command that is private so it isn't passed over the network to other users.
	 *
	 * @param userStripping Username who called strip.
	 */
	IBaseCommand strip(String userStripping);

	/**
	 * Get value of Username
	 *
	 * @return Returns the value of Username
	 */
	String getUsername();

	/**
	 * Set the value of username variable
	 *
	 * @param username The value to set username member variable to
	 */
	void setUsername(String username);

	/**
	 * returns the index of hte command according to what is holding it. -1 if not set.
	 *
	 * @return The index of the command (when it was executed)
	 */
	int getIndex();

	/**
	 * sets when the command was executed.
	 *
	 * @param index The index of the command.
	 */
	void setIndex(int index);

	class CommandException extends Exception
	{
		String exception = "";
		int errorCode = ErrorMessage.MILD_ERROR;

		public CommandException(String exception, int errorCode)
		{
			super(exception);
			this.exception = exception;
			this.errorCode = errorCode;
		}

		@Override
		public String getMessage()
		{
			return exception;
		}

		/**
		 * Get value of ErrorCode
		 *
		 * @return Returns the value of ErrorCode
		 */
		public int getErrorCode()
		{
			return errorCode;
		}
	}
}

