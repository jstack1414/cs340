package edu.byu.cs340.command;

import edu.byu.cs340.message.IMessage;

/**
 * Created by joews on 2/1/2017.
 *
 * Abstraction of IBase command specifying username.
 */
public abstract class BaseCommand implements IBaseCommand
{
	protected int mIndex = -1;
	protected String mUsername;

	/**
	 * Class name of the command
	 */
	protected final String mClassName = getClass().getSimpleName().replace("Server", "").replace("Client", "");
	/**
	 * Package directory
	 */
	protected final String mClassPackage = getClass().getPackage().getName();

	public BaseCommand(String username)
	{
		mUsername = username;
	}

	public BaseCommand(BaseCommand that)
	{
		mUsername = that.mUsername;
		mIndex = that.mIndex;
	}

	/**
	 * Executes the command.
	 */
	public IMessage execute() throws CommandException
	{
		return null;
	}

	public void undo()
	{

	}

	/**
	 * Get value of Username
	 *
	 * @return Returns the value of Username
	 */
	public String getUsername()
	{
		return mUsername;
	}

	/**
	 * Set the value of username variable
	 *
	 * @param username The value to set username member variable to
	 */
	public void setUsername(String username)
	{
		mUsername = username;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		return this;
	}

	/**
	 * Get value of Index
	 *
	 * @return Returns the value of Index
	 */
	@Override
	public int getIndex()
	{
		return mIndex;
	}

	/**
	 * Set the value of index variable
	 *
	 * @param index The value to set index member variable to
	 */
	@Override
	public void setIndex(int index)
	{
		mIndex = index;
	}

	@Override
	public int compareTo(IBaseCommand o)
	{
		Integer i = this.getIndex();
		return i.compareTo(o.getIndex());
	}
}

