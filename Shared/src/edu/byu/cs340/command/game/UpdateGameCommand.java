package edu.byu.cs340.command.game;

/**
 * Created by Sean on 2/8/2017.
 * Base command mData for getting an update on the lobby
 */
public class UpdateGameCommand extends BaseGameCommand
{
	public UpdateGameCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
	}
}
