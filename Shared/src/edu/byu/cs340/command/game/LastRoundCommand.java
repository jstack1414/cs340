package edu.byu.cs340.command.game;

/**
 * Created by crown on 3/24/2017.
 */
public class LastRoundCommand extends BaseGameCommand {
	public LastRoundCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	public LastRoundCommand(BaseGameCommand that) {
		super(that);
	}
}
