package edu.byu.cs340.command.game;

import edu.byu.cs340.models.gameModel.bank.ITrainCard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by crown on 3/18/2017.
 */
public class ClaimRouteCommand extends BaseGameCommand {
	protected String mRouteID;
	protected List<ITrainCard> mCardsUsed;
	protected List<ITrainCard> mRevealed;
	protected Map<String, Integer> mNewScores;
	protected int mNewNumTrains;
	protected int mNewTrainDiscardSize;
	protected Set<String> mUsersLongestRoute;
	protected int mLengthLongestRoute;
	protected int mPrivateScore;
	protected int mNewTrainDeckSize;
	
	public ClaimRouteCommand(String username, int lastCommand, String gameID, String routeID, List<ITrainCard> cardsUsed) {
		super(username, lastCommand, gameID);
		mRouteID = routeID;
		mCardsUsed = cardsUsed;
		mRevealed = new ArrayList<>();
	}

	public ClaimRouteCommand(ClaimRouteCommand that) {
		super(that);
		mRouteID = that.mRouteID;
		if(that.mCardsUsed != null){
			mCardsUsed = new ArrayList<>();
			for(ITrainCard card : that.mCardsUsed){
				mCardsUsed.add(card);
			}
		} else {
			mCardsUsed = null;
		}
		mNewScores = that.mNewScores;
		mNewNumTrains = that.mNewNumTrains;
		mNewTrainDiscardSize = that.mNewTrainDiscardSize;
		mRevealed = that.mRevealed;
		mUsersLongestRoute = that.mUsersLongestRoute;
		mLengthLongestRoute = that.mLengthLongestRoute;
		mPrivateScore = that.mPrivateScore;
		mNewTrainDeckSize = that.mNewTrainDeckSize;
	}
}
