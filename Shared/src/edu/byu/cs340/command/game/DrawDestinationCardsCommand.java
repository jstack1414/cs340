package edu.byu.cs340.command.game;

import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public class DrawDestinationCardsCommand extends BaseGameCommand
{
	protected List<IDestinationCard> mTemporaryCards;
	protected int mMaxCardsToReturn;

	public DrawDestinationCardsCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
		mMaxCardsToReturn = 0;
		mTemporaryCards = new ArrayList<>();
	}

	public DrawDestinationCardsCommand(DrawDestinationCardsCommand that)
	{
		super(that);
		if (that.mTemporaryCards != null)
		{
			mTemporaryCards = new ArrayList<>();
			for (IDestinationCard card : that.mTemporaryCards)
			{
				mTemporaryCards.add(new DestinationCard(card));
			}
		} else
		{
			mTemporaryCards = null;
		}
		this.mMaxCardsToReturn = that.mMaxCardsToReturn;
	}
}
