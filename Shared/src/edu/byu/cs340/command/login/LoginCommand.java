package edu.byu.cs340.command.login;

import edu.byu.cs340.command.IBaseCommand;

/**
 * Created by joews on 2/1/2017.
 *
 * Base command holding mData for login
 */
public class LoginCommand extends BaseLoginCommand
{

	public LoginCommand(String username, String password, String authenticationToken)
	{
		super(username, password, authenticationToken);
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
//		super.strip(userStripping);
//		if (userStripping != mUsername)
//		{
//			mAuthenticationToken = null;
//			mPassword = null;
//		}
		return this;
	}
}
