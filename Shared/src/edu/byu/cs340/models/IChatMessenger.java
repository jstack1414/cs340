package edu.byu.cs340.models;

import java.util.List;

/**
 * Created by Rebekah on 2/13/2017.
 * this interface is implemented by ChatMessenger
 * it handles communication between users in a lobby
 */
public interface IChatMessenger
{

	/**
	 * @param message the message to be sent
	 * @post the messge will be added to the list of messages in order of time sent
	 */
	void addChatMessage(ChatMessage message);

	/**
	 * @param lastMessage if lastmessage is null, all messages will be sent back. otherwise it sends back all the
	 *                    messages recived after the lastmessage
	 * @return returns a list of messges in order of timestamp
	 *
	 * @pre the constructor has been called for the class
	 * @post lastmessage will not be modified, only a list will be sent back.
	 */
	List<ChatMessage> getNewMessages(long lastMessage);

	long getLastIndex();


}
