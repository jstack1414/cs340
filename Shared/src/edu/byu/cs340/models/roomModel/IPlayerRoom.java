package edu.byu.cs340.models.roomModel;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.models.ChatMessage;

import java.util.List;

/**
 * Created by joews on 2/1/2017.
 *
 * IPlayerRoom handles the adding and removing of mPlayers from the room. This is to be inherited
 * by the lobby and game classes.
 */
public interface IPlayerRoom
{

	/**
	 * @param user username of player to add
	 * @return Returns true if the player is added.
	 *
	 * @pre you cannot add more than 5 players to a lobby or game Adds player to to the room with the given username
	 * @pre user can not be in room (getPlayer(user) == null)
	 */
	boolean addPlayer(String user);

	/**
	 * Removes player from the room.
	 *
	 * @param user Player to remove
	 * @return returns true if the user was removed, false otherwise.
	 *
	 * @pre user must be in room (getPlayer(user) != null)
	 */
	boolean removePlayer(String user);

	/**
	 * Returns a copy of the list of IPlayer's in the room.
	 *
	 * @return The list of IPlayers (empty if no players)
	 */
	List<IPlayer> getPlayers();

	/**
	 * Returns a copy of the player associated with the username
	 *
	 * @param username Username of player to get
	 * @return Returns IPlayer associated with username or null if not in room
	 */
	IPlayer getPlayer(String username);

	/**
	 * Get value of RoomName
	 *
	 * @return Returns the value of RoomName
	 */
	String getRoomName();

	/**
	 * Returns the Unique ID of the room.
	 *
	 * @return Room mId assocaited with this room
	 */
	String getRoomID();

	/**
	 * Returns true if the game has a player associated with the username
	 *
	 * @param username the name of the user in question
	 * @return a bool value indicating whether or not a player is associated with a user name
	 */
	boolean hasPlayer(String username);

	/**
	 * For use in removing empty rooms after removing the last player
	 *
	 * @return Returns whether there are any players in the room
	 */
	boolean isEmpty();


	/**
	 * Returns the index of the last command this room stored
	 */
	int getLastCommand();

	/**
	 * Returns the index of the last command this room stored for the player
	 *
	 * @param username User to query about
	 * @pre username!=null
	 */
	int getLastCommand(String username);

	/**
	 * Adds the given command to the list of commands in this room
	 */
	boolean addCommand(IBaseCommand command);

	/**
	 * @param lastCommand the index of the command from which to get the command list
	 * @return A list of IBaseCommand containing all commands executed in this room since lastCommand
	 */
	List<IBaseCommand> getRecentCommands(int lastCommand, String userName);

	/**
	 * this gets the number of players in this room
	 *
	 * @return the int number of players in the room
	 */
	int getNumPlayers();

	/**
	 * @return
	 */
	String getHost();

	/**
	 * gets the messages from the server
	 *
	 * @return a list of chatmessages from this lobby from the server
	 */
	List<ChatMessage> getChatMessages(long lastIndex);

	long getLastIndex();

	void addChatMessage(List<ChatMessage> message);

	void addChatMessage(ChatMessage message);



//	/**
//	 * Returns the version of the state.
//	 *
//	 * @return Retturns the state of the room.
//	 */
//	PlayerManager getPlayerManager();
//
//	/**
//	 * Sets the state
//	 *
//	 * @param state state to set
//	 */
//	void setPlayerManager(PlayerManager state);

}
