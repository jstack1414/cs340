package edu.byu.cs340.models.roomModel;

/**
 * Created by tjacobhi on 04-Feb-17.
 *
 * Player object holds a username nad whether the user is host.
 *
 * @invariant getUsername!=null
 */
public class Player implements IPlayer
{
	protected String mUsername;
	protected boolean isHost = false;

	/**
	 * Constructor for player.
	 *
	 * @param username Username of player
	 * @pre username!=null && username.isEmpty()==false
	 * @post getUsername().equals(username)
	 */
	public Player(String username)
	{
		this.mUsername = username;
	}

	/**
	 * Get value of the username.
	 *
	 * @return Returns the value of username.
	 *
	 * @post result!=null && result.isEmpty()==false
	 */
	public String getUsername()
	{
		return mUsername;
	}

	/**
	 * Gets whether the player is host.
	 *
	 * @return True if the player is host, false otherwise.
	 */
	@Override
	public boolean isHost()
	{
		return isHost;
	}

	/**
	 * Sets whether hte player is the host or not.
	 *
	 * @param isHost Set as treu if the player is host, false otherwise.
	 * @post isHost()==isHost
	 */
	@Override
	public void setIsHost(boolean isHost)
	{
		this.isHost = isHost;
	}
}

