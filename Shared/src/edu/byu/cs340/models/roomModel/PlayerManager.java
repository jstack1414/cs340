package edu.byu.cs340.models.roomModel;

import java.util.*;

/**
 * Created by joews on 2/22/2017.
 */
public abstract class PlayerManager
{
	protected Map<String, IPlayer> mPlayers;
	protected String mHostName;

	public PlayerManager()
	{
		this.mPlayers = new HashMap<>();
		this.mHostName = "";
	}

	public PlayerManager(String hostName)
	{
		mPlayers = new HashMap<>();
		addPlayer(hostName);
		mPlayers.get(hostName).setIsHost(true);
		setHost(hostName);
	}

	public boolean addPlayer(String playerName)
	{
		if (hasPlayer(playerName))
		{
			return false;
		} else
		{
			IPlayer player = createPlayer(playerName);
			if (player != null)
			{
				mPlayers.put(playerName, player);
				return true;
			} else
			{
				return false;
			}
		}
	}

	protected abstract IPlayer createPlayer(String playerName);

	public boolean removePlayer(String playerName)
	{
		if (hasPlayer(playerName))
		{
			mPlayers.remove(playerName);
			if (playerName.equals(mHostName) && !mPlayers.isEmpty())
			{
				String newHost = (String) mPlayers.keySet().toArray()[0];
				setHost(newHost);
			}
			return true;
		}
		return false;
	}

	public IPlayer getPlayer(String playerName)
	{
		if (!hasPlayer(playerName))
		{
			return null;
		} else
		{
			return mPlayers.get(playerName);
		}
	}

	public List<IPlayer> getPlayers()
	{
		List<IPlayer> players = new ArrayList<>(mPlayers.values());
		return Collections.unmodifiableList(players);
	}

	public boolean hasPlayer(String playerName)
	{
		return playerName != null && mPlayers.containsKey(playerName);
	}

	public int getNumPlayers()
	{
		return mPlayers.size();
	}

	public void setHost(String hostName)
	{
		if (mHostName != null)
		{
			IPlayer oldHost = mPlayers.get(mHostName);
			if (oldHost != null)
			{
				oldHost.setIsHost(false);
			}
		}
		mHostName = hostName;
		IPlayer newHost = mPlayers.get(hostName);
		if (newHost != null)
		{
			newHost.setIsHost(true);
		}

	}

	public String getHost()
	{
		return mHostName;
	}

	public boolean isEmpty()
	{
		return mPlayers.isEmpty();
	}


}
