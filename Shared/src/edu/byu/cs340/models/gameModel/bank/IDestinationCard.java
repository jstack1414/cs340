package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.ICity;

import java.util.List;

/**
 * Created by joews on 3/4/2017.
 */
public interface IDestinationCard extends ICard
{
	/**
	 * Get the mCities associated with the destination card. This result is unmodifiable.
	 *
	 * @return Returns the Cities as unmodifiable list
	 */
	List<ICity> getCities();

	/**
	 * Get value of Points.
	 *
	 * @return Returns the value of Points
	 */
	int getPoints();
}
