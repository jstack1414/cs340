package edu.byu.cs340.models.gameModel.bank;

import java.util.UUID;

/**
 * Created by joews on 2/22/2017.
 *
 */
public abstract class Card implements ICard
{
	protected String mId;

	/**
	 * Copy Constructor
	 *
	 * @param that Card to Copy.
	 * @pre that!=null
	 */
	public Card(Card that)
	{
		this.mId = that.mId;
	}

	/**
	 * Default constructor that creates the card and sets its ID.
	 */
	public Card()
	{
		mId = UUID.randomUUID().toString();
	}

	@Override
	public String getId()
	{
		return mId;
	}

	public void setId(String id)
	{
		mId = id;
	}

	/**
	 * Overide equal to compare cards by ID.
	 *
	 * @param obj Object to compare to.
	 * @return True if objects are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Card)
		{
			return ((Card) obj).getId().equals(this.mId);
		} else
		{
			return false;
		}
	}
}
