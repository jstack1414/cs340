package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.City;
import edu.byu.cs340.models.gameModel.map.ICity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by joews on 2/22/2017.
 *
 * Class that represents a destination card holding the cities and
 *
 * @invariant getCities().size == 2
 * @invariant getPoints()>=0
 */
public class DestinationCard extends Card implements IDestinationCard
{
	private ICity[] mCities;
	private int mPoints = 0;

	/**
	 * Copy constructor.
	 *
	 * @param that DestinationCard to copy.
	 * @pre that!=null
	 */
	public DestinationCard(IDestinationCard that)
	{
		super((DestinationCard) that);
		this.mPoints = that.getPoints();
		ICity[] newCities = new ICity[2];
		for (int i = 0; i < 2; i++)
		{
			newCities[i] = new City(that.getCities().get(i));
		}
		this.mCities = newCities;
	}

	/**
	 * Conastructor that creataes the destination card.
	 *
	 * @param cities the two mCities
	 * @param points the mPoints associted with the destination card
	 * @pre cities!=null && cities.size()==2 && cities[0]!=null && cities[1]!=null
	 * @pre points >= 0
	 * @post getCities() == mCities
	 * @post getPoints() = mPoints
	 */
	public DestinationCard(ICity[] cities, int points)
	{
		this.mCities = new ICity[2];
		if (cities != null)
		{
			for (int i = 0; i < cities.length && i < 2; i++)
			{
				this.mCities[i] = cities[i];
			}
		}
		this.mPoints = points;
	}

	/**
	 * Get the mCities associated with the destination card. This result is unmodifiable.
	 *
	 * @return Returns the Cities as unmodifiable list
	 */
	public List<ICity> getCities()
	{
		return Collections.unmodifiableList(Arrays.asList(mCities));
	}

	/**
	 * Get value of Points.
	 *
	 * @return Returns the value of Points
	 */
	public int getPoints()
	{
		return mPoints;
	}
}
