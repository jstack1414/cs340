package edu.byu.cs340.models.gameModel.map;

import java.util.UUID;

/**
 * Created by joews on 2/23/2017.
 *
 * @invariant getId()!=null
 * @invariant getName()!=null
 */
public class City implements ICity
{
	private String mId;
	private String mName;

	/**
	 * Constructor for creating a city by id and name.
	 *
	 * @param name The name to set for the city
	 *             @pre name!=null
	 *             @pre !name.isEmpty()
	 */
	public City(String name)
	{
		mId = UUID.randomUUID().toString();
		mName = name;
	}

	/**
	 * Copy constructor.
	 *
	 * @param that Object to copy.
	 * @pre that!=null
	 * @post all variables in this are the same as that.
	 */
	public City(ICity that)
	{
		mId = that.getId();
		mName = that.getName();
	}

	@Override
	public String getId()
	{
		return mId;
	}

	@Override
	public String getName()
	{
		return mName;
	}

	/**
	 * Override equals to compare the cities by ID.
	 *
	 * @param obj Object to compare to.
	 * @return Returns true if the objects are equal, false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof City)
		{
			return ((City) obj).getId().equals(this.getId());
		} else
		{
			return false;
		}
	}

	/**
	 * Overrides hashcode based on id
	 *
	 * @return mId.hashCode()
	 */
	@Override
	public int hashCode()
	{
		return getId().hashCode();
	}
}
