package edu.byu.cs340.models.gameModel.gameOver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joews on 3/24/2017.
 */
public class GameOverData implements IGameOverData
{
	List<PlayerScoreStruct> mPlayerScoreStructList;

	public GameOverData()
	{
		mPlayerScoreStructList = new ArrayList<>();
	}

	@Override
	public List<PlayerScoreStruct> getData()
	{
		return mPlayerScoreStructList;
	}

	@Override
	public void addPlayerStruct(PlayerScoreStruct data)
	{
		mPlayerScoreStructList.add(data);
	}
}
