package edu.byu.cs340.models.gameModel.gameOver;

import java.util.List;

/**
 * Created by joews on 3/24/2017.
 */
public interface IGameOverData
{
	List<PlayerScoreStruct> getData();

	void addPlayerStruct(PlayerScoreStruct data);

}
