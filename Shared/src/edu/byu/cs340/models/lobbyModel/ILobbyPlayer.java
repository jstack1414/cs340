package edu.byu.cs340.models.lobbyModel;

import edu.byu.cs340.models.roomModel.IPlayer;

/**
 * Created by tjacobhi on 04-Feb-17.
 */
public interface ILobbyPlayer extends IPlayer
{
	/**
	 * Get value of Ready
	 *
	 * @return Returns the value of Ready
	 */
	boolean isReady();

	/**
	 * Set the value of ready variable
	 *
	 * @param ready The value to set ready member variable to
	 */
	void setReady(boolean ready);
}
