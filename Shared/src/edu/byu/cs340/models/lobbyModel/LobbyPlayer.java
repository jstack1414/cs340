package edu.byu.cs340.models.lobbyModel;

import edu.byu.cs340.models.roomModel.Player;

/**
 * Created by joews on 2/2/2017.
 *
 * Player that extends IPlayer and adds whether the player is ready or not in the lobby.
 */
public class LobbyPlayer extends Player implements ILobbyPlayer
{
	private boolean isReady;

	public LobbyPlayer(String userName)
	{
		super(userName);
		this.isReady = false;
	}

	/**
	 * Get if the player is ready. If they are the host it wil also return true.
	 *
	 * @return Returns the value of Ready
	 */
	@Override
	public boolean isReady()
	{
		return isHost || isReady;
	}

	/**
	 * Set the value of ready variable
	 *
	 * @param ready The value to set ready member variable to
	 */
	@Override
	public void setReady(boolean ready)
	{
		isReady = ready;
	}
}
