package edu.byu.cs340.dataGeneration;

import com.google.gson.*;
import edu.byu.cs340.models.gameModel.bank.DestinationCard;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.bank.TrainCard;
import edu.byu.cs340.models.gameModel.map.City;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.gameModel.map.Route;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 2/28/2017.
 *
 * Object for parsing the game objects.
 */
public class GameObjectFileParser
{
	public static String FILE_PATH = "jsonResources/serverObjects.json";
	protected String mFileName;
	protected DTO mData;
	protected Map<String, ICity> mCityMap;

	public GameObjectFileParser(String fileName)
	{
		this.mFileName = fileName;
		mData = new DTO();
		mCityMap = new HashMap<>();
	}

	public void readInFile()
	{
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(mFileName)));
			StringBuilder builder = new StringBuilder();
			String line = reader.readLine();
			while(line != null)
			{
				builder.append(line);
				line = reader.readLine();
			}
			String text = builder.toString();
			Gson gson = getGSON();
			mData = gson.fromJson(text, DTO.class);

		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void writeToFile()
	{
		try
		{
			File file = new File(System.getProperty("user.id") + mFileName);
			if (!file.exists())
			{
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			Gson gson = getGSON();
			String json = gson.toJson(mData);
			bw.write(json);
			bw.close();
		} catch (IOException e)
		{
			System.out.println("Could not write to file\n" + e.getMessage());
		}
	}

	public List<IDestinationCard> getDestinationCards()
	{
		return mData.mDestinationCardList;
	}

	public List<ITrainCard> getTrainCards()
	{
		return mData.mTrainCardList;
	}

	public List<ICity> getCities()
	{
		return mData.mCityList;
	}

	public List<IRoute> getRoutes()
	{
		return mData.mRouteList;
	}

	public void addDestinationCards(DestinationCard destinationCard)
	{
		mData.mDestinationCardList.add(destinationCard);
	}

	public void addTrainCards(TrainCard card)
	{
		mData.mTrainCardList.add(card);
	}

	public void addCities(City city)
	{
		mCityMap.put(city.getName(), city);
		mData.mCityList.add(city);
	}

	public void addRoutes(Route route)
	{
		mData.mRouteList.add(route);
	}

	public ICity[] getCityPair(String s1, String s2)
	{
		return new ICity[]{mCityMap.get(s1), mCityMap.get(s2)};
	}

	class DTO
	{
		List<IDestinationCard> mDestinationCardList;
		List<ITrainCard> mTrainCardList;
		List<ICity> mCityList;
		List<IRoute> mRouteList;

		public DTO()
		{
			mDestinationCardList = new ArrayList<>();
			mTrainCardList = new ArrayList<>();
			mCityList = new ArrayList<>();
			mRouteList = new ArrayList<>();
		}

	}

	protected static Gson getGSON()
	{
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(ICity.class, new InterfaceAdapter<City>())
				.registerTypeAdapter(IRoute.class, new InterfaceAdapter<Route>())
				.registerTypeAdapter(IDestinationCard.class, new InterfaceAdapter<DestinationCard>())
				.registerTypeAdapter(ITrainCard.class, new InterfaceAdapter<TrainCard>());
		return gsonBuilder.create();
	}

	protected static class InterfaceAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T>
	{

		private final static String CLASSNAME = "type";
		private final static String DATA = "data";

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Serializes the object and stores its class type.
		 */
		public JsonElement serialize(T object, Type interfaceType, JsonSerializationContext context)
		{
			final JsonObject wrapper = new JsonObject();
			wrapper.addProperty(CLASSNAME, getObjectClassName(object));
			wrapper.add(DATA, context.serialize(object));
			return wrapper;
		}

		private String getObjectClassName(T object)
		{
			return object.getClass().getName();
		}

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Deserializes the object based on the type stored in the Json.
		 */
		public T deserialize(JsonElement elem, Type interfaceType, JsonDeserializationContext context) throws
				JsonParseException
		{
			final JsonObject wrapper = (JsonObject) elem;
			final JsonElement typeName = get(wrapper, CLASSNAME);
			final JsonElement data = get(wrapper, DATA);
			final Type actualType = typeForName(typeName);
			return context.deserialize(data, actualType);
		}

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Attempts to get the class type from the Json string.
		 */
		private Type typeForName(final JsonElement typeElem)
		{
			try
			{
				String className = typeElem.getAsString();
				return Class.forName(className);
			} catch (ClassNotFoundException e)
			{
				throw new JsonParseException(e);
			}
		}

		/**
		 * SHOULD NOT BE INVOKED EXCEPT BY GSON
		 * Gets the json element associated with the wrapper. This will either be the class type or the mData.
		 */
		private JsonElement get(final JsonObject wrapper, String memberName)
		{
			final JsonElement elem = wrapper.get(memberName);
			if (elem == null)
				throw new JsonParseException("no '" + memberName + "' member found in what was expected to be an " +
						"interface wrapper");
			return elem;
		}
	}

}
