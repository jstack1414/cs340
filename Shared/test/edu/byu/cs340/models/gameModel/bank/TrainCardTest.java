package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.map.TrainType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by joews on 2/23/2017.
 */
class TrainCardTest
{
	List<TrainCard> mTrainCards = new ArrayList<>();
	List<TrainType> mTrainTypes = new ArrayList<>();

	@BeforeEach
	void setUp()
	{
		mTrainCards.add(new TrainCard(TrainType.Box));
		mTrainCards.add(new TrainCard(TrainType.Caboose));
		mTrainCards.add(new TrainCard(TrainType.Locomotive));
		mTrainCards.add(new TrainCard(TrainType.Hopper));

		mTrainTypes.add(TrainType.Box);
		mTrainTypes.add(TrainType.Caboose);
		mTrainTypes.add(TrainType.Locomotive);
		mTrainTypes.add(TrainType.Hopper);
	}

	@AfterEach
	void tearDown()
	{
		mTrainCards.clear();
		mTrainTypes.clear();

	}

	@Test
	void getTrainType()
	{
		for (int i = 0; i < mTrainCards.size(); i++)
		{
			assertEquals(mTrainTypes.get(i), mTrainCards.get(i).getTrainType());
		}
	}

	@Test
	void getId()
	{
		//Ensure all IDS are unique and not null
		Set<String> ids = new HashSet<>();
		for (int i = 0; i < mTrainCards.size(); i++)
		{
			if (mTrainCards.get(i).getId() != null)
			{
				ids.add(mTrainCards.get(i).getId());
			}
		}
		assertEquals(mTrainCards.size(), ids.size());
	}

}