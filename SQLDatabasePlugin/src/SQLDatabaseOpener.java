import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Jenessa on 4/13/2017.
 * 
 * Helper class to open databases and remove duplicate code
 */
public class SQLDatabaseOpener {
	public static Connection getConnection(String databaseName){
		Connection databaseConnection = null;
		try{
			Class.forName("org.sqlite.JDBC");
			databaseConnection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
		} catch(Exception e){
			System.err.printf("Error initializing connection with database %s.\n", databaseName);
		}
		return databaseConnection;
	}
}
