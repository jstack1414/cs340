import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Jenessa on 4/14/2017.
 * 
 * Testing the SQLRoomDAO
 */
class SQLGameDAOTest {
	@Test
	void setGame() {
		SQLGameDAO dao = new SQLGameDAO();
		dao.setGame("game1", "serializedGame1");
		assertEquals("serializedGame1", dao.getGame("game1"));
	}

	@Test
	void addCommandToGame() {
		SQLGameDAO dao = new SQLGameDAO();
		dao.setGame("game1", "serializedGame1");
		dao.addCommandToGame("game1", "command1");
		List<String> commands = dao.getGameCommands("game1");
		assertEquals("command1", commands.get(0));
	}

	@Test
	void getGames() {
		SQLGameDAO dao = new SQLGameDAO();
		dao.setGame("game1", "serializedGame1");
		assertEquals("serializedGame1", dao.getGames().get(0));
	}

	@Test
	void getGame() {

	}

	@Test
	void getGameCommands() {

	}

	@Test
	void removeGame() {
		SQLGameDAO dao = new SQLGameDAO();
		dao.setGame("game1", "serializedGame1");
		dao.removeGame("game1");
		assertEquals(0, dao.getGames().size());
	}

	@Test
	void clear(){
		SQLGameDAO dao = new SQLGameDAO();
		dao.setGame("game1", "serializedGame1");
		dao.setGame("game2", "serializedGame2");
		dao.clearDAO();
		assertEquals(0, dao.getGames().size());
	}
}