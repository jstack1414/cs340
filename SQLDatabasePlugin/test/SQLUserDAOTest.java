import edu.byu.cs340.UserDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Rebekah on 4/15/2017.
 */
class SQLUserDAOTest {
	SQLUserDAO dao;

	@BeforeEach
	void setUp() {
		dao = new SQLUserDAO();
	}

	@AfterEach
	void tearDown() {
		dao.clearDAO();
	}

	@Test
	void setUser() {
		UserDTO dto = new UserDTO("failsafe", 3117, "legit");
		assertEquals(dto.getUsername(), "failsafe");
		assertEquals(dto.getPasshash(), 3117);
		assertEquals(dto.getAuthenticationToken(), "legit");
		dao.setUser(dto);
		dao.setUser(dto);
		List<UserDTO> count = dao.getUsers();
		assertEquals(count.size(), 1);
	}

	@Test
	void getUsers() {
		UserDTO dto = new UserDTO("failsafe", 3117, "legit");
		dao.setUser(dto);
		List<UserDTO> users = dao.getUsers();
		assertEquals(users.get(0).getUsername(), dto.getUsername());
		assertEquals(users.get(0).getPasshash(), dto.getPasshash());
		assertEquals(users.get(0).getAuthenticationToken(), dto.getAuthenticationToken());
	}

	@Test
	void getUsernameToRoomIDMap() {
		Map<String, String> usertoroom = new HashMap<>();
		usertoroom.put("failsafe", "kaaCity");
		usertoroom.put("leget", "nalHutta");
		dao.setUsernameToRoomIDMap(usertoroom);
		Map<String, String> map = dao.getUsernameToRoomIDMap();
		assertEquals(map.get("failsafe"), "kaaCity");
		assertEquals(map.get("leget"), "nalHutta");
	}

	@Test
	void setUsernameToRoomIDMap() {
		Map<String, String> usertoroom = new HashMap<>();
		usertoroom.put("failsafe", "kaaCity");
		usertoroom.put("leget", "nalHutta");
		dao.setUsernameToRoomIDMap(usertoroom);
		dao.setUsernameToRoomIDMap(usertoroom);
		Map<String, String> map = dao.getUsernameToRoomIDMap();
		assertEquals(map.keySet().size(), 2);
	}

}