package edu.byu.cs340;

import java.util.List;

/**
 * Created by joews on 4/11/2017.
 */
public interface IGameDAO
{
	void setGame(String gameID, String serializedGame);

	void addCommandToGame(String gameID, String serializedCmd);

	List<String> getGames();

	String getGame(String gameID);

	List<String> getGameCommands(String gameID);
	
	void removeGame(String gameID);

	void clearDAO();
}
