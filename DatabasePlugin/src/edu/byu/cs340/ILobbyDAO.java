package edu.byu.cs340;

import java.util.List;

/**
 * Created by joews on 4/11/2017.
 */
public interface ILobbyDAO
{
	void setLobby(String lobbyID, String serializedLobby);

	void addCommandToLobby(String lobbyID, String serializedCmd);

	List<String> getLobbies();

	String getLobby(String lobbyID);

	List<String> getLobbyCommands(String lobbyID);

	void removeLobby(String lobbyID);

	void clearDAO();
}
