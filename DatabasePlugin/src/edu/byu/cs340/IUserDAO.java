package edu.byu.cs340;

import java.util.List;
import java.util.Map;

/**
 * Created by joews on 4/11/2017.
 */
public interface IUserDAO
{
	void setUser(UserDTO userData);

	List<UserDTO> getUsers();

	Map<String, String> getUsernameToRoomIDMap();

	void setUsernameToRoomIDMap(Map<String, String> map);

	void clearDAO();
}
