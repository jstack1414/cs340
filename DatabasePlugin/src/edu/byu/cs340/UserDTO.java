package edu.byu.cs340;

/**
 * Created by joews on 4/14/2017.
 */
public class UserDTO
{
	private String mUsername;
	private int mPasshash;
	private String mAuthenticationToken;

	/**
	 * Get value of Username
	 *
	 * @return Returns the value of Username
	 */
	public String getUsername()
	{
		return mUsername;
	}

	/**
	 * Set the value of username variable
	 *
	 * @param username The value to set username member variable to
	 */
	public void setUsername(String username)
	{
		mUsername = username;
	}

	/**
	 * Get value of Passhash
	 *
	 * @return Returns the value of Passhash
	 */
	public int getPasshash()
	{
		return mPasshash;
	}

	/**
	 * Set the value of passhash variable
	 *
	 * @param passhash The value to set passhash member variable to
	 */
	public void setPasshash(int passhash)
	{
		mPasshash = passhash;
	}

	/**
	 * Get value of AuthenticationToken
	 *
	 * @return Returns the value of AuthenticationToken
	 */
	public String getAuthenticationToken()
	{
		return mAuthenticationToken;
	}

	/**
	 * Set the value of authenticationToken variable
	 *
	 * @param authenticationToken The value to set authenticationToken member variable to
	 */
	public void setAuthenticationToken(String authenticationToken)
	{
		mAuthenticationToken = authenticationToken;
	}

	public UserDTO(String username, int passhash, String authenticationToken)
	{
		mUsername = username;
		mPasshash = passhash;
		mAuthenticationToken = authenticationToken;
	}
}
