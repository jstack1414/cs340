package edu.byu.cs340.plugin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 4/12/2017.
 * <p>
 * DAO for accessing Room data
 */
public class FileRoomDAO
{
	public static final String COMMANDS = "commands";
	public static final String FILENAME = "filename";
	public static final String ROOM_ID = "roomId";
	public static final String COMMAND = "command";
	public static final String ROOM = "room";
	private String ROOMTOFILENAME = "roomstofiles";
	private String FILENAME_PREFIX = "room_";
	private final String FILEAPPENDIX = "filedatabase/";
	private String decodedPath;


	public FileRoomDAO(String roomToFileName, String filenamePrefix)
	{
		ROOMTOFILENAME = roomToFileName;
		FILENAME_PREFIX = filenamePrefix;

		decodedPath = System.getProperty("user.dir") + "/";
		File file = getRoomtoFileFile();
		if (!file.exists())
		{
			file.getParentFile().mkdirs();
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try (JsonWriter writer = getRoomToFileWriter())
			{
				Gson gson = new Gson();
				JsonElement emptyArray = new JsonArray();
//				JsonWriter jsonWriter = gson.newJsonWriter(writer);
				gson.toJson(emptyArray, writer);
				writer.close();
			} catch (FileNotFoundException e)
			{
				System.err.println("file not found");
			} catch (IOException e)
			{
				System.err.println("IOException when opening file");
			}
		}
	}

	//<editor-fold desc="Getters for files">
	public File getRoomFile(String RoomID)
	{
		// delete each file
		return new File(getRoomPath(RoomID));
	}

	private JsonWriter getRoomWriter(String RoomID)
	{
		try
		{
			return new JsonWriter(new FileWriter(new File(getRoomPath(RoomID))));
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Reader getRoomReader(String RoomID)
	{
		try
		{
			return new BufferedReader(new InputStreamReader(new FileInputStream(getRoomPath(RoomID))));
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private String getRoomPath(String RoomID)
	{
		return decodedPath + FILEAPPENDIX + RoomID + ".json";
	}

	private String getRoomToFilePath()
	{
		return decodedPath + FILEAPPENDIX + ROOMTOFILENAME + ".json";
	}

	public File getRoomtoFileFile()
	{
		return new File(getRoomToFilePath());
	}

	private JsonWriter getRoomToFileWriter()
	{
		try
		{
			return new JsonWriter(new FileWriter(new File(getRoomToFilePath())));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	private Reader getRoomToFileReader()
	{
		try
		{
			return new BufferedReader(new InputStreamReader(new FileInputStream(getRoomToFilePath())));
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	//</editor-fold>

	public void clearDAO()
	{
		try
		{
			Reader reader = getRoomToFileReader();
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			if (array != null)
			{
				for (JsonElement e : array)
				{
					File toDelete = getRoomFile(e.getAsJsonObject().get(FILENAME).getAsString());
					if (toDelete != null)
					{
						if (!toDelete.delete())
						{
							System.err.println("Unable to delete file for some reason");
						}
					}
				}
			}

			JsonArray newArray = new JsonArray();
			JsonWriter writer = getRoomToFileWriter();
			writer.flush();
			gson.toJson(newArray, writer);
			writer.close();

		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}
	}


	/**
	 * @param roomID         ID for the room to be set in the file
	 * @param serializedRoom data for the entire room
	 */

	public void setRoom(String roomID, String serializedRoom)
	{
		try
		{
			Reader reader = getRoomToFileReader();
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			for (JsonElement e : array)
			{
				JsonObject obj = e.getAsJsonObject();
				if (obj.get(ROOM_ID).getAsString().equals(roomID))
				{
					// go get the file and open it yo
					JsonWriter writer = getRoomWriter(obj.get(FILENAME).getAsString());
					JsonObject root = new JsonObject();
					root.addProperty("room", serializedRoom);
					root.add(COMMANDS, new JsonArray());
					gson.toJson(root, writer);
					writer.close();
					//then return so we don't mess things up
					return;
				}
			}
			JsonObject obj = new JsonObject();
			obj.addProperty(ROOM_ID, roomID);
			// WILL NOT WORK WITH DUPLICATE GAME NAMES
			obj.addProperty(FILENAME, FILENAME_PREFIX + roomID);
			array.add(obj);

			// save new file in index file
			JsonWriter writer = getRoomToFileWriter();// new JsonWriter(new FileWriter(new File(decodedPath +
			// FILEAPPENDIX + ROOMTOFILENAME)));
			gson.toJson(array, writer);
			writer.close();

			// save room to file
			writer = getRoomWriter(FILENAME_PREFIX + roomID);//new JsonWriter(new FileWriter(new File(decodedPath +
			// FILEAPPENDIX + roomID + ".json")));
			JsonObject root = new JsonObject();
			root.addProperty(ROOM, serializedRoom);
			root.add(COMMANDS, new JsonArray());
			gson.toJson(root, writer);
			writer.close();

		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}
	}

	/**
	 * @param roomID        ID for the room to be altered
	 * @param serializedCmd command to be appended to the list of commands
	 */

	public void addCommandToRoom(String roomID, String serializedCmd)
	{
		try
		{
			Reader reader = getRoomToFileReader();//new BufferedReader(new InputStreamReader(new FileInputStream(
//					decodedPath + FILEAPPENDIX + ROOMTOFILENAME)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			for (JsonElement e : array)
			{
				JsonObject obj = e.getAsJsonObject();
				if (obj.get(ROOM_ID).getAsString().equalsIgnoreCase(roomID))
				{
					// go get the file and open it
//					String filename = decodedPath + FILEAPPENDIX + obj.get(FILENAME).getAsString();
					File file = getRoomFile(obj.get(FILENAME).getAsString());//new File(filename);
					if (!file.exists())
					{
						file.getParentFile().mkdirs();
						file.createNewFile();
					}
					reader = getRoomReader(obj.get(FILENAME).getAsString());//new BufferedReader(new
					// InputStreamReader(new FileInputStream(filename)));
					JsonObject root = gson.fromJson(reader, JsonObject.class);
					reader.close();

					//add command to array
					JsonArray commands = root.get(COMMANDS).getAsJsonArray();
					JsonObject newCommand = new JsonObject();
					newCommand.addProperty(COMMAND, serializedCmd);
					commands.add(newCommand);

					//re-write file
					JsonWriter writer = getRoomWriter(obj.get(FILENAME).getAsString());//new JsonWriter(new
					// FileWriter(new File(filename)));
					gson.toJson(root, writer);
					writer.close();
					return;

				}
			}
		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}
	}


	/**
	 * @return list of all serialized rooms
	 */

	public List<String> getRooms()
	{
		List<String> rooms = new ArrayList<>();
		try
		{
			Reader reader = getRoomToFileReader();//new BufferedReader(new InputStreamReader(new FileInputStream
			// (decodedPath + FILEAPPENDIX +ROOMTOFILENAME)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			if (array == null)
			{
				array = new JsonArray();
			}
			for (JsonElement e : array)
			{
				JsonObject obj = e.getAsJsonObject();
				// go get the file and open it
//				String filename = decodedPath + FILEAPPENDIX + obj.get(FILENAME).getAsString();

				// read the room info and put it into the list
				reader = getRoomReader(obj.get(FILENAME).getAsString());//new BufferedReader(new InputStreamReader
				// (new FileInputStream(filename)));
				JsonObject room = gson.fromJson(reader, JsonObject.class);
				if (room != null)
				{
					rooms.add(room.get(ROOM).getAsString());
				}
				reader.close();
			}
		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}
		return rooms;
	}

	/**
	 * LOW PRIORITY
	 *
	 * @param roomID room ID to get
	 * @return serialized room
	 */

	public String getRoom(String roomID)
	{
		try
		{
			Reader reader = getRoomToFileReader();//new BufferedReader(new InputStreamReader(new FileInputStream
			// (decodedPath + FILEAPPENDIX +ROOMTOFILENAME)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			for (JsonElement e : array)
			{
				JsonObject obj = e.getAsJsonObject();
				if (obj.get(ROOM_ID).getAsString().equals(roomID))
				{
					// go get the file and open it
//					String filename = decodedPath + FILEAPPENDIX + obj.get(FILENAME).getAsString();
					reader = getRoomReader(obj.get(FILENAME).getAsString());//= new BufferedReader(new
					// InputStreamReader(new FileInputStream(filename)));

					JsonObject root = gson.fromJson(reader, JsonObject.class);
					reader.close();
					return root.get(ROOM).toString();
				}
			}
		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}
		return null;
	}


	public List<String> getRoomCommands(String roomID)
	{
		List<String> commands = new ArrayList<>();

		try
		{
			Reader reader = getRoomToFileReader();// new BufferedReader(new InputStreamReader(new FileInputStream
			// (decodedPath + FILEAPPENDIX +ROOMTOFILENAME)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			for (JsonElement e : array)
			{
				JsonObject obj = e.getAsJsonObject();
				if (obj.get(ROOM_ID).getAsString().equals(roomID))
				{
					// go get the file and open it
//					String filename = decodedPath + FILEAPPENDIX + obj.get(FILENAME).getAsString();
					reader = getRoomReader(obj.get(FILENAME).getAsString());
//					reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

					JsonObject root = gson.fromJson(reader, JsonObject.class);
					reader.close();
					JsonArray jsonCommands = root.getAsJsonArray(COMMANDS);
					// add each command from the array to the list
					for (JsonElement command : jsonCommands)
					{
						commands.add(command.getAsJsonObject().get(COMMAND).getAsString());
					}
				}
			}
		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}

		return commands;
	}


	public void removeRoom(String roomID)
	{
		// find the file from the index using the roomID
		try
		{
			Reader reader = getRoomToFileReader();// new BufferedReader(new InputStreamReader(new FileInputStream
			// (decodedPath + FILEAPPENDIX +
//					ROOMTOFILENAME)));
			Gson gson = new Gson();
			JsonArray array = gson.fromJson(reader, JsonArray.class);
			reader.close();

			for (JsonElement e : array)
			{
				if (e.getAsJsonObject().get(ROOM_ID).getAsString().equals(roomID))
				{
					// delete the file
					File toDelete = getRoomFile(e.getAsJsonObject().get(FILENAME).getAsString());
					if (!toDelete.delete())
					{
						System.err.println("Unable to delete file for some reason");
					}
					// remove the entry for it in the index
					JsonArray newArray = new JsonArray();
					for (JsonElement elem : array)
					{
						if (!elem.equals(e))
						{
							newArray.add(elem);
						}
						JsonWriter writer = getRoomToFileWriter();// new JsonWriter(new FileWriter(new File(
//								decodedPath + FILEAPPENDIX + ROOMTOFILENAME)));
						gson.toJson(newArray, writer);
						writer.close();
						return;
					}
				}

			}
		} catch (FileNotFoundException e)
		{
			System.err.println("User file not found");
		} catch (IOException e)
		{
			System.err.println("IOException when opening user file");
		}
	}
}
