package edu.byu.cs340;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 4/12/2017.
 */
public class MockGameDAO implements IGameDAO
{

	private Map<String, String> data;
	private Map<String, List<String>> cmdMap;

	public MockGameDAO()
	{
		data = new HashMap<>();
		cmdMap = new HashMap<>();
	}
	//persistence

	@Override
	public void removeGame(String GameID)
	{
		cmdMap.remove(GameID);
	}

	@Override
	public void setGame(String gameID, String serializedGame)
	{
		data.put(gameID, serializedGame);
		if (cmdMap.containsKey(gameID))
		{
			cmdMap.remove(gameID);
		}
	}

	@Override
	public void addCommandToGame(String gameID, String serializedCmd)
	{
		if (cmdMap.containsKey(gameID))
		{
			cmdMap.get(gameID).add(serializedCmd);
		} else
		{
			List<String> temp = new ArrayList<>();
			temp.add(serializedCmd);
			cmdMap.put(gameID, temp);
		}
	}

	@Override
	public List<String> getGames()
	{
		return new ArrayList<>(data.values());
	}

	@Override
	public String getGame(String gameID)
	{
		return data.get(gameID);
	}

	@Override
	public List<String> getGameCommands(String gameID)
	{
		return cmdMap.get(gameID);
	}

	@Override
	public void clearDAO()
	{
		cmdMap.clear();
		data.clear();
	}
}
