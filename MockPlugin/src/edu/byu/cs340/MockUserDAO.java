package edu.byu.cs340;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 4/12/2017.
 */
public class MockUserDAO implements IUserDAO
{
	private Map<String, String> map;
	private List<UserDTO> list;
	public MockUserDAO()
	{
		map = new HashMap<>();
		list = new ArrayList<>();

	}

	@Override
	public void setUser(UserDTO dto)
	{
		list.add(dto);
	}

	@Override
	public List<UserDTO> getUsers()
	{
		return list;
	}

	@Override
	public Map<String, String> getUsernameToRoomIDMap()
	{
		return map;
	}

	@Override
	public void setUsernameToRoomIDMap(Map<String, String> map)
	{
		this.map = map;

	}

	@Override
	public void clearDAO()
	{
		map.clear();
		list.clear();
	}
}