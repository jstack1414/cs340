package edu.byu.cs340.models.gameModel.map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Created by joews on 2/27/2017.
 *
 * Tests for the game map object.
 */
class GameMapTest
{
	@BeforeEach
	void setUp()
	{

	}

	@AfterEach
	void tearDown()
	{

	}

	@Test
	void getRoute()
	{

	}

	@Test
	void hasRoute()
	{

	}

	@Test
	void claimRoute()
	{

	}

	@Test
	void getRoutes()
	{

	}

	@Test
	void getCities()
	{

	}

	@Test
	void getLongestRoutePlayer()
	{

	}

	@Test
	void setRoutes()
	{

	}

	@Test
	void setCities()
	{

	}

	@Test
	void isInitialized()
	{

	}

}