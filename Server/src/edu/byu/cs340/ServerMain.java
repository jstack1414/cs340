package edu.byu.cs340;

import edu.byu.cs340.communication.HTTPServerCommunicator;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.database.DatabaseWriter;
import edu.byu.cs340.models.gameModel.GameParameters;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joews on 1/26/2017.
 *
 * Main that launches the server.
 */
public class ServerMain
{
	private static final String MIN_USER_KEY = "minUser";
	private static final String NUM_TRAINS = "numTrains";
	private static final String PORT = "port";
	private static final String PLUGIN = "plugin";
	private static final String COMMANDS_BETWEEN_SAVES = "numCommands";
	private static final String CLEAR_TABLE = "clearTable";

	public static void main(String[] args)
	{
		String port = null;
		Map<String, String> argsMap = new HashMap<>();
		for (int i = 0; i < args.length; i++)
		{
			switch (args[i].charAt(0))
			{
				case '-':
					if (i + 1 < args.length)
					{
						argsMap.put(args[i].substring(1), args[i + 1]);
						System.out.println("Added valid command argument: " + args[i] + ": " + args[i + 1]);
						i++;
					}
					break;
				default:
					System.out.println("Invalid arg");
					break;
			}
		}
		//now process it
		if (argsMap.containsKey(PLUGIN))
		{
			//TODO: Jacob implement here
//			DatabaseLoader.getInstance().createFactory(null);
//			IAbstractFactory abstractFactory = DatabaseLoader.getInstance().getFactory();
			//pseudo for injection of abstract factory
			try
			{
				String pluginsFile = System.getProperty("user.dir") + "/data.plugins";
				ArrayList<String> plugins = new ArrayList<>();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(pluginsFile))));
				String file = null;
				do
				{
					file = bufferedReader.readLine();
					if (file != null)
						plugins.add(file);
				} while(file != null);

				String path = plugins.get(Integer.parseInt(argsMap.get(PLUGIN)));
				DatabaseLoader.getInstance().createFactory(path);
				
				IAbstractFactory abstractFactory = DatabaseLoader.getInstance().getFactory();//new MockAbstractFactory(); //temporary until loader is working.
				DatabaseWriter.getInstance().setAbstractFactory(abstractFactory);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		if (argsMap.containsKey(COMMANDS_BETWEEN_SAVES))
		{
			int numCommands = Integer.parseInt(argsMap.get(COMMANDS_BETWEEN_SAVES));
			//TODO when setter is implemented, call it
		}
		if (argsMap.containsKey(CLEAR_TABLE) && argsMap.containsKey(PLUGIN))
		{
			if (argsMap.get(CLEAR_TABLE).equalsIgnoreCase("y"))
			{
				DatabaseWriter.getInstance().clearDatabase();
				System.out.println("Cleared database");
			}
		}
		if (argsMap.containsKey(PORT))
		{
			port = argsMap.get(PORT);
		}
		if (argsMap.containsKey(NUM_TRAINS))
		{
			GameParameters.STARTING_NUM_TRAINS = Integer.parseInt(argsMap.get(NUM_TRAINS));
		}
		if (argsMap.containsKey(MIN_USER_KEY))
		{
			GameParameters.MIN_PLAYERS = Integer.parseInt(argsMap.get(MIN_USER_KEY));
		}
		//after setting everything else up load in from the database
		if (argsMap.containsKey(PLUGIN))
		{

			ServerFacade.getInstance(ServerFacade.class).loadFromDatabase();
		}
		HTTPServerCommunicator server = new HTTPServerCommunicator();
		server.run(port);

	}
}
