package edu.byu.cs340;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by tjacobhi on 15-Apr-17.
 */
public class JarLoader extends URLClassLoader
{
	public JarLoader(URL[] urls)
	{
		super(urls);
	}

	public void addFile (String path) throws MalformedURLException
	{
		String urlPath = "jar:file://" + path + "!/";
		addURL(new URL(urlPath));
	}
}
