package edu.byu.cs340.communication;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.Authenticator;
import edu.byu.cs340.models.BaseUser;
import edu.byu.cs340.models.IAuthenticator;
import edu.byu.cs340.models.RoomManager;

/**
 * Created by joews on 2/1/2017.
 *
 * Facade for managing actions on the server.
 */
public class ServerFacade implements IAuthenticatableServer, ICommandServerFacade
{
	private static ServerFacade mSINGLETON;

	/**
	 * Gets the server facade cast as type
	 *
	 * @param type Type (interface) to cast the server facade as
	 * @param <T>  same as type
	 * @return Returns the server facade cast as type
	 */
	public static <T extends IServerFacade> T getInstance(Class<T> type)
	{
		if (mSINGLETON == null)
		{
			mSINGLETON = new ServerFacade();
		}
		return type.cast(mSINGLETON);
	}

	private IAuthenticator mAuthenticator;
	private RoomManager mRoomManager;

	/**
	 * private constructor.
	 */
	private ServerFacade()
	{
		mAuthenticator = new Authenticator();
		mRoomManager = new RoomManager();
	}

	public void loadFromDatabase()
	{
		mAuthenticator.loadFromDatabase();
		mRoomManager.loadFromDatabase();
	}

	//<editor-fold desc="Methods required by IAuthenticatableServer interface">

	/**
	 * Method that executes command on server.
	 *
	 * @param baseCommand Command to execute
	 * @return IMessage specifing result
	 *
	 * @throws IBaseCommand.CommandException thrown with error nad holds error message and error code.
	 */
	@Override
	public IMessage executeCommand(IBaseCommand baseCommand) throws IBaseCommand.CommandException
	{
		return baseCommand.execute();
	}

	/**
	 * Checks to verify the user is authenticated by matching up the auhenticaitonToken with the user.
	 *
	 * @param authenticationToken The authentication token to test for validity.
	 * @return The user associated with said token (null if none).
	 */
	@Override
	public BaseUser authenticate(String authenticationToken)
	{
		return mAuthenticator.authenticate(authenticationToken);
	}
	//</editor-fold>


	//<editor-fold desc="Methods required by the ICommandServerFacade interface">

	/**
	 * Gets the manager of the rooms.
	 *
	 * @return Returns the RoomManager
	 */
	@Override
	public RoomManager getRoomManager()
	{
		return mRoomManager;
	}

	/**
	 * Gets the authenticator
	 *
	 * @return Returns the Authenticator.
	 */
	@Override
	public IAuthenticator getAuthenticator()
	{
		return mAuthenticator;
	}
	//</editor-fold>
}
