package edu.byu.cs340.communication;

/**
 * Created by Rebekah on 1/30/2017.
 */
public interface IServerCommunicator {
	void run(String port);
}
