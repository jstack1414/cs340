package edu.byu.cs340.communication;

import edu.byu.cs340.models.BaseUser;

/**
 * Created by joews on 2/4/2017.
 *
 * Adds funcitonality to server to allow for authenticaing a user.
 */
public interface IAuthenticatableServer extends IServerFacade
{
	/**
	 * Checks to verify the user is authenticated by matching up the auhenticaitonToken with the user.
	 *
	 * @param authenticationToken The authentication token to test for validity.
	 * @return The user associated with said token (null if none).
	 */
	BaseUser authenticate(String authenticationToken);
}
