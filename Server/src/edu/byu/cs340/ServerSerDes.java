package edu.byu.cs340;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import edu.byu.cs340.command.BaseCommand;
import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.models.ChatMessenger;
import edu.byu.cs340.models.ServerChatMessenger;
import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.gameModel.bank.*;
import edu.byu.cs340.models.gameModel.gameOver.GameOverData;
import edu.byu.cs340.models.gameModel.gameOver.IGameOverData;
import edu.byu.cs340.models.gameModel.map.*;
import edu.byu.cs340.models.gameModel.server_state.IServerStatable;
import edu.byu.cs340.models.gameModel.server_state.IServerState;
import edu.byu.cs340.models.gameModel.server_state.ServerState;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.roomModel.*;

/**
 * Created by joews on 4/14/2017.
 */
public class ServerSerDes extends SerDes
{

	public synchronized static Gson getGSON(Source src)
	{
		GsonBuilder gsonBuilder = new GsonBuilder();
		if (src == Source.CLIENT)
		{
			gsonBuilder.registerTypeAdapter(IRoomsListInfo.class, new ClientInterfaceAdapter<RoomsListInfo>())
					.registerTypeAdapter(IRoomInfo.class, new ClientInterfaceAdapter<RoomInfo>())
					.registerTypeAdapter(IPlayer.class, new ClientInterfaceAdapter<Player>())
					.registerTypeAdapter(ICity.class, new ClientInterfaceAdapter<City>())
					.registerTypeAdapter(IRoute.class, new ClientInterfaceAdapter<Route>())
					.registerTypeAdapter(LobbyModel.class, new ClientInterfaceAdapter<LobbyModel>())
					.registerTypeAdapter(IDestinationCard.class, new ClientInterfaceAdapter<DestinationCard>())
					.registerTypeAdapter(IGameOverData.class, new ClientInterfaceAdapter<GameOverData>())
					.registerTypeAdapter(ITrainCard.class, new ClientInterfaceAdapter<TrainCard>());
		} else
		{
			gsonBuilder.registerTypeAdapter(IRoomsListInfo.class, new ServerInterfaceAdapter<RoomsListInfo>())
					.registerTypeAdapter(IRoomInfo.class, new ServerInterfaceAdapter<RoomInfo>())
					.registerTypeAdapter(IPlayer.class, new ServerInterfaceAdapter<Player>())
					.registerTypeAdapter(ICity.class, new ServerInterfaceAdapter<City>())
					.registerTypeAdapter(IRoute.class, new ServerInterfaceAdapter<Route>())
					.registerTypeAdapter(LobbyModel.class, new ServerInterfaceAdapter<LobbyModel>())
					.registerTypeAdapter(IDestinationCard.class, new ServerInterfaceAdapter<DestinationCard>())
					.registerTypeAdapter(IGameOverData.class, new ServerInterfaceAdapter<GameOverData>())
					.registerTypeAdapter(ITrainCard.class, new ServerInterfaceAdapter<TrainCard>())
					.registerTypeAdapter(IGameBank.class, new ServerInterfaceAdapter<GameBank>())
					.registerTypeAdapter(IGameMap.class, new ServerInterfaceAdapter<GameMap>())
					.registerTypeAdapter(IServerState.class, new ServerInterfaceAdapter<ServerState>())
					.registerTypeAdapter(IBaseCommand.class, new ServerInterfaceAdapter<BaseCommand>())
					.registerTypeAdapter(IServerStatable.class, new ServerInterfaceAdapter<ServerGameModel>())
					.registerTypeAdapter(PlayerManager.class, new ServerInterfaceAdapter<PlayerManager>())
					.registerTypeAdapter(ChatMessenger.class, new ServerInterfaceAdapter<ServerChatMessenger>());
		}

		return gsonBuilder.create();
	}

	public static String serializeObject(Object obj, Source source)
	{
		Gson gson = getGSON(source);
		return gson.toJson(obj);
	}

	public static Object deserializeObject(String jsonString, Source source) throws
			ClassNotFoundException
	{
		if (jsonString == null || jsonString.isEmpty() || jsonString.equalsIgnoreCase("null"))
		{
			return null;
		}
		JsonObject jsonObject = getGSON(source).fromJson(jsonString, JsonObject.class);
		//Get the commands inherited type for rebuilding
		String classRoot = jsonObject.get("mClassName").getAsString();
		String pkg = jsonObject.get("mClassPackage").getAsString();
		Class c = Class.forName(pkg + "." + source.getName() + classRoot);//reflection
		//create the command and execute it
		return getGSON(source).fromJson(jsonString, c);
	}


}
