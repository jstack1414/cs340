package edu.byu.cs340.command.lobby;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.roomModel.IRoomsListInfo;

/**
 * Created by joews on 2/7/2017.
 *
 * Command to get list of games(lobbies) the user could join
 */
public class ServerGetGamesListCommand extends GetGamesListCommand
{

	public static final String ROOM_LIST_CAN_NOT_BE_GENERATED = "Room list can not be generated.";

	public ServerGetGamesListCommand(String username, IRoomsListInfo roomsListInfo)
	{
		super(username, roomsListInfo);
	}

	/**
	 * Gets the lobbies the player could join.
	 *
	 * @return An IMessage that holds this command with th games list inside.
	 *
	 * @throws CommandException Exception saying what type of error occured and an error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager roomManager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);

		//OPERATIONS

		//if not in a game or lobby then move on.
		mRoomsListInfo = roomManager.getLobbyList();
		//if successful then send back this command to the client.
		if (mRoomsListInfo != null)
		{
			String cmd = SerDes.serializeCommand(this, SerDes.Source.SERVER);
			result = new CommandMessage(cmd);
		} else
		{
			throw new CommandException(ROOM_LIST_CAN_NOT_BE_GENERATED, ErrorMessage.MILD_ERROR);
		}
		return result;
	}
}
