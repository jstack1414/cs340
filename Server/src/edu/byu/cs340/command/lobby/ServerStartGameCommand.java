package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.command.game.ServerDrawDestinationCardsCommand;
import edu.byu.cs340.command.game.ServerDrawTrainCardCommand;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.dataGeneration.GameObjectFileParser;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.GameParameters;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.lobbyModel.LobbyModel;

import java.util.List;

/**
 * Created by joews on 2/1/2017.
 *
 * Command that starts the game from the lobby.
 */
public class ServerStartGameCommand extends StartGameCommand
{

	public static final String PLAYER_NOT_HOST_CAN_NOT_START_GAME = "Player not host. Can not start game.";
	public static final String NOT_ENOUGH_PLAYERS_TO_START_GAME = "Not enough players to start game.";
	public static final String TOO_MANY_PLAYERS_TO_START_GAME = "Too many players to start game.";
	public static final String NOT_ALL_PLAYERS_ARE_READY = "Not all players are ready.";

	public ServerStartGameCommand(String username, int lastCommand, String lobbyID)
	{
		super(username, lastCommand, lobbyID);
	}

	public ServerStartGameCommand(ServerStartGameCommand that)
	{
		super(that);
	}

	/**
	 * Gets the lobby and creates a game from it if it is able to start.
	 *
	 * @return An IMessage that holds the list of comands that the user needs to run.
	 *
	 * @throws CommandException Exception saying what type of error occurred and an error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();


		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		ServerCommandUtils.validatePlayerInLobby(mUsername, mLobbyID);

		if (!manager.getRoom(mLobbyID).getHost().equals(mUsername))
		{
			throw new CommandException(PLAYER_NOT_HOST_CAN_NOT_START_GAME, ErrorMessage.MILD_ERROR);
		} else if (manager.getRoom(mLobbyID).getNumPlayers() < GameParameters.MIN_PLAYERS)
		{
			throw new CommandException(NOT_ENOUGH_PLAYERS_TO_START_GAME, ErrorMessage.MILD_ERROR);
		} else if (manager.getRoom(mLobbyID).getNumPlayers() > GameParameters.MAX_PLAYERS)
		{
			throw new CommandException(TOO_MANY_PLAYERS_TO_START_GAME, ErrorMessage.MILD_ERROR);
		} else if (!((LobbyModel) manager.getRoom(mLobbyID)).isAllReady())
		{
			throw new CommandException(NOT_ALL_PLAYERS_ARE_READY, ErrorMessage.MILD_ERROR);
		}

		//OPERATIONS:
		GameObjectFileParser parse = new GameObjectFileParser(GameObjectFileParser.FILE_PATH);
		parse.readInFile();
		mLobbyName = manager.getLobby(mLobbyID).getRoomName();
		manager.createGame(mLobbyID, parse.getTrainCards(), parse.getDestinationCards(), parse.getCities(), parse
				.getRoutes());
		IServerGameModel game = manager.getGame(mLobbyID);
		mNumDestinationCardsInDeck = game.getNumDestinationCardsInDeck();
		mNumTrainCardsInDeck = game.getNumTrainCardsInDeck();
		mNumTrainsPerPlayer = GameParameters.STARTING_NUM_TRAINS;
		mRevealedTrainCards = game.getRevealedCards();
		mPlayerOrder = game.getPlayerOrder();
		mCities = game.getCities();
		mRoutes = game.getRoutes();
//		mDemoTrainCardDeck = game.getDemoTrainCards();
//		mDemoDestinationCardDeck = game.getDemoDestinationCards();
		game.addCommand(this);
		//now force a game save
//		DatabaseWriter.getInstance().setGame(game);

		//for each player, draw destination cards
		List<String> playerNames = game.getPlayerOrder();
		for (String playerName : playerNames)
		{
			//draw the destination cards
			IBaseCommand cmd = new ServerDrawDestinationCardsCommand(playerName, -1, mLobbyID);
			cmd.execute();
			//draw 4 train cards
			for (int i = 0; i < 4; i++)
			{
				cmd = new ServerDrawTrainCardCommand(playerName, -1, mLobbyID, 0);
				cmd.execute();
			}
		}

		//get the commands to send back to the usre who started the game
		result = ServerCommandUtils.getCommandsFromGame(mLobbyID, -1, mUsername);

		return result;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		return new ServerStartGameCommand(this);
	}
}
