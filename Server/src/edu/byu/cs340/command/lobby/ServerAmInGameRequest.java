package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandListMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;

import java.util.ArrayList;

/**
 * Created by joews on 4/4/2017.
 */
public class ServerAmInGameRequest extends AmInGameRequest
{
	public ServerAmInGameRequest(String username)
	{
		super(username);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager roomManager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);

		//OPERATIONS
		String lobbyID = roomManager.getPlayerLobbyID(mUsername);
		String gameID = roomManager.getPlayerGameID(mUsername);
		if (lobbyID != null)
		{
			result = ServerCommandUtils.getCommandsFromLobby(lobbyID, -1, mUsername);
		} else if (gameID != null)
		{
			result = ServerCommandUtils.getCommandsFromGame(gameID, -1, mUsername);
		} else
		{
			result = new CommandListMessage(new ArrayList<>());
		}
		return result;
	}

}
