package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.roomModel.IPlayerRoom;

/**
 * Created by joews on 2/1/2017.
 *
 * Command that readies up the player on the server.
 */
public class ServerReadyUpCommand extends ReadyUpCommand
{
	public ServerReadyUpCommand(String username, int lastCommand, String lobbyID, boolean isReady)
	{
		super(username, lastCommand, lobbyID, isReady);
	}

	/**
	 * Ready's up the player in the lobby specified in the command on the server.
	 *
	 * @return An IMessage that holds the list of comands that the user needs to run.
	 *
	 * @throws CommandException Exception saying what type of error occured and an error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		ServerCommandUtils.validatePlayerInRoom(mUsername, mLobbyID);

		//OPERATIONS:
		IPlayerRoom room = manager.getRoom(mLobbyID);
		if (room instanceof LobbyModel)
		{
			LobbyModel lobby = (LobbyModel) room;
			if (mIsReady)
			{
				lobby.readyUser(mUsername);
			} else
			{
				lobby.unReadyUser(mUsername);
			}
			lobby.addCommand(this);
			result = ServerCommandUtils.getCommandsFromLobby(mLobbyID, mLastCommand, mUsername);
		} else
		{
			//if the game has started and we are in the game models just send all the gamea commands
			result = ServerCommandUtils.getCommandsFromGame(mLobbyID, -1, mUsername);
		}

		return result;
	}
}
