package edu.byu.cs340.command.lobby;

import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.GameParameters;
import edu.byu.cs340.models.roomModel.IPlayer;
import edu.byu.cs340.models.roomModel.IPlayerRoom;

/**
 * Created by joews on 2/8/2017.
 *
 * Command to join an already  existent lobby.
 */
public class ServerJoinLobbyCommand extends JoinLobbyCommand
{

	public static final String JOINING_LOBBY_FAILED = "Joining Lobby Failed";

	public ServerJoinLobbyCommand(String username, String lobbyID, IPlayer joiningPlayer)
	{
		super(username, lobbyID, joiningPlayer);
	}

	/**
	 * Joins the player to the lobby as specified by the lobby ID in the command.
	 *
	 * @return An IMessage that holds the list of comands that the user needs to run.
	 *
	 * @throws CommandException Exception saying what type of error occured and an error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		ServerCommandUtils.validateLobby(mLobbyID);

		//OPERATIONS
		if (manager.getLobby(mLobbyID).hasPlayer(mUsername))
		{
			//simply send back the lobby state as is, not a list of a bunch of commands to get there when joining.
			result = ServerCommandUtils.getCommandsFromLobby(mLobbyID, mLastCommand,mUsername);
		} else if (manager.getLobby(mLobbyID).getNumPlayers() > GameParameters.MAX_PLAYERS)
		{
			throw new CommandException(ServerCommandUtils.ROOM_IS_FULL, ErrorMessage.MILD_ERROR);
		} else
		{
			boolean success = manager.addPlayer(mUsername, mLobbyID);
			if (success)
			{
				IPlayerRoom lobby = manager.getRoom(mLobbyID);
				mLobbyName = lobby.getRoomName();
				mJoiningPlayer = lobby.getPlayer(mUsername);
				lobby.addCommand(this);
				result = ServerCommandUtils.getCommandsFromLobby(mLobbyID, mLastCommand,mUsername);
			} else
			{
				throw new CommandException(JOINING_LOBBY_FAILED, ErrorMessage.MILD_ERROR);
			}
		}
		return result;
	}
}
