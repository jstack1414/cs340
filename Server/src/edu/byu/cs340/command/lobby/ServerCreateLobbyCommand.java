package edu.byu.cs340.command.lobby;

import edu.byu.cs340.SerDes;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.CommandMessage;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;

/**
 * Created by joews on 2/5/2017.
 *
 * Command that creates a lobby on the server.
 */
public class ServerCreateLobbyCommand extends CreateLobbyCommand
{

	public static final String UNABLE_TO_CREATE_LOBBY_ON_SERVER = "Unable to create lobby on server.";

	public ServerCreateLobbyCommand(String username, String lobbyID, String lobbyName)
	{
		super(username, lobbyID, lobbyName);
	}

	/**
	 * Adds a game to the romo manager with the player who sent the command inside.
	 *
	 * @return An IMessage that holds the list of comands that the user needs to run.
	 *
	 * @throws CommandException Exception saying what type of error occured and an error code.
	 */
	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager roomManager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateLobbyName(mLobbyName);
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);


		//OPERATIONS:
		//create the lobby and set the user in it as host. Get ID
		mLobbyID = roomManager.addLobby(mLobbyName, mUsername);
		//if succesfull then send back this command to the client.
		if (mLobbyID != null)
		{
			roomManager.getRoom(mLobbyID).addCommand(this);
//			now force a game save
//			DatabaseWriter.getInstance().setLobby(roomManager.getLobby(mLobbyID));
			//if this operation worked then serilalized and set command as result
			String cmd = SerDes.serializeCommand(this, SerDes.Source.SERVER);
			result = new CommandMessage(cmd);
		} else
		{
			throw new CommandException(UNABLE_TO_CREATE_LOBBY_ON_SERVER, ErrorMessage.MILD_ERROR);
		}

		return result;
	}
}
