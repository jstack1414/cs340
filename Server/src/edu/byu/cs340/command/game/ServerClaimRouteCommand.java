package edu.byu.cs340.command.game;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.players.GamePlayer;

import java.util.List;

/**
 * Created by crown on 3/18/2017.
 */
public class ServerClaimRouteCommand extends ClaimRouteCommand
{
	public ServerClaimRouteCommand(String username, int lastCommand, String gameID, String routeID, List<ITrainCard>
			cardsUsed)
	{
		super(username, lastCommand, gameID, routeID, cardsUsed);
	}

	public ServerClaimRouteCommand(ClaimRouteCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePlayerInGame(mUsername, mGameID);

		IServerGameModel game = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager().getGame(mGameID);
		if (!game.isInitialized())
		{
			throw new CommandException(ServerCommandUtils.GAME_NOT_INITIALIZED, ErrorMessage.MILD_ERROR);
		} else if (game.isInitialState())
		{
			throw new CommandException(ServerCommandUtils.SET_UP_NOT_COMPLETE, ErrorMessage.MILD_ERROR);
		} else if (!game.isPlayersTurn(mUsername))
		{
			throw new CommandException(ServerCommandUtils.NOT_YOUR_TURN, ErrorMessage.MILD_ERROR);
		} else if (!game.claimRoute(mRouteID, mCardsUsed))
		{
			throw new CommandException(ServerCommandUtils.CLAIM_ROUTE_ERROR, ErrorMessage.MILD_ERROR);
		}
		//mNewScore = game.getScore(mUsername);
		mNewScores = game.getPublicScore();
		mNewNumTrains = game.getNumTrains(mUsername);
		mRevealed = game.getRevealedCards();
		//mNewTrainDiscardSize = game.getNumTrainCardsInDiscard(); TODO change when method is implemented
		mNewTrainDiscardSize = game.getNumTrainCardsInDiscard();
		mNewTrainDeckSize = game.getNumTrainCardsInDeck();
		mUsersLongestRoute = game.getLongestRoutePlayers();
		mLengthLongestRoute = ((GamePlayer) game.getPlayer(mUsersLongestRoute.iterator().next()))
				.getLongestRouteSize();
		mPrivateScore = game.getHiddenScore(mUsername);
		game.addCommand(this);

		//GO TO NEXT PLAYER TURN
		IBaseCommand cmd = new ServerNextPlayerCommand(mUsername, mLastCommand, mGameID, null);
		cmd.execute();
		//get cmds to send back
		return ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);
	}
}
