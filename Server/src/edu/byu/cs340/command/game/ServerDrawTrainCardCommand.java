package edu.byu.cs340.command.game;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.ErrorMessage;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.RoomManager;
import edu.byu.cs340.models.gameModel.IServerGameModel;

/**
 * Created by joews on 3/4/2017.
 */
public class ServerDrawTrainCardCommand extends DrawTrainCardCommand
{

	public static final String ERROR_DRAWING_TRAIN_CARD_ON_SERVER = "Error Drawing Train card on server";

	public ServerDrawTrainCardCommand(String username, int lastCommand, String gameID, int drawnZone)
	{
		super(username, lastCommand, gameID, 0);
	}

	public ServerDrawTrainCardCommand(ServerDrawTrainCardCommand that)
	{
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		IMessage result = null;
		ICommandServerFacade server = ServerFacade.getInstance(ICommandServerFacade.class);
		RoomManager manager = server.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePlayerInGame(mUsername, mGameID);

		if (!manager.getGame(mGameID).isInitialized())
		{
			throw new IBaseCommand.CommandException(ServerCommandUtils.GAME_NOT_INITIALIZED, ErrorMessage.MILD_ERROR);
		}//else if it is not the setup and its not the player's turn
		else if (!manager.getGame(mGameID).isInitialState() && !manager.getGame(mGameID).isPlayersTurn(mUsername))
		{
			throw new IBaseCommand.CommandException(ServerCommandUtils.NOT_YOUR_TURN, ErrorMessage.MILD_ERROR);
		}

		//OPERATIONS:
		IServerGameModel gameModel = manager.getGame(mGameID);
		if (mDrawnZone == 0)
		{
			mTrainCard = gameModel.drawTrainCardFromDeck(mUsername);
		} else
		{
			mTrainCard = gameModel.drawTrainCardFromRevealed(mDrawnZone - 1, mUsername);
		}
		mRevealed = gameModel.getRevealedCards();

		mNewTrainDeckSize = gameModel.getNumTrainCardsInDeck();
		mNewDiscardSize = gameModel.getNumTrainCardsInDiscard();
		mCanDrawAnother = gameModel.canDrawAnotherCard();

		if (mTrainCard != null)
		{
			gameModel.addCommand(this);
		} else
		{
			throw new CommandException(ERROR_DRAWING_TRAIN_CARD_ON_SERVER, ErrorMessage.FATAL_ERROR);
		}
		if (!gameModel.isInitialState() && !mCanDrawAnother)
		{
			IBaseCommand cmd = new ServerNextPlayerCommand(mUsername, mLastCommand, mGameID, null);
			cmd.execute();
		}
		result = ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);

		return result;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		if (mUsername == null || userStripping == null || !mUsername.equals(userStripping))
		{
			//strip!!
			ServerDrawTrainCardCommand cmd = new ServerDrawTrainCardCommand(this);
			cmd.mTrainCard = null;
			return cmd;
		} else
		{
			return new ServerDrawTrainCardCommand(this);
		}
	}
}
