package edu.byu.cs340.command.game;

import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.gameModel.IServerGameModel;

/**
 * Created by tjacobhi on 01-Mar-17.
 *
 */
public class ServerGetGameStateCommand extends GetGameState
{

	public ServerGetGameStateCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);

	}

	@Override
	public IMessage execute() throws CommandException
	{
		IServerGameModel model = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager().getGame(super.mGameID);
		for(int j = 0; j < model.getPlayerOrder().size(); j++)
		{
			mPlayers.add(model.getPlayerOrder().get(j));
		}
		return super.execute();
	}
}
