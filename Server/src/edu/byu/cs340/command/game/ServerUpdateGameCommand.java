package edu.byu.cs340.command.game;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.IAuthenticator;
import edu.byu.cs340.models.RoomManager;

/**
 * Created by joews on 2/11/2017.
 */
public class ServerUpdateGameCommand extends UpdateGameCommand
{
	public ServerUpdateGameCommand(String username, int lastCommand, String gameID)
	{
		super(username, lastCommand, gameID);
	}

	@Override
	public IMessage execute() throws CommandException
	{
		String message = null;
		boolean success;
		IMessage result;

		//get data  manager to work with
		ICommandServerFacade serverFacade = ServerFacade.getInstance(ICommandServerFacade.class);
		IAuthenticator authenticator = serverFacade.getAuthenticator();
		RoomManager roomManager = serverFacade.getRoomManager();

		//DATA VALIDATION:
		ServerCommandUtils.validateUsernameInAuthenticator(mUsername);
		//validate room because if the game has launched that is fine, we want ot just see if the room exists. The
		// operation will auto pull form the game if it started
		ServerCommandUtils.validatePlayerInRoom(mUsername, mGameID);


		//OPERATIONS:
		//simply pull the recent commnads
		result = ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);

		return result;
	}

	@Override
	public IBaseCommand strip(String userStripping)
	{
		return null;
	}
}
