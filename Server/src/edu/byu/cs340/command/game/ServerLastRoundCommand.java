package edu.byu.cs340.command.game;

/**
 * Created by crown on 3/24/2017.
 */
public class ServerLastRoundCommand extends LastRoundCommand {
	public ServerLastRoundCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	public ServerLastRoundCommand(BaseGameCommand that) {
		super(that);
	}
}
