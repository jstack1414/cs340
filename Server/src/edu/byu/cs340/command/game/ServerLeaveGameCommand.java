package edu.byu.cs340.command.game;

import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.communication.ICommandServerFacade;
import edu.byu.cs340.communication.ServerFacade;
import edu.byu.cs340.message.IMessage;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.server_state.IServerStatable;
import edu.byu.cs340.models.gameModel.server_state.PlayerTurnState;

/**
 * Created by crown on 3/24/2017.
 */
public class ServerLeaveGameCommand extends LeaveGameCommand {
	public ServerLeaveGameCommand(String username, int lastCommand, String gameID) {
		super(username, lastCommand, gameID);
	}

	public ServerLeaveGameCommand(BaseGameCommand that) {
		super(that);
	}

	@Override
	public IMessage execute() throws CommandException {
		ServerCommandUtils.validateUsername(mUsername);
		ServerCommandUtils.validatePlayerInGame(mUsername, mGameID);

		IServerGameModel game = ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager().getGame(mGameID);
		boolean wasMyTurn = game.isPlayersTurn(mUsername);

		ServerFacade.getInstance(ICommandServerFacade.class).getRoomManager().removePlayer(mUsername, mGameID);
		game.addCommand(this);

		if (wasMyTurn)
		{
			ServerNextPlayerCommand cmd = new ServerNextPlayerCommand(mUsername, -1, game.getRoomID(), game
					.getCurrentPlayerName());
			game.addCommand(cmd);
		}

		((IServerStatable) game).setState(new PlayerTurnState((IServerStatable) game));


		//test if its this players turn FIRST.
		//TODO does this need to change turns if it's this player's turn?

		return ServerCommandUtils.getCommandsFromGame(mGameID, mLastCommand, mUsername);
	}
}
