package edu.byu.cs340.models;

/**
 * Created by joews on 2/1/2017.
 */
public interface IAuthenticator
{
	String login(String username, String password);

	String register(String username, String password);

	BaseUser authenticate(String authenticationToken);

	boolean hasUserByName(String username);

	boolean hasUserByAuthentication(String username);

	boolean signOut(String username);

	void loadFromDatabase();
}
