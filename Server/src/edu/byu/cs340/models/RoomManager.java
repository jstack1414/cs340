package edu.byu.cs340.models;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.database.DatabaseWriter;
import edu.byu.cs340.models.gameModel.GameParameters;
import edu.byu.cs340.models.gameModel.IServerGameModel;
import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.map.ICity;
import edu.byu.cs340.models.gameModel.map.IRoute;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.lobbyModel.ServerLobbyModel;
import edu.byu.cs340.models.roomModel.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joews on 2/1/2017.
 *
 * Class for managing LobbyModels and GameModels.
 * Adds, removes, and gets information about the rooms
 *
 * @invariant mLobbyMap != null
 * @invariant mGameMap != null
 */
public class RoomManager
{
	//- rooms:Map<mRoomID:String, room:LobbyModel>
	protected Map<String, ServerLobbyModel> mLobbyMap;
	//- rooms:Map<mRoomID:String, room:GameModel>
	protected Map<String, ServerGameModel> mGameMap;

	protected Map<String, String> mUsernameToRoomID;


	/**
	 * This is the default (and only) constructor, which initializes the room maps
	 *
	 * @post mLobbyMap != null
	 * @post mGameMap != null
	 */
	public RoomManager()
	{
		mLobbyMap = new HashMap<>();
		mGameMap = new HashMap<>();
		mUsernameToRoomID = new HashMap<>();
	}

	/**
	 * Adds a player to a room
	 *
	 * @param username the name of the user to add
	 * @param roomID   the ID of the room to add the player to
	 * @return true if the player was successfully added, false otherwise
	 *
	 * @pre username != null && !username.isEmpty()
	 * @pre roomID != null && !roomID.isEmpty()
	 * @pre the given roomID maps to an existing room
	 * @post a user with the given username is added to the room with the given roomID
	 */
	public boolean addPlayer(String username, String roomID)
	{
		boolean result = false;
		boolean isGame = false;
		IPlayerRoom room = null;
		if (mLobbyMap.containsKey(roomID))
		{
			room = mLobbyMap.get(roomID);
			isGame = false;

		} else if (mGameMap.containsKey(roomID))
		{
			room = mGameMap.get(roomID);
			isGame = true;
		}
		if (room != null)
		{
			if (room.getNumPlayers() < GameParameters.MAX_PLAYERS)
			{
				if (!room.hasPlayer(username))
				{
					result = room.addPlayer(username);
					mUsernameToRoomID.put(username, roomID);
					//update database
					updateDatabaseMap();
				}
			}
			if (isGame)
			{
				mGameMap.put(roomID, (ServerGameModel) room);
			} else
			{
				mLobbyMap.put(roomID, (ServerLobbyModel) room);
			}
		}
		return result;
	}

	/**
	 * @param username the username of the player to remove
	 * @param roomID   the ID of the room to remove them from
	 * @return true if the remove was successful, false otherwise
	 *
	 * @pre username != null && !username.isEmpty()
	 * @pre roomID != null && !roomID.isEmpty()
	 * @pre the given roomID maps to an existing room
	 * @post the specified player has been removed from the specified room
	 */
	public boolean removePlayer(String username, String roomID)
	{
		boolean result = false;
		IPlayerRoom gameRoom = null;
		IPlayerRoom lobbyoom = null;
		if (mLobbyMap.containsKey(roomID))
		{
			lobbyoom = mLobbyMap.get(roomID);

		}
		if (mGameMap.containsKey(roomID))
		{
			gameRoom = mGameMap.get(roomID);
		}
		if (lobbyoom != null)
		{
			if (lobbyoom.hasPlayer(username))
			{
				result = lobbyoom.removePlayer(username);
				if (lobbyoom.isEmpty())
				{
					mLobbyMap.remove(roomID);
					DatabaseWriter.getInstance().removeLobby(roomID);
				}
			}
		}
		if (gameRoom != null)
		{
			if (gameRoom.hasPlayer(username))
			{
				result = gameRoom.removePlayer(username);
				if (gameRoom.isEmpty())
				{
					mGameMap.remove(roomID);
					DatabaseWriter.getInstance().removeGame(roomID);
				}
			}
		}
		mUsernameToRoomID.remove(username);
		//update database
		updateDatabaseMap();
		return result;
	}

	public String getPlayerLobbyID(String username)
	{
		if (mUsernameToRoomID.containsKey(username))
		{
			String res = mUsernameToRoomID.get(username);
			if (hasLobby(res))
			{
				return res;
			}
		}
		return null;
	}

	public String getPlayerGameID(String username)
	{
		if (mUsernameToRoomID.containsKey(username))
		{
			String res = mUsernameToRoomID.get(username);
			if (hasGame(res))
			{
				return res;
			}
		}
		return null;
	}

	/**
	 * @param roomID the ID of the room to return
	 * @return an IPlayerRoom matching the given ID
	 *
	 * @pre roomID != null && !roomID.isEmpty()
	 * @pre roomID maps to an existing room in this manager
	 */
	public IPlayerRoom getRoom(String roomID)
	{
		if (mLobbyMap.containsKey(roomID))
		{
			return mLobbyMap.get(roomID);
		} else if (mGameMap.containsKey(roomID))
		{

			return mGameMap.get(roomID);
		} else
		{
			return null;
		}
	}

	/**
	 * @param roomID the ID of the room to return
	 * @return an IServerGameModel matching the given ID
	 *
	 * @pre roomID != null && !roomID.isEmpty()
	 * @pre hasGame(roomID)==true
	 * @post the game models or null if the models doesn't exist.
	 */
	public IServerGameModel getGame(String roomID)
	{
		if (hasGame(roomID))
		{
			return mGameMap.get(roomID);
		} else
		{
			return null;
		}
	}

	/**
	 * Checks if a lobby is in the manager
	 *
	 * @param roomID the ID of the lobby to check for
	 * @return whether the lobby with the given ID is among this manager's lobbies
	 *
	 * @pre roomID != null && !roomID.isEmpty()
	 */
	public boolean hasLobby(String roomID)
	{
		return mLobbyMap.containsKey(roomID);
	}

	/**
	 * Checks if a game is in the manager
	 *
	 * @param roomID the ID of the game to check for
	 * @return whether the game with the given ID is among this manager's games
	 *
	 * @pre roomID != null && !roomID.isEmpty()
	 */
	public boolean hasGame(String roomID)
	{
		return mGameMap.containsKey(roomID);
	}

	/**
	 * Checks if a room, either lobby or game, is in the manager
	 *
	 * @param roomID the ID of the room to check for
	 * @return whether the room with the given ID is in the manager
	 *
	 * @pre roomID != null && !roomID.isEmpty()
	 */
	public boolean hasRoom(String roomID)
	{
		try
		{
			return mLobbyMap.containsKey(roomID) || mGameMap.containsKey(roomID);
		} catch (Exception e)
		{
			return false;
		}
	}

	/**
	 * Adds a lobby that already exists to the manager
	 *
	 * @param lobby the lobby to add
	 * @return true if adding the lobby was successful, false otherwise
	 *
	 * @pre lobby != null
	 * @pre lobby instanceof LobbyModel
	 * @pre the lobby is not already in the manager
	 * @post the lobby has been added to this manager
	 */
	public boolean addLobby(LobbyModel lobby)
	{
		if (!mLobbyMap.containsKey(lobby.getRoomID()))
		{
			mLobbyMap.put(lobby.getRoomID(), (ServerLobbyModel) lobby);
			DatabaseWriter.getInstance().setLobby((ServerLobbyModel) lobby);
			return true;
		}
		return false;
	}

	/**
	 * Adds a game that already exists to the manager
	 *
	 * @param game the game to add
	 * @return true if adding the game was successful, false otherwise
	 *
	 * @pre game != null
	 * @pre game instanceof GameModel
	 * @pre the game is not already in the manager
	 * @post the game has been added to this manager
	 */
	public boolean addGame(ServerGameModel game)
	{
		if (!mGameMap.containsKey(game.getRoomID()))
		{
			mGameMap.put(game.getRoomID(), (ServerGameModel) game);
			DatabaseWriter.getInstance().setGame((IServerGameModel) game);
			return true;
		}
		return false;
	}

	/**
	 * Creates a lobby with the given name and host, then adds it to this manager
	 *
	 * @param roomName     the name to give the new lobby
	 * @param hostUsername the username of the player to make the host
	 * @return the ID of the created lobby
	 *
	 * @pre roomName != null && !roomName.isEmpty()
	 * @pre hostUsername != null && !hostUsername.isEmpty()
	 * @post a lobby has been created with the given name and host and added to the manager
	 */
	public String addLobby(String roomName, String hostUsername)
	{
		String roomID = null;
		LobbyModel room = new ServerLobbyModel(roomName, hostUsername);
		if (getRoom(room.getRoomID()) == null)
		{
			boolean result = addLobby(room);
			if (result)
			{
				roomID = room.getRoomID();
				mUsernameToRoomID.put(hostUsername, roomID);
				//update database
				updateDatabaseMap();
			}
		}
		return roomID;
	}

	private void updateDatabaseMap()
	{
		DatabaseWriter.getInstance().setUsernameToRoomIDMap(mUsernameToRoomID);
	}

	/**
	 * Creates a game with the given name and host, then adds it to the manager
	 *
	 * @param roomName     the name to give the new game
	 * @param hostUsername the username of the player to make the host
	 * @return the ID of the created game
	 *
	 * @pre roomName != null && !roomName.isEmpty()
	 * @pre hostUsername != null && !hostUsername.isEmpty()
	 * @post a game has been created with the given name and host and added to the manager
	 */
	public String addGame(String roomName, String hostUsername)
	{
		String roomID = null;
		ServerGameModel room = new ServerGameModel(roomName, hostUsername);
		if (getRoom(room.getRoomID()) == null)
		{
			boolean result = addGame(room);
			if (result)
			{
				roomID = room.getRoomID();
				mUsernameToRoomID.put(hostUsername, roomID);
				//update database
				updateDatabaseMap();
			}
		}
		return roomID;
	}

	/**
	 * @param roomID the ID of the room to remove
	 * @return whether the remove was successful
	 *
	 * @pre roomID != null
	 * @pre roomID maps to and existing room in this manager
	 * @post the specified room has been removed from this manager
	 */
	public boolean removeRoom(String roomID)
	{
		if (mLobbyMap.containsKey(roomID))
		{
			LobbyModel lobby = mLobbyMap.get(roomID);
			for (IPlayer player : lobby.getPlayers())
			{
				mUsernameToRoomID.put(player.getUsername(), roomID);
				//update database
				updateDatabaseMap();
			}
			mLobbyMap.remove(roomID);
			DatabaseWriter.getInstance().removeLobby(roomID);
			return true;
		} else if (mGameMap.containsKey(roomID))
		{
			ServerGameModel game = mGameMap.get(roomID);
			for (IPlayer player : game.getPlayers())
			{
				mUsernameToRoomID.put(player.getUsername(), roomID);
				//update database
				updateDatabaseMap();
			}
			mGameMap.remove(roomID);
			DatabaseWriter.getInstance().removeGame(roomID);
			return true;
		}
		return false;
	}

	/**
	 * Creates a RoomsListInfo object from the list of lobbies currently in this manager
	 *
	 * @return a RoomsListInfo containing information about this manager's lobbies
	 */
	public RoomsListInfo getLobbyList()
	{
		RoomsListInfo roomsListInfo = new RoomsListInfo();
		for (Map.Entry<String, ServerLobbyModel> o : mLobbyMap.entrySet())
		{
			IRoomInfo info = new RoomInfo(o.getValue());
			roomsListInfo.updateAddGame(info);
		}
		return roomsListInfo;
	}

	/**
	 * Creates a game from the lobby mapped to by roomID.
	 * Adds the game to the manager and removes the lobby from the manager.
	 *
	 * @param lobbyID the ID of the lobby to turn into a game
	 * @pre lobbyID != null && !lobbyID.isEmpty()
	 * @pre there is not already a game with ID lobbyID in this manager
	 * @post a game with the given ID has been added to this manager, with the same players as the specified lobby
	 * @post the specified lobby has been removed from this manager
	 */
	public void createGame(String lobbyID, List<ITrainCard> trainCards, List<IDestinationCard> destinationCards,
	                       List<ICity> cities,
	                       List<IRoute> routes)
	{
		ServerLobbyModel lobby = mLobbyMap.get(lobbyID);
		ServerGameModel game = new ServerGameModel();
		game.initializeFromLobby(lobby, trainCards, destinationCards, cities, routes);
		mLobbyMap.remove(lobbyID);
		DatabaseWriter.getInstance().removeLobby(lobbyID);
		DatabaseWriter.getInstance().setGame(game);
		mGameMap.put(game.getRoomID(), game);
	}

	public ServerLobbyModel getLobby(String lobbyId)
	{
		if (hasLobby(lobbyId))
		{
			return mLobbyMap.get(lobbyId);
		} else
		{
			return null;
		}
	}

	public void loadFromDatabase()
	{
		mLobbyMap = new HashMap<>();
		mGameMap = new HashMap<>();
		mUsernameToRoomID = new HashMap<>();

		//get the lobbies
		List<ServerLobbyModel> lobbies = DatabaseWriter.getInstance().getLobbies();
		for (ServerLobbyModel lobby : lobbies)
		{
			mLobbyMap.put(lobby.getRoomID(), lobby);
			//get the users from the lobby and add them to the map
			for (IPlayer player : lobby.getPlayers())
			{
				mUsernameToRoomID.put(player.getUsername(), lobby.getRoomID());
			}
			//get commands assocaited with this lobby
			List<IBaseCommand> cmds = DatabaseWriter.getInstance().getLobbyCommands(lobby.getRoomID());
			//SET LOBBY TO CLEAR
			DatabaseWriter.getInstance().setLobby(lobby);
			//run these commands in the lobby
			for (IBaseCommand cmd : cmds)
			{
				//execute the command
				try
				{
					if (cmd != null)
					{
						cmd.execute();
					}
				} catch (IBaseCommand.CommandException e)
				{
					e.printStackTrace();
				}
			}
		}

		//get the games
		List<ServerGameModel> games = DatabaseWriter.getInstance().getGames();
		for (ServerGameModel game : games)
		{
			mGameMap.put(game.getRoomID(), game);
			//get the users from the lobby and add them to the map
			for (IPlayer player : game.getPlayers())
			{
				mUsernameToRoomID.put(player.getUsername(), game.getRoomID());
			}
			//get commands assocaited with this lobby
			List<IBaseCommand> cmds = DatabaseWriter.getInstance().getGameCommands(game.getRoomID());
			//SET Game TO CLEAR
			DatabaseWriter.getInstance().setGame(game);
			//run these commands in the lobby
			for (IBaseCommand cmd : cmds)
			{
				//execute the command
				try
				{
					if (cmd != null)
					{
						cmd.execute();
					}
				} catch (IBaseCommand.CommandException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}
