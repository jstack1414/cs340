package edu.byu.cs340.models;

import java.util.Calendar;

/**
 * Created by joews on 3/17/2017.
 */
public class ServerChatMessenger extends ChatMessenger
{
	@Override
	public void addChatMessage(ChatMessage message)
	{
		message.setIndex(Calendar.getInstance().getTimeInMillis());
		super.addChatMessage(message);
	}
}
