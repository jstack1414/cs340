package edu.byu.cs340.models.gameModel.bank;

import edu.byu.cs340.models.gameModel.GameParameters;
import edu.byu.cs340.models.gameModel.map.TrainType;

import java.util.*;

/**
 * Created by joews on 2/22/2017.
 *
 * OBject representing the game bank. This holds the destination cards, and train cards associated with the game in
 * shared space.
 */
public class GameBank implements IGameBank
{
	private Deque<IDestinationCard> mDestinationCards;
	private Stack<ITrainCard> mDiscardedTrainCards;
	private Deque<ITrainCard> mTrainCards;
	private ITrainCard[] mRevealedTrainCards;
	private boolean mIsTrainCardsSet = false;
	private boolean mIsDestinationCardsSet = false;

	/**
	 * Constructor for creating the game bank. This sets the destination, train and revealed cars to nothing.
	 */
	public GameBank()
	{
		mDestinationCards = new ArrayDeque<>();
		mDiscardedTrainCards = new Stack<>();
		mTrainCards = new ArrayDeque<>();
		mRevealedTrainCards = new TrainCard[GameParameters.NUM_REVEALED_CARDS];
	}


	@Override
	public void shuffleDestinationCards()
	{
		mDestinationCards = new ArrayDeque<>(shuffleCards(mDestinationCards));
	}

	@Override
	public void shuffleTrainCards()
	{
		mTrainCards = new ArrayDeque<>(shuffleCards(mTrainCards));
	}

	/**
	 * Shuffles a collection of ICard's and returns the shuffled bank as a collection in order.
	 */
	private <T extends ICard> Collection<T> shuffleCards(Collection<T> cards)
	{
		List<T> cardList = new ArrayList<>(cards);
		Collections.shuffle(cardList);
		return cardList;
	}

	@Override
	public void setDestinationCards(List<IDestinationCard> destinationCards)
	{
		if (destinationCards == null)
		{
			mDestinationCards = new ArrayDeque<>();
		} else
		{
			mDestinationCards = new ArrayDeque<>(destinationCards);
		}
		mIsDestinationCardsSet = true;
	}

	@Override
	public void setTrainCards(List<ITrainCard> trainCards)
	{
		mDiscardedTrainCards = new Stack<>();
		mRevealedTrainCards = new TrainCard[GameParameters.NUM_REVEALED_CARDS];
		if (trainCards == null)
		{
			mTrainCards = new ArrayDeque<>();
		} else
		{
			mTrainCards = new ArrayDeque<>(trainCards);
			updateRevealedTrainCards();
		}
		mIsTrainCardsSet = true;
	}

	@Override
	public IDestinationCard drawDestinationCardFromDeck()
	{
		if (mDestinationCards.size() > 0)
		{
			return mDestinationCards.pop();
		} else
		{
			return null;
		}
	}

	@Override
	public ITrainCard drawTrainCardFromRevealed(int revealedZone)
	{
		if (0 <= revealedZone && 4 >= revealedZone)
		{
			ITrainCard result = mRevealedTrainCards[revealedZone];
			if (result != null)
			{
				result = new TrainCard((TrainCard) result);
			}
			mRevealedTrainCards[revealedZone] = null;
			updateRevealedTrainCards();
			return result;
		} else
		{
			return null;
		}
	}

	@Override
	public ITrainCard drawTrainCardFromDeck()
	{
		return drawTrainCardFromDeck(true);
	}

	private ITrainCard drawTrainCardFromDeck(boolean updateCards)
	{
		ITrainCard result = null;
		if (mTrainCards.size() > 1)
		{
			result = mTrainCards.pop();
		} else if (mTrainCards.size() > 0)
		{
			result = mTrainCards.pop();
			shuffleInDiscardedTrains();
		} else if (mDiscardedTrainCards.size() > 0)
		{
			shuffleInDiscardedTrains();
			if (mDiscardedTrainCards.size() > 0)
			{
				result = mTrainCards.pop();
			}
		}
		if (updateCards)
		{
			updateRevealedTrainCards();
		}
		return result;
	}

	private void shuffleInDiscardedTrains()
	{
		if (mDiscardedTrainCards != null)
		{
			mTrainCards = new ArrayDeque<>();
			for (ITrainCard card : mDiscardedTrainCards)
			{
				if (card != null)
				{
					mTrainCards.add(card);
				}
			}
		}
		shuffleTrainCards();
		mDiscardedTrainCards.clear();
	}

	@Override
	public List<ITrainCard> getRevealedCards()
	{
		return Collections.unmodifiableList(Arrays.asList(mRevealedTrainCards));
	}

	@Override
	public void putDestinationCardsOnBottom(List<IDestinationCard> destinationCardList)
	{
		if (destinationCardList != null)
		{
			for (IDestinationCard card : destinationCardList)
			{
				this.mDestinationCards.add(card);
			}
		}
	}

	@Override
	public void putTrainCardsInDiscard(List<ITrainCard> trainCards)
	{
		if (trainCards != null)
		{
			for (ITrainCard card : trainCards)
			{
				this.mDiscardedTrainCards.push(card);
			}
		}
		if (getNumTrainCardsInDeck() <= 0)
		{
			shuffleInDiscardedTrains();
		}
		updateRevealedTrainCards();
	}


	@Override
	public void updateRevealedTrainCards()
	{
		int maxIterations = 10; //this prevents us from recurring forever if there are to many locomotives
		boolean success = false;
		//itearte untill a valid revealed zone is established
		for (int j = 0; j < maxIterations && !success; j++)
		{
			//the number of locomotives
			int locomotiveCount = 0;
			//iterate through each zone to ensure it has a card
			for (int i = 0; i < GameParameters.NUM_REVEALED_CARDS; i++)
			{
				//if the zone is empty draw a card an put it there
				if (mRevealedTrainCards[i] == null)
				{
					mRevealedTrainCards[i] = drawTrainCardFromDeck(false);
				}
				//test if locomotive, increment if so
				if (mRevealedTrainCards[i] != null)
				{
					TrainType type = mRevealedTrainCards[i].getTrainType();
					if (type != null && type.equals(TrainType.Locomotive))
					{
						locomotiveCount++;
					}
				}
			}
			//if the number of locomotives is too many
			if (locomotiveCount >= GameParameters.MAX_LOCOMOTIVE_ALLOWED &&
					j < maxIterations - 1)
			{
				//clearDAO each zone and put the bank in the discard pile.
				for (int i = 0; i < GameParameters.NUM_REVEALED_CARDS; i++)
				{
					mDiscardedTrainCards.push(mRevealedTrainCards[i]);
					mRevealedTrainCards[i] = null;
				}
				//continue loop
			} else
			{
				//end loop
				success = true;
			}
		}
	}

	@Override
	public boolean isIntialized()
	{
		return mIsDestinationCardsSet && mIsTrainCardsSet;
	}

	@Override
	public int getNumTrainCardsInDeck()
	{
		return mTrainCards.size();
	}

	@Override
	public int getNumDestinationCardsInDeck()
	{
		return mDestinationCards.size();
	}

//	//TODO: DEMO ONLY
//	@Override
//	public List<IDestinationCard> getDestinationCardDeck()
//	{
//		return new ArrayList<>(mDestinationCards);
//	}
//
//	//TODO: DEMO ONLY
//	@Override
//	public List<ITrainCard> getDemoTrainCardDeck()
//	{
//		return new ArrayList<>(mTrainCards);
//	}

	@Override
	public int trainCardDiscardCount()
	{
		return mDiscardedTrainCards.size();
	}
}

