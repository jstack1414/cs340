package edu.byu.cs340.models.gameModel.map;

import edu.byu.cs340.models.gameModel.players.GamePlayer;

import java.util.*;

/**
 * Created by joews on 2/24/2017.
 *
 * The game map containing the cities and routes.
 */
public class GameMap implements IGameMap
{
	private Map<String, ICity> mCities;
	private Map<String, IRoute> mRoutes;
	private Map<String, Set<IRoute>> mCitiesToRoutes;
	private Set<String> mUserLongestRoute;


	public GameMap()
	{
		mCitiesToRoutes = new HashMap<>();
		mUserLongestRoute = new HashSet<>();
	}

	@Override
	public IRoute getRoute(String routeID)
	{
		if (mRoutes != null)
		{
			return new Route(mRoutes.get(routeID));
		} else
		{
			return null;
		}
	}

	@Override
	public boolean hasRoute(String routeID)
	{
		return (mRoutes != null) && mRoutes.containsKey(routeID);
	}

	@Override
	public boolean claimRoute(String routeID, String username)
	{
		IRoute route = null;
		if (mRoutes != null)
		{
			route = mRoutes.get(routeID);
		}
		if (route != null && !route.isClaimed())
		{
			return route.claimRoute(username);
		} else
		{
			return false;
		}
	}

	@Override
	public List<IRoute> getRoutes()
	{
		return Collections.unmodifiableList(new ArrayList<>(mRoutes.values()));
	}

	@Override
	public List<ICity> getCities()
	{
		return Collections.unmodifiableList(new ArrayList<>(mCities.values()));
	}

	@Override
	public Set<String> getLongestRoutePlayer()
	{
		return mUserLongestRoute;
	}

	public void setLongestRoutePlayer(Set<String> usernames)
	{
		mUserLongestRoute = usernames;
	}

	@Override
	public int calculateLongestRoute(GamePlayer player)
	{
		Set<IRoute> visitedRoutes = new HashSet<>();
		int result = 0;

		for (IRoute route : player.getRoutes())
		{
			int current = route.getNumTrains();
			int max = 0;
			if (!visitedRoutes.contains(route))
			{
				visitedRoutes.add(route);
				for (ICity city : route.getCities())
				{
					int newCurrent = current + calculateBranchSize(visitedRoutes, city, player);
					max = newCurrent > max ? newCurrent : max;
				}
				visitedRoutes.remove(route);
			}
			result = max > result ? max : result;
		}
		return result;
	}

	private int calculateBranchSize(Set<IRoute> visitedRoutes, ICity city, GamePlayer player)
	{
		int result = 0;
		for (IRoute route : mCitiesToRoutes.get(city.toString()))
		{
			if(!route.isClaimed() || visitedRoutes.contains(route))
			{
				continue;
			}
			if (!route.getClaimedUsername().equals(player.getUsername()))
			{
				continue;
			}

			int current = route.getNumTrains();

			visitedRoutes.add(route);
			for (ICity otherCity : route.getCities())
			{
				if (!otherCity.equals(city))
				{
					current += calculateBranchSize(visitedRoutes, otherCity, player);
				}
			}
			visitedRoutes.remove(route);
			result = current > result ? current : result;
		}


		return result;
	}


	@Override
	public void setRoutes(List<IRoute> routes)
	{
		mRoutes = new HashMap<>();
		for (IRoute route : routes)
		{
			for (ICity city : route.getCities())
			{
				mCitiesToRoutes.putIfAbsent(city.toString(), new HashSet<>());
				mCitiesToRoutes.get(city.toString()).add(route);
			}
			mRoutes.put(route.getId(), route);
		}
	}

	@Override
	public void setCities(List<ICity> cities)
	{
		mCities = new HashMap<>();
		for (ICity city : cities)
		{
			mCities.put(city.getId(), city);
		}
	}


	@Override
	public boolean isInitialized()
	{
		return mRoutes != null && mCities != null;
	}
}
