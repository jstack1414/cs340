package edu.byu.cs340.models.gameModel;


import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.command.game.ServerLastRoundCommand;
import edu.byu.cs340.command.lobby.StartGameCommand;
import edu.byu.cs340.database.DatabaseWriter;
import edu.byu.cs340.models.ServerChatMessenger;
import edu.byu.cs340.models.gameModel.bank.*;
import edu.byu.cs340.models.gameModel.gameOver.GameOverData;
import edu.byu.cs340.models.gameModel.gameOver.PlayerScoreStruct;
import edu.byu.cs340.models.gameModel.map.*;
import edu.byu.cs340.models.gameModel.players.GamePlayer;
import edu.byu.cs340.models.gameModel.players.GamePlayerManager;
import edu.byu.cs340.models.gameModel.server_state.*;
import edu.byu.cs340.models.lobbyModel.LobbyModel;
import edu.byu.cs340.models.lobbyModel.LobbyPlayerManager;
import edu.byu.cs340.models.roomModel.CommandManager;
import edu.byu.cs340.models.roomModel.IPlayer;
import edu.byu.cs340.models.roomModel.PlayerManager;
import edu.byu.cs340.models.roomModel.PlayerRoom;

import java.util.*;

/**
 * Created by joews on 2/1/2017.
 *
 * Base class for the game models. Shared code between the client and server version goes here.
 */
public class ServerGameModel extends PlayerRoom implements IServerGameModel, IServerStatable
{
	private boolean mIsInitialized = false;
	private IGameBank mGameBank;
	private IGameMap mGameMap;
	private List<IDestinationCard> mDestinationCardsWaitingToBePicked;
	private boolean mSetupComplete = false;
	private IServerState mServerState;
	private boolean mIsFinalRound;
	private String mFinalRoundLastPlayer;
	private boolean mIsLastTurn;
	/**
	 * Class name of the command
	 */
	protected final String mClassName = getClass().getSimpleName().replace("Server", "");
	/**
	 * Package directory
	 */
	protected final String mClassPackage = getClass().getPackage().getName();


	/**
	 * Constructor for creating the game models. This constructor does not fully initialize the models.
	 *
	 * @post isInitialized()==false
	 */
	public ServerGameModel()
	{
		super();
		mGameBank = new GameBank();
		mGameMap = new GameMap();
		mDestinationCardsWaitingToBePicked = new ArrayList<>();
		setState(new StartGameState());
		mIsFinalRound = false;
//		mChatMessenger = new ServerChatMessenger(); //override messenger with server version that assigns indices
	}

	/**
	 * Constructor setting the room name
	 *
	 * @param roomName
	 * @param username
	 */
	public ServerGameModel(String roomName, String username)
	{
		super(roomName, username);
//		mChatMessenger = new ServerChatMessenger(); //override messenger with server version that assigns indices
	}


	//<editor-fold desc="Required by PlayerRoom">
	@Override
	protected PlayerManager createPlayerManager()
	{
		return new GamePlayerManager();
	}

	@Override
	protected PlayerManager createPlayerManager(String hostName)
	{
		return new GamePlayerManager(hostName);
	}

	/**
	 * Override the getPlayer functionality by casting the player as a GamePlayer.
	 *
	 * @param username
	 * @return
	 */
	@Override
	public GamePlayer getPlayer(String username)
	{
		return (GamePlayer) super.getPlayer(username);
	}

	@Override
	public GamePlayerManager getPlayerManager()
	{
		return ((GamePlayerManager) mPlayerManager);
	}
	//</editor-fold>


	//<editor-fold desc="IServerGameModel">
	@Override
	public List<IDestinationCard> drawDestinationCards(String username)
	{
		//test to see if player has temporary cards already, if not then draw newo nes
		mServerState.verifyState(StateActions.DrawDestinationCards, this);

		if (mServerState instanceof StartGameState)
		{
			((StartGameState) mServerState).setPlayer(getPlayer(username));
		}

		List<IDestinationCard> cards = new ArrayList<>();
		GamePlayer player = getPlayer(username);

		if (player.equals(getCurrentPlayer()) || getState() instanceof StartGameState)
		{
			List<IDestinationCard> list = (List<IDestinationCard>) mServerState.takeAction(StateActions
					.DrawDestinationCards, this);
			return list;
		}

		return null;
	}

	@Override
	public ITrainCard drawTrainCardFromDeck(String username)
	{
		mServerState.verifyState(StateActions.DrawTrainCardDeck, this);
		if (getCurrentPlayer().getUsername().equals(username) || getState() instanceof StartGameState)
		{
			Object card = mServerState.takeAction(StateActions.DrawTrainCardDeck, this);
			if (card instanceof ITrainCard)
				return (ITrainCard) card;
		}

		return null;
	}

	@Override
	public ITrainCard drawTrainCardFromRevealed(int revealedZone, String username)
	{
		mServerState.verifyState(StateActions.DrawTrainCardRevealed, this);
		if (getCurrentPlayer().equals(getPlayer(username)))
		{
			Object card = mServerState.takeAction(StateActions.DrawTrainCardRevealed, this, revealedZone);
//			ITrainCard card = this.mGameBank.drawTrainCardFromRevealed(revealedZone);
			if (card != null && card instanceof ITrainCard)
			{
				getPlayer(username).addTrainCard((ITrainCard) card);
				return (ITrainCard) card;
			}
		}
		return null;
	}

	@Override
	public boolean canDrawAnotherCard()
	{
		int cardsAvailable = mGameBank.getNumTrainCardsInDeck();
		for (ICard card : mGameBank.getRevealedCards())
		{
			if (card != null)
			{
				cardsAvailable++;
			}
		}

		if (cardsAvailable == 0)
		{
			return false;
		} else if (getState() instanceof DrawTrainCardState)
		{
			return true;
		} else if (getState() instanceof StartGameState)
		{
			return true;
		}
		return false;
	}

	@Override
	public List<ITrainCard> getRevealedCards()
	{
		return this.mGameBank.getRevealedCards();
	}


//	@Override
//	public void setDestinationCards(List<DestinationCard> destinationCards)
//	{
//		this.mGameBank.setDestinationCards(destinationCards);
//	}
//
//
//	@Override
//	public void setTrainCards(List<TrainCard> trainCards)
//	{
//		this.mGameBank.setTrainCards(trainCards);
//	}


	@Override
	public boolean returnDestinationCardsToBottom(List<IDestinationCard> destinationCardList, String username)
	{
		mServerState.verifyState(StateActions.DrawTrainCardRevealed, this);

		if (mServerState instanceof StartGameState)
		{
			((StartGameState) mServerState).setPlayer(getPlayer(username));
		}
		try
		{
			boolean result = (boolean) mServerState.takeAction(StateActions.ReturnDestinationCards, this,
					destinationCardList);
			updateScore();
			return result;
		} catch (ClassCastException e)
		{
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public int getNumTrains(String username)
	{
		GamePlayer player = getPlayer(username);
		return player.getTrainsRemaining();
	}

	@Override
	public int getNumTrainCardsInDeck()
	{
		return mGameBank.getNumTrainCardsInDeck();
	}

	@Override
	public int getNumTrainCardsInDiscard()
	{
		return mGameBank.trainCardDiscardCount();
	}

	@Override
	public int getNumDestinationCardsInDeck()
	{
		return mGameBank.getNumDestinationCardsInDeck();
	}

	//	@Override
//	public void putTrainCardsInDiscard(List<TrainCard> trainCards)
//	{
//		this.mGameBank.putTrainCardsInDiscard(trainCards);
//	}

	@Override
	public void initializeFromLobby(LobbyModel lobby, List<ITrainCard> trainCards, List<IDestinationCard>
			destinationCards, List<ICity> cities,
	                                List<IRoute> routes)
	{
		GamePlayerManager playerManager = new GamePlayerManager();
		playerManager.initializeFromLobby((LobbyPlayerManager) lobby.getPlayerManager());
		List<IPlayer> players = lobby.getPlayerManager().getPlayers();
		this.mPlayerManager = playerManager;
		mCommandManager = new CommandManager();
		for (IPlayer player : players)
		{
			mCommandManager.addPlayer(player.getUsername());
		}
		this.mRoomID = lobby.getRoomID();
		this.mRoomName = lobby.getRoomName();
		if (mGameMap != null)
		{
			if (cities != null)
			{
				mGameMap.setCities(cities);
			}
			if (routes != null)
			{
				mGameMap.setRoutes(routes);
			}
			if (trainCards != null)
			{
				mGameBank.setTrainCards(trainCards);
				mGameBank.shuffleTrainCards();
				//reveal cards as needed
				mGameBank.updateRevealedTrainCards();
			}
			if (destinationCards != null)
			{
				mGameBank.setDestinationCards(destinationCards);
				mGameBank.shuffleDestinationCards();
			}
		}
		mChatMessenger = new ServerChatMessenger();//lobby.getChatMessenger();
		this.mIsInitialized = true;
	}


	@Override
	public boolean isPlayersTurn(String username)
	{
		return getPlayerManager().isCurrentPlayer(username);
	}

	@Override
	public boolean nextPlayer()
	{
		//check if all of hte cards have been returned to the game bank
		for (IPlayer player : getPlayers())
		{
			GamePlayer gamePlayer = (GamePlayer) player;
			if (gamePlayer.hasTemporaryCards())
			{
				return false;
			}
		}
		getPlayerManager().nextPlayer();
		return true;
	}

	@Override
	public boolean setNextPlayer(String nextPlayer)
	{
		//check if all of hte cards have been returned to the game bank
		for (IPlayer player : getPlayers())
		{
			GamePlayer gamePlayer = (GamePlayer) player;
			if (gamePlayer.hasTemporaryCards())
			{
				return false;
			}
		}
		getPlayerManager().setNextPlayer(nextPlayer);
		return true;
	}


	@Override
	public String getNextPlayerName()
	{
		return getPlayerManager().getNextPlayerName();
	}
//
//	//TODO: DEMO ONLY
//	@Override
//	public List<ITrainCard> getDemoTrainCards()
//	{
//		return mGameBank.getDemoTrainCardDeck();
//	}
//
//	//TODO: DEMO ONLY
//	@Override
//	public List<IDestinationCard> getDemoDestinationCards()
//	{
//		return mGameBank.getDestinationCardDeck();
//	}

	@Override
	public boolean isGameOver()
	{
		return mServerState instanceof GameOverState;
	}

	public GameOverData getGameOverData()
	{
		GameOverData data = new GameOverData();
		for (IPlayer player : getPlayers())
		{
			GamePlayer gamePlayer = (GamePlayer) player;
			PlayerScoreStruct struct = gamePlayer.getScoreStruct();
			data.addPlayerStruct(struct);
		}
		return data;
	}

	@Override
	public boolean isFinalRound()
	{
		return mIsFinalRound;
	}

	public String getFinalRoundLastPlayer()
	{
		return mFinalRoundLastPlayer;
	}

	@Override
	public void markLastTurn()
	{
		mIsLastTurn = true;
	}

	public boolean isLastTurn()
	{
		return mIsLastTurn;
	}

	@Override
	public boolean hasDestinationCards()
	{
		return (mGameBank.getNumDestinationCardsInDeck() > 0);
	}

	@Override
	public int getNumDestinationCards(String username)
	{
		return getPlayer(username).getDestinationCards().size();
	}


	public void markFinalRound()
	{
		mIsFinalRound = true;
		this.addCommand(new ServerLastRoundCommand(getCurrentPlayerName(), -1, getRoomID()));
	}

	@Override
	public boolean addCommand(IBaseCommand command)
	{
		boolean result = super.addCommand(command);
		if (command instanceof StartGameCommand)
		{
			DatabaseWriter.getInstance().setGame(this);
		} else
		{
			DatabaseWriter.getInstance().addCommandToGame(this.getRoomID(), command);
			ServerCommandUtils.testSaveGame(this);
		}
		return result;
	}

	@Override
	public String getCurrentPlayerName()
	{
		if (getCurrentPlayer() != null)
		{
			return getCurrentPlayer().getUsername();
		} else
		{
			return null;
		}
	}

	@Override
	public boolean isInitialized()
	{
		return mIsInitialized;
	}

	@Override
	public boolean claimRoute(String routeID, List<ITrainCard> trainCards)
	{
		if (mServerState instanceof PlayerTurnState)
		{
			if (routeID == null)
			{
				return false;
			} else if (routeID.isEmpty())
			{
				return false;
			} else if (!mGameMap.hasRoute(routeID))
			{
				return false;
			} else if (trainCards == null)
			{
				return false;
			} else if (trainCards.isEmpty())
			{
				return false;
			} else if (!getCurrentPlayersTrainCards().containsAll(trainCards))
			{
				return false;
			} else if (trainCards.size() != mGameMap.getRoute(routeID).getNumTrains())
			{
				return false;
			} else if (mGameMap.getRoute(routeID).isClaimed())
			{
				return false;
			} else if (getCurrentPlayer().getTrainsRemaining() < mGameMap.getRoute(routeID).getNumTrains())
			{
				return false;
			} else
			{
				IRoute route = mGameMap.getRoute(routeID);
				TrainType requiredType = route.getType();
				for (ITrainCard card : trainCards)
				{
					if (requiredType == TrainType.None && card.getTrainType() != TrainType.Locomotive)
					{
						requiredType = card.getTrainType();
					} else if (!(card.getTrainType() == requiredType || card.getTrainType() == TrainType.Locomotive))
					{
						return false;
					}
				}
				boolean result = mGameMap.claimRoute(routeID, getCurrentPlayer().getUsername());
				if (result)
				{
					getCurrentPlayer().claimRoute(mGameMap.getRoute(routeID));
					getCurrentPlayer().discardTrainCards(trainCards);
					mGameBank.putTrainCardsInDiscard(trainCards);
				}

				getCurrentPlayer().setLongestRouteSize(mGameMap.calculateLongestRoute(getCurrentPlayer()));

				testFinalRound();
				//reset to player to trigger construcotr
				setState(new PlayerTurnState(this));
				return result;
			}
		}

		return false;
	}

//	private void testGameOver()
//	{
//
//		//if not in the last round
//		if (!mIsFinalRound)
//		{
//			for (IPlayer player : getPlayerManager().getPlayers())
//			{
//				if (((GamePlayer) player).getTrainsRemaining() <= FINAL_ROUND_TRAINS_REMAINING)
//				{
//					markFinalRound();
//					mFinalRoundLastPlayer = player.getUsername();
//					break;
//				}
//			}
//		} else if (mIsFinalRound && getCurrentPlayerName().equals(mFinalRoundLastPlayer))
//		{
//			setState(new GameOverState(this));
//		}
//	}

	private void testFinalRound()
	{
		for (IPlayer player : getPlayerManager().getPlayers())
		{
			if (((GamePlayer) player).getTrainsRemaining() <= GameParameters.FINAL_ROUND_TRAINS_REMAINING &&
					!mIsFinalRound)
			{
				markFinalRound();
				mFinalRoundLastPlayer = player.getUsername();
				break;
			}
		}
	}

	@Override
	public List<String> getPlayerOrder()
	{
		return getPlayerManager().getPlayerOrder();
	}

	@Override
	public int getTrainsRemaining(String username)
	{
		return getPlayer(username).getTrainsRemaining();
	}

	@Override
	public List<IRoute> getRoutes()
	{
		return mGameMap.getRoutes();
	}

	@Override
	public List<ICity> getCities()
	{
		return mGameMap.getCities();
	}

	@Override
	public Set<String> getLongestRoutePlayers()
	{
		return mGameMap.getLongestRoutePlayer();
	}

	@Override
	public List<IDestinationCard> getCurrentPlayersDestinationCards()
	{
		List<IDestinationCard> cards = new ArrayList<>();
		GamePlayer currentplayer = getCurrentPlayer();
		if (currentplayer != null)
		{
			return Collections.unmodifiableList(currentplayer.getDestinationCards());
		} else
		{
			return Collections.unmodifiableList(new ArrayList<>());
		}
	}

	@Override
	public List<IDestinationCard> getCurrentPlayersTemporaryDestinationCards()
	{
		List<IDestinationCard> cards = new ArrayList<>();
		GamePlayer currentplayer = getCurrentPlayer();
		if (currentplayer != null)
		{
			return Collections.unmodifiableList(currentplayer.getTemporaryDestinationCards());
		} else
		{
			return Collections.unmodifiableList(new ArrayList<>());
		}
	}


	@Override
	public List<ITrainCard> getCurrentPlayersTrainCards()
	{
		List<IDestinationCard> cards = new ArrayList<>();
		GamePlayer currentplayer = getCurrentPlayer();
		if (currentplayer != null)
		{
			return Collections.unmodifiableList(currentplayer.getTrainCards());
		} else
		{
			return Collections.unmodifiableList(new ArrayList<>());
		}
	}

	@Override
	public Map<String, Integer> getPublicScore()
	{
		updateScore();
		Map<String, Integer> scoreMaps = new HashMap<>();
		List<IPlayer> players = getPlayerManager().getPlayers();
		for (IPlayer player : players)
		{
			GamePlayer gamePlayer = (GamePlayer) player;
			scoreMaps.put(gamePlayer.getUsername(), gamePlayer.getPublicScore());
		}
		return Collections.unmodifiableMap(scoreMaps);
	}

	@Override
	public int getPublicScore(String username)
	{
		updateScore();
		return getPlayer(username).getPublicScore();
	}

	@Override
	public int getHiddenScore(String username)
	{
		updateScore();
		return getPlayer(username).getHiddenScore();
	}

	//</editor-fold>

	@Override
	public boolean removePlayer(String username)
	{

		if (isFinalRound())
		{

			if (getFinalRoundLastPlayer().equals(username))
			{
				if (getFinalRoundLastPlayer().equals(getCurrentPlayerName()))
				{
					markLastTurn();
				}
				mFinalRoundLastPlayer = getPlayerManager().getPlayerBefore(username);
			}
		}
		getPlayerManager().removePlayer(getPlayer(username));
		return true;
	}

	/**
	 * Goes through each player and updates scores for each of them.
	 */


	public void updateScore()
	{
		//todo: fIX LONGEST ROUTE STUFF, THENE REMOVE PRINTING
		boolean PRINT = true;
		Set<String> longestRoutePlayers = mGameMap.getLongestRoutePlayer();
		int longestRoute = 1;
		for (IPlayer p : mPlayerManager.getPlayers())
		{
			int routeLength = ((GamePlayer) p).getLongestRouteSize();
			if (routeLength > longestRoute)
			{
				longestRoute = routeLength;
				longestRoutePlayers.clear();
				longestRoutePlayers.add(p.getUsername());
			} else if (routeLength == longestRoute)
			{
				longestRoutePlayers.add(p.getUsername());
			}
			if (PRINT)
			{
				System.out.println(p.getUsername() + " route len: " + routeLength);
			}
		}
		mGameMap.setLongestRoutePlayer(longestRoutePlayers);
		for (IPlayer p : mPlayerManager.getPlayers())
		{
			if (longestRoutePlayers.contains(p.getUsername()))
			{
				((GamePlayer) p).updateScore(true);
			} else
			{

				((GamePlayer) p).updateScore(false);
			}
		}
	}

	/**
	 * Gets the current player.
	 *
	 * @return Game player that is current.
	 *
	 * @post result==null if there is not current player
	 */
	private GamePlayer getCurrentPlayer()
	{
		return getPlayerManager().getCurrentPlayer();
	}

	@Override
	public boolean isInitialState()
	{
		return (getState() instanceof StartGameState);
	}

	@Override
	public IServerState getState()
	{
		return mServerState;
	}

	@Override
	public void setState(IServerState state)
	{
		if (!(mServerState instanceof GameOverState))
		{
			mServerState = state;
			System.out.println("State Changed: " + state.toString());
		}
	}

	public ITrainCard drawTrainCardFromDeck()
	{
		return mGameBank.drawTrainCardFromDeck();
	}

	public IDestinationCard drawDestinationCard()
	{
		return mGameBank.drawDestinationCardFromDeck();
	}

	public IGameBank getGameBank()
	{
		return mGameBank;
	}
}
