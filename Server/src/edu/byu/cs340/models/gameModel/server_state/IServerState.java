package edu.byu.cs340.models.gameModel.server_state;

/**
 * Created by tjacobhi on 23-Mar-17.
 *
 * This represents the state for the server.
 */
public interface IServerState
{
	Object takeAction(StateActions action, IServerStatable args);
	Object takeAction(StateActions action, IServerStatable args, Object values);
	void verifyState(StateActions action, IServerStatable args);
}
