package edu.byu.cs340.models.gameModel.server_state;

import edu.byu.cs340.models.gameModel.IServerGameModel;

/**
 * Created by tjacobhi on 23-Mar-17.
 */
public interface IServerStatable extends IServerGameModel
{
	IServerState getState();
	void setState(IServerState state);
}
