package edu.byu.cs340.models.gameModel.server_state;

import edu.byu.cs340.command.game.ServerGameOverCommand;
import edu.byu.cs340.models.gameModel.ServerGameModel;

/**
 * Created by tjacobhi on 24-Mar-17.
 */
public class GameOverState extends ServerState
{
	public GameOverState(IServerStatable args)
	{
		((ServerGameModel) args).updateScore();
		args.addCommand(new ServerGameOverCommand(args.getCurrentPlayerName(), -1, args.getRoomID(), args
				.getGameOverData()));

	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args)
	{
		return null;
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args, Object values)
	{
		return null;
	}

	@Override
	public void verifyState(StateActions action, IServerStatable args)
	{

	}
}
