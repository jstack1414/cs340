package edu.byu.cs340.models.gameModel.server_state;

import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.players.GamePlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tjacobhi on 24-Mar-17.
 *
 */
public class DrawDestinationCardsState extends ServerState
{

	public DrawDestinationCardsState(IServerStatable args)
	{
		takeAction(StateActions.DrawDestinationCards, args);
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args)
	{
		if(action.equals(StateActions.DrawDestinationCards))
		{
			GamePlayer player = ((ServerGameModel) args).getPlayer(args.getCurrentPlayerName());

			List<IDestinationCard> cards = new ArrayList<>();
			if (player.hasTemporaryCards())
			{
				cards.addAll(player.getTemporaryDestinationCards());
			}
			for (int i = cards.size(); i < ServerGameModel.NUM_DESTINATION_CARDS_TO_DRAW; i++)
			{
				IDestinationCard card = ((ServerGameModel) args).drawDestinationCard();
				if (card != null)
				{
					cards.add(card);
				}
			}

			player.addTemporaryDestinationCards(cards);
			return Collections.unmodifiableList(cards);
		}

		else if(action.equals(StateActions.ReturnDestinationCards))
		{

		}

		return null;
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args, Object values)
	{
		if (action.equals(StateActions.ReturnDestinationCards))
		{
			//check to make sure the destination cards are in the players list
			GamePlayer player = ((ServerGameModel)args).getPlayer(args.getCurrentPlayerName());
			if (player == null)
			{
				return false;
			}
			List<IDestinationCard> tempCards = player.getTemporaryDestinationCards();
			List<IDestinationCard> toAdd = new ArrayList<>();

			List<IDestinationCard> destinationCardList = (List<IDestinationCard>)values;
			if (tempCards.containsAll(destinationCardList))
			{
				for (IDestinationCard card : tempCards)
				{
					if (!destinationCardList.contains(card))
					{
						toAdd.add(card);
					}
				}
				player.addDestinationCards(toAdd);
				player.removeTemporaryDestinationCards();
				((ServerGameModel) args).getGameBank().putDestinationCardsOnBottom(destinationCardList);
				args.setState(new PlayerTurnState(args));
				return true;
			} else
			{
				return false;
			}
		}
		return takeAction(action, args);
	}

	@Override
	public void verifyState(StateActions action, IServerStatable args)
	{

	}
}
