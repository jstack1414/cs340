package edu.byu.cs340.models.gameModel.server_state;

import edu.byu.cs340.models.gameModel.ServerGameModel;
import edu.byu.cs340.models.gameModel.bank.IDestinationCard;
import edu.byu.cs340.models.gameModel.bank.ITrainCard;
import edu.byu.cs340.models.gameModel.players.GamePlayer;

import java.util.*;

/**
 * Created by tjacobhi on 25-Mar-17.
 */
public class StartGameState extends ServerState
{
	GamePlayer mPlayer;
	Map<String, Boolean> playerReturnedCards;

	public StartGameState()
	{
		mPlayer = null;
		playerReturnedCards = new HashMap<>();
	}

	public void setPlayer(GamePlayer player)
	{
		mPlayer = player;
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args)
	{
		if(action.equals(StateActions.DrawDestinationCards))
		{
			List<IDestinationCard> cards = new ArrayList<>();
			if (mPlayer.hasTemporaryCards())
			{
				cards.addAll(mPlayer.getTemporaryDestinationCards());
			}
			for (int i = cards.size(); i < ServerGameModel.NUM_DESTINATION_CARDS_TO_DRAW; i++)
			{
				IDestinationCard card = ((ServerGameModel) args).drawDestinationCard();
				if (card != null)
				{
					cards.add(card);
				}
			}
			playerReturnedCards.put(mPlayer.getUsername(), false);

			mPlayer.addTemporaryDestinationCards(cards);
			return Collections.unmodifiableList(cards);
		}

		else if(action.equals(StateActions.DrawTrainCardDeck))
		{
			ITrainCard card = ((ServerGameModel) args).drawTrainCardFromDeck();
			mPlayer.addTrainCard(card);
			return card;
		}
		return null;
	}

	@Override
	public Object takeAction(StateActions action, IServerStatable args, Object values)
	{
		if(action.equals(StateActions.ReturnDestinationCards))
		{
			if (mPlayer == null)
			{
				return false;
			}
			List<IDestinationCard> tempCards = mPlayer.getTemporaryDestinationCards();
			List<IDestinationCard> toAdd = new ArrayList<>();

			List<IDestinationCard> destinationCardList = (List<IDestinationCard>)values;
			if (tempCards.containsAll(destinationCardList))
			{
				for (IDestinationCard card : tempCards)
				{
					if (!destinationCardList.contains(card))
					{
						toAdd.add(card);
					}
				}
				mPlayer.addDestinationCards(toAdd);
				mPlayer.removeTemporaryDestinationCards();
				((ServerGameModel) args).getGameBank().putDestinationCardsOnBottom(destinationCardList);
//				args.setState(new PlayerTurnState());
				playerReturnedCards.replace(mPlayer.getUsername(), true);

				if(hasAllPlayersReturnedCards())
				{
					args.setState(new PlayerTurnState(args));
				}

				return true;
			} else
			{
				return false;
			}
		}

		return takeAction(action, args);
	}

	private boolean hasAllPlayersReturnedCards()
	{
		for (String player : playerReturnedCards.keySet())
		{
			if(playerReturnedCards.get(player).equals(Boolean.FALSE))
				return false;
		}
		return true;
	}

	@Override
	public void verifyState(StateActions action, IServerStatable args)
	{

	}
}
