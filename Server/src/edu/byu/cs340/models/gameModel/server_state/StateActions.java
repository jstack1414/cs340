package edu.byu.cs340.models.gameModel.server_state;

/**
 * Created by tjacobhi on 23-Mar-17.
 */
public enum StateActions
{
	None,
	DrawTrainCardDeck,
	DrawDestinationCards,
	DrawTrainCardRevealed,
	ReturnDestinationCards
}
