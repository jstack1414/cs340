package edu.byu.cs340.models.lobbyModel;

import edu.byu.cs340.command.IBaseCommand;
import edu.byu.cs340.command.ServerCommandUtils;
import edu.byu.cs340.command.lobby.ServerCreateLobbyCommand;
import edu.byu.cs340.database.DatabaseWriter;
import edu.byu.cs340.models.ServerChatMessenger;

/**
 * Created by joews on 2/1/2017.
 *
 * Server implementation of a lobby
 */
public class ServerLobbyModel extends LobbyModel
{
	/**
	 * Class name of the command
	 */
	protected final String mClassName = getClass().getSimpleName().replace("Server", "");
	/**
	 * Package directory
	 */
	protected final String mClassPackage = getClass().getPackage().getName();

	public ServerLobbyModel()
	{
		super();
		mChatMessenger = new ServerChatMessenger(); //override messenger with server version that assigns indices
	}

	public ServerLobbyModel(String roomName, String hostUsername)
	{
		super(roomName, hostUsername);
		mChatMessenger = new ServerChatMessenger(); //override messenger with server version that assigns indices
	}

	@Override
	public boolean addCommand(IBaseCommand command)
	{
		boolean result = super.addCommand(command);
		if (!(command instanceof ServerCreateLobbyCommand))
		{
			DatabaseWriter.getInstance().addCommandToLobby(this.getRoomID(), command);
			ServerCommandUtils.testSaveLobby(this);
		} else
		{
			DatabaseWriter.getInstance().setLobby(this);
		}
		return result;
	}

}
