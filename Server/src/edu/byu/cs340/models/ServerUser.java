package edu.byu.cs340.models;

import java.util.UUID;

/**
 * Created by joews on 2/1/2017.
 */
public class ServerUser extends BaseUser
{
	private String mAuthenticationToken;
	private int mPassHash;


	public ServerUser(String userName, String password)
	{
		super(userName);
		hashPassword(password);
	}

	public ServerUser(String username, int passHash, String token)
	{
		super(username);
		mPassHash = passHash;
		mAuthenticationToken = token;
	}

	private void hashPassword(String password)
	{
		mPassHash = password.hashCode() ^ 31 * mUserName.hashCode();
	}

	public String login(String username, String password)
	{
		if (mUserName.equals(username) && (password.hashCode() ^ 31 * mUserName.hashCode()) == mPassHash)
		{
			generateAuthentication();
			return mAuthenticationToken;
		} else
		{
			return null;
		}
	}

	/**
	 * Get value of AuthenticationToken
	 *
	 * @return Returns the value of AuthenticationToken
	 */
	public String getAuthenticationToken()
	{
		return mAuthenticationToken;
	}

	public boolean validateAuthenticationToken(String authenticationToken)
	{
		return authenticationToken.equals(mAuthenticationToken);
	}

	private void generateAuthentication()
	{
		mAuthenticationToken = UUID.randomUUID().toString();
	}

	public void removeAuthenticationToken() {
		this.mAuthenticationToken = null;
	}

	public int getPassHash()
	{
		return mPassHash;
	}
}
